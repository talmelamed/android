package com.adinlay.adinlay;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adinlay.adinlay.objects.Ad;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class CampaignsRvFrag extends Fragment implements
        SwipeRefreshLayout.OnRefreshListener {

    SelectedAdViewModel viewModel;

    private RecyclerView recyclerView;
    private TextView messageTv;
    private SwipeRefreshLayout refreshLayout;

    private RecyclerView.Adapter adapter;
    private GridLayoutManager layoutManager;
    private GlideRequests gr = null;
    private WeakReference<ItemClick> wr_itemClick = new WeakReference<>(null);
    private boolean onFirstFetch = false;
    private boolean isFirstList = true;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null){
            viewModel = ViewModelProviders.of(getActivity()).get(SelectedAdViewModel.class);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.frag_recyclerview, container, false);

        if (viewModel == null && getActivity() != null){
            viewModel = ViewModelProviders.of(getActivity()).get(SelectedAdViewModel.class);
        }

        messageTv = root.findViewById(R.id.recycler_frag_message);
        refreshLayout = root.findViewById(R.id.refresh_layout);

        recyclerView = root.findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        int topPad = getResources().getDimensionPixelSize(R.dimen.toolbar_h) + getResources().getDimensionPixelSize(R.dimen.upper_bar_h);
        recyclerView.setPadding(0, topPad, 0, 0);

        layoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(layoutManager);

        refreshLayout = root.findViewById(R.id.refresh_layout);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeResources(android.R.color.black, R.color.colorAdinlayYellow);
        refreshLayout.setProgressViewOffset(false, 0, 2 * getResources().getDimensionPixelSize(R.dimen.upper_bar_h));

        if (getActivity() != null){
            gr = GlideApp.with(getActivity());
        } else {
            gr = GlideApp.with(this);
        }

        viewModel.getPingAds().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                refreshLayout.setRefreshing(false);
                if (SelectedAdViewModel.FETCH_OBJECTS_OK.equals(s)){
                    ArrayList<Ad> ads = viewModel.getAdsArrayList();
                    if (ads == null){ // shouldn't happen!
                        ads = new ArrayList<>();
                    }
                    adapter = new AdsRecyclerViewAdapter(ads, gr, wr_itemClick.get());
                    if (isFirstList){
                        isFirstList = false;
                        ((AdsRecyclerViewAdapter) adapter).setGetFullSizeThumbnail(false);
                    }
                    // remember position:
                    int pos = layoutManager.findFirstVisibleItemPosition();
                    View firstItem = layoutManager.findViewByPosition(pos);
                    int offset = firstItem == null ? 0 : firstItem.getTop() - recyclerView.getPaddingTop();
                    recyclerView.setAdapter(adapter);
                    // re-set position:
                    if (pos > 0 || (pos == 0 && offset != 0)){
                        layoutManager.scrollToPositionWithOffset(pos, offset);
                    }
                } else {
                    String message = s != null ? s : getResources().getString(R.string.something_wrong_get_ads);
                    //todo: message dialog, perhaps change message from "fetching" to "no items to display"
                }
                if (viewModel.getAdsArrayList() == null || viewModel.getAdsArrayList().size() == 0){
                    messageTv.setText(R.string.no_items_yet);
                    messageTv.setVisibility(View.VISIBLE);
                } else {
                    messageTv.setVisibility(View.GONE);
                }
            }
        });

        ArrayList<Ad> ads = viewModel.getAdsArrayList();
        if (ads == null){
            ads = new ArrayList<>();
            refreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    if (viewModel.getAdsArrayList() == null){
                        refreshLayout.setRefreshing(true);
                        messageTv.setVisibility(View.VISIBLE);
                        messageTv.setText(R.string.fetching_items);
                        onRefresh();
                    }
                }
            });
            onFirstFetch = true;
        }
        adapter = new AdsRecyclerViewAdapter(ads, gr, wr_itemClick.get());
        recyclerView.setAdapter(adapter);

        return root;
    }


    @Override
    public void onRefresh() {
        viewModel.getAvailableAds();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!refreshLayout.isRefreshing() && !onFirstFetch){
            onRefresh();
        }
        onFirstFetch = false;
    }

    public void setItemClik(ItemClick itemClick){
        wr_itemClick = new WeakReference<>(itemClick);
    }
}
