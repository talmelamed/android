package com.adinlay.adinlay;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adinlay.adinlay.objects.SettingsRvItem;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class SettingsRvAdapter extends RecyclerView.Adapter<TopicsRvAdapter.TopicListItemViewHolder> {

    private ArrayList<SettingsRvItem> items;
    private WeakReference<ItemClick> wr_itemClickCallback;

    public SettingsRvAdapter(@NonNull ArrayList<SettingsRvItem> items, ItemClick itemClick) {
        this.items = items;
        wr_itemClickCallback = new WeakReference<>(itemClick);
    }

    @NonNull
    @Override
    public TopicsRvAdapter.TopicListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_topic_entry, parent, false);
        return new TopicsRvAdapter.TopicListItemViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull TopicsRvAdapter.TopicListItemViewHolder holder, int position) {
        SettingsRvItem item = items.get(position);

        holder.tv.setText(item.getNameResId());
        holder.button.setImageResource(item.getButtonResId());
        holder.button.setTag(R.id.object_tag, item.getNameResId());
        holder.button.setOnClickListener(itemClickListener);


        AyUtils.setOddEvenBackgroundColor(position, holder.itemView);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag(R.id.object_tag) instanceof Integer && v.getTag(R.id.object_tag) != null){
                if (wr_itemClickCallback.get() != null){
                    wr_itemClickCallback.get().onItemClicked(v.getTag(R.id.object_tag));
                }
            }
        }
    };
}
