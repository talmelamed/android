package com.adinlay.adinlay;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.objects.ProfileTopic;
import com.adinlay.adinlay.objects.ProfileUserRvItem;

import org.json.JSONArray;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class ProfileUserRvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private ArrayList<ProfileUserRvItem> items;
    private WeakReference<ItemClick> wr_itemClickCallback;


    public ProfileUserRvAdapter(@NonNull ArrayList<ProfileUserRvItem> items, ItemClick itemClick) {
        this.items = items;
        this.wr_itemClickCallback = new WeakReference<>(itemClick);
    }

    @Override
    public int getItemViewType(int position) {
        ProfileUserRvItem item = items.get(position);
        return item.isTopicItem() ? 0 :1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0){
            View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_topic_entry, parent, false);
            return new TopicsRvAdapter.TopicListItemViewHolder(root);
        }
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mock_topic_entry, parent, false);
        return new MockTopicItemViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ProfileUserRvItem rvItem = items.get(position);
        View button = null;
        if (holder instanceof TopicsRvAdapter.TopicListItemViewHolder) {
            TopicsRvAdapter.TopicListItemViewHolder topicHolder = (TopicsRvAdapter.TopicListItemViewHolder) holder;
            String title = "";
            if (rvItem.getTopic() != null) {
                ProfileTopic topic = rvItem.getTopic();
                title = topic.getTitle();
                if (topic.isHierarchic()){
                    User user = AdinlayApp.getUser();
                    if (user != null) {
                        JSONArray selectedArray = user.getProfileTopicSelection(topic.getId(), true);
                        String selectionString = selectedArray.length() > 0 ? selectedArray.optString(0) : "";
                        if (!selectionString.isEmpty()){
                            if (ProfileTopic.LOCATION.equals(topic.getId())){
                                String selectionText = AdinlayApp.getLocationName(selectionString);
                                selectionString = selectionText != null ? selectionText.replace("\\", ", ") : selectionString;
                            }
                            title += ": " + selectionString; // this is the ID, but will do for now
                        }
                    }
                }
            }
            topicHolder.tv.setText(title);
            button = topicHolder.button;
        } else if (holder instanceof MockTopicItemViewHolder){
            MockTopicItemViewHolder mtHolder = (MockTopicItemViewHolder) holder;
            if (rvItem.getNameResId() > 0){
                mtHolder.tv.setText(rvItem.getNameResId());
            }

            int buttonRes = rvItem.getButtonResId() > 0 ? rvItem.getButtonResId() : 0;
            mtHolder.button.setImageResource(buttonRes);
            if (rvItem.getImageRes() > 0){
                mtHolder.logoIv.setVisibility(View.VISIBLE);
                mtHolder.logoIv.setImageResource(rvItem.getImageRes());
            } else {
                mtHolder.logoIv.setVisibility(View.GONE);
            }
            button = mtHolder.button;
        }

        if (button != null) {
            button.setTag(R.id.position_tag, position);
            button.setTag(R.id.object_tag, rvItem);
            button.setOnClickListener(itemClickListener);
        }

        AyUtils.setOddEvenBackgroundColor(position, holder.itemView);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (v.getTag(R.id.object_tag) instanceof ProfileUserRvItem){
                ProfileUserRvItem rvItem = (ProfileUserRvItem) v.getTag(R.id.object_tag);

                if (wr_itemClickCallback.get() != null){
                    if (rvItem.isTopicItem()){
                        AdinlayApp.setUserTopicPos(rvItem.getTopicPos());
                        AdinlayApp.setLoginStage(SignInManager.UPDATE_YOU_STAGE);
                    }
                    wr_itemClickCallback.get().onItemClicked(rvItem.getNameResId());
                }

            }

        }
    };

    public static class MockTopicItemViewHolder extends RecyclerView.ViewHolder {

        TextView tv;
        ImageView button;
        ImageView logoIv;

        public MockTopicItemViewHolder(View itemView) {
            super(itemView);
            tv = itemView.findViewById(R.id.item_topic_name_tv);
            button = itemView.findViewById(R.id.item_topic_button);
            logoIv = itemView.findViewById(R.id.item_topic_additional_iv);
        }
    }
}
