package com.adinlay.adinlay;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.adinlay.adinlay.objects.RedeemEntry;

import java.util.ArrayList;

/**
 * Created by Noya on 7/23/2018.
 */

public class RedeemHistoryActivity extends AppCompatActivity implements ItemClick {

    private final static String TAG = RedeemHistoryActivity.class.getSimpleName();

    private RedeemHistoryViewModel model;

    private RecyclerView rv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.redeem_history);

        // light background to status bar:
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= 23){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
        }

        model = ViewModelProviders.of(this).get(RedeemHistoryViewModel.class);
        rv = findViewById(R.id.redeem_history_recycler);

        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setHasFixedSize(true);

        findViewById(R.id.toolbar_right_tv).setVisibility(View.GONE);
        TextView btn1 = findViewById(R.id.toolbar_left_tv);
        btn1.setText(getResources().getString(R.string.arrow_and_back));
        btn1.setTextColor(Color.BLACK);
        findViewById(R.id.toolbar_left_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        model.getPingRE().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (RedeemHistoryViewModel.FETCH_OBJECTS_OK.equals(s)){
                    ArrayList<RedeemEntry> entries = model.getEntriesArrayList();
                    if (entries == null){
                        entries = new ArrayList<>();
                    }
                    RedeemEntryRvAdapter adapter = new RedeemEntryRvAdapter(entries, RedeemHistoryActivity.this);
                    rv.setAdapter(adapter);
                }
            }
        });

        model.getRedeemEntries();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (AdinlayApp.getUser() == null){
            // lost state - let's let previous activity deal with it
            finish();
        }
    }

    @Override
    public void onItemClicked(Object object) {
        if (object instanceof RedeemEntry){
            RedeemEntry entry = (RedeemEntry) object;
            String uriPath = entry.getUri();
            Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uriPath));
            startActivity(webIntent);

            model.resendRedeemRequest(entry.getEntryId());
        }
    }
}
