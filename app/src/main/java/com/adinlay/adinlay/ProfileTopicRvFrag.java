package com.adinlay.adinlay;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adinlay.adinlay.objects.CheckableObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Noya on 10/7/2018.
 */

public class ProfileTopicRvFrag extends ProfileTopicItemsFrag {

    private static final String TAG = ProfileTopicRvFrag.class.getSimpleName();

//    LoginViewModel viewModel;
    ArrayList<CheckableObject> items = null;
    boolean isSingleChoice = false;
    boolean mustMakeChoice = false;
    boolean isHierarchic = false;
    private WeakReference<ItemClick> wr_itemCallback = new WeakReference<ItemClick>(null);
    ChecklistRvAdapter adapter = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_recyclerview, container, false);

//        if (viewModel == null && getActivity() != null){
//            viewModel = ViewModelProviders.of(getActivity()).get(LoginViewModel.class);
//        }

//        viewModel.updateFlowData(R.drawable.group_next_profile, getResources().getString(R.string.next_allcaps), false, true);

        if (items != null){

            RecyclerView recyclerView = root.findViewById(R.id.recycler);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(layoutManager);
            recyclerView.setPadding(0,0,0, 0);
            recyclerView.setHasFixedSize(true);
            adapter = new ChecklistRvAdapter(items, isSingleChoice, mustMakeChoice, wr_itemCallback.get(), isHierarchic);
            recyclerView.setAdapter(adapter);
        }

        root.findViewById(R.id.refresh_layout).setEnabled(false);


        return root;
    }

    public void setItems(@NonNull ArrayList<CheckableObject> items, boolean isSingleChoice,
                         boolean mustMakeChoice, ItemClick itemCallback, boolean isHierarchic) {
        this.items = items;
        this.isSingleChoice = isSingleChoice;
        this.mustMakeChoice = mustMakeChoice;
        wr_itemCallback = new WeakReference<ItemClick>(itemCallback);
        this.isHierarchic = isHierarchic;
        Timber.d("topic_dbg number of items: %s", items.size());
    }

    @NonNull
    public ArrayList<String> getSelection(){
        return adapter != null ? adapter.getChosenItemsIds() : new ArrayList<String>();
    }


//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (getActivity() != null) {
//            viewModel = ViewModelProviders.of(getActivity()).get(LoginViewModel.class);
//        }
//    }


}
