package com.adinlay.adinlay;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;

/**
 * Created by Noya on 4/15/2018.
 */

public class ProfileFrag extends Fragment {

    private final static String TAG = ProfileFrag.class.getSimpleName();

    private MainViewModel model = null;
    private ContentLoadingProgressBar spinner = null;
    private View spinnerFrame = null;
    private TextView name = null;
    private TextView email = null;
    private TextView phoneTv = null;

    private int[] tabIds = new int[]{R.id.profile_tab_social_media_tv,
            R.id.profile_tab_settings_tv};
    private int[] tabButtonIds = new int[]{R.id.profile_tab_social_media,
            R.id.profile_tab_setting};


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_profile, container, false);

        if (model == null && getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        }

        spinnerFrame = root.findViewById(R.id.profile_spinner_frame);
        spinner = root.findViewById(R.id.profile_spinner);

        spinnerFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // prevent clicks
            }
        });

        root.findViewById(R.id.vcf_logout_adinlay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    Bundle args = new Bundle();
                    args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.logout_adinlay_camel);
                    args.putInt(AyDialogFragment.ARG_MESSAGE_RES, R.string.logout_adinlay_ru_sure);
                    args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.cancel);
                    args.putInt(AyDialogFragment.ARG_BUTTON_R_RES, R.string.logout);
                    AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                    dialogFragment.setListener(AyDialogFragment.VALUE_RIGHT, new AyDialogFragment.AyClickListener() {
                        @Override
                        public void onButtonClicked(View v) {
                            model.deleteFcmToken();
                        }
                    });
                    AyUtils.showDialogFragment(dialogFragment, getFragmentManager());
                }
            }
        });

        name = root.findViewById(R.id.vcf_name_tv);
        email = root.findViewById(R.id.vcf_email_tv);
        phoneTv = root.findViewById(R.id.vcf_phone_num_tv);

        View socMedBtn = root.findViewById(R.id.profile_tab_social_media);
        socMedBtn.setOnClickListener(tabClickListener);
        root.findViewById(R.id.profile_tab_setting).setOnClickListener(tabClickListener);

        root.findViewById(R.id.vcf_edit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ProfileEditActivity.class);
                startActivity(intent);
            }
        });

        model.getSpinnerLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean show) {
                if (show == null || !show){
                    spinner.hide();
                    spinnerFrame.setVisibility(View.GONE);
                } else {
                    spinnerFrame.setVisibility(View.VISIBLE);
                    spinner.show();
                }
            }
        });
        socMedBtn.callOnClick();

        model.getPingFcmDelete().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (s == null){
                    return;
                } else {
                    model.getPingFcmDelete().postValue(null);
                }

                if (MainViewModel.DELETE_FCM_OK.equals(s)){
                    // logout user:
                    AyUtils.logoutUser();
                    if (getActivity() != null){
                        Intent intent = new Intent(getActivity(), SplashActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                } else {
                    // oh-uh! a problem deleting the token. cannot fully log out:
                    Activity activity = getActivity();
                    if (activity == null){
                        return;
                    }
                    Bundle args = new Bundle();
                    args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.error);
                    args.putInt(AyDialogFragment.ARG_MESSAGE_RES, R.string.something_wrong_logout_adinlay);
                    args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
                    AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                    if (!activity.isFinishing() && isAdded()) {
                        AyUtils.showDialogFragment(dialogFragment, getFragmentManager());
                    }
                }

            }
        });

        root.findViewById(R.id.mock_vid_1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent videoIntent = new Intent(getActivity(), VideoActivity.class);
                videoIntent.putExtra(VideoActivity.ORIGIN, "ProfileFrag");
                startActivity(videoIntent);
            }
        });

        ImageView avatarIv = root.findViewById(R.id.vcf_profile_photo);
        if (AdinlayApp.getUser() != null && !AdinlayApp.getUser().getAvatarUrl().isEmpty() && avatarIv != null){
            RequestOptions options = new RequestOptions().error(R.drawable.avtar_icon)
                    .placeholder(R.drawable.avtar_icon)
                    .transform(new CenterCrop());
            GlideApp.with(ProfileFrag.this).load(AdinlayApp.getUser().getAvatarUrl())
                    .apply(options).into(avatarIv);
        }

        return root;
    }

    View.OnClickListener tabClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (v.getParent() instanceof ViewGroup) {
                ViewGroup vParent = (ViewGroup) v.getParent();
                for (int i = 0; i < tabIds.length; i++) {
                    int buttonId = tabButtonIds.length > i ? tabButtonIds[i] : 0;
                    int tabId = tabIds[i];
                    View tv = vParent.findViewById(tabId);
                    if (buttonId == v.getId() && tv instanceof TextView){
                        ((TextView)tv).setTextColor(Color.BLACK);
                        Typeface typeface = ResourcesCompat.getFont(v.getContext(), R.font.assistant_bold);
                        ((TextView) tv).setTypeface(typeface);
                    } else if (tv instanceof TextView){
                        ((TextView)tv).setTextColor(getResources().getColor(R.color.color_tab_unselected));
                        Typeface typeface = ResourcesCompat.getFont(v.getContext(), R.font.assistant);
                        ((TextView) tv).setTypeface(typeface);
                    }
                }
            }

            int anim = 0;
            FragmentManager fm = getChildFragmentManager();
            Fragment currFrag = fm.findFragmentByTag(TAG);
            Fragment newFrag = null;

            switch (v.getId()){
                case R.id.profile_tab_social_media:
                    if (currFrag instanceof SocialMediaFrag){
                        return;
                    }
                    newFrag = new SocialMediaFrag();
                    anim = currFrag == null ? anim : -1;
                    break;
//                case R.id.profile_tab_preferences:
//                    if (currFrag instanceof ProfilePreferencesFrag){
//                        return;
//                    }
//                    newFrag = new ProfilePreferencesFrag();
//                    anim = currFrag == null ? anim : currFrag instanceof SocialMediaFrag ? 1 : -1;
//                    break;
                case R.id.profile_tab_setting:
                    if (currFrag instanceof ProfileSettingsFrag){
                        return;
                    }
                    newFrag = new ProfileSettingsFrag();
                    anim = currFrag == null ? anim : 1;
                    break;
            }
            if (newFrag == null){
                return;
            }
            FragmentTransaction ft = fm.beginTransaction();
            if (anim == -1){
                ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            } else if (anim == 1){
                ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            }
            ft.replace(R.id.profile_inner_frag, newFrag, TAG);
            ft.commit();
        }
    };

    @Override
    public void onResume() {
        super.onResume();

        if (name != null){
            name.setText(AdinlayApp.getUserVcfName());
        }
        if (email != null){
            email.setText(AdinlayApp.getUserEmail());
        }
        if (phoneTv != null){
            phoneTv.setText(AdinlayApp.getUserPhone());
        }
        Activity activity = getActivity();
        if (activity != null){
            int strength = AdinlayApp.getUserStrength();
            AyUtils.setResToStar((ImageView) activity.findViewById(R.id.vcf_1_star), 0, strength);
            AyUtils.setResToStar((ImageView) activity.findViewById(R.id.vcf_2_star), 1, strength);
            AyUtils.setResToStar((ImageView) activity.findViewById(R.id.vcf_3_star), 2, strength);
            AyUtils.setResToStar((ImageView) activity.findViewById(R.id.vcf_4_star), 3, strength);
            AyUtils.setResToStar((ImageView) activity.findViewById(R.id.vcf_5_star), 4, strength);
        }
        // strength api - need text
    }


}
