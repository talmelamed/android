package com.adinlay.adinlay;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.adinlay.adinlay.objects.Ad;
import com.adinlay.adinlay.objects.DraftPost;
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;

import org.threeten.bp.Instant;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.temporal.ChronoUnit;

import java.util.Date;

import timber.log.Timber;

/**
 * Created by Noya on 8/8/2018.
 */

public class ShareSummaryFrag extends Fragment {

    private static final String TAG = ShareSummaryFrag.class.getSimpleName();

    private SwitchCompat immediateSwitch = null;
    private SwitchCompat setWhenSwitch = null;
    private SwitchCompat followBrandSwitch = null;
    private TextView setWhenTv = null;

    private boolean mustPostImmediately = false;

    private  SingleDateAndTimePickerDialog sdtPickerDialog = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_share_summary, container, false);

        immediateSwitch = root.findViewById(R.id.sum_immediate_switch);
        setWhenSwitch = root.findViewById(R.id.sum_set_when_switch);
        setWhenTv = root.findViewById(R.id.sum_set_when_tv);
        followBrandSwitch = root.findViewById(R.id.sum_follow_brand_switch);

        Ad ad = AdinlayApp.getSelectedAd();
        if (ad != null){
            TextView minPayment = root.findViewById(R.id.sum_min_earning);
            String priceMin = AyUtils.getStringAmountWithCurrencySymbol(ad.getAmountBigDec(Ad.PRICE_MIN_KEY),
                    AyUtils.DECIMAL_PLACES_DEFAULT, minPayment.getResources());
            if (ad.isFreeCampaign()) {
                minPayment.setVisibility(View.INVISIBLE);
                root.findViewById(R.id.sum_title_min_earning).setVisibility(View.INVISIBLE);
                TextView noPaymentTitle = root.findViewById(R.id.sum_explanation);
                noPaymentTitle.setText(R.string.no_earnings_fun_campaign);
                noPaymentTitle.setVisibility(View.VISIBLE);
            } else {
                minPayment.setText(priceMin);
            }

            int visibility = ad.isTargetMediaIg() ? View.VISIBLE : View.GONE;
            View followBrandButton = root.findViewById(R.id.sum_click_follow_brand);
            View followBrandTv = root.findViewById(R.id.sum_follow_brand_text);
            followBrandButton.setVisibility(visibility);
            followBrandTv.setVisibility(visibility);
            followBrandSwitch.setVisibility(visibility);
            if (ad.isTargetMediaIg()){
                followBrandButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        boolean wantsToFollow = !followBrandSwitch.isChecked();
                        followBrandSwitch.setChecked(wantsToFollow);
                        DraftPost draftPost = AdinlayApp.getDraftPost();
                        if (draftPost != null){
                            draftPost.setFollowBrand(wantsToFollow);
                        }
                    }
                });
            } else {
                followBrandButton.setOnClickListener(null);
            }

//            TextView perClickPayment = root.findViewById(R.id.sum_earning_per_click);
//            String pricePerClick = AyUtils.getContext().getResources().getString
//                    (R.string.formatted_price_dollar, ad.getPrice(Ad.PRICE_PER_CLICK_KEY));
//            perClickPayment.setText(pricePerClick);

            TextView maxPayment = root.findViewById(R.id.sum_potential_earning);
            if (ad.campaignHasOffers() && !ad.isFreeCampaign()) {
                String priceMax = AyUtils.getStringAmountWithCurrencySymbol(ad.getAmountBigDec(Ad.PRICE_MAX_KEY),
                        AyUtils.DECIMAL_PLACES_DEFAULT, maxPayment.getResources());
                maxPayment.setText(priceMax);
                maxPayment.setVisibility(View.VISIBLE);
                root.findViewById(R.id.sum_title_potential_earning).setVisibility(View.VISIBLE);
            } else {
                maxPayment.setVisibility(View.GONE);
                root.findViewById(R.id.sum_title_potential_earning).setVisibility(View.GONE);
            }


            if (!ad.needsApproval()){
                root.findViewById(R.id.sum_explain_choose_approval).setVisibility(View.GONE);
                root.findViewById(R.id.sum_immediate_tv).setVisibility(View.GONE);
                root.findViewById(R.id.sum_immediate_switch).setVisibility(View.GONE);
                root.findViewById(R.id.sum_set_when_tv).setVisibility(View.GONE);
                root.findViewById(R.id.sum_set_when_switch).setVisibility(View.GONE);
                root.findViewById(R.id.sum_title_max_approval_time).setVisibility(View.GONE);
                root.findViewById(R.id.sum_max_approval_time).setVisibility(View.GONE);
            } else {
//                root.findViewById(R.id.sum_title_per_click_earning).setVisibility(View.GONE);
//                root.findViewById(R.id.sum_earning_per_click).setVisibility(View.GONE);
//                TextView approvalRateTitle = root.findViewById(R.id.sum_title_disapproval_rate); //.setVisibility(View.GONE);
//                approvalRateTitle.setText(getResources().getString(R.string.average_approval_rate));
//                TextView approvalRate = root.findViewById(R.id.sum_disapproval_rate);
//                approvalRate.setText("4h");
                TextView approvalTimeTv = root.findViewById(R.id.sum_max_approval_time);
                int approvalTimeInt = ad.getApprovalTime();
                String time = approvalTimeInt > 0 ? approvalTimeInt + "h" : "";
                approvalTimeTv.setText(time);
                if(ad.isTestingCampaign() || "facebook".equalsIgnoreCase(ad.getTargetMedia())){
                    root.findViewById(R.id.sum_explain_choose_approval).setVisibility(View.GONE);
                    root.findViewById(R.id.sum_immediate_tv).setVisibility(View.GONE);
                    root.findViewById(R.id.sum_immediate_switch).setVisibility(View.GONE);
                    root.findViewById(R.id.sum_set_when_tv).setVisibility(View.GONE);
                    root.findViewById(R.id.sum_set_when_switch).setVisibility(View.GONE);
                } else {

                    // clickListeners:
                    long campaignEndMilli = ad.getCampaignEndDate();
                    long selectStartTime = System.currentTimeMillis() + approvalTimeInt * 60 * 60 * 1000;
                    if (campaignEndMilli > 0 && selectStartTime >= campaignEndMilli){
                        Timber.d("ZDT_DBG mustPostImmediately because campaignEndMilli: %s selectStartTime: %s", campaignEndMilli, selectStartTime);
                        mustPostImmediately = true;
                    }
                    root.findViewById(R.id.sum_click_immediate).setOnClickListener(switchClickListener);
                    root.findViewById(R.id.sum_click_set_when).setOnClickListener(switchClickListener);
                }

            }


        }

        return root;
    }


    View.OnClickListener switchClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (v.getId() == R.id.sum_click_immediate){
                DraftPost draftPost = AdinlayApp.getDraftPost();
                if (draftPost != null){
                    draftPost.setScheduledPostTimes(null, DraftPost.POST_SCHEDULE_UNDEFINED);
                }
                if (immediateSwitch.isChecked()){
                    return;
                }
                immediateSwitch.setChecked(true);
                setWhenSwitch.setChecked(false);
                setWhenTv.setText(getResources().getString(R.string.when_post_set));
            } else if (v.getId() == R.id.sum_click_set_when){

                if (mustPostImmediately){
                    Toast.makeText(v.getContext().getApplicationContext(),
                            "Sorry, but the campaign ends soon, so you must post immediately",
                            Toast.LENGTH_LONG).show();
                    return;
                }

                if (sdtPickerDialog != null && sdtPickerDialog.isDisplaying()){
                    // probably a double-click, dialog already showing
                    return;
                }

                ZonedDateTime zdt = ZonedDateTime.now();
                if (zdt.getMinute() != 0){
                    zdt = zdt.plusHours(1);
                }
                Ad ad = AdinlayApp.getSelectedAd();
                if (ad != null){
                    zdt = zdt.plusHours(ad.getApprovalTime());
//                    Timber.v("ZDT_DBG approvalTime: %s zdt:" %s", ad.getApprovalTime(), zdt);
                }
                zdt = zdt.truncatedTo(ChronoUnit.HOURS); // bug in sdtPickerDialog.minutesStep(30) -- dialog ignores minute part of defaultDate
                Timber.v("ZDT_DBG zdt after truncated: %s", zdt);
                Date startDate = new Date(zdt.toInstant().toEpochMilli());
                Timber.v("ZDT_DBG date: %s", startDate);
                ZonedDateTime endZdt = zdt.plusDays(2);
                ZonedDateTime defaultZdt = zdt.plusDays(1);
//                Timber.v("ZDT_DBG end zdt: %s", endZdt);
                long campaignEndMilli = ad == null ? -1 : ad.getCampaignEndDate(); //"startDate":1535922000000,"endDate":1538341200000,
                if (campaignEndMilli > 0 && campaignEndMilli < endZdt.toInstant().toEpochMilli()){
                    Instant endInstant = Instant.ofEpochMilli(campaignEndMilli);
                    endZdt = ZonedDateTime.ofInstant(endInstant, ZoneId.systemDefault()).truncatedTo(ChronoUnit.HOURS);
//                    Timber.v("ZDT_DBG after changed endZdt: %s", endZdt);
                    // also check the defaultDzt:
                    if (campaignEndMilli < defaultZdt.toInstant().toEpochMilli()){
                        long halfDif = (campaignEndMilli - zdt.toInstant().toEpochMilli()) / 2;
//                        Timber.i("Z_D_T halfDif: %s", halfDif);
                        if (halfDif >= 0){
                            defaultZdt = ZonedDateTime.ofInstant(Instant.ofEpochMilli(zdt.toInstant().toEpochMilli() + halfDif),
                                    ZoneId.systemDefault()).truncatedTo(ChronoUnit.HOURS);
                        } else {
                            defaultZdt = zdt;
                        }
//                        Timber.i("Z_D_T defZdt: %s, start: %s, end: %s", defaultZdt, zdt, endZdt);
                    }
                }
                Date endDate = new Date(endZdt.toInstant().toEpochMilli());
                Timber.v("ZDT_DBG end date: %s", endDate);

                DraftPost draftPost = AdinlayApp.getDraftPost();
                Date defaultDate = draftPost != null && draftPost.getScheduledPostTimeMilli() > 0 &&
                        draftPost.getScheduledPostTimeMilli() >= startDate.getTime() &&
                        draftPost.getScheduledPostTimeMilli() <= endDate.getTime()?
                        new Date(draftPost.getScheduledPostTimeMilli()) : new Date(defaultZdt.toInstant().toEpochMilli());

                sdtPickerDialog = new SingleDateAndTimePickerDialog.Builder(v.getContext())
                        .curved()
                        .defaultDate(defaultDate)
                        .minDateRange(startDate)
                        .maxDateRange(endDate)
                        .displayMinutes(true)
                        .minutesStep(30) // bug in dialog.minutesStep(30) --> dialog ignores minute part of defaultDate
                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                            @Override
                            public void onDisplayed(SingleDateAndTimePicker picker) {
//                                Timber.v("picker onDisplayed");
                                picker.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                                if (picker.getParent() instanceof View){
                                    ((View) picker.getParent()).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                                }
                            }
                        })
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date) {
//                                Timber.v("picker onDateSelected: %s", date);
                                DraftPost draftPost = AdinlayApp.getDraftPost();
                                if (draftPost != null){
                                    immediateSwitch.setChecked(false);
                                    setWhenSwitch.setChecked(true);
                                    Instant selectedInstant = Instant.ofEpochMilli(date.getTime());
                                    ZonedDateTime zdt =  ZonedDateTime.ofInstant(selectedInstant, ZoneId.systemDefault());
                                    draftPost.setScheduledPostTimes(zdt.toOffsetDateTime().toString(), date.getTime());
                                    String pattern24 = DateFormat.is24HourFormat(getActivity()) ? "d MMM, H:mm" : "d MMM, h:mm a";
                                    String formatted = DateTimeFormatter.ofPattern(pattern24).format(zdt);
                                    setWhenTv.setText(formatted);
                                }
                                sdtPickerDialog = null;
                            }
                        })
                        .title(getResources().getString(R.string.when_post_set))
                        .mainColor(Color.BLACK)
                        .titleTextColor(Color.YELLOW)
                        .build();

                        sdtPickerDialog.display();
            }

        }
    };

    public boolean clearPicker(){
        boolean ret = false;
        if (sdtPickerDialog != null){
            ret = sdtPickerDialog.isDisplaying();
            if (ret){
                sdtPickerDialog.dismiss();
            }
            sdtPickerDialog = null;
        }
        return ret;
    }

    @Override
    public void onDestroyView() {
        clearPicker();
        super.onDestroyView();
    }
}
