package com.adinlay.adinlay;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;

/**
 * Created by Noya on 6/5/2018.
 */

public class Login13LetsStartFrag extends LoginFrag {

    private static final String TAG = Login13LetsStartFrag.class.getSimpleName();
    LoginViewModel viewModel;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_login13_start_app, container, false);

        String name = AdinlayApp.getUserNameUppercase();
        TextView tv = root.findViewById(R.id.login13_name_tv);
        tv.setText(name);
        TextView vcfName = root.findViewById(R.id.vcf_name_tv);
        vcfName.setTextSize(22);
        vcfName.setText(AdinlayApp.getUserVcfName());
        TextView email = root.findViewById(R.id.vcf_email_tv);
        email.setTextSize(15);
        email.setText(AdinlayApp.getUserEmail());
        TextView phoneTv = root.findViewById(R.id.vcf_phone_num_tv);
        phoneTv.setText(AdinlayApp.getUserPhone());
        phoneTv.setTextSize(15);
        TextView strengthTv = root.findViewById(R.id.vcf_strength_level_tv);
        strengthTv.setTextSize(15);


        root.findViewById(R.id.vcf_logout_adinlay).setVisibility(View.INVISIBLE);
        root.findViewById(R.id.vcf_edit_button).setVisibility(View.INVISIBLE);
        root.findViewById(R.id.vcf_change_pic_tv).setVisibility(View.GONE);

        // todo: update the strength text

        // light up the stars
        int strength = AdinlayApp.getUserStrength();
        int resize = AyUtils.convertDpToPixel(27, inflater.getContext());
        setResToStar((ImageView) root.findViewById(R.id.l13_star1), 0, strength);
        setResToStar((ImageView) root.findViewById(R.id.vcf_1_star), 0, strength, resize);
        setResToStar((ImageView) root.findViewById(R.id.l13_star2), 1, strength);
        setResToStar((ImageView) root.findViewById(R.id.vcf_2_star), 1, strength, resize);
        setResToStar((ImageView) root.findViewById(R.id.l13_star3), 2, strength);
        setResToStar((ImageView) root.findViewById(R.id.vcf_3_star), 2, strength, resize);
        setResToStar((ImageView) root.findViewById(R.id.l13_star4), 3, strength);
        setResToStar((ImageView) root.findViewById(R.id.vcf_4_star), 3, strength, resize);
        setResToStar((ImageView) root.findViewById(R.id.l13_star5), 4, strength);
        setResToStar((ImageView) root.findViewById(R.id.vcf_5_star), 4, strength, resize);

        ImageView avatarIv = root.findViewById(R.id.vcf_profile_photo);
        ViewGroup.LayoutParams lp = avatarIv.getLayoutParams();
        resize = AyUtils.convertDpToPixel(120, inflater.getContext());
        lp.height = resize;
        lp.width = resize;
        if (AdinlayApp.getUser() != null && !AdinlayApp.getUser().getAvatarUrl().isEmpty() && avatarIv != null) {
//            int radius = AyUtils.convertDpToPixel(8, inflater.getContext());
            RequestOptions options = new RequestOptions().error(R.drawable.avtar_icon)
                    .placeholder(R.drawable.avtar_icon)
                    .transform(new CenterCrop());
//                    .transforms(new CenterCrop(), new RoundedCorners(radius));
            GlideApp.with(Login13LetsStartFrag.this).load(AdinlayApp.getUser().getAvatarUrl())
                    .apply(options).into(avatarIv);
        }

        viewModel.updateFlowData(0, getResources().getString(R.string.lets_start_allcaps), false, true);
        return root;
    }

    private void setResToStar(ImageView star, int pos, int strength){
        setResToStar(star, pos, strength, -1);
    }

    private void setResToStar(ImageView star, int pos, int strength, int resize){
        int src = strength > pos ? R.drawable.star : R.drawable.star_copy_4;
        star.setImageResource(src);
        if (resize > 0){
            ViewGroup.LayoutParams lp = star.getLayoutParams();
            lp.height = resize;
            lp.width = resize;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity()).get(LoginViewModel.class);
    }

    @Override
    public boolean onMainClicked() {
        return true;
    }

    @Override
    public boolean onBackClicked() {
        return false;
    }

    @Override
    protected String getFragStageName() {
        return AdinlayApp.getLoginStage();
    }
}
