package com.adinlay.adinlay;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.adinlay.adinlay.objects.Ad;
import com.adinlay.adinlay.objects.ApprovedPost;
import com.bumptech.glide.load.resource.bitmap.CenterInside;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Noya on 7/25/2018.
 */

public class ApprovedPostActivity extends AppCompatActivity implements ItemClick{

//    private static final String TAG = ApprovedPostActivity.class.getSimpleName();

    private ImageView mainIv = null;
    private RecyclerView recyclerView = null;
//    private View topSizer = null;
    private ContentLoadingProgressBar spinner = null;
    private View spinnerFrame = null;
    private ImageView cancelIv = null;
    private FrameLayout infoFrame = null;

    private ApprovedPostViewModel viewModel = null;
    private GridLayoutManager layoutManager = null;
    private ApprovedPostsRvAdapter adapter = null;
    private GlideRequests gr = null;

    private ApprovedPost currPost = null;

    private ShareDialog shareDialog = null;
    private CallbackManager fcbkCallbackManager = null;

//    private int tabletHeight = -1;

    BroadcastReceiver fcmReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent == null ? null : intent.getExtras();
//            Timber.i("NFT_ update? %s", (extras != null && "update".equals(extras.getString("approved"))));
            if (extras != null && AyUtils.UPDATE_VAL.equals(extras.getString(AyUtils.APPROVED_KEY))){
                viewModel.fetchApprovedPosts();
            }
        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.approved_posts_activity);

        setSupportActionBar(null);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= 23){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
        }

        // toolbar:
        findViewById(R.id.toolbar_right_tv).setVisibility(View.GONE);
        TextView btn1 = findViewById(R.id.toolbar_left_tv);
        btn1.setText(getResources().getString(R.string.arrow_and_back));
        btn1.setTextColor(Color.BLACK);
        findViewById(R.id.toolbar_left_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        FrameLayout tabsFrame = findViewById(R.id.ap_top_bar);
        if (tabsFrame.getChildCount() > 0){
            tabsFrame.removeAllViews();
        }
        tabsFrame.addView(getLayoutInflater().inflate(R.layout.bar_campaigns, tabsFrame, false));
        TextView approvedButtonTv = findViewById(R.id.approved_tv);
        TextView campaignsButtonTv = findViewById(R.id.campaigns_btn_tv);
//        TextView approvedBadge = findViewById(R.id.approved_counter_tv); // fixme add counter
        ImageView approvedIcon = findViewById(R.id.approved_icon_iv);

        Typeface typeface = ResourcesCompat.getFont(this, R.font.assistant);
        Typeface typefaceSelected = ResourcesCompat.getFont(this, R.font.assistant_bold);
        approvedButtonTv.setTypeface(typefaceSelected);
        approvedButtonTv.setTextColor(Color.BLACK);
        approvedIcon.setImageResource(R.drawable.ic_a_small);
        campaignsButtonTv.setTextColor(getResources().getColor(R.color.color_tab_unselected));
        campaignsButtonTv.setTypeface(typeface);
        campaignsButtonTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mainIv = findViewById(R.id.ap_main_iv);
        recyclerView = findViewById(R.id.ap_recycler);
        layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(layoutManager);

        spinner = findViewById(R.id.ap_spinner);
        spinnerFrame = findViewById(R.id.ap_spinner_frame);
        cancelIv = findViewById(R.id.ap_delete_post);
        infoFrame = findViewById(R.id.approved_info_frame);

        viewModel = ViewModelProviders.of(this).get(ApprovedPostViewModel.class);

        gr = GlideApp.with(ApprovedPostActivity.this);

        ImageView shareButtonIv = findViewById(R.id.ap_share);
        shareButtonIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currPost == null){
                    return;
                }
                if (currPost.isFcbk()){
                    final String f_adinlay_postId = currPost.getAdinlayPostId();
                    shareDialog.registerCallback(fcbkCallbackManager, new FacebookCallback<Sharer.Result>() {
                        @Override
                        public void onSuccess(Sharer.Result result) {
                            Timber.d("fb_dbg onSuccess: %s for adinlayPostId: %s", result.getPostId(), f_adinlay_postId);
                            viewModel.reportFcbkPostResult(f_adinlay_postId, true);
                        }

                        @Override
                        public void onCancel() {
                            Timber.i("fb_dbg onCancel");
//                            viewModel.reportFcbkPostResult(f_adinlay_postId, false); // do nothing?
                        }

                        @Override
                        public void onError(FacebookException error) {
                            Timber.i(error, "fb_dbg onError"); // testing shows facebook display a toast "Something went wrong. Please try again."
//                            viewModel.reportFcbkPostResult(f_adinlay_postId, false); // do nothing?
                        }
                    });
                    String hashTag = currPost.getAddedTag();
                    ShareLinkContent slContent = new ShareLinkContent.Builder()
                            .setContentUrl(Uri.parse(currPost.getShareUrl()))
                            .setShareHashtag(new ShareHashtag.Builder()
                                    .setHashtag(hashTag)
                                    .build())
                            .build();
                    shareDialog.show(slContent, ShareDialog.Mode.FEED);
                }
            }
        });


//        findViewById(R.id.photo_hub_ads_shadow).setVisibility(View.GONE);
        cancelIv.setVisibility(View.GONE);
        cancelIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle args = new Bundle();
                args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.delete_post);
                args.putInt(AyDialogFragment.ARG_MESSAGE_RES, R.string.delete_post_explain);
                args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.cancel);
                args.putInt(AyDialogFragment.ARG_BUTTON_R_RES, R.string.delete_button);
                AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                dialogFragment.setListener(AyDialogFragment.VALUE_RIGHT, new AyDialogFragment.AyClickListener() {
                    @Override
                    public void onButtonClicked(View v) {
                        if (currPost != null && currPost.isFcbk()){
                            viewModel.getSpinnerLiveData().setValue(true);
                            viewModel.deleteFcbkPost(currPost.getAdinlayPostId());
                        }
                    }
                });

                AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
            }
        });

//        topSizer = findViewById(R.id.photo_hub_top_sizer);
//        ConstraintLayout.LayoutParams recyclerLP = (ConstraintLayout.LayoutParams) recyclerView.getLayoutParams();
//        recyclerLP.topToBottom = R.id.photo_hub_top_sizer;
//        if (AyUtils.getMinRecyclerHeightDp() > 0){
//            recyclerLP.height = AyUtils.convertDpToPixel(AyUtils.getMinRecyclerHeightDp() + 16, ApprovedPostActivity.this);
//            ConstraintLayout.LayoutParams lpIv = (ConstraintLayout.LayoutParams) topSizer.getLayoutParams();
//            lpIv.dimensionRatio = "w,1:1";
//            lpIv.bottomToTop = R.id.photo_hub_recycler;
//            Timber.d("fb_dbg changed sizer lp");
//            recyclerView.requestLayout();
//            topSizer.requestLayout();
//        } else {
//            Timber.d("fb_dbg didn't changed sizer lp");
//            recyclerView.requestLayout();
//        }

        fcbkCallbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);

        viewModel.getPingServerResponse().observe(ApprovedPostActivity.this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (ApprovedPostViewModel.FETCH_OK.equals(s)){
                    setUpRecyclerAndMainPost();
                } else if (ApprovedPostViewModel.SHARE_OK.equals(s) || ApprovedPostViewModel.DELETE_OK.equals(s)){
                    if (recyclerView.getChildCount() <= 1) {
                        AdinlayApp.setIsOneApprovedPostRemoved(true);
                        Intent returnIntent = new Intent();
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    } else {
                        viewModel.getSpinnerLiveData().postValue(true);
                        viewModel.fetchApprovedPosts();
                    }
                } else if (s != null && (s.startsWith(ApprovedPostViewModel.SHARE_FAIL) ||
                        s.startsWith(ApprovedPostViewModel.DELETE_FAIL))){
                    // show error dialog:
                    String message = s.replace(ApprovedPostViewModel.SHARE_FAIL, "");
                    if (message.isEmpty()){
                        message = getString(R.string.something_wrong_share_post);
                    } else {
                        message = message.replace(ApprovedPostViewModel.DELETE_FAIL, "");
                        if (message.isEmpty()){
                            message = getString(R.string.something_wrong_delete_post);
                        }
                    }

                    Bundle args = new Bundle();
                    args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.error);
                    args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, message);
                    args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
                    args.putInt(AyDialogFragment.ARG_RESULT_ICON, AyDialogFragment.VALUE_FALSE);
                    AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                    if (!isFinishing()) {
                        AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
                    }

                    viewModel.fetchApprovedPosts(); // refresh our info
                }
            }
        });

        viewModel.getSpinnerLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean show) {
                if (show == null || !show){
                    spinner.hide();
                    spinnerFrame.setVisibility(View.GONE);
                } else {
                    spinnerFrame.setVisibility(View.VISIBLE);
                    spinner.show();
                }
            }
        });


        setUpRecyclerAndMainPost();

        spinnerFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do nothing - prevent further clicks
            }
        });

//        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
//        if (wm != null){
//            Display display = wm.getDefaultDisplay();
//            DisplayMetrics metrics = new DisplayMetrics();
//            display.getMetrics(metrics);
//            if (metrics.heightPixels < metrics.widthPixels){
//                tabletHeight = metrics.heightPixels;
//                ConstraintLayout.LayoutParams sizerLP = (ConstraintLayout.LayoutParams) topSizer.getLayoutParams();
//                sizerLP.height = tabletHeight /2;
//                topSizer.requestLayout();
//            }
//        }

    }

    private void setUpRecyclerAndMainPost(){
        ArrayList<ApprovedPost> pendingPosts = new ArrayList<>();
        if (AdinlayApp.getApprovedPosts() != null){
            pendingPosts.addAll(AdinlayApp.getApprovedPosts());
        }
        Fragment priorFrag = getSupportFragmentManager().findFragmentByTag("dialog");
        if (pendingPosts.size() == 0 && (priorFrag == null || !priorFrag.isAdded())){
            finish();
        }
        adapter = new ApprovedPostsRvAdapter(pendingPosts, gr, this);
        recyclerView.setAdapter(adapter);
        setCurrPost(pendingPosts.isEmpty() ? null : pendingPosts.get(0));
    }

    private void setCurrPost(ApprovedPost post){
        currPost = post;
        if (currPost == null){
            mainIv.setImageResource(0);
            return;
        }

        mainIv.post(new Runnable() {
            @Override
            public void run() {
                if (currPost == null){
                    mainIv.setImageResource(0);
                    cancelIv.setVisibility(View.GONE);
                    return;
                }
                cancelIv.setVisibility(View.VISIBLE);

//                ConstraintLayout.LayoutParams lpIv = (ConstraintLayout.LayoutParams) mainIv.getLayoutParams();
//                if (currPost.isFcbk()){
//                    if (tabletHeight <= 0) {
//                        lpIv.dimensionRatio = "1200:630";
//                    } else {
//                        lpIv.height = tabletHeight / 4;
//                        lpIv.dimensionRatio = "1200:630";
//                        ConstraintLayout.LayoutParams recyclerLP = (ConstraintLayout.LayoutParams) recyclerView.getLayoutParams();
//                        recyclerLP.topToBottom = R.id.photo_hub_top_sizer;
//                    }
//                    cancelIv.setVisibility(View.VISIBLE);
//                } else if (AyUtils.getMinRecyclerHeightDp() > 0){
//                    cancelIv.setVisibility(View.GONE);
//                    lpIv.dimensionRatio = "w,1:1";
//                    lpIv.bottomToTop = R.id.photo_hub_recycler;
//                } else {
//                    cancelIv.setVisibility(View.GONE);
//                    lpIv.dimensionRatio = "1:1";
//                    lpIv.bottomToTop = ConstraintLayout.LayoutParams.UNSET;
//                }
//                mainIv.requestLayout();

                if (!currPost.getImageUrl().isEmpty()){
                    GlideRequest<Drawable> requestBuilder = gr.asDrawable()
                            .transforms(new CenterInside())
                            .load(currPost.getImageUrl())
                            .thumbnail(0.2f);
                    int w = mainIv.getWidth();
//                    Timber.i("fb_dbg topSizer w: %s, general w: %s", topSizer.getMeasuredWidth(), AyUtils.getScreenWidthPixels());
                    if (w > 0){
                        int h = currPost.isFcbk() ? w * 630 / 1200 : w;
//                        if (tabletHeight > 0){
//                            h = tabletHeight / 4;
//                            w = h * 1200 / 630;
//                        }
                        requestBuilder = requestBuilder.override(w, h);
                    }
                    requestBuilder.into(mainIv);
                } else {
                    mainIv.setImageResource(0);
                }

                if (currPost.isFcbk()){
                    if (infoFrame.getChildCount() == 0 || !(infoFrame.findViewById(R.id.aprvd_item_min_earning) instanceof TextView)){
                        infoFrame.removeAllViews();
                        View v = LayoutInflater.from(ApprovedPostActivity.this).inflate(R.layout.item_approved_summary, infoFrame, false);
                        infoFrame.addView(v,0);
                    }

                    TextView minPayment = infoFrame.findViewById(R.id.aprvd_item_min_earning);
                    String priceMin = AyUtils.getStringAmountWithCurrencySymbol(currPost.getAmountBigDec(Ad.PRICE_MIN_KEY),
                            AyUtils.DECIMAL_PLACES_DEFAULT, minPayment.getResources());
                    minPayment.setText(priceMin);

                    TextView maxPayment = infoFrame.findViewById(R.id.aprvd_item_potential_earning);
                    if (currPost.hasOffers()) {
                        String priceMax = AyUtils.getStringAmountWithCurrencySymbol(currPost.getAmountBigDec(Ad.PRICE_MAX_KEY),
                                AyUtils.DECIMAL_PLACES_DEFAULT, maxPayment.getResources());
                        maxPayment.setText(priceMax);
                        maxPayment.setVisibility(View.VISIBLE);
                        infoFrame.findViewById(R.id.aprvd_item_title_potential_earning).setVisibility(View.VISIBLE);
                    } else {
                        maxPayment.setVisibility(View.GONE);
                        infoFrame.findViewById(R.id.aprvd_item_title_potential_earning).setVisibility(View.GONE);
                    }

                    View topExplanation = infoFrame.findViewById(R.id.approved_item_explain_share);
                    ConstraintLayout.LayoutParams teLp = (ConstraintLayout.LayoutParams) topExplanation.getLayoutParams();
                    teLp.topMargin = getResources().getDimensionPixelSize(R.dimen.approved_margin_side);

                } else {
                    infoFrame.removeAllViews();
                }
            }
        });


    }

    @Override
    public void onItemClicked(Object object) {
        if (object instanceof ApprovedPost){
            setCurrPost((ApprovedPost) object);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(fcmReceiver,
                new IntentFilter(getString(R.string.filter_approvals_action)));
        long now = System.currentTimeMillis();
        boolean testPending = AdinlayApp.getUser() != null && AdinlayApp.getUser().socialMediaTestIsPending();
        if (testPending || now - AyUtils.getLongPref(AyUtils.PREF_LAST_UPDATE_APPROVED_long, 0) >
                AyUtils.WAIT_INTERVAL_UPDATE_APPROVED){
            viewModel.fetchApprovedPosts();
        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(fcmReceiver);
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (AdinlayApp.getUser() == null){
            // lost state - let's let MainActivity deal with it
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fcbkCallbackManager.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case R.id.intent_share_new_post & 0x0000FFFF:
                if (resultCode == RESULT_OK){
//                    Timber.i("share_dbg");
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
                break;
        }
    }


}
