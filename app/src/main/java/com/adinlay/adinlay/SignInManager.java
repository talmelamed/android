package com.adinlay.adinlay;

import android.support.annotation.NonNull;

import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.SignInMethodQueryResult;

import timber.log.Timber;

/**
 * Created by Noya on 5/27/2018.
 */

public class SignInManager {

    // todo: make this singleton?

    private static final String TAG = SignInManager.class.getSimpleName();

    // stages:
//    public static final String SIGN_IN_STAGE = "sign_in_stage";
    public static final String SIGN_UP_STAGE = "sign_up_stage";
    public static final String EXPLAIN_INCOMPLETE_STAGE = "explain_incomplete_stage";
    public static final String CONFIRM_STAGE = "confirm_stage";
    public static final String EXPLAIN_YOU_STAGE = "explain_you_stage";
    public static final String YOU_STAGE = "you_stage";
    public static final String FOLLOWERS_STAGE = "followers_stage";
    public static final String EXPLAIN_FOLLOWERS_STAGE = "explain_followers_stage";
    public static final String SOCIAL_MEDIA_STAGE = "social_media_stage";
    public static final String LETS_START_STAGE = "lets_start_stage";
    public static final String COMPLETE_STAGE = "complete_stage";
    public static final String UPDATE_CONFIRM_STAGE = "update_confirm_stage";
    public static final String UPDATE_YOU_STAGE = "update_you_stage";
    public static final String UPDATE_FOLLOWERS_STAGE = "update_followers_stage";


    private static FirebaseAuth mAuth;

    public static FirebaseAuth getmAuth(){
        if (mAuth == null){
            mAuth = FirebaseAuth.getInstance();
        }
        return mAuth;
    }


    public static void createNewUser(String email, String password, final ServerManager.Callback callback){
        Timber.i("createUserWithEmail for %s", email);

        getmAuth().createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    Timber.i("createUserWithEmail:success");
                    getTokensForFirebaseUser(callback, null);
                } else {
                    logSignInException(task.getException(), "createUserWithEmail: failure");
                    sendCallbackError(callback, task.getException(), AyUtils.getStringResource(R.string.something_wrong_adinlay_register));
                }
            }
        });

    }

    public static void signInUser(final String email, final String password, final ServerManager.Callback callback){
        Timber.i("signInUser for %s", email);
        getmAuth().fetchSignInMethodsForEmail(email).addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
            @Override
            public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
                String block = "";
                if (task.isSuccessful() && task.getResult() != null &&
                        task.getResult().getSignInMethods() != null && task.getResult().getSignInMethods().size() > 0){
                    String method = task.getResult().getSignInMethods().get(0);
                    if ("facebook.com".equalsIgnoreCase(method)){
                        block = "Facebook";
                    } else if ("google.com".equalsIgnoreCase(method)){
                        block = "Google";
                    }
                }
                if (block.isEmpty()){
                    // go ahead with the signing in the user:
                    getmAuth().signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()){
                                Timber.i("signInWithEmail:success");
                                getTokensForFirebaseUser(callback, null);
                            } else {
                                // task failed
                                logSignInException(task.getException(), "signInWithEmail:failure ");
                                sendCallbackError(callback, task.getException(),
                                        AyUtils.getStringResource(R.string.something_wrong_adinlay_login));
                            }
                        }
                    });
                } else {
                    sendCallbackError(callback, null,
                            AyUtils.getStringResource(R.string.email_already_used_for_another_login_method, block));
                }
            }
        });
    }

    public static void handleFacebookSignIn(AccessToken fcbkAccessToken, final ServerManager.Callback callback){
        final String fcbkToken = fcbkAccessToken.getToken();
        AuthCredential credential = FacebookAuthProvider.getCredential(fcbkAccessToken.getToken());
        getmAuth().signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    Timber.i("handleFacebookSignIn:success");
                    getTokensForFirebaseUser(callback, fcbkToken);
                } else {
                    // task failed
                    sendCallbackError(callback, task.getException(),
                            AyUtils.getStringResource(R.string.something_wrong_adinlay_login));
                    logSignInException(task.getException(), "handleFacebookSignIn:failure ");
                    // using a different facebook-app-id than the one in firebase, causes this: handleFacebookSignIn:failure!!! com.google.firebase.FirebaseException: An internal error has occurred. [ Unsuccessful debug_token response from Facebook:{"error":{"message":"(#100) The App_id in the input_token did not match the Viewing App","type":"OAuthException","code":100,"fbtrace_id":"DjbbU7tHLKu"}} ]
                }
            }
        });
    }

    public static void  handleGoogleSignIn(GoogleSignInAccount acct, final ServerManager.Callback callback){
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        getmAuth().signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    Timber.i("handleGoogleSignIn:success");
                    getTokensForFirebaseUser(callback, null);
                } else {
                    // task failed
                    logSignInException(task.getException(), "handleGoogleSignIn:failure ");
                    sendCallbackError(callback, task.getException(),
                            AyUtils.getStringResource(R.string.something_wrong_adinlay_login));
                }
            }
        });

    }



    private static void getTokensForFirebaseUser(final ServerManager.Callback callback, final String fcbkToken){
        FirebaseUser user = getmAuth().getCurrentUser(); // fixme: when to clear the currentUser
        String name = user == null ? "user is null!?!" : user.getEmail() + " id: " + user.getUid();
        Timber.d("getTokensForFirebaseUser %s", name);


        if (user != null) {
            user.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                @Override
                public void onComplete(@NonNull Task<GetTokenResult> task) {
                    if (task.isSuccessful()) {
                        final String idToken = task.getResult().getToken();
                        Timber.i( "getToken:success, will send to server: %s", idToken);
                        if (idToken != null) {
                            ServerManager.getUserServerToken(idToken, callback, fcbkToken);

                        } else {
                            // idToken is null!
                            Timber.w("getToken: task successful but idToken is null!!! ");
                            sendCallbackError(callback, task.getException(),
                                    AyUtils.getStringResource(R.string.something_wrong_adinlay_login));
                            clearFrbsUser();
                        }
                    } else {
                        // getToken task failed
                        logSignInException(task.getException(), "getToken:failure ");
                        sendCallbackError(callback, task.getException(),
                                AyUtils.getStringResource(R.string.something_wrong_adinlay_login));
                    }
                }
            });
        } else {
            // user is null!!!!
            Timber.w("getTokensForFirebaseUser: user is null!!!");
            sendCallbackError(callback, null,
                    AyUtils.getStringResource(R.string.something_wrong_adinlay_login));
        }
    }

    public static void sendCallbackError(ServerManager.Callback callback, Exception exception, String defaultMessage){
        if (callback == null){
            return;
        }
        String message = exception != null && exception.getMessage() != null
                && !exception.getMessage().isEmpty() ? exception.getMessage() : defaultMessage;
        callback.onError(message);
    }

    public static void clearFrbsUser(){
        getmAuth().signOut();
    }

    public static void setNextStage(){
        String currStage = AdinlayApp.getLoginStage();
        switch (currStage){
            case SIGN_UP_STAGE:
            case EXPLAIN_INCOMPLETE_STAGE:
                AdinlayApp.setLoginStage(CONFIRM_STAGE);
                return;
            case CONFIRM_STAGE:
                AdinlayApp.setLoginStage(EXPLAIN_YOU_STAGE);
                return;
            case EXPLAIN_YOU_STAGE:
                if (AdinlayApp.getUserProfileTopics() != null &&
                        AdinlayApp.getUserProfileTopics().size() > 0){
                    AdinlayApp.setUserTopicPos(0); // initialize iteration
                    AdinlayApp.setLoginStage(YOU_STAGE); // move to You stage
                } else if (hasFollowersStage()){
                    AdinlayApp.setLoginStage(EXPLAIN_FOLLOWERS_STAGE);
                } else {
                    AdinlayApp.setLoginStage(LETS_START_STAGE);
                }
                return;
            case YOU_STAGE:
                if (AdinlayApp.getUserProfileTopics() != null){
                    int pos = AdinlayApp.getUserTopicPos() + 1;
                    AdinlayApp.setUserTopicPos(pos);
                    Timber.i("topic_dbg changed pos to: %s", AdinlayApp.getUserTopicPos());
                    if (pos < AdinlayApp.getUserProfileTopics().size()){
                        // next stage is from the UserTopics list
                        return;
                    }
                }
                if (hasFollowersStage()){
                    AdinlayApp.setLoginStage(EXPLAIN_FOLLOWERS_STAGE);
                }
                else {
                    AdinlayApp.setLoginStage(LETS_START_STAGE);
                }
                return;
            case EXPLAIN_FOLLOWERS_STAGE:
                if (hasFollowersStage()){ // should be true - we check before getting here
                    AdinlayApp.setLoginStage(FOLLOWERS_STAGE);
                    AdinlayApp.setFollowersTopicPos(0);  // initialize iteration
                    Timber.i("topic_dbg changed followers pos to: %s", AdinlayApp.getFollowersTopicPos());
                }
                else {
                    AdinlayApp.setLoginStage(LETS_START_STAGE);
                }
                return;
            case FOLLOWERS_STAGE:
            if (AdinlayApp.getFollowersProfileTopics() != null){
                    int pos = AdinlayApp.getFollowersTopicPos() + 1;
                    AdinlayApp.setFollowersTopicPos(pos);
                    if (pos < AdinlayApp.getFollowersProfileTopics().size()){
                        // next stage is from the followersTopics list
                        return;
                    }
                }
            AdinlayApp.setLoginStage(LETS_START_STAGE);
            return;
            case SOCIAL_MEDIA_STAGE:
                AdinlayApp.setLoginStage(LETS_START_STAGE);
                return;
            case LETS_START_STAGE:
                AdinlayApp.setLoginStage(COMPLETE_STAGE);
                return;
        }
    }

    public static boolean isLastTopic(){
        String currStage = AdinlayApp.getLoginStage();
        if ((YOU_STAGE.equals(currStage) && !hasFollowersStage()) || FOLLOWERS_STAGE.equals(currStage)){
            int pos =  YOU_STAGE.equals(currStage) ? AdinlayApp.getUserTopicPos() :
                    AdinlayApp.getFollowersTopicPos();
            int size = YOU_STAGE.equals(currStage) && AdinlayApp.getUserProfileTopics() != null ?
                    AdinlayApp.getUserProfileTopics().size() :
                    FOLLOWERS_STAGE.equals(currStage) && AdinlayApp.getFollowersProfileTopics() != null ?
                    AdinlayApp.getFollowersProfileTopics().size() : 0;
            return pos + 1 >= size;
        }

        return false;
    }

    public static int getTotalStages(){
        // you-stages + followersExplain + followers-stages + social-media
        return 2 + AdinlayApp.getAllTopicsLength();
    }

    private static boolean hasFollowersStage(){
        return AdinlayApp.getFollowersProfileTopics() != null &&
                AdinlayApp.getFollowersProfileTopics().size() > 0;
    }

    private static void logSignInException(Exception e, String msg){
        if (e != null) {
            Timber.w(e, msg);
        } else {
            msg = msg + " null exception";
            Timber.w(msg);
        }
    }

    public static FirebaseUser getFirebaseUser(){
        return getmAuth().getCurrentUser();
    }



}
