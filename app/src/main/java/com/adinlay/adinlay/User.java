package com.adinlay.adinlay;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.adinlay.adinlay.objects.ProfileTopic;
import com.adinlay.adinlay.objects.PromoItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;

import timber.log.Timber;


/**
 * Created by Noya on 5/31/2018.
 */

public class User {

    private static final String TAG = User.class.getSimpleName();

    public static final String ID_KEY = "_id";
    public static final String DISPLAY_NAME_KEY = "displayName";
    public static final String FIRST_NAME_KEY = "firstName";
    public static final String LAST_NAME_KEY = "lastName";
    public static final String EMAIL_KEY = "email";
    public static final String PHOTO_URL_KEY = "photoURL";
    public static final String GENDER_KEY = "gender";
    public static final String AGE_RANGE_KEY = "ageRange";
    public static final String AGE_FROM_KEY = "from";
    public static final String AGE_TO_KEY = "to";
    public static final String HOBBIES_KEY = "hobbies";
    public static final String REG_STATUS_KEY = "registrationStatus";
//    public static final String CREATED_DATE_KEY = "createdDate";
    public static final String STRENGTH_KEY = "strength";
    public static final String PHONE_KEY = "phone";
    public static final String PROFILE_KEY = "profile";
    public static final String PROFILE_USER_KEY = "user";
    public static final String PROFILE_FOLLOWERS_KEY = "followers";
    public static final String EXCHANGE_RATE_KEY = "exchangeRate";
    public static final String TEST_STATUS_KEY = "socialMediaTestingStatus";
    public static final String SOCIAL_MEDIA_PROFILE_KEY = "socialMediaProfile";
    public static final String SOCIAL_MEDIA_ID_KEY = "socialMediaId";

    public static final String PROVIDER_FACEBOOK = "facebook.com";
    public static final String PROVIDER_GOOGLE = "google.com";
    public static final String PROVIDER_PASSWORD = "password";

    public static final String ISRAEL_COUNTRY_CODE = "ISR"; // FIXME: affects currency and local requirements, shouldn't be hard-coded!

    private JSONObject userJson;

    private JSONObject pendingJson = new JSONObject();
    private JSONObject socMedPendingJson = new JSONObject();

    private boolean connectedToInstagram = false;
    private int instaFollowers = 0;
    private String igUsername = "";
    private String igUserId = "";
    private boolean connectedToFacebook = false;
    private int facebookFriends = 0;
    private String fbUsername = "";
    private String fbUserId = "";

    // {"id":"5b0a9a8a8d5f28776e9d3343","displayName":"","firstName":"","lastName":"",
    // "email":"tst1@test.com","photoURL":"","gender":"unknown","ageRange":{"from":0,"to":0},"hobbies":[],
    // "registrationStatus":"started","createdDate":"2018-05-29T11:38:42.610Z","strength":4}


    public User(JSONObject object){
        userJson = object;
    }



    public String getStringValFromJson(String key, String defVal, JSONObject jsonObject){
        if (jsonObject == null || !jsonObject.has(key)){
            return defVal;
        }
        return jsonObject.optString(key, defVal);
    }

    private int getIntValFromJson(String key, int defVal, JSONObject jsonObject){
        if (jsonObject == null || !jsonObject.has(key)){
            return defVal;
        }
        return jsonObject.optInt(key, defVal);
    }


    @Override
    public String toString() {
        String bndStr = pendingJson == null && socMedPendingJson == null ? " no updates" :
                socMedPendingJson == null ? " + Updates: " + pendingJson.toString() :
                        pendingJson != null ? " + Updates: " + pendingJson.toString() + " " + socMedPendingJson.toString() :
                                " + Updates: " + socMedPendingJson.toString();
        return (userJson == null ? super.toString() : userJson.toString()) + bndStr;
    }

    @NonNull
    public String getRegistrationStatus() {
        return getStringValFromJson(REG_STATUS_KEY, "started", userJson); // started, completed
    }

    public JSONObject getPendingJson(){
        if (pendingJson == null){
            pendingJson = new JSONObject();
        }
        return pendingJson;
    }

    public JSONObject getSocMedPendingJson(){
        if (socMedPendingJson == null){
            socMedPendingJson = new JSONObject();
        }
        return socMedPendingJson;
    }

    @NonNull
    public String getShortName(){
        return getStringValFromJson(FIRST_NAME_KEY, getFirstName(), pendingJson);
    }

    @NonNull
    public String getVcfName(){
        String ret = getStringValFromJson(DISPLAY_NAME_KEY, "", userJson);
        if (ret.isEmpty()){
            ret = getStringValFromJson(FIRST_NAME_KEY, "", userJson) + " " + getStringValFromJson(LAST_NAME_KEY, "", userJson);
            ret = ret.trim();
        }
        return ret;
    }

    @NonNull
    public String getFirstName() {
        return getStringValFromJson(FIRST_NAME_KEY, "", userJson);
    }

    @NonNull
    public String getLastName() {
        return getStringValFromJson(LAST_NAME_KEY, "", userJson);
    }

    @NonNull
    public String getPhoneNumber() {
        return getStringValFromJson(PHONE_KEY, "", userJson);
    }

    @NonNull
    public String geteMail() {
        return getStringValFromJson(EMAIL_KEY, "", userJson);
    }

    @NonNull
    public String getDisplayName() {
        return getStringValFromJson(DISPLAY_NAME_KEY, "", userJson);
    }

    @Nullable
    public String getId() {
        String id = getStringValFromJson(ID_KEY, null, userJson);
        if (id == null  && BuildConfig.SERVER_HOST.contains("dev-api")){
            id = getStringValFromJson("id", null, userJson); // backward compatibility
        }
        return id;
    }

    @NonNull
    public String getGender() {
        return getStringValFromJson(GENDER_KEY, "", userJson);
    }

    @NonNull
    public String getAvatarUrl(){
        return getStringValFromJson(PHOTO_URL_KEY, "", userJson);
    }

    /**
     * @return if from returns AgeRange.from else returns AgeRange.to
     */
    public int getAgeRangeField(boolean from){
        int ret = 0;
        String which = from? AGE_FROM_KEY : AGE_TO_KEY;
        if (userJson.has(AGE_RANGE_KEY) && userJson.optJSONObject(AGE_RANGE_KEY) != null){
            ret = userJson.optJSONObject(AGE_RANGE_KEY).optInt(which);
        }

        return ret;
    }

    @NonNull
    public ArrayList<String> getHobbiesArray(){
        ArrayList<String> hobbies = new ArrayList<>();
        JSONArray jArr = userJson.optJSONArray(HOBBIES_KEY);
        if (jArr != null){
            for (int i = 0; i < jArr.length(); i++){
                if (jArr.optString(i, null) != null){
                    hobbies.add(jArr.optString(i, null));
                }
            }
        }
        return hobbies;
    }


    public int getStrength(){
        return getIntValFromJson(STRENGTH_KEY, 0, userJson);
    }

    public boolean needsToTestSocialMedia(){
        return "started".equalsIgnoreCase(getStringValFromJson(TEST_STATUS_KEY, "started", userJson));
    }

    public boolean socialMediaTestIsPending(){
        return "pending".equalsIgnoreCase(getStringValFromJson(TEST_STATUS_KEY, "started", userJson));
    }

    public boolean socialMediaTestIncomplete(){
        return !("completed".equalsIgnoreCase(getStringValFromJson(TEST_STATUS_KEY, "started", userJson)));
    }

    public boolean isConnectedToInstagram() {
        return connectedToInstagram;
    }

    public void setConnectedToInstagram(boolean connectedToIstagram) {
        this.connectedToInstagram = connectedToIstagram;
    }

    public int getInstaFollowers() {
        return instaFollowers;
    }

    public void setInstaFollowers(int instaFollowers) {
        this.instaFollowers = instaFollowers;
    }

    @NonNull
    public String getIgUsername() {
        if (igUsername == null){
            igUsername = "";
        }
        return igUsername;
    }

    public void setIgUsername(String igUsername) {
        this.igUsername = igUsername;
    }

    @NonNull
    public String getIgUserId() {
        if (igUserId == null){
            igUserId = "";
        }
        return igUserId;
    }

    public void setIgUserId(String igUserId) {
        this.igUserId = igUserId;
    }

    public boolean isConnectedToFacebook() {
        return connectedToFacebook;
    }

    public void setConnectedToFacebook(boolean connectedToFacebook) {
        this.connectedToFacebook = connectedToFacebook;
    }

    public int getFacebookFriends() {
        return facebookFriends;
    }

    public void setFacebookFriends(int facebookFriends) {
        this.facebookFriends = facebookFriends;
    }

    @NonNull
    public String getFbUsername() {
        if (fbUsername == null){
            fbUsername = "";
        }
        return fbUsername;
    }

    public void setFbUsername(String fbUsername) {
        this.fbUsername = fbUsername;
    }

    @NonNull
    public String getFbUserId() {
        if (fbUserId == null){
            fbUserId = "";
        }
        return fbUserId;
    }

    public void setFbUserId(String fbUserId) {
        this.fbUserId = fbUserId;
    }

    public void updateUserJsonField(@NonNull String key, Object value){
        updateJsonField(key, value, userJson);
    }

    public void changePendingJsonField(@NonNull String key, Object value){
        updateJsonField(key, value, getPendingJson());
    }

    public void changePendingSocMedProfileField(@NonNull String topicId, @NonNull ArrayList<String> values, @NonNull String mediaId){
        // todo: currently only supporting single update. Otherwise, will need to check if we have pending changes for this mediaId...
        JSONObject userSocMedProfile = getSocialMediaProfile(mediaId);

        if (userSocMedProfile == null){
            // if the user never edited the soc-med-profile, we should still have the userSocMedProfile,
            // without the profile pair. Not getting the userSocMedProfile indicates a discrepancy
            // between the userInfo and socialMediaConnections info.
            socMedPendingJson = null;
            return;
        }

        socMedPendingJson = null; // clear any pending changes
        try {
            socMedPendingJson = new JSONObject(userSocMedProfile.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        JSONObject followersInfo = null;
        // make sure we have a "followers" jsonObject
        if (socMedPendingJson.optJSONObject(PROFILE_FOLLOWERS_KEY) == null){ // indicates the user never edited this profile, should use default
            // need to copy the default
            JSONObject defaultProfileJson = userJson.optJSONObject(PROFILE_KEY);
            try {
                followersInfo = defaultProfileJson != null && defaultProfileJson.optJSONObject(PROFILE_FOLLOWERS_KEY) != null ?
                        new JSONObject(defaultProfileJson.optJSONObject(PROFILE_FOLLOWERS_KEY).toString()) : new JSONObject();
                socMedPendingJson.put(PROFILE_FOLLOWERS_KEY, new JSONObject(followersInfo.toString()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Timber.d("changePendingSocMedProfileField had to use default for followers, so pending is: %s", followersInfo);
        }
        followersInfo = socMedPendingJson.optJSONObject(PROFILE_FOLLOWERS_KEY);
        if (followersInfo == null){ // shouldn't happen!
            followersInfo = new JSONObject();
        }

        JSONArray valuesArray = new JSONArray();
        for (String val : values){
            valuesArray.put(val);
        }

        try {
            followersInfo.put(topicId, valuesArray);
            socMedPendingJson.put(PROFILE_FOLLOWERS_KEY, new JSONObject(followersInfo.toString()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Timber.d("changePendingSocMedProfileField eventually got: %s", socMedPendingJson);

    }

    public void changePendingJsonProfileField(@NonNull String topicId, @NonNull ArrayList<String> values, boolean isAboutUser){

        // check if we started an update:
        JSONObject profileJson = pendingJson.optJSONObject(PROFILE_KEY);
        if (profileJson == null){
            // no update - clone the existing info
            JSONObject jsonObject = userJson.optJSONObject(PROFILE_KEY);
            if (jsonObject != null){
                try {
                    profileJson = new JSONObject(jsonObject.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        if (profileJson == null){
            profileJson = new JSONObject();
        }

        // get the proper section (user or followers)
        String arrayKey = isAboutUser ? PROFILE_USER_KEY : PROFILE_FOLLOWERS_KEY;
        JSONObject sectionJson  = profileJson.optJSONObject(arrayKey);
        if (sectionJson == null){
            sectionJson = new JSONObject();
            try {
                profileJson.put(arrayKey, sectionJson);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        JSONArray valuesArray = new JSONArray();
        for (String val : values){
            valuesArray.put(val);
        }

        try {
            sectionJson.put(topicId, valuesArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Timber.i("topic_dbg prepared new profile json: %s", profileJson);
        changePendingJsonField(PROFILE_KEY, profileJson);

    }

    private void updateJsonField(@NonNull String key, Object value, JSONObject jsonObject){
        if (jsonObject != null){
            try {
                jsonObject.put(key, value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void updatePendingChanges(){
        if (userJson == null){
            return;
        }
        JSONObject updates = getPendingJson();
        Iterator<?> keys = updates.keys();
        while (keys.hasNext()){
            String key = (String) keys.next();
            try {
                userJson.put(key, updates.opt(key));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        pendingJson = new JSONObject();
        Timber.i("updatePendingChanges done: %s", this.toString());
    }

    public void setNewUserJson(JSONObject newObject){
        if (newObject == null){
            return;
        }
        userJson = newObject;
        pendingJson = new JSONObject();
        socMedPendingJson = new JSONObject();
    }

    public void updateSocMedStatus(JSONObject socMedJson){
        if (socMedJson != null && socMedJson.has(TEST_STATUS_KEY)){
            String currStatus = getStringValFromJson(TEST_STATUS_KEY, "", userJson);
            String newVal = socMedJson.optString(TEST_STATUS_KEY, currStatus);
//            Timber.i("SOC_MED had %s, got %s", currStatus, newVal);
            if (!newVal.isEmpty() && !newVal.equals(currStatus)) {
                updateJsonField(TEST_STATUS_KEY, newVal, userJson);
            }
        }
    }

    public void resetPendingJson(){
        pendingJson = new JSONObject();
        socMedPendingJson = new JSONObject();
    }

    @NonNull
    public JSONArray getProfileTopicSelection(String topicId, String mediaId, boolean isAboutUser){
        if (mediaId != null && !mediaId.isEmpty() && !isAboutUser){
            JSONObject smProfile = getSocialMediaProfile(mediaId);
            Timber.d("getProfileTopicSelection smProfile: %s", smProfile);
            if (smProfile != null){
                JSONObject selectionJson = smProfile.optJSONObject("followers");
                Timber.d("getProfileTopicSelection selectionJson: %s", selectionJson);
                if (selectionJson != null && selectionJson.length() > 0){ // if no "followers" section or isEmpty - return the default
                    JSONArray topicSelection = selectionJson.optJSONArray(topicId);
                    return topicSelection == null ? new JSONArray() : topicSelection;
                }
            }
        }
        return getProfileTopicSelection(topicId, isAboutUser, false);
    }

    private JSONObject getSocialMediaProfile(String mediaId){
        if (mediaId == null || mediaId.isEmpty()){
            return null;
        }
        JSONArray socMedProfiles = userJson.optJSONArray(SOCIAL_MEDIA_PROFILE_KEY);
        if (socMedProfiles != null){
            for (int i = 0; i < socMedProfiles.length(); i++){
                JSONObject smProfile = socMedProfiles.optJSONObject(i);
                if (smProfile != null && mediaId.equals(smProfile.optString(SOCIAL_MEDIA_ID_KEY))){
                    return smProfile;
                }
            }
        }

        return null;
    }

    /**
     * When we get userInfo, we get a soc-med-profile object for each account the user is logged into.
     * If the user never edited the soc-med-profile, the "profile" key should be missing,
     * but we still get the object. If the object is missing -
     * it means we need to refresh the userInfo and/or the socialMediaConnections info!
     */
    public boolean hasSocialMediaProfile(String mediaId){
        return getSocialMediaProfile(mediaId) != null;
    }

    @NonNull
    public JSONArray getProfileTopicSelection(String topicId, boolean isAboutUser){
        return getProfileTopicSelection(topicId, isAboutUser, false);
    }

    @NonNull
    public JSONArray getProfileTopicSelection(String topicId, boolean isAboutUser, boolean checkPending){
        JSONArray topicSelection = null;
        JSONObject profileJson = userJson.optJSONObject(PROFILE_KEY);
        String arrayKey = isAboutUser ? PROFILE_USER_KEY : PROFILE_FOLLOWERS_KEY;
        JSONObject sectionJson  = profileJson != null ? profileJson.optJSONObject(arrayKey) : null;
        if (sectionJson != null){
            topicSelection = sectionJson.optJSONArray(topicId);
        }
        if (topicSelection == null && checkPending){
            profileJson = pendingJson.optJSONObject(PROFILE_KEY);
            sectionJson = profileJson != null ? profileJson.optJSONObject(arrayKey) : null;
            if (sectionJson != null){
                topicSelection = sectionJson.optJSONArray(topicId);
            }
        }
        return topicSelection == null ? new JSONArray() : topicSelection;
    }

    public boolean isOver21(){
        JSONArray ageArr = getProfileTopicSelection("birthdate", true, true);
        if (ageArr.length() == 0){
            return false;
        }
        String ageString = ageArr.optString(0, null);
        if (ageString == null || ageString.isEmpty()){
            return false;
        }
        try {
            BigDecimal ageBigDecimal = new BigDecimal(ageString);
            ZonedDateTime zdt21 = ZonedDateTime.now().minusYears(21).toLocalDate().atStartOfDay(ZoneId.systemDefault());
//            Timber.d("isOver21 bd: %s, zdt21: %s", ageBigDecimal.longValue(), zdt21.toEpochSecond()*1000);
            if (ageBigDecimal.longValue() < zdt21.toEpochSecond()*1000){
                return true;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return false;


    }

    public boolean isCurrencyIls(){
        JSONArray locations = getProfileTopicSelection(ProfileTopic.LOCATION, true);
        if (locations.length() == 0){
            return true;
        }
        String userLocation = locations.optString(0);
        int breakPoint = userLocation.indexOf(".");
        if (breakPoint > 0){
            userLocation = userLocation.substring(0, breakPoint);
        }
        if (ISRAEL_COUNTRY_CODE.equals(userLocation)){
            return true;
        }
        return false;
    }

    public double getIlsExchangeRate(){
        return userJson.optDouble(EXCHANGE_RATE_KEY, 1);
    }

    public BigDecimal getUserExchangeRate(){
        double rate = 1;
        if (isCurrencyIls()){
            rate = getIlsExchangeRate();
        }
        return BigDecimal.valueOf(rate);
    }

    @NonNull
    public String getLoginProvider(){
        return getStringValFromJson("loginProvider", "", userJson);
    }

    public boolean isEmailVerified(){
        return userJson != null && userJson.optBoolean("emailVerified", false);
    }

    public boolean canEditEmail(){
        return !getLoginProvider().isEmpty() && !PROVIDER_GOOGLE.equals(getLoginProvider());
    }

    public ArrayList<PromoItem> getPromotionCodes(){
        ArrayList<PromoItem> codes = new ArrayList<>();

        JSONArray jsonArr = userJson.optJSONArray("promotionCodes");
        if (jsonArr == null || jsonArr.length() == 0){
            return codes;
        }

        for (int i = 0; i < jsonArr.length(); i++){
            JSONObject currPromoJson = jsonArr.optJSONObject(i);
            if (currPromoJson != null){
                PromoItem item = new PromoItem(currPromoJson);
                codes.add(item);
            }
        }

        return codes;
    }

//    public boolean isDeveloper(){
//        return userJson != null && userJson.optBoolean("isDeveloper");
//    }

    public boolean isIgLoginViaClient(){
        return userJson != null && userJson.optBoolean("igLoginViaClient");
    }

}
