package com.adinlay.adinlay;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import java.util.ArrayList;

/**
 * Created by Noya on 11/15/2018.
 */

public abstract class ProfileTopicItemsFrag extends Fragment {



    @NonNull
    public abstract ArrayList<String> getSelection();

    /**
     * @return true if back was consumed
     */
    public boolean consumeBackClick(){
        return false;
    }



}
