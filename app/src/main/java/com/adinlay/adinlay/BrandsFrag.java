package com.adinlay.adinlay;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.adinlay.adinlay.objects.Brand;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Noya on 4/8/2018.
 */

public class BrandsFrag extends android.support.v4.app.Fragment implements
        SwipeRefreshLayout.OnRefreshListener, BrandsRecyclerViewAdapter.ItemClick {

    private static final String TAG = BrandsFrag.class.getSimpleName();

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private GridLayoutManager layoutManager;

    private TextView messageTv = null;
    private SwipeRefreshLayout refreshLayout = null;

    private static final int SPAN_COUNT = 2;

    MainViewModel model = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_recyclerview, container, false);

        if (model == null && getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        }

        recyclerView = root.findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);

        messageTv = root.findViewById(R.id.recycler_frag_message);

        refreshLayout = root.findViewById(R.id.refresh_layout);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeResources(android.R.color.black, R.color.colorAdinlayYellow);

        layoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
        recyclerView.setLayoutManager(layoutManager);

        model.getPingBrands().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                refreshLayout.setRefreshing(false);
                if (MainViewModel.FETCH_POSTS_OK.equals(s)){
                    ArrayList<Brand> brands = model.getBrandsArrayList();
                    if (brands == null){ // shouldn't happen at this point
                        brands = new ArrayList<>();
                    }
                    GlideRequests gr = GlideApp.with(BrandsFrag.this);
                    adapter = new BrandsRecyclerViewAdapter(brands, SPAN_COUNT, gr, BrandsFrag.this);
                    recyclerView.swapAdapter(adapter, true);
                    setMessageVisibility(brands.size());
                } else {
                    if (s != null && !s.isEmpty() && getActivity() != null){
                        Toast.makeText(getActivity().getApplicationContext(), "Error: " + s, Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        ArrayList<Brand> brands = model.getBrandsArrayList();
        if (brands == null){
            // haven't fetched brands
            brands = new ArrayList<>();
            refreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    if (model.getBrandsArrayList() == null){
                        refreshLayout.setRefreshing(true);
                        onRefresh();
                    }
                }
            });
        }
        GlideRequests gr = GlideApp.with(this);
        adapter = new BrandsRecyclerViewAdapter(brands, SPAN_COUNT, gr, this);
        recyclerView.setAdapter(adapter);
        setMessageVisibility(brands.size());

        int position = model.getBrandsPos();
        if (position > 0){
            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    int pos =  model.getBrandsPos();
                    Timber.d(" trying to scroll to pos: %s of: %s",pos, layoutManager.getItemCount());
                    if (pos < layoutManager.getItemCount()){
                        layoutManager.scrollToPosition(pos);
                    }
                }
            });
        }

        return root;
    }

    @Override
    public void onPause() {
        super.onPause();
        int pos = layoutManager.findFirstVisibleItemPosition();
        model.setBrandsPos(pos);
    }

    private void setMessageVisibility(int arrSize){
        if (arrSize <= 0){
            messageTv.setVisibility(View.VISIBLE);
        } else if (messageTv.getVisibility() != View.GONE){
            messageTv.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefresh() {
        model.getBrands();
    }

    @Override
    public void onItemClicked(Object object) {
        if (object instanceof Brand && getActivity() != null){
//            Timber.d("onItemClicked %s", object);
            if (getActivity() != null){
                Intent intent = new Intent(getActivity(), FilteredPostsActivity.class);
                intent.putExtra(FilteredPostsActivity.FILTER_EXTRA_KEY, ((Brand) object).getId());
                getActivity().startActivity(intent);
            }
        }
    }
}
