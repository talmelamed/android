package com.adinlay.adinlay;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adinlay.adinlay.objects.Ad;


/**
 * Created by Noya on 8/15/2018.
 */

public class ShareInfoFrag extends Fragment {

    private static final String TAG = ShareInfoFrag.class.getSimpleName();


//    EditPostViewModel model = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getActivity() != null){
//            model = ViewModelProviders.of(getActivity()).get(EditPostViewModel.class);
//        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_share_info, container, false);

//        if (model == null && getActivity() != null){
//            model = ViewModelProviders.of(getActivity()).get(EditPostViewModel.class);
//        }


        Ad ad = AdinlayApp.getSelectedAd();
        if (ad != null){

            String text = "instagram".equalsIgnoreCase(ad.getTargetMedia()) ?
                    ad.getInstagramText() + "\n" + "\n" : "";
            text = text + ad.getInstagramTag();
            TextView tv = root.findViewById(R.id.si_text_tv);
            if (text.trim().isEmpty()){
                text = tv.getResources().getString(R.string.no_inlay_text);
                tv.setGravity(Gravity.CENTER);
            }
            tv.setText(text);
            tv.setMovementMethod(new ScrollingMovementMethod());

        }

        return root;
    }



}
