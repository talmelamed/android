package com.adinlay.adinlay;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adinlay.adinlay.objects.Ad;
import com.adinlay.adinlay.objects.PostFilter;
import com.bumptech.glide.load.resource.bitmap.CenterInside;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import timber.log.Timber;

/**
 * Created by Noya on 8/7/2018.
 */

public class FiltersRecyclerViewAdapter extends RecyclerView.Adapter<PreviewRecyclerViewAdapter.ViewHolder> {

    private final static String TAG =FiltersRecyclerViewAdapter.class.getSimpleName();

    private String imagePath;
    private ArrayList<PostFilter> filters;
    private Ad ad;
    private GlideRequest<Drawable> requestBuilder;
    private WeakReference<ItemClick> wr_itemClickCallback;
    private int spanCount;
    private Bitmap thumbnail;

    private int size = -1;
    private final int REGULAR = 1;
    private final int PLACEHOLDER = 2;

    public FiltersRecyclerViewAdapter(String imagePath, ArrayList<PostFilter> filters, Ad ad, int spanCount, GlideRequests glideRequests, ItemClick itemClickCallback) {
        this.imagePath = imagePath;
        this.ad = ad;
        this.spanCount = spanCount;
        initiateFilters(filters);
        requestBuilder = glideRequests.asDrawable().transforms(new CenterInside());
        wr_itemClickCallback = new WeakReference<ItemClick>(itemClickCallback);
        initiateThumbnail();
    }

    private void initiateFilters(ArrayList<PostFilter> filtersList){
        filters = new ArrayList<PostFilter>();
        if (filtersList != null){
            filters.addAll(filtersList);
        }
        if (filters.size() > 2*spanCount){
            if (filters.get(filters.size() -1) != null && filters.get(filters.size() -1).isPlaceholder()){
                return;
            }
            int toAdd = filters.size() % spanCount == 0 ?  3*spanCount : 4*spanCount - filters.size() % spanCount;
            for (int i = 0; i < toAdd; i++){
                filters.add(new PostFilter(true));
            }
        }
    }

    private void initiateThumbnail(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                getBaseThumbnail();
            }
        });
    }

    synchronized private Bitmap getBaseThumbnail(){
        if (thumbnail == null){
            thumbnail = AyUtils.getSizedBitmap(imagePath, 400, null);
        }
        return thumbnail;
    }

    public void updateImagePath(@NonNull String imagePath){
        if (!imagePath.equals(this.imagePath)) {
            this.imagePath = imagePath;
            thumbnail = null;
            initiateThumbnail();
        }
    }

    @Override
    public int getItemViewType(int position) {
        PostFilter filter = filters.get(position);
        return filter == null || filter.isPlaceholder() ? PLACEHOLDER : REGULAR;
    }

    @NonNull
    @Override
    public PreviewRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post_preview, parent, false);

//        Timber.d("parent height: %s parent width: %s", parent.getMeasuredHeight(), parent.getMeasuredWidth());
        GridLayoutManager.LayoutParams lp = (GridLayoutManager.LayoutParams) root.getLayoutParams();
        lp.height = parent.getMeasuredWidth() / spanCount;

        if (size <= 0){
            size = lp.height;
        }

        return viewType == REGULAR ? new PreviewRecyclerViewAdapter.ObjectViewHolder(root) : new PreviewRecyclerViewAdapter.PlaceholderVH(root);
    }

    @Override
    public void onBindViewHolder(@NonNull PreviewRecyclerViewAdapter.ViewHolder holder, int position) {
        PostFilter filter = filters.get(position);

        if (holder instanceof PreviewRecyclerViewAdapter.PlaceholderVH){
            ((PreviewRecyclerViewAdapter.PlaceholderVH) holder).border.setVisibility(View.GONE);
            holder.itemView.setTag(R.id.object_tag, null);
            holder.itemView.setTag(R.id.position_tag, null);
            return;
        }


        if (holder instanceof PreviewRecyclerViewAdapter.ObjectViewHolder) {
            Object taggedObject = holder.itemView.getTag(R.id.object_tag);
            if (taggedObject instanceof PostFilter  && !(filter.getName()).isEmpty() &&
                    filter.getName().equals(((PostFilter) taggedObject).getName())){
                holder.itemView.setTag(R.id.position_tag, position);
                // same image, no need to redraw
                return;
            }
            PreviewRecyclerViewAdapter.ObjectViewHolder objectHolder = (PreviewRecyclerViewAdapter.ObjectViewHolder) holder;
            objectHolder.border.setVisibility(View.GONE);
            if (filter.isPlaceholder()){ // shouldn't happen!!!!
                holder.itemView.setTag(R.id.object_tag, null);
                holder.itemView.setTag(R.id.position_tag, null);
                objectHolder.titleTv.setText("");
                objectHolder.titleTv.setVisibility(View.INVISIBLE);
                objectHolder.adIv.setImageResource(0);
                objectHolder.photoIv.setImageResource(0);
                objectHolder.itemView.setOnClickListener(null);
                return;
            }

            if (size <= 0){
                requestBuilder.clone().load(ad.getSelectedLayoutUrl(false)).skipMemoryCache(true).into(objectHolder.adIv);
            } else {
                requestBuilder.clone().load(ad.getSelectedLayoutUrl(false)).skipMemoryCache(true).override(size, size).into(objectHolder.adIv);
            }

            objectHolder.titleTv.setVisibility(View.VISIBLE);
            objectHolder.titleTv.setText(filter.getName());

            objectHolder.itemView.setOnClickListener(itemClickListener);

//        Timber.i("getFiltersAdapter creating loader for %s, is thumbnail is null? %s", filter.getName(), thumbnail == null);
            LoadFilterThumbnail loader = new LoadFilterThumbnail(objectHolder, filter, getBaseThumbnail(), imagePath);
            loader.execute();
        }

        holder.itemView.setTag(R.id.object_tag, filter);
        holder.itemView.setTag(R.id.position_tag, position);
    }

    @Override
    public int getItemCount() {
        return filters.size();
    }

    View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (wr_itemClickCallback.get() != null){
                Object object = v.getTag(R.id.object_tag);
                if (object instanceof PostFilter && v.getTag((R.id.position_tag)) instanceof Integer){
                    Integer pos = (Integer) v.getTag((R.id.position_tag));
                    ((PostFilter) object).setAdapterPosition(pos);
                }
                wr_itemClickCallback.get().onItemClicked(object);
            }
        }
    };

    private static class LoadFilterThumbnail extends AsyncTask<Void, Void, Bitmap> {

        WeakReference<PreviewRecyclerViewAdapter.ObjectViewHolder> wr_holder;
        PostFilter filter;
        Bitmap imgThumbnail;
        String path;

        public LoadFilterThumbnail(PreviewRecyclerViewAdapter.ObjectViewHolder holder, PostFilter filter,
                                   Bitmap imgThumbnail, String imagePath) {
            wr_holder = new WeakReference<PreviewRecyclerViewAdapter.ObjectViewHolder>(holder);
            this.filter = filter;
            this.imgThumbnail = imgThumbnail;
            path = imagePath;
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            Context context = wr_holder.get() == null ? null : wr_holder.get().photoIv.getContext();
            if (imgThumbnail == null){
                Timber.d("getFiltersAdapter filter task no thumbnail, making new for %s", filter.getName());
                imgThumbnail = AyUtils.getSizedBitmap(path, 400, null);
            }

            GPUImageFilter gpuImageFilter = filter == null ? null : filter.getGpuImageFilter(context);
            return AyUtils.applyFilterToBitmap(imgThumbnail, gpuImageFilter, context);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            PreviewRecyclerViewAdapter.ObjectViewHolder holder = wr_holder.get();
            if (holder == null){
                return;
            }

            Object taggedObject = holder.itemView.getTag(R.id.object_tag);
            if (taggedObject instanceof PostFilter  && !(filter.getName()).isEmpty() &&
                    !filter.getName().equals(((PostFilter) taggedObject).getName())){
                // changed the filter, do not load
                return;
            }

            if (bitmap != null){
                holder.photoIv.setImageBitmap(bitmap);
            } else {
                AyUtils.loadSizedBitmapToIv(path, holder.photoIv, filter.getGpuImageFilter(holder.photoIv.getContext()), 400, null);
                holder.itemView.setTag(R.id.object_tag, null);
            }
        }
    }


}
