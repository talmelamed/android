package com.adinlay.adinlay;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import com.adinlay.adinlay.objects.Post;

public class FilteredPostsActivity extends AppCompatActivity implements PostsRecyclerViewAdapter.ItemClick{

    final private static String TAG = FilteredPostsActivity.class.getSimpleName();
    final public static String FILTER_EXTRA_KEY = "filter_extra";

    MainViewModel model = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frag_home);

        setSupportActionBar(null);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= 23){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else if (Build.VERSION.SDK_INT >= 21){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
        }

        ViewGroup toolbarFrame = findViewById(R.id.home_toolbar);
        LayoutInflater inflater = getLayoutInflater();
        inflater.inflate(R.layout.toolbar, toolbarFrame);

        findViewById(R.id.home_tabs).setVisibility(View.GONE);

        findViewById(R.id.toolbar_right_tv).setVisibility(View.GONE);
        TextView btn1 = findViewById(R.id.toolbar_left_tv);
        btn1.setText(getResources().getString(R.string.arrow_and_back));
        btn1.setTextColor(Color.BLACK);
        findViewById(R.id.toolbar_left_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        model = ViewModelProviders.of(this).get(MainViewModel.class);
        model.setPadFor2tabstrips(false);
//        Timber.v("got model: %s", model);
        // get the filter param and pass it to the server query:
        Intent intent = getIntent();
        if (intent.hasExtra(FILTER_EXTRA_KEY)){
            String filter = intent.getStringExtra(FILTER_EXTRA_KEY);
            model.setFilterByBrand(filter);
        }

        PostsFrag frag = new PostsFrag();
        frag.setDisplayMode(HomeFrag.ALL_ADS_MOSAIC);
        frag.setItemClickCallback(this);
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.home_frag_mainFrame, frag, TAG);
        ft.commit();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (AdinlayApp.getUser() == null){
            // lost state - let's let previous activity deal with it
            if (isTaskRoot()) {
                Intent intent = new Intent(FilteredPostsActivity.this, SplashActivity.class);
                startActivity(intent);
            }
            finish();
            return;
        }
        View v = findViewById(R.id.recycler);
        if (v != null){
            v.setPadding(v.getPaddingLeft(), v.getPaddingTop(), v.getPaddingRight(), 0);
        }
    }

    @Override
    public void onMosaicItemClicked(Post post) {
        if (post != null){
            Intent intent = new Intent(this, SinglePostActivity.class);
            intent.putExtra(SinglePostActivity.POST_EXTRA_KEY, post.getJsonString());
            startActivity(intent);
        }
    }
}
