package com.adinlay.adinlay;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.adinlay.adinlay.objects.WalletEntry;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by Noya on 4/12/2018.
 */

public class WalletFrag extends Fragment {

    MainViewModel model = null;
    TextView balanceTv = null;
    TextView pendingTv = null;
    TextView redeemedTv = null;
    View redeemDimmer = null;
    View redeemButton = null;
    private ContentLoadingProgressBar spinner = null;

    private RecyclerView recyclerView = null;
    private RecyclerView.Adapter adapter = null;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_wallet, container, false);

        if (model == null && getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        }

        balanceTv = root.findViewById(R.id.balance_amount_tv);
        pendingTv = root.findViewById(R.id.pending_amount_tv);
        redeemedTv = root.findViewById(R.id.wallet_redeemed_amount_tv);
        redeemDimmer = root.findViewById(R.id.fw_redeem_dimmer);
        spinner = root.findViewById(R.id.fw_spinner);
        redeemButton = root.findViewById(R.id.fw_redeem_button);

        redeemDimmer.setVisibility(View.VISIBLE);

        redeemButton.setOnClickListener(redeemClickListener);

        root.findViewById(R.id.fw_view_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), RedeemHistoryActivity.class);
                startActivity(intent);
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(inflater.getContext());
        recyclerView = root.findViewById(R.id.fw_recycler);
        recyclerView.setLayoutManager(layoutManager);

        SwipeRefreshLayout refresher = root.findViewById(R.id.fw_refresh_layout);
        refresher.setEnabled(false);

        model.getBalanceAmountLD().observe(this, new Observer<BigDecimal>() {
            @Override
            public void onChanged(@Nullable BigDecimal bigDecimal) {
                if (bigDecimal != null){
                    balanceTv.setText(AyUtils.getStringAmountWithCurrencySymbol(bigDecimal, AyUtils.DECIMAL_PLACES_DEFAULT, balanceTv.getResources()));
                    int visibility = bigDecimal.compareTo(BigDecimal.ZERO) > 0 ? View.GONE : View.VISIBLE;
                    redeemDimmer.setVisibility(visibility);
                }
                spinner.hide();
            }
        });

        model.getPendingAmountLD().observe(this, new Observer<BigDecimal>() {
            @Override
            public void onChanged(@Nullable BigDecimal bigDecimal) {
                if (bigDecimal != null){
                    pendingTv.setText(AyUtils.getStringAmountWithCurrencySymbol(bigDecimal, AyUtils.DECIMAL_PLACES_DEFAULT, pendingTv.getResources()));
                }
                spinner.hide();
            }
        });

        model.getRedeemedAmountLD().observe(this, new Observer<BigDecimal>() {
            @Override
            public void onChanged(@Nullable BigDecimal bigDecimal) {
                if (bigDecimal != null){
                    redeemedTv.setText(AyUtils.getStringAmountWithCurrencySymbol(bigDecimal, AyUtils.DECIMAL_PLACES_DEFAULT, redeemedTv.getResources()));
                }
                spinner.hide();
            }
        });

        model.getPingWalletList().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (MainViewModel.FETCH_POSTS_OK.equals(s)){
                    ArrayList<WalletEntry> entries = model.getWalletEntriesArrayList();
                    if (entries == null){
                        entries = getBasicList();
                    }
                    adapter = new WalletEntryRvAdapter(entries);
                    recyclerView.swapAdapter(adapter, true);

                }
                spinner.hide();
            }
        });

        model.getPingWalletError().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Activity activity = getActivity();
                spinner.hide();
                Fragment priorFrag = getFragmentManager() != null ?
                        getFragmentManager().findFragmentByTag("dialog") : null;
                if (s == null || activity == null ||
                        (priorFrag instanceof DialogFragment && priorFrag.isAdded())){
                    if (s != null){
                        model.getPingWalletError().setValue(null);
                    }
                    return;
                }
//                boolean isList = s.startsWith(MainViewModel.WALLET_LIST_PREFIX);
                String message = s.replace(MainViewModel.WALLET_LIST_PREFIX, "")
                        .replace(MainViewModel.WALLET_HEADERS_PREFIX, "");
                message = !message.trim().isEmpty() ? message :
                        getResources().getString(R.string.something_wrong_get_info);

                Bundle args = new Bundle();
                args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.error);
                args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, message);
                args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
                args.putInt(AyDialogFragment.ARG_RESULT_ICON, AyDialogFragment.VALUE_FALSE);
                AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                if (!activity.isFinishing() && isAdded()) {
                    AyUtils.showDialogFragment(dialogFragment, getFragmentManager());
                }
                model.getPingWalletError().setValue(null);

            }
        });

        model.getPingRecheckEmailVerified().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (s != null ){
                    model.getPingRecheckEmailVerified().postValue(null);
                } else {
                    return;
                }
                spinner.hide();
                redeemButton.setOnClickListener(redeemClickListener); // re-set the click-listener
                Activity activity = getActivity();
                float balance = model.getBalance();
                if (activity == null || balance == 0){
                    return;
                }

                if (AdinlayApp.getUser() != null && AdinlayApp.getUser().isEmailVerified() && isAdded()) {
                    Intent intent = new Intent(activity, RedeemActivity.class);
                    intent.putExtra(RedeemActivity.BALANCE_STRING_KEY, balanceTv.getText().toString());
                    intent.putExtra(RedeemActivity.BALANCE_FLOAT_KEY, balance);
                    startActivityForResult(intent, AyUtils.get16BitId(R.id.intent_redeem));
                } else if (isAdded()) {
                    // offer user to verify their email:
                    String message = getString(R.string.confirm_email_block_message, AdinlayApp.getUserEmail());
                    Bundle args = new Bundle();
                    args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.confirm_email_block_title);
                    args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, message);
                    args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.re_send);
                    args.putInt(AyDialogFragment.ARG_BUTTON_R_RES, R.string.ok_allcaps);
                    AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                    dialogFragment.setListener(AyDialogFragment.VALUE_LEFT, new AyDialogFragment.AyClickListener() {
                        @Override
                        public void onButtonClicked(View v) {
                            model.resendEmailVerification();
                        }
                    });

                    if (!activity.isFinishing() && isAdded()) {
                        AyUtils.showDialogFragment(dialogFragment, getFragmentManager());
                    }
                }
            }
        });

        model.getPingResendEmailVerification().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                model.getPingResendEmailVerification().postValue(null);
                if (MainViewModel.FETCH_POSTS_OK.equals(s) && getActivity() != null){
                    Toast t = Toast.makeText(getActivity().getApplicationContext(),
                            getString(R.string.verification_email_was_sent_short, AdinlayApp.getUserEmail()), Toast.LENGTH_SHORT);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                } else if ((MainViewModel.RESEND_VERIFICATION_ERROR.equals(s) ||
                        MainViewModel.RESEND_VERIFICATION_ERROR2.equals(s))  && getActivity() != null){
                    int messageRes = MainViewModel.RESEND_VERIFICATION_ERROR.equals(s) ?
                            R.string.something_wrong_resend_verification :
                            R.string.something_wrong_resend_verification_need_new_login;
                    Toast t = Toast.makeText(getActivity().getApplicationContext(),
                            messageRes, Toast.LENGTH_LONG);
                    t.setGravity(Gravity.CENTER, 0, 0);
                    t.show();
                }

            }
        });

        ArrayList<WalletEntry> entries = model.getWalletEntriesArrayList();
        if (entries == null){
            entries = getBasicList();
        }
        adapter = new WalletEntryRvAdapter(entries);
        recyclerView.swapAdapter(adapter, true);

        model.fetchWalletInfo();
        spinner.show();

        return root;
    }

    private ArrayList<WalletEntry> getBasicList(){
        ArrayList<WalletEntry> basicList = new ArrayList<>();
        basicList.add(WalletEntry.getTitleEntry());
        return basicList;
    }

    private View.OnClickListener redeemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            float balance = model.getBalance();
            if (balance == 0){
                return;
            }
            // check verification
            if (AdinlayApp.getUser() != null && AdinlayApp.getUser().isEmailVerified()) {
                Intent intent = new Intent(getActivity(), RedeemActivity.class);
                intent.putExtra(RedeemActivity.BALANCE_STRING_KEY, balanceTv.getText().toString());
                intent.putExtra(RedeemActivity.BALANCE_FLOAT_KEY, balance);
                startActivityForResult(intent, AyUtils.get16BitId(R.id.intent_redeem));
            } else {
                v.setOnClickListener(null); // prevent more clicks
                model.recheckEmailVerified();
                spinner.show();
            }
        }
    };

//    private void setCheckEmailText(TextView tv){
//        if (tv == null){
//            return;
//        }
//        String bold = AdinlayApp.getUserEmail();
//        String text = getResources().getString(R.string.wallet_confirm_email, bold);
//        Spannable spannable = new SpannableString(text);
//        int substringStart=0;
//        int start = text.indexOf(bold, substringStart);
//        while(start >=0){
//            spannable.setSpan(
//                    new StyleSpan(Typeface.BOLD),
//                    start,start + bold.length(),
//                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
//            );
//            substringStart = start + bold.length();
//            start = text.indexOf(bold, substringStart);
//        }
//        tv.setText(spannable, TextView.BufferType.SPANNABLE);
//    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == (R.id.intent_redeem & 0x0000FFFF) && resultCode == Activity.RESULT_OK) {
            balanceTv.setText("");
            redeemedTv.setText("");
            redeemDimmer.setVisibility(View.VISIBLE);
            model.fetchBalanceInfo();
            spinner.show();
        }
    }

}
