package com.adinlay.adinlay.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.adinlay.adinlay.AdinlayApp;
import com.adinlay.adinlay.AyUtils;
import com.adinlay.adinlay.BuildConfig;
import com.adinlay.adinlay.R;
import com.adinlay.adinlay.ServerManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import timber.log.Timber;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

//        Timber.i("NTFY_ onMessageReceived data: %s", remoteMessage.getData()); // NTFY_ onMessageReceived data: {Nick=Mario, Room=PortugalVSDenmark, body=great match!}

        if (remoteMessage.getNotification() != null) {
//            Timber.i("NTFY_ onMessageReceived got: %s", remoteMessage.getNotification().getBody()); // [25488-26507] NTFY_ onMessageReceived got: Notification from FCM
            // we should display this notification - app is in the foreground:
            sendNotification(remoteMessage.getNotification().getBody(),remoteMessage.getNotification().getTitle());
        }
        // send broadcast according to notification data:
        if (remoteMessage.getData() != null){

            LocalBroadcastManager broadcaster = LocalBroadcastManager.getInstance(getBaseContext());
            Intent intent = new Intent(getString(R.string.filter_approvals_action));
            boolean sendBroadcast = false;
            if (AyUtils.UPDATE_VAL.equals(remoteMessage.getData().get(AyUtils.APPROVED_KEY))){
//                Timber.i("NFT_ update approved");
                intent.putExtra(AyUtils.APPROVED_KEY, AyUtils.UPDATE_VAL);
                sendBroadcast = true;
            }
            if (AyUtils.UPDATE_VAL.equals(remoteMessage.getData().get(AyUtils.WALLET_KEY))){
//                Timber.i("NFT_ update wallet");
                intent.putExtra(AyUtils.WALLET_KEY, AyUtils.UPDATE_VAL);
                sendBroadcast = true;
            }
            if (sendBroadcast) {
                broadcaster.sendBroadcast(intent);
            }
        }
    }

    @Override
    public void onNewToken(String token) {
        String old = AyUtils.getStringPref(AyUtils.PREF_FCM_TOKEN);
        Timber.v("NTFY_ onNewToken got: %s to replace %s", token, old);
        AyUtils.saveStringPreference(AyUtils.PREF_FCM_TOKEN, token);
        if (old != null){
            AyUtils.saveStringPreference(AyUtils.PREF_REPLACED_FCM_TOKEN, old);
        }
        // if app knows user - notify server of new token, and remove old one
        if (AdinlayApp.getUser() != null){
            ServerManager.sendFcmTokenToServer();
        }
    }

    @Override
    public void onDeletedMessages() {
        super.onDeletedMessages();
        Timber.i("NTFY_ onDeletedMessages");
        // should sync with server
        // this deletion occurs when there are too many messages (>100) pending for your app on a particular device at the time it connects or if the device hasn't connected to FCM in more than one month
    }

    private void sendNotification(String messageBody, String title) {
        if (messageBody == null || messageBody.isEmpty()){
            return;
        }

        title = title == null || title.isEmpty() ? getString(R.string.app_name) : title;

        // see more about notifications in https://medium.com/@krossovochkin/android-notifications-overview-and-pitfalls-517d1118ec83

        PackageManager pm = getPackageManager();
        Intent launchIntent = pm.getLaunchIntentForPackage(BuildConfig.APPLICATION_ID);
        int num = (int) System.currentTimeMillis(); // so that notifications will not replace each other
        PendingIntent pendingIntent = PendingIntent.getActivity(this, num, launchIntent, 0);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_small_notification)
                        .setContentTitle(title)
                        .setContentText(messageBody)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(messageBody))
                        .setAutoCancel(true)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent); // fixme: consider deleteIntent

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            notificationBuilder = notificationBuilder.setColor(getResources().getColor(R.color.color_notification));
        }

        NotificationManager notificationManager = // ***OR*** use NotificationManagerCompat
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (notificationManager != null) {
            // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(channelId,
                        getString(R.string.app_name),
                        NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(channel);
            }

            notificationManager.notify(num /* ID of notification */, notificationBuilder.build());
            // fixme: need a proper notification id, to enable updates (or delete the notification), or it can be unique
        }

    }


}
