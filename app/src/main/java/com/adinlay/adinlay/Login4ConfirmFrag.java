package com.adinlay.adinlay;


import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Noya on 5/22/2018.
 */

public class Login4ConfirmFrag extends LoginFrag {

    private static final String TAG = Login4ConfirmFrag.class.getSimpleName();

    private LoginViewModel viewModel;
    private EditText firstNameEt = null;
    private EditText lastNameEt = null;
    private EditText emailEt = null;
    private EditText phoneEt = null;

    private boolean canEditEmail = false;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_login4_name, container, false);
        firstNameEt = root.findViewById(R.id.login4_first_name);
        lastNameEt = root.findViewById(R.id.login4_last_name);
        emailEt = root.findViewById(R.id.login4_email);
        phoneEt = root.findViewById(R.id.login4_phone);
        int res = SignInManager.UPDATE_CONFIRM_STAGE.equals(AdinlayApp.getLoginStage()) ?
                R.drawable.group_next_profile : R.drawable.group_confirm;
        viewModel.updateFlowData(res, getResources().getString(R.string.confirm_allcaps), false, true, true);

        viewModel.setFlowStart(viewModel.isSingleUpdate() ? -1 : System.currentTimeMillis());

        User user = AdinlayApp.getUser();
        if (user != null){
            if (!user.getFirstName().isEmpty()){
                firstNameEt.setText(user.getFirstName());
            }
            if (!user.getLastName().isEmpty()){
                lastNameEt.setText(user.getLastName());
            }
            if (!user.geteMail().isEmpty()){
                emailEt.setText(user.geteMail());
            }
            if (!user.getPhoneNumber().isEmpty()){
                phoneEt.setText(user.getPhoneNumber());
            }
            canEditEmail = user.canEditEmail();
        }

        // disable emailEt if necessary:
        emailEt.setEnabled(canEditEmail);
        int colorRes = canEditEmail ? Color.BLACK : getResources().getColor(R.color.disabled_black);
        emailEt.setTextColor(colorRes);

        root.requestFocus(); // remove focus from lower Edit Texts


        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity()).get(LoginViewModel.class);
    }

    @Override
    public boolean onMainClicked() {

        String firstName = firstNameEt == null ? "" : firstNameEt.getText().toString();
        String lastName = lastNameEt == null ? "" : lastNameEt.getText().toString();
        String email = emailEt == null ? "" : emailEt.getText().toString();

        if (!firstName.isEmpty() && !lastName.isEmpty() &&
                (!canEditEmail || email.matches(AyUtils.email_REGEX))){
            User user = AdinlayApp.getUser();
            if (user != null){
                user.changePendingJsonField(User.FIRST_NAME_KEY, firstName);
                user.changePendingJsonField(User.LAST_NAME_KEY, lastName);
                if (canEditEmail && !email.equals(user.geteMail())) {
                    user.changePendingJsonField(User.EMAIL_KEY, email);
                    if (!user.isEmailVerified()) {
                        viewModel.setSentEmailUpdate(true);
                    }
                }
                String phone = phoneEt == null ? "" : phoneEt.getText().toString();
                if (!phone.isEmpty() || !user.getPhoneNumber().isEmpty()){
                    user.changePendingJsonField(User.PHONE_KEY, phone);
                }
                // send the request to the server - and return false
                viewModel.sendAccountUpdate();
                return false;
            }
        } else if (getActivity() != null) {

            if (firstName.isEmpty()){
                firstNameEt.setHintTextColor(getResources().getColor(R.color.redHint));
            }
            if (lastName.isEmpty()){
                lastNameEt.setHintTextColor(getResources().getColor(R.color.redHint));
            }
            if (canEditEmail && !email.matches(AyUtils.email_REGEX)){
                emailEt.setTextColor(getResources().getColor(R.color.red));
                addTextWatcherToEmailEt();
            }

            // toast:
            String text = (firstName.isEmpty() || lastName.isEmpty()) ?
                    getResources().getString(R.string.toast_empty_name) :
                    getResources().getString(R.string.invalid_email);
            Toast toast = Toast.makeText(getActivity().getApplicationContext(), text, Toast.LENGTH_LONG);
            toast.setGravity(Gravity.BOTTOM, 0, AyUtils.convertDpToPixel(100, getActivity()));
            toast.show();
        }
        return false;
    }

    @Override
    public boolean onBackClicked() {
        return false;
    }


    private void addTextWatcherToEmailEt(){
        emailEt.removeTextChangedListener(emailEtTextWatcher);
        emailEt.addTextChangedListener(emailEtTextWatcher);
    }

    TextWatcher emailEtTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (emailEt != null) {
                // return the text color to black
                emailEt.setTextColor(Color.BLACK);
                // remove the textWatcher
                emailEt.removeTextChangedListener(emailEtTextWatcher);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    protected String getFragStageName() {
        return AdinlayApp.getLoginStage();
    }

    @Override
    protected boolean shouldChangeScreenName() {
        return SignInManager.CONFIRM_STAGE.equals(AdinlayApp.getLoginStage()); // not changing firebase-screenName for updates
    }
}
