package com.adinlay.adinlay;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 * Ensures that Glide's generated API is created
 */
@GlideModule
public class AdinlayGlideModule extends AppGlideModule {
}
