package com.adinlay.adinlay;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.adinlay.adinlay.objects.ProfileTopic;
import com.adinlay.adinlay.objects.ProfileUserRvItem;

import java.util.ArrayList;

public class ProfileEditActivity extends BasicActivity implements ItemClick {

    private TextView title = null;
    private TextView vcfName = null;
    private TextView email = null;
    private TextView phoneTv = null;

    ProfileUserRvAdapter adapter;
    ProfileEditViewModel model;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (AdinlayApp.getUser() == null){
            finish();
            return;
        }

        model = ViewModelProviders.of(this).get(ProfileEditViewModel.class);

        FrameLayout headerFrame = findViewById(R.id.profile_vcf_frame);
        headerFrame.addView(getLayoutInflater().inflate(R.layout.profile_vcf, headerFrame, false));

//        // home button: do we need? we have back. does home go to AllAds?
//        findViewById(R.id.toolbar_right_iv).setVisibility(View.VISIBLE);
//        findViewById(R.id.toolbar_right_btn).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent returnIntent = new Intent();
//                setResult(Activity.RESULT_OK, returnIntent);
//                finish();
//            }
//        });

        vcfName = findViewById(R.id.vcf_name_tv);
        email = findViewById(R.id.vcf_email_tv);
        phoneTv = findViewById(R.id.vcf_phone_num_tv);
        title = findViewById(R.id.profile_prefs_title_tv);
        RecyclerView rv = findViewById(R.id.profile_prefs_rv);

        ImageView doneButton = findViewById(R.id.vcf_edit_button);
        doneButton.setImageResource(R.drawable.button_done);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rv.setLayoutManager(layoutManager);
        rv.setHasFixedSize(true);
        ArrayList<ProfileUserRvItem> items = new ArrayList<>();
        items.add(new ProfileUserRvItem(R.string.account, R.drawable.button_edit));
        ArrayList<ProfileTopic> youTopics = AdinlayApp.getUserProfileTopics();
        if (youTopics != null){
            for (int i = 0; i < youTopics.size(); i++){
                ProfileTopic topic = youTopics.get(i);
                items.add(new ProfileUserRvItem(topic, i, SignInManager.UPDATE_YOU_STAGE));
            }
        }
        items.add(new ProfileUserRvItem(R.string.get_ranking, R.drawable.button_go, R.drawable.igaudit_ic));
        adapter = new ProfileUserRvAdapter(items, this);
        rv.setAdapter(adapter);

        // logout
        findViewById(R.id.vcf_logout_adinlay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle args = new Bundle();
                args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.logout_adinlay_camel);
                args.putInt(AyDialogFragment.ARG_MESSAGE_RES, R.string.logout_adinlay_ru_sure);
                args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.cancel);
                args.putInt(AyDialogFragment.ARG_BUTTON_R_RES, R.string.logout);
                AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                dialogFragment.setListener(AyDialogFragment.VALUE_RIGHT, new AyDialogFragment.AyClickListener() {
                    @Override
                    public void onButtonClicked(View v) {
                        // delete fcm and only if ok logout user:
                        model.deleteFcmToken();
                    }
                });
                AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());

            }
        });

        model.getPingServerResponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (s == null){
                    return;
                }
                if (MainViewModel.DELETE_FCM_OK.equals(s)){
                    // logout user:
                    AyUtils.logoutUser();
                    finish(); // once we go back to main and there is no user - main calls splash and finishes
                } else {
                    // oh-uh! a problem deleting the token. cannot fully log out:
                    Bundle args = new Bundle();
                    args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.error);
                    args.putInt(AyDialogFragment.ARG_MESSAGE_RES, R.string.something_wrong_logout_adinlay);
                    args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
                    AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                    if (!isFinishing()) {
                        AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
                    }
                }
            }
        });
    }

    @Override
    int getLayoutResourceId() {
        return R.layout.activity_profile_prefs;
    }

    @Override
    protected void onResume() {
        super.onResume();

        String titleStr = AdinlayApp.getUserVcfName().toUpperCase() + " " + getResources().getString(R.string.profile).toUpperCase();
        title.setText(titleStr);

        if (vcfName != null){
            vcfName.setText(AdinlayApp.getUserVcfName());
        }
        if (email != null){
            email.setText(AdinlayApp.getUserEmail());
        }
        if (phoneTv != null){
            phoneTv.setText(AdinlayApp.getUserPhone());
        }
        int strength = AdinlayApp.getUserStrength();
        AyUtils.setResToStar((ImageView) findViewById(R.id.vcf_1_star), 0, strength);
        AyUtils.setResToStar((ImageView) findViewById(R.id.vcf_2_star), 1, strength);
        AyUtils.setResToStar((ImageView) findViewById(R.id.vcf_3_star), 2, strength);
        AyUtils.setResToStar((ImageView) findViewById(R.id.vcf_4_star), 3, strength);
        AyUtils.setResToStar((ImageView) findViewById(R.id.vcf_5_star), 4, strength);

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClicked(Object object) {
        if (object instanceof Integer){
            int objectId = ((Integer)object);
            switch (objectId) {
                case R.string.account:
                case R.string.preferences_allcaps:
                    if (objectId == R.string.account) {
                        AdinlayApp.setLoginStage(SignInManager.UPDATE_CONFIRM_STAGE);
                    }
                    Intent intent = new Intent(ProfileEditActivity.this, Login2FlowActivity.class);
                    intent.putExtra(AyUtils.IS_SINGLE_UPDATE, AyUtils.IS_SINGLE_UPDATE);
                    startActivityForResult(intent, AyUtils.get16BitId(R.id.intent_update_profile));
                    break;
                case R.string.get_ranking:
                    Intent rankingsLinkIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://igaudit.io/"));
                    startActivity(rankingsLinkIntent);
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        switch (requestCode){
            case R.id.intent_update_profile & 0x0000FFFF:
                if (resultCode == RESULT_OK){ // we send this if the user clicks "home" from next screen
                    setResult(RESULT_OK, new Intent());
                    finish();
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
