package com.adinlay.adinlay;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by Noya on 6/10/2018.
 */

public class ConnectToInstagramActivity extends AppCompatActivity {

    private final static String INSTAGRAM_HELP_URL = "https://www.instagram.com/accounts/password/reset/";

    EditText usernameEt;
    EditText passwordEt;
    TextView signInButton;
    View spinnerFrame;
    boolean providedEmail = false;
    boolean providedPassword = false;

    private LoginOptionsViewModel model;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_cnct_instgrm);

        usernameEt = findViewById(R.id.l10_email);
        passwordEt = findViewById(R.id.l10_password);
        signInButton = findViewById(R.id.l10_login_btn);
        spinnerFrame = findViewById(R.id.l10_spinner_frame);

        model = ViewModelProviders.of(this).get(LoginOptionsViewModel.class);

        model.getSpinnerLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean show) {
                int visibility = show == null || !show ? View.GONE : View.VISIBLE;
                spinnerFrame.setVisibility(visibility);
            }
        });


        model.getServerRequestLiveData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if ("ready".equals(s) && AdinlayApp.getUser() != null){
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    String message = s != null && !s.isEmpty() ?  s:
                            getResources().getString(R.string.something_wrong_inst_login);
                    Bundle args = new Bundle();
                    args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.error);
                    args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, message);
                    args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
                    args.putInt(AyDialogFragment.ARG_RESULT_ICON, AyDialogFragment.VALUE_FALSE);
                    AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                    if (!isFinishing()) {
                        AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
                    }
                    FirebaseAnalytics.getInstance(ConnectToInstagramActivity.this).logEvent("ig_connect_fail", null);
                }
            }
        });

        usernameEt.addTextChangedListener(new EnableButtonTextWatcher(R.id.l10_email));
        passwordEt.addTextChangedListener(new EnableButtonTextWatcher(R.id.l10_password));

        signInButton.setEnabled(false);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName = usernameEt.getText().toString();
                String password = passwordEt.getText().toString();

                hideKeyboard(v);

                if (password.isEmpty() || userName.isEmpty()){
                    // shouldn't happen!
                    Toast.makeText(getApplicationContext(),
                            getResources().getString(R.string.empty_username_password), Toast.LENGTH_SHORT).
                            show();
                    return;
                }

                model.connectToInstagram(userName, password);


            }
        });

        spinnerFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { // does nothing - but blocks other clicks
            }
        });

        findViewById(R.id.l10_inst_help).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri webpage = Uri.parse(INSTAGRAM_HELP_URL);
                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        });


        // add keyboard listener:
        final View layoutRoot = findViewById(R.id.connect_inst_con_layout);
        layoutRoot.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect rect = new Rect();
                layoutRoot.getWindowVisibleDisplayFrame(rect);

                int heightDiff = layoutRoot.getRootView().getHeight() - (rect.bottom - rect.top);

                int visibility = View.VISIBLE;
                if (heightDiff > AyUtils.convertDpToPixel(100, layoutRoot.getContext())){
                    visibility = View.GONE;
                }

                toggleViewVisibility(layoutRoot.findViewById(R.id.l10_bottom_spacer), visibility);
            }
        });

        layoutRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
            }
        });


    }

    private void toggleViewVisibility(final View v, final int visibility){
        if (v != null){
            v.setVisibility(visibility);
        }
    }

    private void hideKeyboard(View v){

        if (v != null){
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (AdinlayApp.getUser() == null){
            // lost state - let's let previous activity deal with it
            finish();
        }
    }

    private class EnableButtonTextWatcher implements TextWatcher{

        int viewId;

        public EnableButtonTextWatcher(int viewId) {
            this.viewId = viewId;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            boolean gaveInput = s.length() > 0;
            if (viewId == R.id.l10_email){
                providedEmail = gaveInput;
            } else if (viewId == R.id.l10_password){
                providedPassword = gaveInput;
            }

            if (providedEmail && providedPassword){
                if (!signInButton.isEnabled()){
                    signInButton.setEnabled(true);
                    signInButton.setTextColor(Color.WHITE);
                }
            } else {
                if (signInButton.isEnabled()){
                    signInButton.setEnabled(false);
                    signInButton.setTextColor(getResources().getColor(R.color.blue_inst_disabled));
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }

}
