package com.adinlay.adinlay;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Process;
import android.support.annotation.NonNull;

import com.adinlay.adinlay.objects.Ad;
import com.adinlay.adinlay.objects.DraftPost;
import com.adinlay.adinlay.objects.ProfileTopic;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.threeten.bp.ZonedDateTime;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import timber.log.Timber;

/**
 * Created by Noya on 5/30/2018.
 */

public class ServerManager {

    private static final String TAG = ServerManager.class.getSimpleName();

    public static final String POOR_CONNECTIVITY = "poorConnectivity";
    public static final String POOR_CONNECTIVITY_USER_INFO = POOR_CONNECTIVITY + "UserInfo";
    public static final String POOR_CONNECTIVITY_GET_TOKEN = POOR_CONNECTIVITY + "GetToken";
    public static final String USER_INFO_TRY_FROM_SCRATCH = "userInfoTryFromScratch";
    public static final String UNSUPPORTED_APP_VERSION = "unsupportedVersionError";

    private static OkHttpClient okHttpClient = null;
    private static ClearableCookieJar cookieJar = null;
//    private static OkHttpClient selfCA_OkHttpClient = null;

    // todo: get a threadPool?

    public static final String HEADRER = "Authorization";

    private static final String ERROR_PREFIX = "Cannot call server";

    public static final int AUTHENTICATE = 0;
    public static final int USER_INFO = 1;
    public static final int INSTAGRAM_LOGIN = 2;
    public static final int INSTAGRAM_LOGOUT = 3;
    public static final int CONNECTIVITY_REQUEST = 4;
    public static final int SEND_MY_POST = 5;
    public static final int GET_ALL_POSTS = 6;
    public static final int PAYMENTS_LIST = 7;
    public static final int BALANCE_REQUEST = 8;
    public static final int REDEEM_REQUEST = 9;
    public static final int RESEND_REDEMPTION = 10;
    public static final int GALLERY_ADS = 11;
    public static final int CAMERA_ADS = 12;
    public static final int BRANDS = 13;
    public static final int POSTS_BY_BRANDS = 14;
    public static final int FILTERS = 15;
    public static final int WALLET_LIST = 16;
    public static final int SEND_MY_POST_FCBK = 17;
    public static final int CHECK_APP_VERSION = 18;
    public static final int GET_MY_POSTS = 19;
    public static final int USER_ACCOUNT_INFO = 20;
    public static final int RECHECK_EMAIL_VERIFIED = 21;
    public static final int FCM_CLIENT_TOKEN = 22;
    public static final int FCBK_POST_SUCCESS = 23;
    public static final int FCBK_POST_REJECT = 24;
    public static final int FCBK_CONNECT = 25;
    public static final int FCBK_DISCONNECT = 26;
    public static final int FCBK_IS_CONNECTED = 27;
    public static final int FCBK_APPROVED_POSTS = 28;
    public static final int PROMO_CODES = 29;
    public static final int SEND_FCBK_FREE_POST = 30;
    public static final int REMOVE_USER = 31;
    public static final int USER_INFO_NO_SETTINGS = 32;
    public static final int IG_FETCH_LOGIN_PARAMS = 33;
    public static final int IG_SEND_LOGIN_RESULT = 34;
    public static final int SOCIAL_MEDIA_PROFILE_UPDATE = 35;


    private static String getUrl(int code){
        return getUrl(code, null);
    }

    private static String getUrl(int code, String param){

        String host = BuildConfig.SERVER_HOST;
        switch (code){
            case AUTHENTICATE:
                return host + "/client/authenticate";
            case USER_INFO:
                return host + "/client/users/me?settings=true";
            case INSTAGRAM_LOGIN:
                return host + "/client/social/instagram/login";
            case INSTAGRAM_LOGOUT:
                return host + "/client/social/instagram/logout";
            case CONNECTIVITY_REQUEST:
                return host + "/client/social/media/account";
            case SEND_MY_POST:
                return host + "/client/social/instagram/posts/me";
            case GET_ALL_POSTS:
                return host + "/client/social/media/posts"; // replace media with instagram for only instagram posts
            case PAYMENTS_LIST:
                return host + "/client/payment/walletAccount/redemptions/user/me";
            case BALANCE_REQUEST:
                return host + "/client/payment/walletAccount/balance/me";
            case REDEEM_REQUEST:
                return host + "/client/payment/walletAccount/redemptions";
            case RESEND_REDEMPTION:
                return host + "/client/payment/walletAccount/redemptions/resend";
            case GALLERY_ADS:
                return host + "/client/personal_ads/me";
            case CAMERA_ADS:
                return host + "/client/personal_ads?target=camera";
            case BRANDS:
                return host + "/client/brands";
            case POSTS_BY_BRANDS:
                return host + "/client/social/media/brands/" + param + "/posts"; // replace media with instagram for only instagram posts
            case FILTERS:
                return host + "/client/filters";
            case WALLET_LIST:
                return host + "/client/payment/walletAccount/payment/user/me";
            case SEND_MY_POST_FCBK:
                return host + "/client/social/facebook/posts/create/me";
            case CHECK_APP_VERSION:
                return host + "/client/version";
            case GET_MY_POSTS:
                return host + "/client/social/media/posts/me";
            case USER_ACCOUNT_INFO:
                return host + "/client/users/account/me";
            case RECHECK_EMAIL_VERIFIED:
                return host + "/client/users/verifyemail/me";
            case FCM_CLIENT_TOKEN:
                return host + "/client/users/notifications/me";
            case FCBK_POST_SUCCESS:
                return host + "/client/social/facebook/posts/success/me";
            case FCBK_POST_REJECT:
                return host + "/client/social/facebook/posts/cancel/me";
            case FCBK_CONNECT:
                return host + "/client/social/facebook/set/token/me";
            case FCBK_DISCONNECT:
                return host + "/client/social/facebook/remove/token/me";
            case FCBK_IS_CONNECTED:
                return host + "/client/social/facebook/account/me";
            case FCBK_APPROVED_POSTS:
                return host + "/client/social/facebook/posts/approved/me";
            case PROMO_CODES:
                return host + "/client/users/settings/me";
            case SEND_FCBK_FREE_POST:
                return host + "/client/social/facebook/posts/editSuccess/me";
            case REMOVE_USER:
                return host + "/client/social/media/users/me";
            case USER_INFO_NO_SETTINGS:
                return host + "/client/users/me";
            case IG_FETCH_LOGIN_PARAMS:
                return host + "/client/social/instagram/user/login/params/me";
            case IG_SEND_LOGIN_RESULT:
                return host + "/client/social/instagram/user/login";
            case SOCIAL_MEDIA_PROFILE_UPDATE:
                return host + "/client/users/me/socialmedia";
            default:
                return host + "/client/version";
        }
    }

    interface Callback{
        void onResponseOk(String responseBody);
        void onError(String message);
    }

    interface InternalCallback{
        Request getRequest();
        @NonNull OkHttpClient getOkClient();
        boolean extractJson();
        void onResponseOk(String responseStr, JSONObject responseJson);
        @NonNull String requestName();
        boolean makeProcessBackground();
    }

    private static ClearableCookieJar getCookieJar(){
        if (cookieJar == null){
            Context context = AyUtils.getContext();
            if (context != null) {
                cookieJar = new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
            }
        }
        return cookieJar;
    }

    public static void initCookieJar(Context context){
        if (cookieJar == null){
            cookieJar = new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
        }
    }

    public static void clearCookieJar(){
        if (cookieJar != null) {
            getCookieJar().clear();
        }
    }

    private static OkHttpClient getRegularOkHttpClient(){
        if (okHttpClient == null){
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            if (getCookieJar() != null){
                builder.cookieJar(getCookieJar());
            }
            okHttpClient = builder.build();
        }
        return okHttpClient;
    }

    private static OkHttpClient getOkHttpClient(ArrayList<String> responseKeeper){
        return getRegularOkHttpClient();

//        if (AdinlayApp.isProdServer() || true){
//        }
//        if (selfCA_OkHttpClient == null){
//            initSelfCA_OkHttpClient(responseKeeper);
//        }
//        return selfCA_OkHttpClient;
    }

     private static OkHttpClient get30SecTimeoutClient(){
        OkHttpClient longerTimeoutClient = getRegularOkHttpClient().newBuilder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(0, TimeUnit.MILLISECONDS)
                .writeTimeout(0, TimeUnit.MILLISECONDS)
                .build();
        return longerTimeoutClient;
    }

    private static OkHttpClient getNoTimeoutClient(){
        return getRegularOkHttpClient().newBuilder()
                .connectTimeout(0, TimeUnit.SECONDS)
                .readTimeout(0, TimeUnit.MILLISECONDS)
                .writeTimeout(0, TimeUnit.MILLISECONDS)
                .build();
    }

//    private static synchronized void initSelfCA_OkHttpClient(ArrayList<String> errorMessage){
//        if (selfCA_OkHttpClient != null){
//            return;
//        }
//        if (errorMessage == null){
//            errorMessage = new ArrayList<>();
//        }
//        errorMessage.clear();
//        Timber.d("SSL_DBG initSelfCA_OkHttpClient");
//        Context resContext = AyUtils.getContext();
//        if (resContext == null){
//            Timber.w("SSL_DBG context is null");
//            errorMessage.add(0, "Cannot call server, error 1");
//            return;
//        }
//        Certificate ca = null;
//        InputStream cert = resContext.getResources().openRawResource(R.raw.server_cert);
//        try {
//            CertificateFactory cf = CertificateFactory.getInstance("X.509");
//            ca = cf.generateCertificate(cert);
//        } catch (CertificateException e){
//            Timber.w(e, "SSL_DBG 1");
//            errorMessage.add(0, "Cannot call server, " + e.getMessage());
//            e.printStackTrace();
//        } finally {
//            try {
//                cert.close();
//            } catch (IOException e) {
//                Timber.w(e, "SSL_DBG 2");
//                e.printStackTrace();
//            }
//        }
//        if (ca == null){
//            Timber.i("SSL_DBG ca is null");
//            if (errorMessage.size() == 0){
//                errorMessage.add(0, "Cannot call server, error 2");
//            }
//            return;
//        }
//
//        // Create a KeyStore containing our trusted CAs
//        String keyStoreType = KeyStore.getDefaultType();
//        KeyStore keyStore = null;
//        try {
//            keyStore = KeyStore.getInstance(keyStoreType);
//            keyStore.load(null, null);
//            keyStore.setCertificateEntry("ca", ca);
//        } catch (KeyStoreException | NoSuchAlgorithmException |
//                IOException | CertificateException e) {
//            Timber.w(e, "SSL_DBG 3");
//            errorMessage.add(0, e.getMessage());
//            e.printStackTrace();
//        }
//
//        if (keyStore == null){
//            Timber.i("SSL_DBG keystore is null");
//            if (errorMessage.size() == 0) {
//                errorMessage.add(0, "Cannot call server, error 3");
//            }
//            return;
//        }
//
//        // Create a TrustManager that trusts the CAs in our KeyStore
//        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
//        TrustManagerFactory tmf = null;
//
//        try {
//            tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
//            tmf.init(keyStore);
//        } catch (NoSuchAlgorithmException | KeyStoreException e) {
//            Timber.w(e, "SSL_DBG 4");
//            errorMessage.add(0, e.getMessage());
//            e.printStackTrace();
//        }
//        if (tmf == null){
//            Timber.i("SSL_DBG tmf is null");
//            if (errorMessage.size() == 0){
//                errorMessage.add(0, "Cannot call server, error 4");
//            }
//            return;
//        }
//
//        // Create an SSLContext that uses our TrustManager
//        SSLContext sslContext = null;
//        try {
//            sslContext = SSLContext.getInstance("TLS");
//            sslContext.init(null, tmf.getTrustManagers(), null);
//        } catch (NoSuchAlgorithmException | KeyManagementException e) {
//            Timber.w(e, "SSL_DBG 5" );
//            errorMessage.add(0, e.getMessage());
//            e.printStackTrace();
//        }
//
//        if (sslContext == null){
//            Timber.i("SSL_DBG SSLContext is null");
//            if (errorMessage.size() == 0){
//                errorMessage.add(0, "Cannot call server, error 5");
//            }
//            return;
//        }
//
//        // Tell the OkHttpClient to use a SocketFactory from our SSLContext:
//        X509TrustManager trustManager = (X509TrustManager) tmf.getTrustManagers()[0];
//        OkHttpClient.Builder builder = new OkHttpClient.Builder().sslSocketFactory(sslContext.getSocketFactory(), trustManager);
//        builder.hostnameVerifier(new HostnameVerifier() {
//            @Override
//            public boolean verify(String hostname, SSLSession session) {
//                Timber.d("SSL_DBG comparing api.adinlay.com with %s", hostname);
//                return "api.adinlay.com".equalsIgnoreCase(hostname);
//            }
//        });
//        selfCA_OkHttpClient = builder.build();
//
//    }

    private static Request.Builder getRequestBuilderWithHeader(String url){
        Request.Builder reqBuilder = new Request.Builder();
        reqBuilder.url(url);

        String token = AyUtils.getDecryptedToken();
        if (token != null){
            reqBuilder.addHeader(HEADRER, "JWT: " + token);
        }
        return reqBuilder;
    }

    /**
     * gets the jwt from the server.
     * If all goes well, fetches the userInfo and sets a new user for the app
     * if something goes wrong, calls callback.onError with a relevant message, or with POOR_CONNECTIVITY
     */
    public static void getUserServerToken(final String firebaseToken, final Callback callback, final String fcbkToken){
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestBody requestBody = new FormBody.Builder()
                        .add("idToken", firebaseToken)
                        .build();
                Request request = new Request.Builder()
                        .url(getUrl(AUTHENTICATE))
                        .post(requestBody)
                        .build();
                Response response = null;
                String errorMessage = AyUtils.getStringResource(R.string.something_wrong_adinlay_login); // if null, we will not call for callback.onError()
                try {
                    ArrayList<String> errors = new ArrayList<>();
                    OkHttpClient client = getOkHttpClient(errors);
                    if (client == null){
                        if (callback != null){
                            errorMessage = errors.size() > 0 ? errors.get(0) : ERROR_PREFIX;
                            callback.onError(errorMessage);
                        }
                        return;
                    }
                    response = client.newCall(request).execute();
                    // afterward, all other server requests must have this JWT token, as header with key "Authorization"
                    // value should be "JWT: ey......"

                    if (response != null){

                        // read the response:
                        String responseStr = null;
                        JSONObject jsonObject = null;
                        try {
                            if (response.body() != null) {
                                responseStr = response.body().string();
                            }
                            if (responseStr != null){
                                try {
                                    jsonObject = new JSONObject(responseStr);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (IOException | NullPointerException | OutOfMemoryError e) {
                            e.printStackTrace();
                        }

                        if (response.code() == 200 && jsonObject != null){
                            // got ok response
                            String token = jsonObject.optString("JWT", null);
                            if (token != null){
                                // save the token and get the user details:
//                              Timber.i("got token to save: %s", token);
                                AyUtils.encryptAndSaveToken(token);
                                getUserInfo(callback, true, fcbkToken);
                                return;
                            } else {
                                // couldn't parse token from response, see if we have a message
                                if (jsonObject.has("message")){
                                    errorMessage = jsonObject.optString("message", errorMessage);
                                }
                            }
                        } else {
                            // response.code is not ok, or couldn't get the body
                            if (jsonObject != null && jsonObject.has("message")){
                                errorMessage = jsonObject.optString("message", errorMessage);
                            } else if (response.message() != null){
                                errorMessage = response.message();
                            }
                        }
                    } else {
                        // response is null, will use the default message
                        Timber.w("getUserServerToken got null response");
                    }
                } catch (IOException e) {
                    // server exception:
                    if (e instanceof SocketTimeoutException){
                        errorMessage = POOR_CONNECTIVITY_GET_TOKEN;
                    } else if (e.getMessage() != null && !e.getMessage().isEmpty()){
                        errorMessage = e.getMessage();
                    }
                    Timber.w(e, "getUserServerToken got exception from calling server");
                    e.printStackTrace();
                } finally {
                    if (response != null){
                        response.close();
                    }
                }
                if (callback != null){
                    callback.onError(errorMessage);
                }
            }
        }).start();
    }

    /**
     * gets the user info from the server, and set this user as the app's user
     *
     * NOTE: since this method is also called from a sign-in flow, error messages sent via the
     * error callback will have a prefix of USER_INFO_TRY_FROM_SCRATCH or POOR_CONNECTIVITY_USER_INFO
     *  @param callback to be called after the new app user has been set, or to handle any errors
     * @param sendFcmToken
     * @param fcbkToken if not null (sent when login is via fcbk), we send to server
     */
    public static void getUserInfo(final Callback callback, final boolean sendFcmToken, final String fcbkToken){
        new Thread(new Runnable() {
            @Override
            public void run() {

                // try to get data with this token:
                Request request = getRequestBuilderWithHeader(getUrl(USER_INFO))
                        .build();

                Timber.d("header: %s", request.headers().toString());
                Response response = null;
                String badResponse = USER_INFO_TRY_FROM_SCRATCH;
                try {
                    ArrayList<String> errors = new ArrayList<>();
                    OkHttpClient client = getOkHttpClient(errors);
                    if (client == null){
                        if (callback != null){
                            badResponse = errors.size() > 0 ? errors.get(0) : ERROR_PREFIX;
                            callback.onError(badResponse);
                        }
                        return;
                    }
                    response = client.newCall(request).execute();
                    Timber.i("getUserInfo response from server: %s", response);
                    JSONObject jsonObject = null;
                    if (response!= null){
                        String responseStr = null;
                        if (response.body() != null) {
                            try {
                                responseStr = response.body().string();
                                jsonObject = new JSONObject(responseStr);
                            } catch (IOException | OutOfMemoryError e) {
                                Timber.w(e, "E_DBG getUserInfo can't read response" );
                                e.printStackTrace();
                                badResponse += e.getMessage();
                            } catch (JSONException e) {
                                Timber.w(e, "E_DBG getUserInfo response isn't a jsonObject");
                                e.printStackTrace();
                                badResponse += e.getMessage();
                            }
                        }
                        Timber.d("getUserInfo response body: %s", responseStr); // {"id":"5ba088186a6f9c5fa9209a33","displayName":"",...}
                        if (response.code() == 200 && jsonObject != null){
                            // got ok code
                            JSONObject userJson = jsonObject.optJSONObject("userinfo");
                            JSONObject settingsObject = jsonObject.optJSONObject("settings");
                            // save the user:
                            if (userJson == null){
                                userJson = jsonObject;
                            }
                            User user  = new User(userJson);
                            if (user.getId() != null || !jsonObject.optString("id").isEmpty()) {
                                AdinlayApp.setUser(user);
                                Timber.d("got user: %s", userJson);
                                if (callback != null){
                                    callback.onResponseOk(responseStr);
                                }

                                // save the settings:
                                AdinlayApp.setProfileSettings(settingsObject);
                                AyUtils.initMinAgeList(jsonObject.optJSONObject("validation"));

                                if (sendFcmToken || AyUtils.getStringPref(AyUtils.PREF_REPLACED_FCM_TOKEN) != null){
                                    sendFcmTokenToServer();
                                }
                                if (fcbkToken != null){
                                    connectToFcbk(fcbkToken, null);
                                }

                                // all is well, we are done
                                return;
                            } else if (jsonObject.has("message")){
                                badResponse = USER_INFO_TRY_FROM_SCRATCH + jsonObject.optString("message", "");
                            }
                        } else {
                            // bad response or code != 200, we need to clear JWT and sign-in again
                            Timber.i("E_DBG getUserInfo bad status code: %s", responseStr); // {"message":"Unauthorized user!"} // Response{protocol=http/1.1, code=401, message=Unauthorized, url=http://ec2-52-29-44-70.eu-central-1.compute.amazonaws.com:3000/users/me}
                            if (USER_INFO_TRY_FROM_SCRATCH.equals(badResponse)) { // haven't added info to it yet
                                if (jsonObject != null && jsonObject.has("message")){
                                    badResponse += jsonObject.optString("message", "");
                                } else if (response.message() != null){
                                    badResponse += response.message();
                                } else {
                                    badResponse += "Got status code " + response.code();
                                }
                            }
                        }
                    } else {
                        // response is null! we need to clear JWT and sign-in again
                        Timber.w("E_DBG getUserInfo get null response");
                        if (USER_INFO_TRY_FROM_SCRATCH.equals(badResponse)){
                            badResponse += "Error getting response from server";
                        }
                    }
                } catch (IOException e) {
                    // problem getting a response from server, eg SocketTimeoutException
                    Timber.w(e, "E_DBG getUserInfo IOException: ");
                    if (e instanceof SocketTimeoutException) {
                        badResponse = POOR_CONNECTIVITY_USER_INFO;
                    } else {
                        badResponse += e.getMessage();
                    }
                    e.printStackTrace();
                } finally {
                    if (response != null && response.body() != null){
                        try {
                            response.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                // todo ask tal: make sure ok to logout whenever the response isn't as expected, unless we were at the catch section
                if (callback != null){
                    callback.onError(badResponse);
                }
            }
        }).start();
    }

    /**
     * updates the user info from the server, and only sets the user's json
     *
     * NOTE: this method should be called when we have a user
     *  @param callback to be called after the new app user has been set, or to handle any errors, called from background thread
     */
    public static void getUpdatedUserInfo(final Callback callback){

        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                return getRequestBuilderWithHeader(getUrl(USER_INFO_NO_SETTINGS))
                        .build();
            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return getRegularOkHttpClient();
            }

            @Override
            public boolean extractJson() {
                return true;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {
                User user = AdinlayApp.getUser();
                if (responseJson != null && user != null) {
                    JSONObject userJson = responseJson.optJSONObject("userinfo");
                    if (userJson == null){
                        userJson = responseJson;
                    }
                    if (userJson.has("id") || userJson.has("_id")){
                        user.setNewUserJson(userJson);
                        if (callback != null){
                            callback.onResponseOk("");
                        }
                    }
                }
            }

            @NonNull
            @Override
            public String requestName() {
                return "getUpdatedUserInfo";
            }

            @Override
            public boolean makeProcessBackground() {
                return true;
            }
        }, callback, 0);
    }

//    public static void getUserInfoWithSettings(final Callback callback){
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//
//                // try to get data with this token:
//                Request request = getRequestBuilderWithHeader(getUrl(USER_INFO) + "?settings=true")
//                        .build();
//
////                Timber.d("header: %s", request.headers().toString());
//                Response response = null;
//                String badResponse = USER_INFO_TRY_FROM_SCRATCH;
//                try {
//                    ArrayList<String> errors = new ArrayList<>();
//                    OkHttpClient client = getOkHttpClient(errors);
//                    if (client == null){
//                        if (callback != null){
//                            badResponse = errors.size() > 0 ? errors.get(0) : ERROR_PREFIX;
//                            callback.onError(badResponse);
//                        }
//                        return;
//                    }
//                    response = client.newCall(request).execute();
//                    Timber.i("getUserInfoWithSettings response from server: %s", response);
//                    JSONObject jsonObject = null;
//                    if (response!= null){
//                        String responseStr = null;
//                        if (response.body() != null) {
//                            try {
//                                responseStr = response.body().string();
//                                jsonObject = new JSONObject(responseStr);
//                            } catch (IOException | OutOfMemoryError e) {
//                                Timber.w(e, "E_DBG getUserInfoWithSettings can't read response" );
//                                e.printStackTrace();
//                                badResponse += e.getMessage();
//                            } catch (JSONException e) {
//                                Timber.w(e, "E_DBG getUserInfoWithSettings response isn't a jsonObject" );
//                                e.printStackTrace();
//                                badResponse += e.getMessage();
//                            }
//                        }
//                        Timber.i("getUserInfoWithSettings response body: %s", responseStr); // {"id":"5ba088186a6f9c5fa9209a33","displayName":"",...}
////                        if (response.code() == 200 && jsonObject != null){
////                            // got ok code
////                            User user  = new User(jsonObject);
////                            if (user.getId() != null) {
////                                AdinlayApp.setUser(user);
////                                Timber.d("got user: %s", user.toString());
////                                if (callback != null){
////                                    callback.onResponseOk(responseStr);
////                                }
////                                // all is well, we are done
////                                return;
////                            } else if (jsonObject.has("message")){
////                                badResponse = USER_INFO_TRY_FROM_SCRATCH + jsonObject.optString("message", "");
////                            }
////                        } else {
////                            // bad response or code != 200, we need to clear JWT and sign-in again
////                            Timber.i("E_DBG getUserInfo bad status code: %s", responseStr); // {"message":"Unauthorized user!"} // Response{protocol=http/1.1, code=401, message=Unauthorized, url=http://ec2-52-29-44-70.eu-central-1.compute.amazonaws.com:3000/users/me}
////                            if (USER_INFO_TRY_FROM_SCRATCH.equals(badResponse)) { // haven't added info to it yet
////                                if (jsonObject != null && jsonObject.has("message")){
////                                    badResponse += jsonObject.optString("message", "");
////                                } else if (response.message() != null){
////                                    badResponse += response.message();
////                                } else {
////                                    badResponse += "Got status code " + response.code();
////                                }
////                            }
////                        }
//                    } else {
//                        // response is null! we need to clear JWT and sign-in again
//                        Timber.w(e, "E_DBG getUserInfoWithSettings get null response");
//                        if (USER_INFO_TRY_FROM_SCRATCH.equals(badResponse)){
//                            badResponse += "Error getting response from server";
//                        }
//                    }
//                } catch (IOException e) {
//                    // problem getting a response from server, eg SocketTimeoutException
//                    Timber.w(e, "E_DBG getUserInfoWithSettings IOException: ");
//                    if (e instanceof SocketTimeoutException) {
//                        badResponse = POOR_CONNECTIVITY_USER_INFO;
//                    } else {
//                        badResponse += e.getMessage();
//                    }
//                    e.printStackTrace();
//                } finally {
//                    if (response != null && response.body() != null){
//                        try {
//                            response.close();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
////                if (callback != null){
////                    callback.onError(badResponse);
////                }
//            }
//        }).start();
//    }


    public static void updateUserInfo(final Callback callback, final boolean isAccount){
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (AdinlayApp.getUser() != null && AdinlayApp.getUser().getPendingJson().length() > 0){

                    JSONObject pendingChanges = AdinlayApp.getUser().getPendingJson();

                    MediaType jsonType = MediaType.parse("application/json; charset=utf-8");
                    RequestBody requestBody = RequestBody.create(jsonType, pendingChanges.toString());

                    String url = !isAccount ? getUrl(USER_INFO) : getUrl(USER_ACCOUNT_INFO);
                    Request request = getRequestBuilderWithHeader(url)
                            .post(requestBody)
                            .build();

                    Response response = null;
                    JSONObject responseJson = null;
                    String errorMessage = "";
                    boolean done = false;
                    try {
                        ArrayList<String> errors = new ArrayList<>();
                        OkHttpClient client  = getOkHttpClient(errors);
                        if (client == null){
                            if (callback != null){
                                errorMessage = errors.size() > 0 ? errors.get(0) : ERROR_PREFIX;
                                callback.onError(errorMessage);
                            }
                            return;
                        }
                        response = client.newCall(request).execute();
                        Timber.i("updateUserInfo response from server: %s", response);
                        if (response!= null && response.body() != null){
                            String responseStr = null;
                            try {
                                responseStr = response.body().string();
                                responseJson = new JSONObject(responseStr);
                            } catch (IOException | OutOfMemoryError | JSONException e) {
                                e.printStackTrace();
                                errorMessage = e.getMessage();
                            }
                            Timber.i("updateUserInfo response body: %s", responseStr);
                            if (response.code() == 200){
                                // got ok code
                                if (responseJson != null){
                                    User user = AdinlayApp.getUser();
                                    if (user != null){
                                        // response should be the userJson
                                        if (!responseJson.optString("_id").isEmpty() ||
                                                !responseJson.optString("id").isEmpty()){
                                            user.setNewUserJson(responseJson);
                                        } else {
                                            // backward compatibility
                                            AdinlayApp.getUser().updateUserJsonField(User.STRENGTH_KEY,
                                                    responseJson.optInt(User.STRENGTH_KEY));
                                            AdinlayApp.getUser().updatePendingChanges();
                                        }
                                    }
                                    if (callback != null){
                                        callback.onResponseOk(responseStr);
                                    }
                                    done = true;
                                }
                            }
                        }
                        if (!done && errorMessage.isEmpty()){
                            if (responseJson != null && responseJson.has("message")){
                                errorMessage = responseJson.optString("message", "");
                            } else if (response != null && response.code() != 200){
                                errorMessage = response.message();
                                if (errorMessage == null || errorMessage.isEmpty()) {
                                    errorMessage = "Got status code " + response.code();
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        errorMessage = e.getMessage();
                    } finally {
                        if (response != null && response.body() != null){
                            try {
                                response.close();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        Timber.d("getUserInfo got to finally");
                        if (!done && callback != null){
                            callback.onError(errorMessage);
                        }
                    }
                } else {
                    // nothing to update - TBD
                    if (callback != null){
                        callback.onResponseOk("");
                    }
                }
            }
        }).start();
    }

    public static void updateSocialMediaProfile(final Callback callback){
        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                JSONObject pendingChanges = AdinlayApp.getUser().getSocMedPendingJson();

                if (pendingChanges == null || pendingChanges.length() == 0){
                    return null;
                }

                MediaType jsonType = MediaType.parse("application/json; charset=utf-8");
                RequestBody requestBody = RequestBody.create(jsonType, pendingChanges.toString());

                return getRequestBuilderWithHeader(getUrl(SOCIAL_MEDIA_PROFILE_UPDATE))
                        .post(requestBody)
                        .build();
            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return getRegularOkHttpClient();
            }

            @Override
            public boolean extractJson() {
                return true;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {
                User user = AdinlayApp.getUser();
                if (user != null && responseJson != null && (!responseJson.optString("_id").isEmpty() ||
                        !responseJson.optString("id").isEmpty())){
                    user.setNewUserJson(responseJson);
                }
                if (callback != null){
                    callback.onResponseOk("");
                }
            }

            @NonNull
            @Override
            public String requestName() {
                return "updateSocialMediaProfile";
            }

            @Override
            public boolean makeProcessBackground() {
                return false;
            }
        }, callback, 0);
    }

    public static void deletePartialUser(final Callback callback){
        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                return getRequestBuilderWithHeader(getUrl(REMOVE_USER))
                        .delete()
                        .build();
            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return getRegularOkHttpClient();
            }

            @Override
            public boolean extractJson() {
                return false;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {
                if (callback != null){
                    callback.onResponseOk("");
                }
            }

            @NonNull
            @Override
            public String requestName() {
                return "deletePartialUser";
            }

            @Override
            public boolean makeProcessBackground() {
                return false;
            }
        }, callback, 0);
    }

    public static void checkAppVersion(final int versionCode, final Callback callback){

        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = getUrl(CHECK_APP_VERSION);
                HttpUrl httpUrl = HttpUrl.parse(url);
                HttpUrl.Builder httpUrlBuider = httpUrl == null ? null : httpUrl.newBuilder();
                if (httpUrlBuider == null){
                    if (callback != null){
                        callback.onResponseOk("");
                    }
                    return;
                }
                httpUrlBuider.addQueryParameter("platform", "android");
                httpUrlBuider.addQueryParameter("build", "" + versionCode);
//                Timber.i("version_ httpUrlBuider: %s", httpUrlBuider.toString());
                Request request = new Request.Builder().url(httpUrlBuider.build()).build();

                Response response = null;
                String errorMessage = null;
                boolean isDone = false;
                try {
                    ArrayList<String> errors = new ArrayList<>();
                    OkHttpClient client = getOkHttpClient(errors);
                    if (client == null){ // historical: when used self-certificate
                        if (callback != null){
                            errorMessage = errors.size() > 0 ? errors.get(0) : ERROR_PREFIX;
                            callback.onError(errorMessage);
                        }
                        return;
                    }
                    response = client.newCall(request).execute();
                    String responseStr = null;
                    try {
                        if (response.body() != null) {
                            responseStr = response.body().string();
                        }
                    } catch (IOException | OutOfMemoryError e) {
                        e.printStackTrace();
                        errorMessage = e.getMessage();
                    }
                    if (response.code() == 200){
                        if (callback != null){
                            callback.onResponseOk(responseStr);
                        }
                        isDone = true;
                    } else if (responseStr != null){
                        try {
                            JSONObject jsonObject = new JSONObject(responseStr);
                            errorMessage = jsonObject.optString("message");
                            if ("unsupportedVersionError".equals(jsonObject.optString("code"))){
                                errorMessage = UNSUPPORTED_APP_VERSION + errorMessage;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (!isDone && (errorMessage == null || errorMessage.isEmpty())){
                        errorMessage = response.message() != null ? response.message() : "Got status code: " + response.code();
                    }

                    Timber.d("checkAppVersion response body: %s", responseStr);
                } catch (IOException e) {
                    e.printStackTrace();
                    errorMessage = e.getMessage();
                } finally {
                    if (!isDone && callback != null){
                        Timber.d("checkAppVersion errorMessage: %s", errorMessage);
                        callback.onError(errorMessage);
                    }
                    if (response != null){
                        response.close();
                    }
                }
            }
        }).start();
    }


    public static void connectToInstagram(final String userName, final String password, final Callback callback){

        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                String userLocation = "";
                User user = AdinlayApp.getUser();
                if (user != null){
                    JSONArray selectedArray = user.getProfileTopicSelection(ProfileTopic.LOCATION, true, true);
                    userLocation = selectedArray.length() > 0 ? selectedArray.optString(0) : "";
                }

                RequestBody requestBody = new FormBody.Builder()
                        .add("username", userName)
                        .add("password", password)
                        .add("location", userLocation)
                        .build();

                Request request = getRequestBuilderWithHeader(getUrl(INSTAGRAM_LOGIN))
                        .post(requestBody)
                        .build();
                return request;
            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return get30SecTimeoutClient();
            }

            @Override
            public boolean extractJson() {
                return true;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {
                // {"message":"login succeeded!","followers":6}
                if (AdinlayApp.getUser() != null) {
                    AdinlayApp.getUser().setConnectedToInstagram(true);
                    int followers = responseJson == null ? 0 :
                            responseJson.optInt("followers", 0);
                    AdinlayApp.getUser().setInstaFollowers(followers);
                    String userName = responseJson == null ? "" :
                            responseJson.optString("username", "");
                    AdinlayApp.getUser().setIgUsername(userName);
                    String userId = responseJson == null ? "" :
                            responseJson.optString("instagramAccountId", "");
                    AdinlayApp.getUser().setIgUserId(userId);
                }
                if (callback != null){
                    callback.onResponseOk("");
                }
                getUpdatedUserInfo(null);
            }

            @NonNull
            @Override
            public String requestName() {
                return "loginInstagram";
            }

            @Override
            public boolean makeProcessBackground() {
                return false;
            }
        }, callback, 0);

    }

    public static void igConnectViaClient(final String userName, final String password, final Callback callback){

        // stage one: get the config for the login request from the server:
        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                RequestBody serverRequestBody = new FormBody.Builder()
                        .add("username", userName)
                        .add("password", password)
                        .build();
                return getRequestBuilderWithHeader(getUrl(IG_FETCH_LOGIN_PARAMS)).post(serverRequestBody).build();
            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return getRegularOkHttpClient();
            }

            @Override
            public boolean extractJson() {
                return true;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {

                String errorMessage = "";
                boolean isDone = false;
                if (responseJson != null){

                    // extract param from response:
                    String loginUrl = responseJson.optString("uri", null);
                    ArrayList<String[]> formParts = new ArrayList<>();
                    ArrayList<String[]> headersList = new ArrayList<>();
                    JSONObject formObject = responseJson.optJSONObject("form");
                    if (formObject != null){
                        Iterator<String> iterator = formObject.keys();
                        while (iterator.hasNext()){
                            String key = iterator.next();
                            String val = formObject.optString(key, null);
                            if (val != null){
                                String[] pair = new String[]{key, val};
                                formParts.add(pair);
                            }
                        }
                    }
                    JSONObject headersObject = responseJson.optJSONObject("headers");
                    if (headersObject != null){
                        Iterator<String> iterator = headersObject.keys();
                        while (iterator.hasNext()){
                            String key = iterator.next();
                            String val = headersObject.optString(key, null);
                            if (val != null){
                                String[] pair = new String[]{key, val};
                                headersList.add(pair);
                            }
                        }
                    }

                    if (loginUrl != null && !loginUrl.isEmpty()) {

                        // stage two: send the login request to ig:
                        FormBody.Builder formBuilder = new FormBody.Builder();
                        for (int i = 0; i < formParts.size(); i++) {
                            String[] bodyPart = formParts.get(i);
                            if (bodyPart != null && bodyPart.length == 2 && bodyPart[0] != null && bodyPart[1] != null) {
                                formBuilder.add(bodyPart[0], bodyPart[1]);
//                                Timber.v("testForIg LOGIN added param param: %s, val: %s", bodyPart[0], bodyPart[1]);
                            }
                        }

                        RequestBody loginRequestBody = formBuilder.build();
                        Request.Builder loginRequestBuilder = new Request.Builder()
                                .url(loginUrl)
                                .post(loginRequestBody);
                        for (int i = 0; i < headersList.size(); i++) {
                            String[] currHeader = headersList.get(i);
                            if (currHeader != null && currHeader.length == 2 && currHeader[0] != null && currHeader[1] != null) {
                                loginRequestBuilder.addHeader(currHeader[0], currHeader[1]);
                            }
                        }
                        Request loginRequest = loginRequestBuilder.build();
                        Response loginResponse = null;
                        try {
                            OkHttpClient igClient =  new OkHttpClient.Builder()
                                    .connectTimeout(30, TimeUnit.SECONDS)
                                    .readTimeout(0, TimeUnit.MILLISECONDS)
                                    .writeTimeout(0, TimeUnit.MILLISECONDS)
                                    .build();
                            loginResponse = igClient.newCall(loginRequest).execute();
                            String loginResponseStr = loginResponse.body() != null ? loginResponse.body().string() : "{}";
//                            Timber.i("testForIg LOGIN response : %s", loginResponse);
//                            Timber.i("testForIg LOGIN response body: %s", loginResponseStr);
//                            Timber.i("testForIg LOGIN response header: %s", loginResponse.headers());
                            List<String> responseHeadersList = loginResponse.headers().values("Set-Cookie");
                            JSONArray headersArr = new JSONArray();
                            for (String h : responseHeadersList){
//                            Timber.i("testForIg LOGIN logging header: %s", h); // e.g.: ds_user_id=7987967003; Domain=.instagram.com; expires=Wed, 18-Sep-2019 05:00:23 GMT; Max-Age=7776000; Path=/; Secure
                                headersArr.put(h);
                            }
//                            Timber.i("testForIg LOGIN headersArray: %s", headersArr);
                            JSONObject igResults = new JSONObject();
                            try {
                                igResults.put("code", loginResponse.code());
                                String statusMessage = loginResponse.message() != null ? loginResponse.message() : "";
                                igResults.put("message", statusMessage);
                                igResults.put("body", loginResponseStr);
                                igResults.put("headers", headersArr.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            // stage three: send the login result to the server:
                            sendServerIgLoginResult(igResults, callback);
                            isDone = true;
                        } catch (IOException | OutOfMemoryError e) {
                            e.printStackTrace();
                            errorMessage = e.getMessage() != null ? e.getMessage() : "";
                        } finally {
                            if (loginResponse != null && loginRequest.body() != null){
                                try {
                                    loginResponse.close();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
//                    else {
//                        errorMessage = "initial response from server is wrong"; // keep errorMessage empty
//                    }
                }
//                else {
//                    errorMessage = "json is null!!!"; // keep errorMessage empty
//                }

                if (!isDone && callback != null){
                    callback.onError(errorMessage);
                }

            }

            @NonNull
            @Override
            public String requestName() {
                return "getIgLoginParams";
            }

            @Override
            public boolean makeProcessBackground() {
                return false;
            }
        }, callback, 0);

    }

    private static void sendServerIgLoginResult(final JSONObject igResults, final Callback callback){
        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                RequestBody backToServerBody = new FormBody.Builder()
                        .add("igResults", igResults.toString())
                        .build();
                return getRequestBuilderWithHeader(getUrl(IG_SEND_LOGIN_RESULT))
                        .post(backToServerBody)
                        .build();
            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return get30SecTimeoutClient();
            }

            @Override
            public boolean extractJson() {
                return true;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {
                // {"message":"login succeeded!","followers":6}
                if (AdinlayApp.getUser() != null) {
                    AdinlayApp.getUser().setConnectedToInstagram(true);
                    int followers = responseJson == null ? 0 :
                            responseJson.optInt("followers", 0);
                    AdinlayApp.getUser().setInstaFollowers(followers);
                    String userName = responseJson == null ? "" :
                            responseJson.optString("username", "");
                    AdinlayApp.getUser().setIgUsername(userName);
                    String userId = responseJson == null ? "" :
                            responseJson.optString("instagramAccountId", "");
                    AdinlayApp.getUser().setIgUserId(userId);
                }
                if (callback != null){
                    callback.onResponseOk("");
                }
                getUpdatedUserInfo(null);
            }

            @NonNull
            @Override
            public String requestName() {
                return "sendServerIgLoginResult";
            }

            @Override
            public boolean makeProcessBackground() {
                return false;
            }
        }, callback, 0);
    }


    public static void logoutInstagram(final Callback callback){

        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                RequestBody requestBody = new FormBody.Builder()
                        .build();

                Request request = getRequestBuilderWithHeader(getUrl(INSTAGRAM_LOGOUT))
                        .post(requestBody)
                        .build();
                return request;
            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return get30SecTimeoutClient();
            }

            @Override
            public boolean extractJson() {
                return true;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {
                // {"message":"logout succeeded!"}
                if (AdinlayApp.getUser() != null) {
                    AdinlayApp.getUser().setConnectedToInstagram(false);
                    AdinlayApp.getUser().setInstaFollowers(0);
                    AdinlayApp.getUser().setIgUsername("");
                    AdinlayApp.getUser().setIgUserId("");
                }
                if (callback != null){
                    callback.onResponseOk("");
                }
            }

            @NonNull
            @Override
            public String requestName() {
                return "logoutInstagram";
            }

            @Override
            public boolean makeProcessBackground() {
                return false;
            }
        }, callback, 0);

    }

    /**
     * checks if appUser is connected to social media platforms, and updates appUser info
     * @param callback if layout needs to be updated
     */
    public static void checkSocMedConnections(final Callback callback){

        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                return getRequestBuilderWithHeader(getUrl(CONNECTIVITY_REQUEST))
                        .build();
            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return getRegularOkHttpClient();
            }

            @Override
            public boolean extractJson() {
                return true;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {
                AdinlayApp.updateUserSocMediaConnections(responseJson);
                if (callback != null){
                    callback.onResponseOk("");
                }
            }

            @NonNull
            @Override
            public String requestName() {
                return "checkSocMedConnections";
            }

            @Override
            public boolean makeProcessBackground() {
                return false;
            }
        }, callback, 0);

    }

    public static void connectToFcbk(final String fcbkToken, final Callback callback){
        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                RequestBody requestBody = new FormBody.Builder()
                        .add("userToken", fcbkToken)
                        .build();

                Request request = getRequestBuilderWithHeader(getUrl(FCBK_CONNECT))
                        .post(requestBody)
                        .build();
                return request;
            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return get30SecTimeoutClient();
            }

            @Override
            public boolean extractJson() {
                return true;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {
                // {"result":"success","friends":0}
                if (AdinlayApp.getUser() != null) {
                    AdinlayApp.getUser().setConnectedToFacebook(true);
                    int friends = responseJson == null ? 0 :
                            responseJson.optInt("friends", 0);
                    AdinlayApp.getUser().setFacebookFriends(friends);
                    String userName = responseJson == null ? "" :
                            responseJson.optString("displayName");
                    AdinlayApp.getUser().setFbUsername(userName);
                    String fbUserId = responseJson == null ? "" :
                            responseJson.optString("facebookUserId");
                    AdinlayApp.getUser().setFbUserId(fbUserId);

                }
                if (callback != null){
                    callback.onResponseOk("");
                }
                getUpdatedUserInfo(null);
            }

            @NonNull
            @Override
            public String requestName() {
                return "connectToFcbk";
            }

            @Override
            public boolean makeProcessBackground() {
                return false;
            }
        }, callback, 0);
    }

    public static void disconnectFromFcbk(final Callback callback){
        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                RequestBody requestBody = new FormBody.Builder()
                        .build();

                Request request = getRequestBuilderWithHeader(getUrl(FCBK_DISCONNECT))
                        .post(requestBody)
                        .build();
                return request;
            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return get30SecTimeoutClient();
            }

            @Override
            public boolean extractJson() {
                return true;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {
                if (AdinlayApp.getUser() != null) {
                    AdinlayApp.getUser().setConnectedToFacebook(false);
                    AdinlayApp.getUser().setFacebookFriends(0);
                    AdinlayApp.getUser().setFbUsername("");
                    AdinlayApp.getUser().setFbUserId("");
                }
                if (callback != null){
                    callback.onResponseOk("");
                }
            }

            @NonNull
            @Override
            public String requestName() {
                return "disconnectFromFcbk";
            }

            @Override
            public boolean makeProcessBackground() {
                return false;
            }
        }, callback, 0);
    }

    private static void sendRequestToServer(@NonNull final InternalCallback internalCallback,
                                            final Callback callback, final int additionalAttempts){

        new Thread(new Runnable() {
            @Override
            public void run() {

                if (internalCallback.makeProcessBackground()){
                    Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                }

                Request request = internalCallback.getRequest();

                if (request == null){
                    if (callback != null){ // need to let ui know
                        callback.onError("");
                    }
                    return;
                }

                Response response = null;
                String errorMessage = "";
                boolean done = false;
                try {
                    OkHttpClient client  = internalCallback.getOkClient();
                    response = client.newCall(request).execute();
                    Timber.i("%s response from server: %s", internalCallback.requestName(), response);
                    if (response!= null){

                        String responseStr = null;
                        JSONObject responseJson = null;
                        try {
                            responseStr = response.body() != null ? response.body().string() : null;
                            if (responseStr != null && internalCallback.extractJson()) {
                                responseJson = new JSONObject(responseStr);
                            }
                        } catch (IOException | OutOfMemoryError | JSONException e) {
                            e.printStackTrace();
                            errorMessage = e instanceof  JSONException || e.getMessage() == null ? "" : e.getMessage();
                        }
                        Timber.i("%s response body: %s", internalCallback.requestName(), responseStr);
                        if (response.code() == 200){
                            // got ok code
                            internalCallback.onResponseOk(responseStr, responseJson);
                            done = true;
                        } else if (additionalAttempts == 0 && callback != null) {
                            // get the error message, if needed (and this is the last attempt)

                            // extract the responseJson, if we haven't yet:
                            if (!internalCallback.extractJson() && responseJson == null && responseStr != null) {
                                try {
                                    responseJson = new JSONObject(responseStr);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            if (responseJson != null) {
                                errorMessage = responseJson.optString("message", "");
                            }

                            if (errorMessage.isEmpty()){
                                if (response.message() != null && !response.message().isEmpty()){
                                    errorMessage = response.message();
                                } else if (response.code() != 200) {
                                    errorMessage = "Got status code " + response.code();
                                }
                            }
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                    errorMessage = e.getMessage() != null ? e.getMessage() : "";
                } finally {
//                    Timber.v("%s at finally", internalCallback.requestName());
                    if (response != null && response.body() != null){
                        try {
                            response.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (!done && additionalAttempts > 0){
                        Timber.i("attempt failed, have %s more tries ", additionalAttempts);
                        sendRequestToServer(internalCallback, callback, additionalAttempts - 1);
                    } else if (!done && callback != null){
                        callback.onError(errorMessage);
                    }
                }
            }
        }).start();
    }



    public static void queryServer(final int urlCode, final String urlParam, final Callback callback){

        if (FCBK_APPROVED_POSTS == urlCode){
            AyUtils.saveLongPref(AyUtils.PREF_LAST_UPDATE_APPROVED_long, System.currentTimeMillis());
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = getUrl(urlCode, urlParam);
                Request request = getRequestBuilderWithHeader(url)
                        .build();

                Response response = null;
                String errorMessage = null;
                boolean isDone = false;
                try {
                    ArrayList<String> errors = new ArrayList<>();
                    OkHttpClient client = getOkHttpClient(errors);
                    if (client == null){
                        if (callback != null){
                            errorMessage = errors.size() > 0 ? errors.get(0) : ERROR_PREFIX;
                            callback.onError(errorMessage);
                        }
                        return;
                    }
                    response = client.newCall(request).execute();
                    Timber.d("queryServer response is: %s", response);
                    String responseStr = null;
                    try {
                        if (response.body() != null) {
                            responseStr = response.body().string();
                        }
                    } catch (IOException | OutOfMemoryError e) {
                        e.printStackTrace();
                        errorMessage = e.getMessage();
                    }
                    if (responseStr != null && response.code() == 200){
                        if (callback != null){
                            callback.onResponseOk(responseStr);
                        }
                        isDone = true;
                    } else if (responseStr != null && response.code() != 200){
                        try {
                            JSONObject jsonObject = new JSONObject(responseStr);
                            errorMessage = jsonObject.optString("message");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (!isDone && (errorMessage == null || errorMessage.isEmpty())){
                        errorMessage = response.message() != null ? response.message() : "Got status code: " + response.code();
                    }

                    Timber.d("queryServer response body: %s", responseStr);
                } catch (IOException e) {
                    e.printStackTrace();
                    errorMessage = e.getMessage();
                } finally {
                    if (!isDone && callback != null){
                        Timber.d("queryServer errorMessage: %s", errorMessage);
                        callback.onError(errorMessage);
                    }
                    if (response != null){
                        response.close();
                    }
                }
            }
        }).start();

    }

    public static void uploadNewPost(final boolean isFcbk, final boolean isFcbkFree, final Callback callback){
        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = getUrl(isFcbkFree ? SEND_FCBK_FREE_POST : !isFcbk ? SEND_MY_POST : SEND_MY_POST_FCBK);

                Ad selectedAd = AdinlayApp.getSelectedAd();
                DraftPost draftPost = AdinlayApp.getDraftPost();
                Bitmap bmp = null;
                try {
                    bmp = draftPost == null ? null :
                            draftPost.getDraftPostBitmap().copy(Bitmap.Config.ARGB_8888, true);
                } catch (Exception e) {
                    e.printStackTrace();
                    bmp = draftPost.getDraftPostBitmap(); // try to use the bitmap without copying it
                    Timber.w(e, "BMP_DBG uploadPost not enough memory to copy the draft bitmap");
                }
                if (bmp == null || selectedAd == null){
                    if (callback != null){
                        callback.onError("");
                    }
                    return;
                }
                Timber.d("fb_dbg bmp size: %s X %s", bmp.getWidth(), bmp.getHeight());
                byte[] data = new byte[0];
                try {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.JPEG, 75, bos);
                    data = bos.toByteArray();
                } catch (OutOfMemoryError e) {
                    e.printStackTrace();
                    Timber.w(e, "BMP_DBG uploadPost not enough memory for creating the byte array");
                }

                if (data.length == 0){
                    if (callback != null){
                        callback.onError("");
                    }
                    return;
                }

                MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("file", "1.jpg", RequestBody.create(MediaType.parse("image/jpeg"), data))
                        // add params:
                        .addFormDataPart("text", AdinlayApp.getDraftPost().getUserText())
                        .addFormDataPart("adId", selectedAd.getId())
                        .addFormDataPart("shareDate", ZonedDateTime.now().toOffsetDateTime().toString());
//                        .build();

                if (draftPost.getScheduledPostTimeIso() != null){
                    builder.addFormDataPart("scheduleDate", draftPost.getScheduledPostTimeIso());
//                    JSONObject jsonObject = new JSONObject();
//                    try {
//                        jsonObject.put("scheduleDate", draftPost.getScheduledPostTimeMilli());
//                        Timber.i("ZDT_DBG adding a param to schedule post: %s", draftPost.getScheduledPostTimeMilli());
//                        MediaType jsonType = MediaType.parse("application/json; charset=utf-8");
//                        RequestBody scheduleRequestBody = RequestBody.create(jsonType, jsonObject.toString());
//                        builder.addPart(scheduleRequestBody);
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
                }
                if (!isFcbk){
                    builder.addFormDataPart("tagX", "0.8");
                    builder.addFormDataPart("tagY", "0.8");
                    builder.addFormDataPart("followAdvertiser", "" + draftPost.isFollowBrand());
                }
                if (isFcbk){
                    try {
                        JSONObject jsonObject = new JSONObject();//"imageSize":{"width":1024,"height":1024}
                        jsonObject.put("width", bmp.getWidth());
                        jsonObject.put("height", bmp.getHeight());
//                        Timber.i("dbg_dbg object: %s", jsonObject.toString());
                        builder.addFormDataPart("imageSize", jsonObject.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                RequestBody requestBody = builder.build();
                // add params to the url, to check the version code:
                url = url +"?platform=android&build=" + BuildConfig.VERSION_CODE;
                Request request = getRequestBuilderWithHeader(url)
                        .post(requestBody)
                        .build();

                Response response = null;
                String errorMessage = null;
                boolean isDone = false;
                try {
                    ArrayList<String> errors = new ArrayList<>();
                    OkHttpClient client = getOkHttpClient(errors);
                    if (client == null){
                        if (callback != null){
                            errorMessage = errors.size() > 0 ? errors.get(0) : ERROR_PREFIX;
                            callback.onError(errorMessage);
                        }
                        isDone = true; // we already called onError, don't call it again from finally!
                        return;
                    }
                    OkHttpClient noTimeoutClient = client.newBuilder()
                            .connectTimeout(0, TimeUnit.MILLISECONDS)
                            .readTimeout(0, TimeUnit.MILLISECONDS)
                            .writeTimeout(0, TimeUnit.MILLISECONDS)
                            .build();
                    response = noTimeoutClient.newCall(request).execute();
                    Timber.i("BMP_DBG response from server: %s", response); // Response{protocol=http/1.1, code=200, message=OK, url=http://ec2-18-194-104-43.eu-central-1.compute.amazonaws.com:3000/social/instagram/posts/me}
                    String responseStr = null;
                    try {
                        if (response.body() != null) {
                            responseStr = response.body().string();
                        }
                    } catch (IOException | OutOfMemoryError e) {
                        e.printStackTrace();
                        errorMessage = e.getMessage();
                    }
                    if (responseStr != null && response.code() == 200){
                        if (callback != null){
                            callback.onResponseOk(responseStr);
                        }
                        isDone = true;
                    } else if (responseStr != null && response.code() != 200){
                        try {
                            JSONObject jsonObject = new JSONObject(responseStr);
                            errorMessage = jsonObject.optString("message");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (!isDone && (errorMessage == null || errorMessage.isEmpty())){
                        errorMessage = response.message() != null ? response.message() : "Got status code: " + response.code();
                    }

                    Timber.i("BMP_DBG response body: %s" , responseStr); // {"message":" (Post 1HjhDPDbt0S51) shared successfully"}

                } catch (IOException e) {
                    e.printStackTrace();
                    errorMessage = e.getMessage();
                } finally {
                    if (response != null){
                        response.close();
                    }
                    if (!isDone && callback != null){
                        callback.onError(errorMessage);
                    }
                }


            }
        }).start();
    }

    public static void reportFacebookPostResult(final String postId, final boolean isSuccess, final Callback callback){

        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                RequestBody requestBody = new FormBody.Builder()
                        .add("postId", postId)
                        .build();

                Request request = getRequestBuilderWithHeader(getUrl(isSuccess ? FCBK_POST_SUCCESS : FCBK_POST_REJECT))
                        .post(requestBody)
                        .build();
                return request;
            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return get30SecTimeoutClient();
            }

            @Override
            public boolean extractJson() {
                return false;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {
                if (callback != null){
                    callback.onResponseOk("");
                }
            }

            @NonNull
            @Override
            public String requestName() {
                return "reportFacebookPostResult_" + isSuccess;
            }

            @Override
            public boolean makeProcessBackground() {
                return false;
            }
        }, callback, 1);

    }


    public static void sendRedeemRequest(final float amount, final String location, final String personalId, final Callback callback){

        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                User user = AdinlayApp.getUser();
                if (user == null || user.getId() == null){
                    if (callback != null){
                        callback.onError("Could not send request");
                    }
                    return null;
                }

                FormBody.Builder builder = new FormBody.Builder()
                        .add("userId", user.getId())
                        .add("amount", "" + amount);

                if (location != null){
                    builder.add("location", location);
                }

                if (personalId != null){
                    builder.add("personalId", personalId);
                }

                RequestBody requestBody = builder.build();

                return getRequestBuilderWithHeader(getUrl(REDEEM_REQUEST))
                        .post(requestBody)
                        .build();
            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return getNoTimeoutClient();
            }

            @Override
            public boolean extractJson() {
                return false;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {
                if (callback != null){
                    callback.onResponseOk(responseStr);
                }
            }

            @NonNull
            @Override
            public String requestName() {
                return "sendRedeemRequest";
            }

            @Override
            public boolean makeProcessBackground() {
                return false;
            }
        }, callback, 0);

    }

    public static void resendRedeemRequest(final String redeemEntryId, final Callback callback){
        if (redeemEntryId == null || redeemEntryId.isEmpty()){
            if (callback != null){
                callback.onError("Could not send request");
            }
            return;
        }

        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                User user = AdinlayApp.getUser();
                if (user == null || user.getId() == null){
                    if (callback != null){
                        callback.onError("Could not send request");
                    }
                    return null;
                }

                FormBody.Builder builder = new FormBody.Builder()
                        .add("userId", user.getId())
                        .add("redemptionId", "" + redeemEntryId);

                RequestBody requestBody = builder.build();

               return getRequestBuilderWithHeader(getUrl(RESEND_REDEMPTION))
                        .post(requestBody)
                        .build();
            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return getRegularOkHttpClient();
            }

            @Override
            public boolean extractJson() {
                return false;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {
                if (callback != null){
                    callback.onResponseOk(responseStr); // {"_id":"5bbdf296b858d40032d7002f","userId":"5b443f8d9c626d19283a9257","extrenalRedeemId":"RA181010-2377-10","amount":5.5,"uri":"https://sandbox.rewardlink.io/r/1/Aij-hCmJtAWDOuEFpASge_xtuvnAnBfEIYTrAmsZz1o","createdDate":"2018-10-10T12:37:42.642Z","__v":0}
                }
            }

            @NonNull
            @Override
            public String requestName() {
                return "resendRedeemRequest";
            }

            @Override
            public boolean makeProcessBackground() {
                return true;
            }
        }, callback, 0);

    }

    public static void recheckEmailVerified(final Callback callback){

        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                FormBody.Builder builder = new FormBody.Builder();

                RequestBody requestBody = builder.build();

                return getRequestBuilderWithHeader(getUrl(RECHECK_EMAIL_VERIFIED))
                        .post(requestBody)
                        .build();
            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return getRegularOkHttpClient();
            }

            @Override
            public boolean extractJson() {
                return true;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {
                if (responseJson != null && (!responseJson.optString("id").isEmpty() ||
                        !responseJson.optString("id").isEmpty())) {
                    User user = AdinlayApp.getUser();
                    if (user == null){ // shouldn't happen
                        AdinlayApp.setUser(new User(responseJson));
                    } else {
                        user.setNewUserJson(responseJson);
                    }
                }
                if (callback != null){
                    callback.onResponseOk(responseStr);
                }
            }

            @NonNull
            @Override
            public String requestName() {
                return "recheckEmailVerified";
            }

            @Override
            public boolean makeProcessBackground() {
                return false;
            }
        }, callback, 0);
    }

    public static void sendFcmTokenToServer(){

        final String oldToken = AyUtils.getStringPref(AyUtils.PREF_REPLACED_FCM_TOKEN);
        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                String token = AyUtils.getStringPref(AyUtils.PREF_FCM_TOKEN);
                if (token == null) {
                    return null;
                }

                FormBody.Builder builder = new FormBody.Builder()
                        .add("clientToken", token);
                if (oldToken != null) {
                    builder = builder.add("replacedClientToken", oldToken);
                }
                RequestBody requestBody = builder.build();

                return getRequestBuilderWithHeader(getUrl(FCM_CLIENT_TOKEN))
                        .post(requestBody)
                        .build();
            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return get30SecTimeoutClient();
            }

            @Override
            public boolean extractJson() {
                return false;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {
                //{"message":"Client registered successfully"}
                if (oldToken != null) {
                    AyUtils.saveStringPreference(AyUtils.PREF_REPLACED_FCM_TOKEN, null);
                }
                AyUtils.saveStringPreference(AyUtils.PREF_UPDATE_FCM_REQUIRED, null);
            }

            @NonNull
            @Override
            public String requestName() {
                return "sendFcmTokenToServer";
            }

            @Override
            public boolean makeProcessBackground() {
                return true;
            }
        }, new Callback() {
            @Override
            public void onResponseOk(String responseBody) {}

            @Override
            public void onError(String message) {
                String errorMessage = "sendFcmTokenToServer error: " + message;
                Timber.w(errorMessage);
                AyUtils.saveStringPreference(AyUtils.PREF_UPDATE_FCM_REQUIRED, "error_" + System.currentTimeMillis());
            }
        }, 0);

    }

    public static void deleteFcmTokenFromServer(final Callback callback){

        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                String token = AyUtils.getStringPref(AyUtils.PREF_FCM_TOKEN);
                if (token == null){
                    if (callback != null){
                        callback.onResponseOk(""); // have no token to delete - allow user to logout
                    }
                    return null;
                }

                RequestBody requestBody = new FormBody.Builder()
                        .add("clientToken", token)
                        .build();

                return getRequestBuilderWithHeader(getUrl(FCM_CLIENT_TOKEN))
                        .delete(requestBody)
                        .build();
            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return get30SecTimeoutClient();
            }

            @Override
            public boolean extractJson() {
                return false;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {
                // {"message":"Client unregistered successfully"}
                if (callback != null){
                    callback.onResponseOk("");
                }
            }

            @NonNull
            @Override
            public String requestName() {
                return "deleteFcmTokenFromServer";
            }

            @Override
            public boolean makeProcessBackground() {
                return false;
            }
        }, callback, 0);

    }

    public static void sendPromoToServer(final String promo, final Callback callback, final boolean isAdd){
        if (promo == null || promo.isEmpty()){
            if (callback != null){
                callback.onError("");
            }
            return;
        }
        sendRequestToServer(new InternalCallback() {
            @Override
            public Request getRequest() {
                String url = getUrl(PROMO_CODES);
                if (isAdd){
                    JSONObject jOuter = new JSONObject();
                    JSONObject jProfile = new JSONObject();
                    JSONObject jCampaign = new JSONObject();
                    JSONArray promoJarr = new JSONArray();
                    try {
                        promoJarr.put(promo);
                        jCampaign.put("promotionCode", promoJarr);
                        jProfile.put("campaigns", jCampaign);
                        jOuter.put("profile", jProfile);
                        MediaType jsonType = MediaType.parse("application/json; charset=utf-8");
                        RequestBody requestBody = RequestBody.create(jsonType, jOuter.toString());
                        return getRequestBuilderWithHeader(getUrl(PROMO_CODES))
                                .post(requestBody)
                                .build();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                RequestBody requestBody = new FormBody.Builder()
                        .add("promotionCode", promo)
                        .build();

                return getRequestBuilderWithHeader(url)
                        .delete(requestBody)
                        .build();

            }

            @NonNull
            @Override
            public OkHttpClient getOkClient() {
                return getRegularOkHttpClient();
            }

            @Override
            public boolean extractJson() {
                return true;
            }

            @Override
            public void onResponseOk(String responseStr, JSONObject responseJson) {
                User user = AdinlayApp.getUser();
                if (user != null && responseJson != null && (!responseJson.optString("_id").isEmpty() ||
                        !responseJson.optString("id").isEmpty())){
                    user.setNewUserJson(responseJson);
                    if (callback != null){
                        callback.onResponseOk("");
                    }
                    return;
                }
            }

            @NonNull
            @Override
            public String requestName() {
                return isAdd ? "sendPromoToServer" : "deletePromoFromServer";
            }

            @Override
            public boolean makeProcessBackground() {
                return false;
            }
        }, callback, 0);
    }

//    public static void trySelfCert(){
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                Timber.i("SSL_DBG trySelfCert");
//                Context resContext = AyUtils.getContext();
//                if (resContext == null){
//                    Timber.w("SSL_DBG context is null");
//                    return;
//                }
//                Certificate ca = null;
//                InputStream cert = resContext.getResources().openRawResource(R.raw.server_cert);
//                try {
//                    CertificateFactory cf = CertificateFactory.getInstance("X.509");
//                    ca = cf.generateCertificate(cert);
//                } catch (CertificateException e){
//                    Timber.w(e, "SSL_DBG 1:");
//                    e.printStackTrace();
//                }finally {
//                    try {
//                        cert.close();
//                    } catch (IOException e) {
//                        Timber.w(e, "SSL_DBG 2:");
//                        e.printStackTrace();
//                    }
//                }
//                if (ca == null){
//                    Timber.i("SSL_DBG ca is null");
//                    return;
//                }
//
//                // Create a KeyStore containing our trusted CAs
//                String keyStoreType = KeyStore.getDefaultType();
//                KeyStore keyStore = null;
//                try {
//                    keyStore = KeyStore.getInstance(keyStoreType);
//                    keyStore.load(null, null);
//                    keyStore.setCertificateEntry("ca", ca);
//                } catch (KeyStoreException | NoSuchAlgorithmException |
//                        IOException | CertificateException e) {
//                    Timber.w(e, "SSL_DBG 3:");
//                    e.printStackTrace();
//                }
//
//                if (keyStore == null){
//                    Timber.i("SSL_DBG keystore is null");
//                    return;
//                }
//
//                // Create a TrustManager that trusts the CAs in our KeyStore
//                String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
//                TrustManagerFactory tmf = null;
//
//                try {
//                    tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
//                    tmf.init(keyStore);
//                } catch (NoSuchAlgorithmException | KeyStoreException e) {
//                    Timber.w(e, "SSL_DBG 4:");
//                    e.printStackTrace();
//                }
//                if (tmf == null){
//                    Timber.i( "SSL_DBG tmf is null");
//                    return;
//                }
//
//                // Create an SSLContext that uses our TrustManager
//                SSLContext context = null;
//                try {
//                    context = SSLContext.getInstance("TLS");
//                    context.init(null, tmf.getTrustManagers(), null);
//                } catch (NoSuchAlgorithmException | KeyManagementException e) {
//                    Timber.w(e, "SSL_DBG 5:");
//                    e.printStackTrace();
//                }
//
//                if (context == null){
//                    Timber.i("SSL_DBG SSLContext is null");
//                    return;
//                }
//
//                // Tell the OkHttpClient to use a SocketFactory from our SSLContext:
//                X509TrustManager trustManager = (X509TrustManager) tmf.getTrustManagers()[0];
//                OkHttpClient.Builder builder = new OkHttpClient.Builder().sslSocketFactory(context.getSocketFactory(), trustManager);
//                builder.hostnameVerifier(new HostnameVerifier() {
//                    @Override
//                    public boolean verify(String hostname, SSLSession session) {
//                        Timber.d("SSL_DBG comparing api.adinlay.com with %s", hostname);
//                        return "api.adinlay.com".equalsIgnoreCase(hostname);
//                    }
//                });
//                OkHttpClient okHttpClient1 = builder.build();
//
//                // header must be from the secured server
//                Request.Builder reqBuilder = new Request.Builder().
//                        url("https://api.adinlay.com/client/users/me").
//                        addHeader(HEADRER, "JWT: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI1YjI4YjVhOTljNjI2ZDE5MjhhZTkxZGMiLCJ1c2VyIjp7ImRpc3BsYXlOYW1lIjoiWWFpciBSaW5vdCIsImZpcnN0TmFtZSI6IllhaXIiLCJsYXN0TmFtZSI6IlJpbm90IiwiZW1haWwiOiJ5cmlub3RAYWRpbmxheS5jb20iLCJmaXJlYmFzZUlkIjoiZmVKbnVPR2JEYmZJNTFsczBlM2JuNTNQVkpQMiIsInBob3RvVVJMIjoiIiwiZ2VuZGVyIjoiTWFsZSIsImFnZVJhbmdlIjp7ImZyb20iOjM1LCJ0byI6MTIwLCJfaWQiOiI1YjM4YzA4ZTMwMDdhZjAwMjdjODZhZDAifSwiaG9iYmllcyI6W10sInJlZ2lzdHJhdGlvblN0YXR1cyI6InN0YXJ0ZWQiLCJjcmVhdGVkRGF0ZSI6IjIwMTgtMDctMDFUMTE6NTI6NDYuOTU3WiIsIl9pZCI6IjViMjhiNWE5OWM2MjZkMTkyOGFlOTFkYyIsIl9fdiI6MH0sImlhdCI6MTUzMDQ0NTk2NiwiZXhwIjoxNTMzMDM3OTY2fQ._VfOh2jX2Iat7K9JsKvbYIlZ3W-O3H6chgcKdAPnt_c");
//                Request request = reqBuilder.build();
//
//                Response response = null;
//                try {
//
//                    response = okHttpClient1.newCall(request).execute();
//                    String responseStr = null;
//                    try {
//                        if (response.body() != null) {
//                            responseStr = response.body().string();
//                        }
//                        Timber.d("SSL_DBG got response: %s", response);
//                        Timber.d("SSL_DBG got responseStr: %s", responseStr);
//                    } catch (IOException | OutOfMemoryError e) {
//                        Timber.w(e, "SSL_DBG 6:");
//                        e.printStackTrace();
//                    }
//                } catch (IOException e) {
//                    Timber.w(e, "SSL_DBG 7:");
//                    e.printStackTrace();
//                } finally {
//                    if (response != null){
//                        response.close();
//                    }
//                }
//            }
//        }).start();
//    }

}
