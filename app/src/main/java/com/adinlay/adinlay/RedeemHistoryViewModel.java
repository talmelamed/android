package com.adinlay.adinlay;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import com.adinlay.adinlay.objects.RedeemEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Noya on 10/11/2018.
 */

public class RedeemHistoryViewModel extends ViewModel {

    private static final String TAG = RedeemHistoryViewModel.class.getSimpleName();
    public static final String FETCH_OBJECTS_OK = "fetchObjectsOk";

    private ArrayList<RedeemEntry> redeemEntryArray = null;
    private MutableLiveData<String> pingRE;


    public MutableLiveData<String> getPingRE(){
        if (pingRE == null){
            pingRE = new MutableLiveData<>();
        }
        return pingRE;
    }



    @Nullable
    public ArrayList<RedeemEntry> getEntriesArrayList(){
        return redeemEntryArray;
    }



    public void getRedeemEntries(){
        ServerManager.queryServer(ServerManager.PAYMENTS_LIST, null, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                // [{},{}...]
                JSONArray jArr = null;
                try {
                    jArr = new JSONArray(responseBody);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jArr != null){
                    ArrayList<RedeemEntry> entries = new ArrayList<>();
                    for (int i = 0; i < jArr.length(); i++) {
                        JSONObject currJson = jArr.optJSONObject(i);
                        if (currJson != null){
                            RedeemEntry entry = new RedeemEntry(currJson);
                            entries.add(entry);
                        }
                    }
                    if(redeemEntryArray == null){
                        redeemEntryArray = new ArrayList<>();
                    }
                    redeemEntryArray.clear();
                    redeemEntryArray.addAll(entries);
                    getPingRE().postValue(FETCH_OBJECTS_OK);
                } else {
                    onError("Unexpected response");
                }

            }

            @Override
            public void onError(String message) {
                getPingRE().postValue(message);
            }
        });
    }

    public void resendRedeemRequest(String redeemEntryId){
        ServerManager.resendRedeemRequest(redeemEntryId, null);
        // fixme: add callback to handle errors
    }

}
