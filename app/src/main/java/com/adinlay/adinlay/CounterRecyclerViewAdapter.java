package com.adinlay.adinlay;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by Noya on 9/9/2018.
 */

public class CounterRecyclerViewAdapter extends RecyclerView.Adapter<CounterRecyclerViewAdapter.CounterViewHolder> {


    int total;
    int selected;

    public CounterRecyclerViewAdapter(int total, int selected) {
        this.total = total;
        this.selected = selected;
    }

    @NonNull
    @Override
    public CounterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_counter_iv, parent, false);
        return new CounterViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull CounterViewHolder holder, int position) {
        int res = (position == selected) ? R.drawable.oval_counter_selected : R.drawable.oval_counter;
        holder.counterIv.setImageResource(res);
    }

    @Override
    public int getItemCount() {
        return total;
    }

    public static class CounterViewHolder extends RecyclerView.ViewHolder {

        ImageView counterIv;

        public CounterViewHolder(View itemView) {
            super(itemView);

            counterIv = itemView.findViewById(R.id.counter_iv);
        }
    }

}
