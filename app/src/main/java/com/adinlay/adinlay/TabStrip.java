package com.adinlay.adinlay;

import android.support.annotation.NonNull;

import com.adinlay.adinlay.objects.TabLauncher;
import com.adinlay.adinlay.objects.TlAdvertisers;
import com.adinlay.adinlay.objects.TlHome;
import com.adinlay.adinlay.objects.TlProfile;
import com.adinlay.adinlay.objects.TlShare;
import com.adinlay.adinlay.objects.TlWallet;

import java.util.HashMap;

import timber.log.Timber;

/**
 * Created by Noya on 3/29/2018.
 */

public class TabStrip {

    final private static String TAG =TabStrip.class.getSimpleName();

    private HashMap<String, TabLauncher> tabs;
    private TabLauncher selectedTab;

    public TabStrip(){
        tabs = new HashMap<>();
        tabs.put(TabLauncher.HOME_TAG, new TlHome(0));
        tabs.put(TabLauncher.ADVERTISERS_TAG, new TlAdvertisers(1));
        tabs.put(TabLauncher.SHARE_TAG, new TlShare(2));
        tabs.put(TabLauncher.WALLET_TAG, new TlWallet(3));
        tabs.put(TabLauncher.PROFILE_TAG, new TlProfile(4));

        // if adding persistence: get the selected tab from share preferences
        selectedTab = tabs.get(TabLauncher.HOME_TAG);

    }

    public TabStrip(@NonNull HashMap<String, TabLauncher> tabs, @NonNull String selectedKey){
        this.tabs = tabs;
        selectedTab = tabs.get(selectedKey);
    }

    public HashMap<String, TabLauncher> getTabs() {
        return tabs;
    }

    public TabLauncher getSelectedTab() {
        return selectedTab;
    }

    public void setSelectedTab(TabLauncher selectedTab) {
        this.selectedTab = selectedTab;
    }

    public void setSelectedTab(int containerId) {

        for (TabLauncher tl : tabs.values()){
            if (tl.getContainerId() == containerId){
                selectedTab = tl;
                return;
            }
        }

        Timber.w("setSelectedTab with container id %s -- tab not found", containerId);
    }
}
