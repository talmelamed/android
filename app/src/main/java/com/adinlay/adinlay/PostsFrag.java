package com.adinlay.adinlay;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.adinlay.adinlay.objects.Post;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Noya on 4/8/2018.
 */

public class PostsFrag extends android.support.v4.app.Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = PostsFrag.class.getSimpleName();

    private RecyclerView recyclerView = null;
    private RecyclerView.Adapter adapter = null;
    private RecyclerView.LayoutManager layoutManager = null;
    private TextView messageTv = null;
    private SwipeRefreshLayout refreshLayout = null;
    private GlideRequests gr;

    private int displayMode = HomeFrag.ALL_ADS_MOSAIC;
    private WeakReference<PostsRecyclerViewAdapter.ItemClick> wr_itemClick = new WeakReference<PostsRecyclerViewAdapter.ItemClick>(null);

    MainViewModel model = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        }
    }

    public void setDisplayMode(int displayMode) {
        this.displayMode = displayMode;
    }

    public int getDisplayMode() {
        return displayMode;
    }

    public void setItemClickCallback(PostsRecyclerViewAdapter.ItemClick callback){
        wr_itemClick = new WeakReference<PostsRecyclerViewAdapter.ItemClick>(callback);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_recyclerview, container, false);

        if (model == null && getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        }

        gr = GlideApp.with(this);

        recyclerView = root.findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(false);

        messageTv = root.findViewById(R.id.recycler_frag_message);

        refreshLayout = root.findViewById(R.id.refresh_layout);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setColorSchemeResources(android.R.color.black, R.color.colorAdinlayYellow);

        layoutManager = new LinearLayoutManager(getActivity());

        recyclerView.setLayoutManager(layoutManager);
        int topPad = getResources().getDimensionPixelSize(R.dimen.toolbar_h);
        int bottomPad = 0;
        if (model.isPadFor2tabstrips()){
            topPad +=  getResources().getDimensionPixelSize(R.dimen.upper_bar_h);
            bottomPad = getResources().getDimensionPixelSize(R.dimen.main_tab_strip_h);
        }
        recyclerView.setPadding(0, topPad, 0, bottomPad);
//        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            int y = 0;
//
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
////                Timber.i("rv onScrollStateChanged state: %s", newState);
//                if (newState == RecyclerView.SCROLL_STATE_IDLE){
//                    if (gr.isPaused()) {
//                        gr.resumeRequests();
//                    }
//                    y = 0;
//                }
////                if (newState == RecyclerView.SCROLL_STATE_DRAGGING){
////                    gr.pauseRequests();
////                }
//                super.onScrollStateChanged(recyclerView, newState);
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
////                Timber.d("rv onScrolled dy: %s", dy);
//                y += dy;
//                if (!gr.isPaused()) {
//                    if (y < -1200 || y > 50){
////                        Timber.i("rv onScrolled pausing for dy: %s and y: %s", dy, y);
//                        gr.pauseRequests();
//                    }
//                }
//            }
//        });

        if (displayMode == HomeFrag.MY_ADS){
            DividerItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
            itemDecoration.setDrawable(getResources().getDrawable(R.drawable.divider_posts));
            recyclerView.addItemDecoration(itemDecoration);
        }

        MutableLiveData<String> liveData = displayMode == HomeFrag.MY_ADS ?
                model.getPingMyAds() : model.getPingAllAds();
        liveData.observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
//                Timber.v("PING got now, inMyAds? %s", inMyAds);
                refreshLayout.setRefreshing(false);
                if (MainViewModel.FETCH_POSTS_OK.equals(s)) {
                    int arrSize;
                    if (displayMode == HomeFrag.ALL_ADS_MOSAIC){

                        ArrayList<Post[]> mos_arr = model.getMosaicPostsList();
                        if (mos_arr == null){
                            mos_arr = new ArrayList<>();
                        }
                        adapter = new PostsMosaicRecyclerViewAdapter(mos_arr,gr, wr_itemClick.get(), getActivity());
                        arrSize = mos_arr.size();
                    } else {

                        ArrayList<Post> mArr = getModelPostsArr();
                        if (mArr == null){
                            // shouldn't happen!!!!
                            mArr = new ArrayList<>();
                        }
                        adapter = new PostsRecyclerViewAdapter(mArr, displayMode, gr, wr_itemClick.get(), getActivity());
                        arrSize = mArr.size();
                    }

                    recyclerView.swapAdapter(adapter, true);
                    setMessageVisibility(arrSize);
                } else {
                    if (s != null && !s.isEmpty() && getActivity() != null){
                        Toast.makeText(getActivity().getApplicationContext(), "Error: " + s, Toast.LENGTH_LONG).show();
                    }
                }
            }
        });


        return root;
    }

    @Override
    public void onStart() {
        super.onStart();

        final boolean refreshAfterShare = checkModelShouldRefresh();
        boolean needToRefresh = refreshAfterShare;
        if (displayMode != HomeFrag.ALL_ADS_MOSAIC) {
            ArrayList<Post> mArr = getModelPostsArr();
            needToRefresh = needToRefresh || mArr == null;

            mArr = mArr == null ? new ArrayList<Post>() : mArr;
            adapter = new PostsRecyclerViewAdapter(mArr, displayMode, gr, wr_itemClick.get(), getActivity());
            setMessageVisibility(mArr.size());
        } else {
            ArrayList<Post[]> mosaicList = model.getMosaicPostsList();
            needToRefresh = needToRefresh || mosaicList == null;

            mosaicList = mosaicList == null ? new ArrayList<Post[]>() : mosaicList;
            adapter = new PostsMosaicRecyclerViewAdapter(mosaicList, gr, wr_itemClick.get(), getActivity());
            setMessageVisibility(mosaicList.size());
        }

        if (needToRefresh){
            // never fetched the items or need to refresh
            informModelHasRefreshed();
            refreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    if (getModelPostsArr() == null || refreshAfterShare){
                        refreshLayout.setRefreshing(true);
                        onRefresh();
                    }
                }
            });
        }
        int pos = getPositionFromModel();
        if (pos > 0){
            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    int pos =  getPositionFromModel();
                    Timber.d("%s trying to scroll to pos: %s of: %s", displayMode, pos, layoutManager.getItemCount());
                    if (pos < layoutManager.getItemCount()){
                        if (layoutManager instanceof LinearLayoutManager) {
                            ((LinearLayoutManager) layoutManager).scrollToPositionWithOffset(pos, 0);
                        } else {
                            layoutManager.scrollToPosition(pos);
                        }
                    }
                }
            });
        }

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onStop() {
        super.onStop();

        adapter = new PostsRecyclerViewAdapter(new ArrayList<Post>(), displayMode, null, wr_itemClick.get(), getActivity());
        recyclerView.setAdapter(adapter);

    }

    private void setMessageVisibility(int arrSize){
        if (arrSize <= 0){
            messageTv.setVisibility(View.VISIBLE);
        } else if (messageTv.getVisibility() != View.GONE){
            messageTv.setVisibility(View.GONE);
        }
    }

    private ArrayList<Post> getModelPostsArr(){
        return displayMode == HomeFrag.MY_ADS ? model.getMyAdsArrayList() :
                model.getAllAdsArrayList();
    }

    private boolean checkModelShouldRefresh(){
        return displayMode == HomeFrag.MY_ADS ? model.isNeedToRefreshMyAds() :
                model.isNeedToRefreshAllAds();
    }

    private void informModelHasRefreshed(){
        if (displayMode == HomeFrag.MY_ADS){
            model.setNeedToRefreshMyAds(false);
            return;
        }
        model.setNeedToRefreshAllAds(false);
    }

    private int getPositionFromModel(){
        switch (displayMode){
            case HomeFrag.MY_ADS:
                return model.getMyAdsPos();
            case HomeFrag.ALL_ADS_MOSAIC:
            default:
                return model.getAllAdsPos();
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        // save the pos info in model:
        int pos = layoutManager instanceof LinearLayoutManager ? ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition() :
                layoutManager instanceof SpannedGridLayoutManager ? ((SpannedGridLayoutManager) layoutManager).getFirstVisibleItemPosition() : 0;
        if (displayMode == HomeFrag.MY_ADS) {
            model.setMyAdsPos(pos);
        } else {
            model.setAllAdsPos(pos);
        }
//            Timber.v("in frag onPause, pos: %s of: %s", pos, layoutManager.getItemCount());

    }

    @Override
    public void onRefresh() {
        model.getPosts(displayMode == HomeFrag.MY_ADS);
    }
}
