package com.adinlay.adinlay.objects;

import android.support.annotation.NonNull;

public class OverlayInfo {

    private int AdapterPosition;
    private String url;

    public OverlayInfo(@NonNull String url, int AdapterPosition) {
        this.url = url;
        this.AdapterPosition = AdapterPosition;
    }

//    public OverlayInfo(@NonNull String url) {
//        this.url = url;
//        this.AdapterPosition = -1;
//    }

    public int getAdapterPosition() {
        return AdapterPosition;
    }

    public String getUrl() {
        return url;
    }
}
