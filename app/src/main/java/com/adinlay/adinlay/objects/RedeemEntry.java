package com.adinlay.adinlay.objects;

import android.support.annotation.NonNull;

import org.json.JSONObject;

import java.math.BigDecimal;

/**
 * Created by Noya on 10/10/2018.
 */

public class RedeemEntry {

    public static final String ID_KEY = "_id";
    public static final String URI_KEY = "uri";
    public static final String AMOUNT_KEY = "amount";
    public static final String CREATED_DATE_KEY = "createdDate";
    public static final String EXTERNAL_ID_KEY = "extrenalRedeemId";

    private JSONObject entryJson;

    public RedeemEntry(@NonNull JSONObject entryJson) {
        this.entryJson = entryJson;
    }

    @NonNull
    public String getEntryId(){
        return entryJson.optString(ID_KEY);
    }

    @NonNull
    public String getEntryExtrnalId(){
        return entryJson.optString(EXTERNAL_ID_KEY);
    }

    @NonNull
    public String getUri(){
        return entryJson.optString(URI_KEY);
    }

    @NonNull
    public String getDateString(){
        return entryJson.optString(CREATED_DATE_KEY); // "2018-10-10T12:37:42.642Z"
    }

    public BigDecimal getAmount(){
        return BigDecimal.valueOf(entryJson.optDouble(AMOUNT_KEY, 0));
    }


}
