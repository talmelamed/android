package com.adinlay.adinlay.objects;

import android.support.annotation.NonNull;

import com.adinlay.adinlay.AdinlayApp;
import com.adinlay.adinlay.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * Created by Noya on 7/2/2018.
 */

public class Ad {

    private static final String TAG = Ad.class.getSimpleName();

    public static final String ID_KEY = "_id";
    public static final String NAME_KEY = "name";
    public static final String URL_KEY = "url";
    public static final String URL_SMALL_KEY = "url_small";
//    public static final String MIN_PRICE_KEY = "minPrice";
    public static final String PRICE_KEY = "price";
    public static final String PRICE_MIN_KEY = "min";
    public static final String PRICE_MAX_KEY = "max";
    public static final String PRICE_PER_CLICK_KEY = "perClick";
    public static final String INSTAGRAM_TAG_KEY = "instagramTag";
    public static final String INSTAGRAM_TEXT_KEY = "instagramText";
    public static final String LAYOUTS_KEY = "layouts";
    public static final String INSTRUCTIONS_KEY = "instructions";
    public static final String CAMPAIGN_KEY = "campaign";
    public static final String APPROVAL_KEY = "approval";
    public static final String APPROVAL_TIME_KEY = "maxReviewTime";
    public static final String APPROVAL_ENABLED_KEY = "enabled";
    public static final String CAMPAIGN_END_KEY = "endDate";
    public static final String CAMPAIGN_TARGET_MEDIA_KEY = "targetSocialMedia";

    private JSONObject adJson;
    private int selectedLayoutPos = 0;

    public Ad(JSONObject postJson) {
        this.adJson = postJson == null ? new JSONObject() : postJson;
    }

    @NonNull
    public String getUrl(boolean fullSize){
        String small = adJson.optString(URL_SMALL_KEY);
        return fullSize || small.isEmpty() ? adJson.optString(URL_KEY, small) : small;
    }

    @NonNull
    public String getId(){
        return adJson.optString(ID_KEY);
    }

    @NonNull
    public String getName(){
        return adJson.optString(NAME_KEY);
    }

    @NonNull
    public String getInstagramTag(){
        String tagText = adJson.optString(INSTAGRAM_TAG_KEY);
        if ("instagram".equalsIgnoreCase(getTargetMedia()) || tagText.isEmpty()){
            return tagText;
        }

        return getFcbkHashTag(tagText);
    }

    /**
     * basic testing of adding a hashtag to facebook link, showed that:
     * empty string is fine, tag won't show
     * but dialog will fail if string doesn't start with '#' and has at least one character after it
     * also, anything after a space or 2nd '#' is removed
     */
    @NonNull
    public static String getFcbkHashTag(String text){
        String tagText = text == null ? "" : text.trim();
//        Timber.i("TAG__ %s", tagText);
        if (tagText.isEmpty()){
            return "";
        }

        String textToSplit = tagText.startsWith("#") ? tagText.replaceFirst("#", "") : tagText;
        String[] splitTags = textToSplit.split("#");
        if (splitTags.length > 0){
            String ret = splitTags[0].trim();
            if (ret.length() > 0) {
                return "#" + ret;
            }
        }
        return "";
    }

    @NonNull
    public String getInstagramText(){
        return adJson.optString(INSTAGRAM_TEXT_KEY);
    }

    @NonNull
    public ArrayList<String> getInstructionsUrls(){
        JSONObject campaign = adJson.optJSONObject(CAMPAIGN_KEY);
        if (campaign == null){
            return new ArrayList<>();
        }
        JSONArray jArr = campaign.optJSONArray(INSTRUCTIONS_KEY);
        ArrayList<String> instructionsList = new ArrayList<>();
        if (jArr != null){
            for (int i = 0; i < jArr.length(); i++){
                ServerImageObject sio = new ServerImageObject(jArr.optJSONObject(i));
                String currUrl = sio.getUrl();
                if (!currUrl.isEmpty()){
                    instructionsList.add(currUrl);
                }
            }
        }
        return instructionsList;
    }

    @NonNull
    public ArrayList<ServerImageObject> getLayoutUrlObjects(){
        ArrayList<ServerImageObject> layouts = new ArrayList<>();
        JSONArray jArr = adJson.optJSONArray(LAYOUTS_KEY);
        if (jArr != null){
            for (int i = 0; i < jArr.length(); i++){
                ServerImageObject sio = new ServerImageObject(jArr.optJSONObject(i));
                layouts.add(sio);
            }
        }
        if (layouts.isEmpty() && !getUrl(false).isEmpty()){
            JSONObject basicJson = new JSONObject();
            try {
                basicJson.put(URL_KEY, getUrl(true));
                basicJson.put(URL_SMALL_KEY, getUrl(false));
                layouts.add(new ServerImageObject(basicJson));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return layouts;
    }

    @NonNull
    public String getSelectedLayoutUrl(boolean fullSize){
        JSONArray jArr = adJson.optJSONArray(LAYOUTS_KEY);
        if (jArr != null && jArr.length() > selectedLayoutPos && selectedLayoutPos >= 0){
            ServerImageObject sio = new ServerImageObject(jArr.optJSONObject(selectedLayoutPos));
            String selectedUrl = fullSize && !sio.getUrl().isEmpty() ? sio.getUrl() : sio.getDefaultUrl();
            if (!selectedUrl.isEmpty()){
                return selectedUrl;
            }
        }
        return getUrl(fullSize);
    }

    public void setSelectedLayoutPos(int pos){
        selectedLayoutPos = pos;
    }

    @NonNull
    public BigDecimal getAmountBigDec(String whichKey){
        JSONObject priceJson = adJson.optJSONObject(PRICE_KEY);
        return priceJson == null ? BigDecimal.valueOf(0) :
                BigDecimal.valueOf(priceJson.optDouble(whichKey, 0));
    }

    public boolean needsApproval(){
        JSONObject campaignJson = adJson.optJSONObject(CAMPAIGN_KEY);
        if (campaignJson == null){
            return false;
        }
        JSONObject approvalJson = campaignJson.optJSONObject(APPROVAL_KEY);
        return approvalJson != null && approvalJson.optBoolean(APPROVAL_ENABLED_KEY);
    }

    /**
     * @return number of max hours till approval
     */
    public int getApprovalTime(){
        JSONObject campaignJson = adJson.optJSONObject(CAMPAIGN_KEY);
        JSONObject approvalJson = campaignJson == null ? null :
                campaignJson.optJSONObject(APPROVAL_KEY);
        if (approvalJson == null){
            return 0;
        }
        double time = approvalJson.optDouble(APPROVAL_TIME_KEY, 0);
        return (int) Math.round(time);
    }

    public long getCampaignEndDate(){
        JSONObject campaignJson = adJson.optJSONObject(CAMPAIGN_KEY);
        if (campaignJson == null){
            return -1;
        }
        return campaignJson.optLong(CAMPAIGN_END_KEY, -1);
    }

    public boolean campaignHasOffers(){
        JSONObject campaignJson = adJson.optJSONObject(CAMPAIGN_KEY);
        return campaignJson != null && !campaignJson.optString("offeringPageUrl").isEmpty();
    }

    @NonNull
    public String getTargetMedia(){
        JSONObject campaignJson = adJson.optJSONObject(CAMPAIGN_KEY);
        if (campaignJson == null){
            return "";
        }
        return campaignJson.optString(CAMPAIGN_TARGET_MEDIA_KEY);
    }

    public boolean isTargetMediaIg(){
        return "instagram".equalsIgnoreCase(getTargetMedia());
    }

    public String getJsonString(){
        return adJson.toString();
    }

    @Override
    public String toString() {
        return adJson.toString();
    }

    public boolean isConnectedToMedia(){
        String targetMedia = getTargetMedia();
        boolean isInsta = "instagram".equalsIgnoreCase(targetMedia);
        boolean isFcbk = "facebook".equalsIgnoreCase(targetMedia);
        User user = AdinlayApp.getUser();
        return user != null && ((isFcbk && user.isConnectedToFacebook()) ||
                (isInsta && user.isConnectedToInstagram()));
    }

    public boolean isFreeCampaign(){
        JSONObject campaignJson = adJson.optJSONObject(CAMPAIGN_KEY);
        return campaignJson != null && campaignJson.optBoolean("free");
    }

    public boolean isTestingCampaign(){
        JSONObject campaignJson = adJson.optJSONObject(CAMPAIGN_KEY);
        return campaignJson != null && campaignJson.optBoolean("testing");
    }

    @NonNull
    public String getTitle(){
        return adJson.optString("title");
    }

    public boolean isStickerCampaign(){
        return adJson.optString("target").equalsIgnoreCase("editor");
    }

}
