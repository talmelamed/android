package com.adinlay.adinlay.objects;

import android.support.annotation.NonNull;

import com.adinlay.adinlay.R;

import org.json.JSONObject;

/**
 * Created by Noya on 7/2/2018.
 */

public class Offer {

    private static final String TAG = Offer.class.getSimpleName();

    public static final String ID_KEY = "_id";
    public static final String PHOTO_KEY = "photoUrl";
    public static final String TITLE_KEY = "title";
    public static final String DESC_KEY = "description";
    public static final String TEMP_ASSET_KEY = "tempAsset";

    private JSONObject postJson;

    public Offer(JSONObject postJson) {
        this.postJson = postJson == null ? new JSONObject() : postJson;
    }

    @NonNull
    public String getPhotoUrl(){
        return postJson.optString(PHOTO_KEY); // if missing, returns an empty string
    }

    @NonNull
    public String getPostId(){
        return postJson.optString(ID_KEY);
    }

    @NonNull
    public String getTitle(){
        return postJson.optString(TITLE_KEY); // 2018-06-25T19:36:33.632Z
    }

    @NonNull
    public String getDesc(){
        return postJson.optString(DESC_KEY);
    }

    @NonNull
    public String getOfferLink(){
        return postJson.optString(Post.OFFER_URL_KEY);
    }


    public String getJsonString(){
        return postJson.toString();
    }

    @Override
    public String toString() {
        return postJson.toString();
    }

    public int getMockupRes(){
        return postJson.optInt(TEMP_ASSET_KEY, R.drawable.rectangle_4_a);
    }

}
