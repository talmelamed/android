package com.adinlay.adinlay.objects;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.adinlay.adinlay.HomeFrag;
import com.adinlay.adinlay.R;

/**
 * Created by Noya on 3/29/2018.
 */

public class TlHome extends TabLauncher {


    public TlHome(int position) {
        super(R.id.home_container, R.id.home_iv, R.drawable.home_unselected, R.drawable.home_selected,
                HOME_TAG, position, R.id.home_tv);
    }

    @Override
    public void onTabClicked(Activity activity) {

//        Toast.makeText(activity.getApplicationContext(),
//                "Home tab - coming soon!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public Fragment getNewFragment() {
        return new HomeFrag();
    }
}
