package com.adinlay.adinlay.objects;

import android.support.annotation.NonNull;

import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Noya on 7/2/2018.
 */

public class PromoItem {


    private JSONObject promoJson;
    private String name = "";
    private String code = "";

    public PromoItem(JSONObject promoJson) {
        this.promoJson = this.promoJson == null ? new JSONObject() : this.promoJson;
        Iterator<String> iter = promoJson.keys();

        while (iter.hasNext()) {
            code = iter.next();
            name = promoJson.optString(code);
            if (!code.isEmpty() && !name.isEmpty()){
                break;
            }
        }
    }

    @NonNull
    public String getName(){
        return name;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return promoJson.toString();
    }

}
