package com.adinlay.adinlay.objects;

/**
 * Created by Noya on 3/29/2018.
 */

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.adinlay.adinlay.ItemClick;

public abstract class TabLauncher {
    private int containerId;
    private int ivId;
    private int srcId;
    private int selectedSrcId;
    private String launcherTag;
    private int position;
    private int tvId;
    protected boolean changeBackground = false;

    public final static String HOME_TAG = "Home";
    public final static String ADVERTISERS_TAG = "Advertisers";
    public final static String SHARE_TAG = "Share";
    public final static String WALLET_TAG = "Wallet";
    public final static String PROFILE_TAG = "Profile";
    public final static String LAYOUTS_TAG = "Layouts";
    public final static String FILTERS_TAG = "Filters";
    public final static String PHOTOS_TAG = "Photos";

    public TabLauncher(int containerId, int ivId, int srcId, int selectedSrcId, String launcherTag, int position){
        this(containerId, ivId, srcId, selectedSrcId, launcherTag, position, 0);
    }

    public TabLauncher(int containerId, int ivId, int srcId, int selectedSrcId, String launcherTag, int position, int tvId) {
        this.containerId = containerId;
        this.ivId = ivId;
        this.srcId = srcId;
        this.selectedSrcId = selectedSrcId;
        this.launcherTag = launcherTag;
        this.position = position;
        this.tvId = tvId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof TabLauncher){
            // for now, only comparing the tags
            return this.launcherTag.equals(((TabLauncher) obj).getLauncherTag());
        }
        return false;
    }

    public int getContainerId() {
        return containerId;
    }

    public int getIvId() {
        return ivId;
    }

    public int getSrcId() {
        return srcId;
    }

    public int getTvId() {
        return tvId;
    }

    public boolean isChangeBackground() {
        return changeBackground;
    }

    public int getSelectedSrcId() {
        return selectedSrcId;
    }

    public String getLauncherTag() {
        return launcherTag;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position){
        this.position = position;
    }

    public boolean shouldReplaceFragOnClick(){
        return true;
    }

    public abstract void onTabClicked(Activity activity);

    public abstract Fragment getNewFragment();

    public Fragment getNewFragment(ItemClick callback) {
        return getNewFragment();
    }

}
