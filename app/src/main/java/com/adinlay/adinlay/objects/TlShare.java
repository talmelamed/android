package com.adinlay.adinlay.objects;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.adinlay.adinlay.AyUtils;
import com.adinlay.adinlay.CampaignsActivity;
import com.adinlay.adinlay.PlaceholderFrag;
import com.adinlay.adinlay.R;

/**
 * Created by Noya on 3/29/2018.
 */

public class TlShare extends TabLauncher {


    public TlShare(int position) {
        super(R.id.share_container, R.id.share_iv, R.drawable.group_8_share, R.drawable.group_8_share,
                SHARE_TAG, position);
    }

    @Override
    public void onTabClicked(Activity activity) {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activity,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            // shouldn't get here (activity to request the permissions)
//            Toast.makeText(activity.getApplicationContext(),
//                    R.string.permission_missing, Toast.LENGTH_LONG).show();
            return;
        }

        Intent intent = new Intent(activity, CampaignsActivity.class);
        activity.startActivityForResult(intent, AyUtils.get16BitId(R.id.intent_share_new_post));
    }

    @Override
    public boolean shouldReplaceFragOnClick() {
        return false;
    }

    @Override
    public Fragment getNewFragment() {
        return new PlaceholderFrag();
    }
}
