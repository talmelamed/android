package com.adinlay.adinlay.objects;

import android.support.annotation.NonNull;

import com.adinlay.adinlay.AdinlayApp;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import timber.log.Timber;

/**
 * Created by Noya on 10/3/2018.
 */

public class ProfileTopic {

    private static final String TAG = ProfileTopic.class.getSimpleName();

    public static final String LOCATION = "location";
    public static final String TYPE_KEY = "type";
    public static final String INPUT_VAL = "input";
    public static final String SELECTION_KEY = "selection";
    public static final String DEFAULT_KEY = "default";
    public static final String ALL_VAL = "all";
    public static final String DEFAULT_ALL_ID = "Default - All";

    private JSONObject topicJson;
    private ArrayList<CheckableObject> items = null;

    public ProfileTopic(JSONObject topicJson) {
        this.topicJson = topicJson == null ? new JSONObject() : topicJson;
    }

    @NonNull
    public String getId(){
        return topicJson.optString("id");
    }

    @NonNull
    public String getTitle(){
        return topicJson.optString("title");
    }

    @NonNull
    public String getDescription(){
        return topicJson.optString("description");
    }

    @NonNull
    public String getImageUrl(){
        return topicJson.optString("imageUrl");
    }

    @NonNull
    public String getSelectionReq(){
        // single / multiple
        return topicJson.optString("selection");
    }

    @NonNull
    public JSONArray getItemsJsonArray(){
        JSONArray jsonArray = topicJson.optJSONArray("items");
        return jsonArray == null ? new JSONArray() : jsonArray;
    }

    @NonNull
    public ArrayList<CheckableObject> getItems(boolean resetList){

        if (items == null || resetList){
            JSONArray jArr = getItemsJsonArray();
            items = new ArrayList<>();
            for (int i = 0; i < jArr.length(); i++){
                JSONObject jsonObject = jArr.optJSONObject(i);
                if (jsonObject != null){
                    items.add(new CheckableObject(jsonObject));
                }
            }
            JSONArray adultArr = topicJson.optJSONArray("adults_items");
            if (adultArr != null && AdinlayApp.getUser() != null && AdinlayApp.getUser().isOver21()){
                for (int i = 0; i < adultArr.length(); i++){
                    JSONObject jsonObject = adultArr.optJSONObject(i);
                    if (jsonObject != null){
                        items.add(new CheckableObject(jsonObject));
                    }
                }
                Collections.sort(items, new Comparator<CheckableObject>() {
                    @Override
                    public int compare(CheckableObject item1, CheckableObject item2) {
                        return item1 == null || item2 == null ? 0 : item1.getTitle().compareTo(item2.getTitle());
                    }
                });
            }
        }

        Timber.d("topic_dbg returning list with %s items",items.size());
        return items;
    }

    @NonNull
    public HashMap<String, String> mapItemNames(){
        HashMap<String, String> idToName = new HashMap<>();
        JSONArray jArr = getItemsJsonArray();
        for (int i = 0; i < jArr.length(); i++){
            JSONObject jsonObject = jArr.optJSONObject(i);
            if (jsonObject != null){
                addToMappedItems(new CheckableObject(jsonObject), idToName);
            }
        }
        return idToName;
    }

    private void addToMappedItems(CheckableObject object, HashMap<String, String> idToName){
        if (!object.getId().isEmpty()){
            idToName.put(object.getId(), object.getFullTitle());
            if (object.getSubItems() != null){
                for (CheckableObject subItem : object.getSubItems()){
                    addToMappedItems(subItem, idToName);
                }
            }
        }
    }

    public boolean isSingleChoice(){
        return topicJson.optString(SELECTION_KEY).equalsIgnoreCase("single");
    }

    public boolean isMultipleSelection(){
        return "multiple".equalsIgnoreCase(topicJson.optString(SELECTION_KEY));
    }

    public boolean mustMakeChoice(){
        return topicJson.optBoolean("required", false);
    }

    public boolean isHierarchic(){
        return topicJson.optBoolean("isHierarchic", false);
    }

    public boolean isTypeInput(){
        return INPUT_VAL.equalsIgnoreCase(topicJson.optString(TYPE_KEY));
    }

    public boolean isDefaultAll(){
        return ALL_VAL.equals(topicJson.optString(DEFAULT_KEY));
    }

    @NonNull
    public String getTypeOfInputType(){
        return topicJson.optString("inputType");
    }

    @Override
    public String toString() {
        return topicJson.toString();
    }

}
