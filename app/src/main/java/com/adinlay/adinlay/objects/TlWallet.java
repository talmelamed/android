package com.adinlay.adinlay.objects;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.adinlay.adinlay.R;
import com.adinlay.adinlay.WalletFrag;

/**
 * Created by Noya on 3/29/2018.
 */

public class TlWallet extends TabLauncher {


    public TlWallet(int position) {
        super(R.id.wallet_container, R.id.wallet_iv, R.drawable.wallet_unselected, R.drawable.wallet_selected,
                WALLET_TAG, position, R.id.wallet_tv);
    }

    @Override
    public void onTabClicked(Activity activity) {
//        Toast.makeText(activity.getApplicationContext(),
//                "Wallet tab - coming soon!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public Fragment getNewFragment() {
        return new WalletFrag();
    }
}
