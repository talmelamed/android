package com.adinlay.adinlay.objects;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.adinlay.adinlay.ProfileFrag;
import com.adinlay.adinlay.R;

/**
 * Created by Noya on 3/29/2018.
 */

public class TlProfile extends TabLauncher {


    public TlProfile(int position) {
        super(R.id.profile_container, R.id.profile_iv, R.drawable.profile_unselected, R.drawable.profile_selected,
                PROFILE_TAG, position, R.id.profile_tv);
    }

    @Override
    public void onTabClicked(Activity activity) {
    }

    @Override
    public Fragment getNewFragment() {
        return new ProfileFrag();
    }
}
