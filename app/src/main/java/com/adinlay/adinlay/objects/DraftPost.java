package com.adinlay.adinlay.objects;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import timber.log.Timber;

/**
 * Created by Noya on 8/15/2018.
 */

public class DraftPost {

    public static final long POST_SCHEDULE_UNDEFINED = -1;
    private String imagePath = null;
    private Ad selectedAd = null;
    private Bitmap draftPostBitmap = null;
    private String userText = "";
    private long scheduledPostTimeMilli = POST_SCHEDULE_UNDEFINED;
    private String scheduledPostTimeIso = null;
    private PostFilter selectedFilter = null;
    private boolean followBrand = true;

    public DraftPost(String imagePath) {
        this.imagePath = imagePath;
    }

    public DraftPost(){
        this.imagePath = null;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Ad getSelectedAd() {
        return selectedAd;
    }

    public void setSelectedAd(Ad selectedAd) {
        this.selectedAd = selectedAd;
    }

    public PostFilter getSelectedFilter() {
        return selectedFilter;
    }

    public void setSelectedFilter(PostFilter selectedFilter) {
        this.selectedFilter = selectedFilter;
    }

    public Bitmap getDraftPostBitmap() {
        return draftPostBitmap;
    }

    public void setDraftPostBitmap(Bitmap draftPostBitmap) {
        this.draftPostBitmap = draftPostBitmap;
    }

    public void clearDraftPostBitmap(){
        if (draftPostBitmap != null){
            draftPostBitmap.recycle();
            draftPostBitmap = null;
        }
    }

    @NonNull
    public String getUserText() {
        return userText == null ? "" : userText;
    }

    public void setUserText(String userText) {
        this.userText = userText;
    }

    public void setScheduledPostTimes(String scheduledPostTimeIso, long scheduledPostDateMilli) {
        Timber.d("ZDT_DBG scheduledPostTimeIso was: %s changing to: %s", this.scheduledPostTimeIso, scheduledPostTimeIso);
        this.scheduledPostTimeIso = scheduledPostTimeIso;
        this.scheduledPostTimeMilli = scheduledPostDateMilli;
    }

    public String getScheduledPostTimeIso() {
        return scheduledPostTimeIso;
    }

    public long getScheduledPostTimeMilli() {
        return scheduledPostTimeMilli;
    }

    public boolean isFollowBrand() {
        return followBrand;
    }

    public void setFollowBrand(boolean followBrand) {
        this.followBrand = followBrand;
    }
}
