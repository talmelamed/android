package com.adinlay.adinlay.objects;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.adinlay.adinlay.ItemClick;
import com.adinlay.adinlay.PhotosFrag;
import com.adinlay.adinlay.R;


public class TlPhotos extends TabLauncher {


    public TlPhotos(int position) {
        super(R.id.ep_photos_container, R.id.ep_photos_iv, R.drawable.bg_clear_unselected_frame, R.drawable.bg_clear_yellow_frame,
                PHOTOS_TAG, position, R.id.ep_photos_tv);
        changeBackground = true;
    }

    @Override
    public void onTabClicked(Activity activity) {
    }

    @Override
    public Fragment getNewFragment() {
        return new PhotosFrag();
    }

    @Override
    public Fragment getNewFragment(ItemClick callback) {
        PhotosFrag frag = new PhotosFrag();
        frag.setItemClickCallback(callback);
        return frag;
    }
}
