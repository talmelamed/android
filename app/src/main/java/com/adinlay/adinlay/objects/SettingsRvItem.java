package com.adinlay.adinlay.objects;

public class SettingsRvItem {

    private int nameResId;
    private int buttonResId;

    public SettingsRvItem(int nameResId, int buttonResId) {
        this.nameResId = nameResId;
        this.buttonResId = buttonResId;
    }

    public int getNameResId() {
        return nameResId;
    }

    public int getButtonResId() {
        return buttonResId;
    }
}
