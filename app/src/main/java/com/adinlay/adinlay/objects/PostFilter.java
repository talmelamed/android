package com.adinlay.adinlay.objects;

import android.content.Context;
import android.graphics.PointF;
import android.support.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImageBrightnessFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageContrastFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilterGroup;
import jp.co.cyberagent.android.gpuimage.GPUImageKuwaharaFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSaturationFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSmoothToonFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageToneCurveFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageToonFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageVignetteFilter;
import timber.log.Timber;

/**
 * Created by Noya on 8/6/2018.
 */

public class PostFilter {

    private static final String TAG = PostFilter.class.getSimpleName();

    public static final String NAME_KEY = "name";
    public static final String SUB_FILTERS_KEY = "subFilters";

    private JSONObject filterJson;

    private int adapterPosition = -1;
    private boolean isPlaceholder = false;

    public PostFilter(JSONObject json) {
        this.filterJson = json;
    }

    public PostFilter(boolean isPlaceholder) {
        this.filterJson = new JSONObject();
        this.isPlaceholder = isPlaceholder;
    }

    @NonNull
    public String getName(){
        return filterJson.optString(NAME_KEY);
    }

    @NonNull
    public JSONArray getSubFiltersArray(){
        JSONArray jArr = filterJson.optJSONArray(SUB_FILTERS_KEY);
        return jArr == null ? new JSONArray() : jArr;
    }

    public GPUImageFilter getGpuImageFilter(Context context){
        JSONArray arr = getSubFiltersArray();
        List<GPUImageFilter> filtersListSivaks = new LinkedList<GPUImageFilter>();
//        Timber.i("handling filter: %s", getName());
        for (int i = 0; i < arr.length(); i++){

            JSONObject subFilterJson = arr.optJSONObject(i);
            if (subFilterJson == null){
                continue;
            }
//            Timber.i("handling json: %s", subFilterJson.toString());

            if (subFilterJson.has("saturation") && subFilterJson.optDouble("saturation") != Double.NaN){
                BigDecimal bigDecimal = BigDecimal.valueOf(subFilterJson.optDouble("saturation"));
                float f = bigDecimal.floatValue();
                if (f < 0f){
                    f = 0f;
                }
                filtersListSivaks.add(new GPUImageSaturationFilter(f));
            }
            if (subFilterJson.has("contrast") && subFilterJson.optDouble("contrast") != Double.NaN){
                BigDecimal bigDecimal = BigDecimal.valueOf(subFilterJson.optDouble("contrast"));
                filtersListSivaks.add(new GPUImageContrastFilter(bigDecimal.floatValue()));
            }
            if (subFilterJson.has("brightness") && subFilterJson.optDouble("brightness") != Double.NaN){
                BigDecimal bigDecimal = BigDecimal.valueOf(subFilterJson.optDouble("brightness"));
                filtersListSivaks.add(new GPUImageBrightnessFilter(bigDecimal.floatValue() / 255f));
            }
            if (subFilterJson.has("vignette") && subFilterJson.optDouble("vignette") != Double.NaN){
                PointF centerPoint = new PointF(0.5f, 0.5f);
                float[] vignetteColor = new float[] {0.0f, 0.0f, 0.0f};
                // TODO Sivaks: need to understand better how vignette works!!!
                float vignetteEnd = 0.75f; //0.75f; // 1.0f
                BigDecimal bigDecimal = BigDecimal.valueOf(subFilterJson.optDouble("vignette"));
                float vignetteStart = vignetteEnd * (bigDecimal.floatValue() / 255f);

                filtersListSivaks.add(new GPUImageVignetteFilter(centerPoint, vignetteColor, vignetteStart, vignetteEnd));
            }
            if (subFilterJson.has("rgbKnots") && subFilterJson.optJSONArray("rgbKnots") != null){
                PointF[] curvePointsRGB = getPointFArr(subFilterJson.optJSONArray("rgbKnots"), 1.0f/255.0f);
                PointF[] curvePointsBLUE = getPointFArr(subFilterJson.optJSONArray("blueKnots"), 1.0f/255.0f);
                PointF[] curvePointsGREEN = getPointFArr(subFilterJson.optJSONArray("greenKnots"), 1.0f/255.0f);
                PointF[] curvePointsRED = getPointFArr(subFilterJson.optJSONArray("redKnots"), 1.0f/255.0f);

                GPUImageToneCurveFilter toneCurveFilter = new GPUImageToneCurveFilter();
                toneCurveFilter.setRgbCompositeControlPoints(curvePointsRGB);
                toneCurveFilter.setBlueControlPoints(curvePointsBLUE);
                toneCurveFilter.setGreenControlPoints(curvePointsGREEN);
                toneCurveFilter.setRedControlPoints(curvePointsRED);

                filtersListSivaks.add(toneCurveFilter);
            }
            if (subFilterJson.has("ToonThreshold") && subFilterJson.optDouble("ToonThreshold") != Double.NaN){
                float paramToonThreshold = (BigDecimal.valueOf(subFilterJson.optDouble("ToonThreshold"))).floatValue();
                float paramToonQuantizationLevels = (BigDecimal.valueOf(subFilterJson.optDouble("ToonQuantizationLevels"))).floatValue();
                filtersListSivaks.add(new GPUImageToonFilter(paramToonThreshold, paramToonQuantizationLevels) );
            }
            if (subFilterJson.has("SmoothToonThreshold") && subFilterJson.optDouble("SmoothToonThreshold") != Double.NaN){
                float paramSmoothToonThreshold = (BigDecimal.valueOf(subFilterJson.optDouble("SmoothToonThreshold"))).floatValue();
                float paramSmoothToonQuantizationLevels = (BigDecimal.valueOf(subFilterJson.optDouble("SmoothToonQuantizationLevels"))).floatValue();
                float paramSmoothToonBlurSize = (BigDecimal.valueOf(subFilterJson.optDouble("SmoothToonBlurSize"))).floatValue();
                GPUImageSmoothToonFilter smoothToonFilter = new GPUImageSmoothToonFilter();
                smoothToonFilter.setThreshold(paramSmoothToonThreshold);
                smoothToonFilter.setQuantizationLevels(paramSmoothToonQuantizationLevels);
                smoothToonFilter.setBlurSize(paramSmoothToonBlurSize);
                filtersListSivaks.add(smoothToonFilter);
            }
            if (subFilterJson.has("KuwaharaRadius") && subFilterJson.optDouble("KuwaharaRadius") != Double.NaN){
                int paramKuwaharaRadius = (BigDecimal.valueOf(subFilterJson.optDouble("KuwaharaRadius"))).intValue();
                filtersListSivaks.add(new GPUImageKuwaharaFilter(paramKuwaharaRadius) );
            }
        }
        return filtersListSivaks.size() == 0 ? null : new GPUImageFilterGroup(filtersListSivaks);
    }

    private PointF[] getPointFArr(JSONArray jArr, float scaleFactor){
        if (jArr == null){
            return new PointF[0];
        }
        PointF[] points = new PointF[jArr.length()];
        for (int i = 0; i < jArr.length(); i++){
            JSONObject jPoint = jArr.optJSONObject(i);
            if (jPoint == null){
                jPoint = new JSONObject();
            }
            PointF point = new PointF(scaleFactor * jPoint.optInt("x"), scaleFactor * jPoint.optInt("y"));
//            Timber.v("calc: %s * %s = %s",  scaleFactor, jPoint.optInt("x"), (scaleFactor * jPoint.optInt("x")));
            points[i] = point;
        }
        return points;
    }

    public int getAdapterPosition() {
        return adapterPosition;
    }

    public void setAdapterPosition(int adapterPosition) {
        this.adapterPosition = adapterPosition;
    }

    public boolean isPlaceholder() {
        return isPlaceholder;
    }

    @Override
    public String toString() {
        return getName() + "_" + super.toString();
    }
}
