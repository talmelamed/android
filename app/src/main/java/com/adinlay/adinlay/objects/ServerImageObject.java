package com.adinlay.adinlay.objects;

import android.support.annotation.NonNull;

import org.json.JSONObject;

/**
 * Created by Noya on 11/20/2018.
 */

public class ServerImageObject {

    private JSONObject urlsJson;
    private boolean isPlaceholder = false;

    public ServerImageObject(JSONObject urlsJson) {
        this.urlsJson = urlsJson == null ? new JSONObject() : urlsJson;
    }

    public ServerImageObject(boolean isPlaceholder) {
        this.urlsJson = new JSONObject();
        this.isPlaceholder = isPlaceholder;
    }

    @NonNull
    public String getUrl(){
        return urlsJson.optString("url"); // if missing, returns an empty string
    }

    @NonNull
    public String getSmallUrl(){
        return urlsJson.optString("url_small"); // if missing, returns an empty string
    }

    @NonNull
    public String getDefaultUrl(){
        return getSmallUrl().isEmpty() ? getUrl() : getSmallUrl();
    }

    public boolean isPlaceholder() {
        return isPlaceholder;
    }
}
