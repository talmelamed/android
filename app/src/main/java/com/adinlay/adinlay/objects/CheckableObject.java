package com.adinlay.adinlay.objects;

import android.support.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Noya on 9/23/2018.
 */

public class CheckableObject {

    public static final String ID_KEY = "id";
    public static final String VALUE_KEY = "value";
    public static final String ITEMS_KEY = "items";

    private JSONObject mJson;
    private boolean isChecked;
    private ArrayList<CheckableObject> subItems = null;
    private String idPath = "";
    private String titlePath = "";

    // for subItems: may want to include a path (default is empty String, which add at the beginning of the id when we call "getId"

    public CheckableObject(String title) {
        this(title, false);
    }

    public CheckableObject(String title, boolean isChecked) {
        this.isChecked = isChecked;
        mJson = new JSONObject();
        try {
            mJson.put(VALUE_KEY, title);
            mJson.put(ID_KEY, title);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public CheckableObject(JSONObject jsonObject){
        mJson = jsonObject == null? new JSONObject() : jsonObject;
    }

    public CheckableObject(JSONObject jsonObject, String idPath, String titlePath){
        mJson = jsonObject == null? new JSONObject() : jsonObject;
        this.idPath = idPath == null ? "" : idPath;
        this.titlePath = titlePath == null ? "" : titlePath;
    }

    public boolean hasSubItems(){
        JSONArray itemsArr = mJson.optJSONArray(ITEMS_KEY);
        return itemsArr != null && itemsArr.length() > 0;
    }

    public ArrayList<CheckableObject> getSubItems(){

        JSONArray jArr = mJson.optJSONArray("items");
        if (jArr == null){
            Timber.v("topic_dbg getSubItems got none");
            return null;
        }
        if (subItems == null){
            subItems = new ArrayList<>();
            for (int i = 0; i < jArr.length(); i++){
                JSONObject jsonObject = jArr.optJSONObject(i);
                if (jsonObject != null){
                    subItems.add(new CheckableObject(jsonObject, getSubItemIdPath(), getSubItemTitlePath()));
                }
            }
        }

        Timber.v("topic_dbg getSubItems returning list with %s items", subItems.size());
        return subItems;
    }

    @NonNull
    private String getSubItemIdPath(){
        return getId() + ".";
    }

    @NonNull
    private String getSubItemTitlePath(){
        return getFullTitle() + "\\";
    }

    @NonNull
    public String getTitle(){
        return mJson.optString(VALUE_KEY);
    }

    @NonNull
    public String getFullTitle(){
        return titlePath + mJson.optString(VALUE_KEY);
    }

    @NonNull
    public String getId(){
        return idPath + mJson.optString(ID_KEY);
    }

    @NonNull
    public String getIdPath(){
        return idPath;
    }

    @Override
    public String toString() {
        return mJson.toString() + " checked: " + isChecked + " path: " + idPath;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
