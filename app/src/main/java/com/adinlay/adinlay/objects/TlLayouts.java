package com.adinlay.adinlay.objects;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.adinlay.adinlay.AdLayoutsFrag;
import com.adinlay.adinlay.ItemClick;
import com.adinlay.adinlay.R;



public class TlLayouts extends TabLauncher {


    public TlLayouts(int position) {
        super(R.id.ep_layouts_container, R.id.ep_layouts_iv, R.drawable.ic_layouts_wt, R.drawable.ic_layouts_ylw,
                LAYOUTS_TAG, position, R.id.ep_layouts_tv);
    }

    @Override
    public void onTabClicked(Activity activity) {
    }

    @Override
    public Fragment getNewFragment() {
        return new AdLayoutsFrag();
    }

    @Override
    public Fragment getNewFragment(ItemClick callback) {
        AdLayoutsFrag frag = new AdLayoutsFrag();
        frag.setItemClickCallback(callback);
        return frag;
    }
}
