package com.adinlay.adinlay.objects;

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;


/**
 * Created by Noya on 7/2/2018.
 */

public class WalletEntry {

    private static final String TAG = WalletEntry.class.getSimpleName();

    public static final String TITLE_ENTRY_ID = "titleEntry";

    public static final String ID_KEY = "_id";
    public static final String POST_ID_KEY = "postId";
    public static final String STATUS_KEY = "status";
    public static final String BRAND_ID_KEY = "brandId";
    public static final String BRAND_NAME_KEY = "brandName";
    public static final String CREATED_DATE_KEY = "createdDate";
    public static final String AMOUNT_KEY = "amount";

    private JSONObject walletJson;

    public WalletEntry(JSONObject postJson) {
        this.walletJson = postJson == null ? new JSONObject() : postJson;
    }

    @NonNull
    public String getEntryId(){
        return walletJson.optString(ID_KEY);
    }

    @NonNull
    public String getPostId(){
        return walletJson.optString(POST_ID_KEY); // if missing, returns an empty string
    }

    @NonNull
    public String getStatus(){
        return walletJson.optString(STATUS_KEY);
    }

    public boolean isPending(){
        return "pending".equalsIgnoreCase(getStatus());
    }

    public boolean isBalanceAvailable(){
        return "done".equalsIgnoreCase(getStatus());
    }

    @NonNull
    public String getBrandId(){
        return walletJson.optString(BRAND_ID_KEY);
    }

    @NonNull
    public String getBrandName(){
        return walletJson.optString(BRAND_NAME_KEY);
    }

    @NonNull
    public String getDateString(){
        return walletJson.optString(CREATED_DATE_KEY); // 2018-06-25T19:36:33.632Z
    }

    public BigDecimal getAmountBigDec(){
        return BigDecimal.valueOf(walletJson.optDouble(AMOUNT_KEY, 0));
    }

    public String getJsonString(){
        return walletJson.toString();
    }

    @Override
    public String toString() {
        return walletJson.toString();
    }

    public static WalletEntry getTitleEntry(){
        JSONObject titleJsonObject  = new JSONObject();
        try {
            titleJsonObject.put(ID_KEY, TITLE_ENTRY_ID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new WalletEntry(titleJsonObject);
    }


}
