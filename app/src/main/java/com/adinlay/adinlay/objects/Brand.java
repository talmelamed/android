package com.adinlay.adinlay.objects;

import android.support.annotation.NonNull;

import org.json.JSONObject;

/**
 * Created by Noya on 7/2/2018.
 */

public class Brand {

    private static final String TAG = Brand.class.getSimpleName();

    public static final String ID_KEY = "_id";
    public static final String NAME_KEY = "name";
    public static final String LOGO_KEY = "logoUrl";
    public static final String LOGO_SMALL_KEY = "logoUrl_small";

    private JSONObject postJson;

    public Brand(JSONObject postJson) {
        this.postJson = postJson == null ? new JSONObject() : postJson;
    }

    @NonNull
    public String getLogoUrl(){
        return postJson.optString(LOGO_KEY); // if missing, returns an empty string
    }

    @NonNull
    public String getSmallestLogoUrl(){
        return postJson.optString(LOGO_SMALL_KEY, getLogoUrl());
    }

    @NonNull
    public String getId(){
        return postJson.optString(ID_KEY);
    }

    @NonNull
    public String getName(){
        return postJson.optString(NAME_KEY); // 2018-06-25T19:36:33.632Z
    }

    public String getJsonString(){
        return postJson.toString();
    }

    @Override
    public String toString() {
        return postJson.toString();
    }

}
