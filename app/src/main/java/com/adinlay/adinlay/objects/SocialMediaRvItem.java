package com.adinlay.adinlay.objects;

import android.support.annotation.NonNull;

import com.adinlay.adinlay.AdinlayApp;

public class SocialMediaRvItem {

    public static final int SOCIAL_MEDIA_IG = 1;
    public static final int SOCIAL_MEDIA_FB = 2;

    private int iconOnResId;
    private int iconOffResId;
    private String name;
    private int media;

    public SocialMediaRvItem(int media, String name, int iconOnResId, int iconOffResId) {
        this.media = media;
        this.name = name == null ? "" : name;
        this.iconOnResId = iconOnResId;
        this.iconOffResId = iconOffResId;
    }

    public int getIconOnResId() {
        return iconOnResId;
    }

    public int getIconOffResId() {
        return iconOffResId;
    }

    public String getName() {
        return name;
    }

    public int getMedia() {
        return media;
    }


    public boolean isConnected(){
        switch (media){
            case SOCIAL_MEDIA_IG:
                return AdinlayApp.getUser() != null && AdinlayApp.getUser().isConnectedToInstagram();
            case SOCIAL_MEDIA_FB:
                return AdinlayApp.getUser() != null && AdinlayApp.getUser().isConnectedToFacebook();
            default:
                return false;
        }
    }

    @NonNull
    public String getDisplayName(){
        String displayName = "";
        switch (media){
            case SOCIAL_MEDIA_IG:
                displayName = AdinlayApp.getUser() != null ? AdinlayApp.getUser().getIgUsername() : name;
                break;
            case SOCIAL_MEDIA_FB:
                displayName = AdinlayApp.getUser() != null ? AdinlayApp.getUser().getFbUsername() : name;
                break;
        }
        return displayName;
    }

    public int getFollowersNum(){
        int num = 0;
        switch (media){
            case SOCIAL_MEDIA_IG:
                num = AdinlayApp.getUser() != null ? AdinlayApp.getUser().getInstaFollowers() : num;
                break;
            case SOCIAL_MEDIA_FB:
                num = AdinlayApp.getUser() != null ? AdinlayApp.getUser().getFacebookFriends() : num;
                break;
        }
        return num;
    }
}