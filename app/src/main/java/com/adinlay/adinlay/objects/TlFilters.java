package com.adinlay.adinlay.objects;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.adinlay.adinlay.FiltersFrag;
import com.adinlay.adinlay.ItemClick;
import com.adinlay.adinlay.R;


public class TlFilters extends TabLauncher {


    public TlFilters(int position) {
        super(R.id.ep_filters_container, R.id.ep_filters_iv, R.drawable.ic_filters_wt, R.drawable.ic_filters_ylw,
                FILTERS_TAG, position, R.id.ep_filters_tv);
    }

    @Override
    public void onTabClicked(Activity activity) {
    }

    @Override
    public Fragment getNewFragment() {
        return new FiltersFrag();
    }

    @Override
    public Fragment getNewFragment(ItemClick callback) {
        FiltersFrag frag = new FiltersFrag();
        frag.setItemClickCallback(callback);
        return frag;
    }
}
