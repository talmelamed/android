package com.adinlay.adinlay.objects;

import android.app.Activity;
import android.support.v4.app.Fragment;

import com.adinlay.adinlay.BrandsFrag;
import com.adinlay.adinlay.R;

/**
 * Created by Noya on 3/29/2018.
 */

public class TlAdvertisers extends TabLauncher {


    public TlAdvertisers(int position) {
        super(R.id.advertisers_container, R.id.advertisers_iv, R.drawable.brands_unselected, R.drawable.brands_selected,
                ADVERTISERS_TAG, position, R.id.brands_tv);
    }

    @Override
    public void onTabClicked(Activity activity) {
//        Toast.makeText(activity.getApplicationContext(),
//                "Advertisers tab - coming soon!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public Fragment getNewFragment() {
        return new BrandsFrag();
    }
}
