package com.adinlay.adinlay.objects;

import android.support.annotation.NonNull;

import org.json.JSONObject;

import java.math.BigDecimal;


/**
 * Created by Noya on 1/12/2019.
 */

public class ApprovedPost {

//    public static final String ID_KEY = "_id";

    private JSONObject objectJson;

    public ApprovedPost(@NonNull JSONObject objectJson) {
        this.objectJson = objectJson;
    }

//    @NonNull
//    public String getItemId(){
//        return objectJson.optString(ID_KEY);
//    }

    @NonNull
    public String getAdinlayPostId(){
        return objectJson.optString("postId");
    }

    @NonNull
    public String getThumbnailUrl(){
        return objectJson.optString("photoUrl_small");
    }

    @NonNull
    public String getImageUrl(){
        return objectJson.optString("photoUrl");
    }

    @NonNull
    public String getShareUrl(){
        return objectJson.optString("pageUrl");
    }

    @NonNull
    public String getExpitrationDateString(){
        return objectJson.optString("expirationDate");
    }

    public boolean hasOffers(){
        return !objectJson.optString("offeringUrl").isEmpty();
    }

    public boolean isFcbk(){ // todo fb
        return true;
    }

    @NonNull
    public BigDecimal getAmountBigDec(String whichKey){
        JSONObject priceJson = objectJson.optJSONObject(Ad.PRICE_KEY);
        return priceJson == null ? BigDecimal.valueOf(0) :
                BigDecimal.valueOf(priceJson.optDouble(whichKey, 0));
    }

    @NonNull
    public String getAddedTag(){
        return Ad.getFcbkHashTag(objectJson.optString("hashTag"));
    }

}
