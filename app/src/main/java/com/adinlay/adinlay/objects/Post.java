package com.adinlay.adinlay.objects;

import android.support.annotation.NonNull;

import org.json.JSONObject;

import java.math.BigDecimal;

/**
 * Created by Noya on 7/2/2018.
 */

public class Post {

    private static final String TAG = Post.class.getSimpleName();

    public static final String ID_KEY = "_id";
    public static final String PHOTO_KEY = "photoUrl";
    public static final String PHOTO_SMALL_KEY = "photoUrl_small";
    public static final String POST_DATE_KEY = "postDate";
    public static final String TEXT_KEY = "text";
    public static final String PRICE_KEY = "price";
    public static final String DISPLAY_NAME_KEY = "displayName";
    public static final String USER_ID_KEY = "userId";
    public static final String POST_ID_KEY = "postId";
    public static final String MEDIA_ID_KEY = "mediaId";
    public static final String AD_ID_KEY = "adId";
    public static final String ENGAGEMENT_KEY = "engagement"; // {"engagement":{"likeCount":0,"commentCount":0,"processDate":"2018-07-19T00:00:40.134Z"}
    public static final String LIKES_COUNT_KEY = "likeCount";
    public static final String BRAND_ID_KEY = "brandId";
    public static final String OFFER_URL_KEY = "offeringUrl";
    public static final String PLATFORM_KEY = "platform";

    public static final String PLATFORM_FACEBOOK = "facebook";
    public static final String PLATFORM_INSTAGRAM = "instagram";

    public static final int BIG_INST_SIZE = 0;
    public static final int SMALL_INST_SIZE = 1;
    public static final int FB_SIZE = 2;

    private JSONObject postJson;

    private boolean showingAllText = false;
    private int mosaicSize = -1;

    public Post(JSONObject postJson) {
        this.postJson = postJson == null ? new JSONObject() : postJson;
    }

    @NonNull
    public String getPhotoUrl(){
        return postJson.optString(PHOTO_KEY); // if missing, returns an empty string
    }

    @NonNull
    public String getThmbnailUrl(){
        return postJson.optString(PHOTO_SMALL_KEY);
    }

    @NonNull
    public String getPostId(){
        return postJson.optString(ID_KEY);
    }

    @NonNull
    public String getDateString(){
        return postJson.optString(POST_DATE_KEY); // 2018-06-25T19:36:33.632Z
    }

    @NonNull
    public String getDisplayName(){
        return postJson.optString(DISPLAY_NAME_KEY);
    }

    @NonNull
    public String getText(){
        return postJson.optString(TEXT_KEY);
    }

    @NonNull
    public String getOfferLink(){
        return postJson.optString(OFFER_URL_KEY);
    }

    public BigDecimal getPrice(){
        return BigDecimal.valueOf(postJson.optDouble(PRICE_KEY, 0));
    }

    public int getLikes(){
        JSONObject engagement = postJson.optJSONObject(ENGAGEMENT_KEY);
        if (engagement != null){
            return engagement.optInt(LIKES_COUNT_KEY);
        }
        return 0;
    }

    @NonNull
    public String getBrandId(){
        return postJson.optString(BRAND_ID_KEY);
    }

    public boolean checkPlatform(@NonNull String platform){
        return postJson.optString(PLATFORM_KEY).equals(platform);
    }

    public boolean isFacebookLinkSize(){
        if (checkPlatform(PLATFORM_FACEBOOK)){
            JSONObject sizeInfo = postJson.optJSONObject("imageSize");
            if (sizeInfo != null){
                int dif = sizeInfo.optInt("width") - sizeInfo.optInt("height");
                if (dif == 0 || (sizeInfo.optInt("width") >= 100 && dif < 5 && dif > - 5)){ // up to 4 pixel dif, we consider it square
                    return false;
                }
            }
            return true; // backward compatibility, if no info, it's link dimens
        }
        return false;
    }

    public String getJsonString(){
        return postJson.toString();
    }

    @Override
    public String toString() {
        return postJson.toString();
    }

    public boolean isShowingAllText() {
        return showingAllText;
    }

    public void setShowingAllText(boolean showingAllText) {
        this.showingAllText = showingAllText;
    }

    public int getMosaicSize() {
        return mosaicSize;
    }

    public void setMosaicSize(int mosaicSize) {
        this.mosaicSize = mosaicSize;
    }
}
