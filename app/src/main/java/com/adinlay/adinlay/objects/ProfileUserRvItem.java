package com.adinlay.adinlay.objects;

import com.adinlay.adinlay.R;
import com.adinlay.adinlay.SignInManager;

public class ProfileUserRvItem extends SettingsRvItem {

    private ProfileTopic topic = null;
    private int topicPos = -1;
    private String loginStage = SignInManager.COMPLETE_STAGE;
    private int imageRes = 0;

    public ProfileUserRvItem(int nameResId, int buttonResId) {
        super(nameResId, buttonResId);
    }

    public ProfileUserRvItem(int nameResId, int buttonResId, int imageRes) {
        super(nameResId, buttonResId);
        this.imageRes = imageRes;
    }

    public ProfileUserRvItem(ProfileTopic topic, int topicPos, String loginStage) {
        super(R.string.preferences_allcaps, R.drawable.button_edit);
        this.topic = topic;
        this.topicPos = topicPos;
        this.loginStage = loginStage;
    }

    public ProfileTopic getTopic() {
        return topic;
    }

    public int getTopicPos() {
        return topicPos;
    }

    public String getLoginStage() {
        return loginStage;
    }

    public int getImageRes() {
        return imageRes;
    }

    public boolean isTopicItem(){
        return topic != null;
    }
}
