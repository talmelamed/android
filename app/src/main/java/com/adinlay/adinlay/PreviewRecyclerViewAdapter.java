package com.adinlay.adinlay;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.objects.Ad;
import com.adinlay.adinlay.objects.OverlayInfo;
import com.adinlay.adinlay.objects.ServerImageObject;
import com.bumptech.glide.load.resource.bitmap.CenterInside;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by Noya on 7/31/2018.
 */

public class PreviewRecyclerViewAdapter extends RecyclerView.Adapter<PreviewRecyclerViewAdapter.ViewHolder>{

    private final static String TAG = PreviewRecyclerViewAdapter.class.getSimpleName();

    private String imagePath;
    private ArrayList<ServerImageObject> adLayouts;
    private Ad ad;
    private GlideRequest<Drawable> requestBuilder;
    private WeakReference<ItemClick> wr_itemClickCallback;
    private int spanCount;
    private int cellSize = 0;

    private final int REGULAR = 1;
    private final int PLACEHOLDER = 2;

    public PreviewRecyclerViewAdapter(String imagePath, Ad ad, int spanCount, GlideRequests glideRequests, ItemClick itemClickCallback) {
        this.imagePath = imagePath;
        this.ad = ad != null ? ad : new Ad(null);
        adLayouts = ad != null ? ad.getLayoutUrlObjects() : new ArrayList<ServerImageObject>();
        requestBuilder = glideRequests.asDrawable().transforms(new CenterInside());
        wr_itemClickCallback = new WeakReference<ItemClick>(itemClickCallback);
        this.spanCount = spanCount;
        finalizeList();
    }

    public  void updateImagePath(@NonNull String path){
        imagePath = path;
        if (!path.equals(imagePath)){
        }
    }

    private void finalizeList(){
        if (adLayouts.size() > 2*spanCount){
            int toAdd = adLayouts.size() % spanCount == 0 ? 3*spanCount : 4*spanCount - adLayouts.size() % spanCount;
            for (int i = 0; i < toAdd; i++){
                adLayouts.add(new ServerImageObject(true));
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        ServerImageObject adLayoutObject = adLayouts.get(position);
        return adLayoutObject == null || adLayoutObject.isPlaceholder() ? PLACEHOLDER : REGULAR;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post_preview, parent, false);

//        Timber.d("parent height: %s width: %s", parent.getMeasuredHeight(), parent.getMeasuredWidth());
        GridLayoutManager.LayoutParams lp = (GridLayoutManager.LayoutParams) root.getLayoutParams();
        lp.height = parent.getMeasuredWidth() / spanCount;

        if (cellSize <=0){
            cellSize = parent.getMeasuredWidth() / spanCount;
        }

        return viewType == REGULAR ? new ObjectViewHolder(root) : new PlaceholderVH(root);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        ServerImageObject adLayoutObject = adLayouts.get(position);
        String adUrl = adLayoutObject.getUrl().isEmpty() ? adLayoutObject.getDefaultUrl() : adLayoutObject.getUrl();

        Object taggedObject = holder.itemView.getTag(R.id.object_tag);
        if (taggedObject instanceof String  && !((String) taggedObject).isEmpty() &&
                taggedObject.equals(adUrl)){
            // same image, no need to redraw
            holder.itemView.setTag(R.id.position_tag, position);
            return;
        }

        if (holder instanceof PlaceholderVH){
            ((PreviewRecyclerViewAdapter.PlaceholderVH) holder).border.setVisibility(View.GONE);
        }

        if (holder instanceof ObjectViewHolder) {
            ObjectViewHolder objectHolder = (ObjectViewHolder) holder;
            GlideRequest<Drawable> requestBuilderImage = requestBuilder.clone()
                    .load(imagePath)
                    .skipMemoryCache(true)
                    .thumbnail(requestBuilder.clone().load(imagePath));
            GlideRequest<Drawable> requestBuilderAdLayout = requestBuilder.clone()
                    .load(adLayoutObject.getDefaultUrl()).skipMemoryCache(true)/*.thumbnail(0.2f)*/;
            String oldUrl = taggedObject instanceof String ? (String) taggedObject : "";
            if (oldUrl.isEmpty()){
                requestBuilderAdLayout = requestBuilderAdLayout.thumbnail(0.2f);
            }else {
                requestBuilderAdLayout = requestBuilderAdLayout.thumbnail(requestBuilder.clone().load(oldUrl));
            }
            if (cellSize > 0){
                requestBuilderImage = requestBuilderImage.override(cellSize, cellSize);
                requestBuilderAdLayout = requestBuilderAdLayout.override(cellSize, cellSize);
            }
            requestBuilderImage.into(objectHolder.photoIv);
            requestBuilderAdLayout.into(objectHolder.adIv);
            objectHolder.adIv.setBackgroundColor(objectHolder.adIv.getResources().getColor(R.color.black_thirty_percent));
            holder.itemView.setOnClickListener(itemClickListener);
        }

        holder.itemView.setTag(R.id.object_tag, adUrl);
        holder.itemView.setTag(R.id.position_tag, position);

    }

    @Override
    public int getItemCount() {
        return adLayouts.size();
    }

    private View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = -1;
            if (v.getTag(R.id.position_tag) instanceof Integer && v.getTag(R.id.position_tag) != null){
                position = (int) v.getTag(R.id.position_tag);
                ad.setSelectedLayoutPos(position);
            }
            if (wr_itemClickCallback.get() != null){
                Object object = v.getTag(R.id.object_tag);
                if (object instanceof String){
                    OverlayInfo overlayInfo = new OverlayInfo((String) object, position);
                    wr_itemClickCallback.get().onItemClicked(overlayInfo);
                }
            }
        }
    };

    public static class ViewHolder extends RecyclerView.ViewHolder{


        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    public static class ObjectViewHolder extends ViewHolder{

        ImageView photoIv;
        ImageView adIv;
        TextView titleTv;
        View border;

        public ObjectViewHolder(View itemView) {
            super(itemView);

            photoIv = itemView.findViewById(R.id.preview_photo_iv);
            adIv = itemView.findViewById(R.id.preview_ad_iv);
            titleTv = itemView.findViewById(R.id.preview_tv);
            border = itemView.findViewById(R.id.preview_border);
        }
    }

    public static class PlaceholderVH extends ViewHolder{

        View border;

        public PlaceholderVH(View itemView) {
            super(itemView);

            border = itemView.findViewById(R.id.preview_border);
        }
    }
}
