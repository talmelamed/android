package com.adinlay.adinlay;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.adinlay.adinlay.objects.CheckableObject;
import com.adinlay.adinlay.objects.ProfileTopic;
import com.adinlay.adinlay.objects.SocialMediaRvItem;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.ArrayList;
import java.util.Arrays;

import timber.log.Timber;

/**
 * Created by Noya on 4/12/2018.
 */

public class SocialMediaFrag extends Fragment implements ItemClick {

    private MainViewModel model = null;

    private SocMedRvAdapter adapter;

    CallbackManager fcbkCallbackManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_social_media, container, false);

        if (model == null && getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        }

        RecyclerView recyclerView = root.findViewById(R.id.fsm_recycler);

        model.getPingInstagramLogout().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (s == null){
                    return;
                } else {
                    model.getPingInstagramLogout().postValue(null);
                }
                refreshConnections();
                if (!s.equals(MainViewModel.LOGOUT_INSTAGRAM_OK) && getActivity() != null){
                    String message = s.replace(MainViewModel.LOGOUT_MEDIA_FAIL_PREFIX, "");
                    if (message.isEmpty()){
                        message = getString(R.string.something_wrong_inst_logout);
                    }
                    showErrorDialog(message);
                }
            }
        });

        fcbkCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(fcbkCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Timber.i("FacebookCallback onSuccess %s", loginResult);
                Timber.i("FacebookCallback onSuccess accessToken.token    %s", loginResult.getAccessToken().getToken());
                // send server the token, get number of followers:
                model.getSpinnerLiveData().postValue(true);
                model.connectToFcbk(loginResult.getAccessToken().getToken());

//                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
//                    @Override
//                    public void onCompleted(JSONObject object, GraphResponse response) {
//                        Timber.i("FacebookCallback meRequest: %s", object); // DEV: {"id":"205159763704535","name":"Mz-g Motional","email":"ms.geny.motion111@gmail.com"}
//                        Timber.i("FacebookCallback meRequest response: %s", response); // DEV: {Response:  responseCode: 200, graphObject: {"id":"205159763704535","name":"Mz-g Motional","email":"ms.geny.motion111@gmail.com"}, error: null}
//                    }
//                });
//                Bundle parameters = new Bundle();
//                parameters.putString("fields", "id,name,email");
//                request.setParameters(parameters);
//                request.executeAsync();

//                String graphPath = "/"+ loginResult.getAccessToken().getUserId() + "/friends";
//                GraphRequest requestFriends = GraphRequest.newGraphPathRequest(
//                        loginResult.getAccessToken(),
//                        graphPath,
//                        new GraphRequest.Callback() {
//                            @Override
//                            public void onCompleted(GraphResponse response) {
//                                // Insert your code here
//                                Timber.i("FacebookCallback graphPath response: %s", response); // here I had some weird info, including Tal's name (?!?) but also the correct number of friends
//                                // for dev: graphPath response: {Response:  responseCode: 200, graphObject: {"data":[]}, error: null}
//                            }
//                        });
//
//                requestFriends.executeAsync();
            }

            @Override
            public void onCancel() {
                model.getSpinnerLiveData().setValue(false);
                Timber.v("FacebookCallback onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Timber.w(error, "FacebookCallback onError");
                model.getSpinnerLiveData().setValue(false);

                //  fb handles this?
            }
        });


        model.getPingFcbkLogin().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (s == null){
                    return;
                } else {
                    model.getPingFcbkLogin().postValue(null);
                }

                refreshConnections();
                if (s.equals(MainViewModel.LOGOUT_FCBK_OK)){
                    LoginManager.getInstance().logOut();
                } else if (!s.equals(MainViewModel.FETCH_POSTS_OK) && getActivity() != null && !s.isEmpty()){
                    String message = s.replace(MainViewModel.LOGIN_FCBK_FAIL_PREFIX, "");
                    if (message.isEmpty()){
                        message = getString(R.string.something_wrong_fcbk_connect);
                    } else {
                        message = message.replace(MainViewModel.LOGOUT_MEDIA_FAIL_PREFIX, "");
                        if (message.isEmpty()){
                            message = getString(R.string.something_wrong_fcbk_logout);
                        }
                    }

                    showErrorDialog(message);
                }
            }
        });

        ProfileTopic socialMediaTopic = AdinlayApp.getSocialMediaSettings();
        ArrayList<CheckableObject> socMedItems = socialMediaTopic.getItems(false);
        boolean hasInst = false;
        boolean hasFcbk = false;
        for (CheckableObject item: socMedItems){
            String itemId = item.getId();
            hasInst = hasInst || "instagram".equalsIgnoreCase(itemId);
            hasFcbk = hasFcbk || "facebook".equalsIgnoreCase(itemId);
        }

        ArrayList<SocialMediaRvItem> items = new ArrayList<>();
        if (hasInst) {
            items.add(new SocialMediaRvItem(SocialMediaRvItem.SOCIAL_MEDIA_IG, "Instagram", R.drawable.inst_share, R.drawable.inst_share));
        }
        if (hasFcbk) {
            items.add(new SocialMediaRvItem(SocialMediaRvItem.SOCIAL_MEDIA_FB, "Facebook", R.drawable.facebook_share, R.drawable.facebook_share));
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new SocMedRvAdapter(items, this, false);
        recyclerView.setAdapter(adapter);

        return root;
    }

    private void refreshConnections(){
        if (adapter != null){
            adapter.notifyDataSetChanged();
        }
    }

    private void showErrorDialog(String message){
        Activity activity = getActivity();
        if (activity == null){
            return;
        }
        Bundle args = new Bundle();
        args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.error);
        args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, message);
        args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
        AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
        if (!activity.isFinishing() && isAdded()) {
            AyUtils.showDialogFragment(dialogFragment, getFragmentManager());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        fcbkCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshConnections();
    }

    @Override
    public void onItemClicked(Object object) {
        if (object instanceof Integer){
            switch ((Integer)object){
                case SocialMediaRvItem.SOCIAL_MEDIA_IG:
                    onClickIg();
                    break;
                case SocialMediaRvItem.SOCIAL_MEDIA_FB:
                    onClickFb();
                    break;
                case -1 * SocialMediaRvItem.SOCIAL_MEDIA_IG: // prefs
                case -1 * SocialMediaRvItem.SOCIAL_MEDIA_FB: // prefs
                    if (getActivity() != null){
                        // make sure we have the relevant info:
                        User user = AdinlayApp.getUser();
                        if (user != null){
                            String mediaId = ((Integer)object) == -1 * SocialMediaRvItem.SOCIAL_MEDIA_IG ?
                                    user.getIgUserId() : user.getFbUserId();
                            if (!user.hasSocialMediaProfile(mediaId)){
                                // oops! let's try to fetch the userInfo again
                                model.refetchUserJson();
                            }
                        }

                        Intent mediaTopicsIntent = new Intent(getActivity(), MediaPrefsListActivity.class);
                        int mediaVal = ((Integer)object) == -1 * SocialMediaRvItem.SOCIAL_MEDIA_IG ?
                                SocialMediaRvItem.SOCIAL_MEDIA_IG : SocialMediaRvItem.SOCIAL_MEDIA_FB;
                        mediaTopicsIntent.putExtra("media", mediaVal);
                        startActivity(mediaTopicsIntent);
                    }
                    break;
            }
        }
    }

    private void onClickIg(){
        if (AdinlayApp.getUser() != null) {
            if (AdinlayApp.getUser().isConnectedToInstagram()) {
                Bundle args = new Bundle();
                args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.logout_instagram_camel);
                args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.cancel);
                args.putInt(AyDialogFragment.ARG_BUTTON_R_RES, R.string.logout);
                AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                dialogFragment.setListener(AyDialogFragment.VALUE_RIGHT, new AyDialogFragment.AyClickListener() {
                    @Override
                    public void onButtonClicked(View v) {
                        model.logoutFromInstagram();
                    }
                });
                AyUtils.showDialogFragment(dialogFragment, getFragmentManager());
            } else {
                Intent intent = new Intent(getActivity(), ConnectToInstagramActivity.class);
                startActivityForResult(intent, AyUtils.get16BitId(R.id.intent_instagram_signin));
            }
        }
    }

    private void onClickFb(){
        User user = AdinlayApp.getUser();
        if (user != null){
            if (user.isConnectedToFacebook()){
                // if fcbk is the login method - do nothing
                if (User.PROVIDER_FACEBOOK.equals(user.getLoginProvider())){
                    Timber.d("not letting user to disconnect from facebook - the login method!");
                    return;
                }

                Bundle args = new Bundle();
                args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.logout_fcbk_camel);
                args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.cancel);
                args.putInt(AyDialogFragment.ARG_BUTTON_R_RES, R.string.logout);
                AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                dialogFragment.setListener(AyDialogFragment.VALUE_RIGHT, new AyDialogFragment.AyClickListener() {
                    @Override
                    public void onButtonClicked(View v) {
                        model.logoutFcbk();
                    }
                });
                AyUtils.showDialogFragment(dialogFragment, getFragmentManager());
            } else {
                // send call to facebook.loginManager to sign-in:
                LoginManager.getInstance().logInWithReadPermissions(SocialMediaFrag.this, Arrays.asList("public_profile", "email", "user_friends", "user_posts"));
            }
        }
    }
}
