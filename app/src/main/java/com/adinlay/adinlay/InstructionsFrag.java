package com.adinlay.adinlay;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adinlay.adinlay.objects.Ad;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Noya on 8/8/2018.
 */

public class InstructionsFrag extends Fragment implements ItemClick {

    private static final String TAG = InstructionsFrag.class.getSimpleName();
    public final int DELAY = 6000;

    private WeakReference<ItemClick> wr_itemClick = new WeakReference<ItemClick>(null);
    private Handler handler = null;
    private ArrayList<String> urls = new ArrayList<>();
    private int instructionPosition = 0;
    private PagerAdapter pagerAdapter;
    private ViewPager viewPager;
    private boolean isCircular = false;
    private View acceptButton = null;
    private View loadingTV = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.item_insructions, container, false);

        root.findViewById(R.id.instrctns_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    getActivity().finish();
                }
            }
        });

        loadingTV = root.findViewById(R.id.instrctns_loading_tv);
        acceptButton = root.findViewById(R.id.instrctns_accept);
        if (acceptButton != null){
            acceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ItemClick clicker = wr_itemClick.get();
                    if (clicker != null){
                        clicker.onItemClicked(null);
                    }
                }
            });
            acceptButton.setEnabled(false);
            acceptButton.setAlpha(0.5f);
        }

        Ad selectedAd = AdinlayApp.getSelectedAd();

        urls = selectedAd == null ?
                new ArrayList<String>() : selectedAd.getInstructionsUrls();

        handler = new Handler();
        viewPager = root.findViewById(R.id.instrctns_viewpager);

        if (urls.isEmpty()){ // shouldn't happen
            ItemClick clicker = wr_itemClick.get();
            if (clicker != null){
                clicker.onItemClicked(null); // this should close the frag
            }
//            Timber.v("NSTR_DBG EMPTY urls size: %s", urls.size());
            return root;
        }
//        Timber.v("NSTR_DBG urls size: %s", urls.size());

        root.findViewById(R.id.instrctns_frame).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do nothing - prevent clicks under this view
            }
        });

        isCircular = urls.size() > 1;
        if (isCircular){
            String fakeFirstPage = urls.get(urls.size() - 1);
            urls.add(0, fakeFirstPage); // adding the last url at the beginning of the list
            String fakeLastPage = urls.get(0);
            urls.add(fakeLastPage); // adding the first url at the end of the list
        }
        pagerAdapter = new InstructionsAdapter(urls, GlideApp.with(InstructionsFrag.this), this);
        viewPager.setAdapter(pagerAdapter);
        if (isCircular) {
            instructionPosition = 1;
            viewPager.setCurrentItem(instructionPosition);
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {

                    int actualPosition = position;
                    if (position == 0){
                        // position 0 is actually copy of last page, need to go to last page:
                        actualPosition = (pagerAdapter.getCount() - 2 >= 0) ? pagerAdapter.getCount() - 2 :
                                (pagerAdapter.getCount() - 1 >= 0) ? pagerAdapter.getCount() - 1 : 0;
                    } else if (position == pagerAdapter.getCount() - 1){
                        // last position is a copy of the actual first position, need to go to position 1:
                        actualPosition = 1;
                    }

                    if (actualPosition != position){
                        final int f_position = actualPosition;
                        viewPager.post(new Runnable() {
                            @Override
                            public void run() {
                                viewPager.setCurrentItem(f_position, false);
                            }
                        });
                    }
                    Timber.d("NSTR_DBG onPageSelected %s actualPosition: %s", position, actualPosition);
                    instructionPosition = actualPosition;
                }

                @Override
                public void onPageScrollStateChanged(int state) {
//                    Timber.i("NSTR_DBG state: %s", state);
                    if (state == ViewPager.SCROLL_STATE_DRAGGING && handler != null){
                        handler.removeCallbacks(imageChangerRunnable);
                    }
                }
            });

            root.findViewById(R.id.instrctns_next).setOnClickListener(switchClickListener);
            root.findViewById(R.id.instrctns_previous).setOnClickListener(switchClickListener);
        }
//        if (viewPager instanceof NoSwipeViewPager){
//            ((NoSwipeViewPager) viewPager).setSwipeEnabled(false);
//        }

        return root;
    }

    public void setItemClickCallback(ItemClick callback){
        wr_itemClick = new WeakReference<ItemClick>(callback);
    }

    View.OnClickListener switchClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            handler.removeCallbacks(imageChangerRunnable);
            if (!isCircular ||
                    !(v.getId() == R.id.instrctns_next || v.getId() == R.id.instrctns_previous)){
                return;
            }
            if (v.getId() == R.id.instrctns_next){
                if (instructionPosition >= pagerAdapter.getCount() - 1){
                    instructionPosition = 1;
                } else {
                    instructionPosition++;
                }
            } else {
                if (instructionPosition == 0){
                    instructionPosition = pagerAdapter.getCount() - 2;
                } else {
                    instructionPosition--;
                }
            }
            instructionPosition = instructionPosition < 0 ? 0 :
                    instructionPosition > pagerAdapter.getCount() - 1 ? pagerAdapter.getCount() - 1 :
                            instructionPosition;
            viewPager.setCurrentItem(instructionPosition, true);

        }
    };

    Runnable imageChangerRunnable = new Runnable() {
        @Override
        public void run() {

            if (instructionPosition >= pagerAdapter.getCount() - 1){
                instructionPosition = 1;
            } else {
                instructionPosition++;
            }

            viewPager.setCurrentItem(instructionPosition, true);

            handler.postDelayed(this, DELAY);

        }
    };

    @Override
    public void onResume() {
        super.onResume();

        if (urls.isEmpty()){
            ItemClick clicker = wr_itemClick.get();
            if (clicker != null){
                clicker.onItemClicked(null); // this should close the frag
            }
        } else {
            if (handler == null) {
                handler = new Handler();
            }
            handler.postDelayed(imageChangerRunnable, DELAY);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(imageChangerRunnable);
    }

    @Override
    public void onItemClicked(Object object) {
        if (object instanceof Boolean && (Boolean) object){
            acceptButton.setAlpha(1f);
            acceptButton.setEnabled(true);
        } else {
            // got error fetching instructions, should quit frag
            if (loadingTV != null){
                loadingTV.setVisibility(View.GONE);
            }
            if (getActivity() != null && isAdded()) {
                ItemClick clicker = wr_itemClick.get();
                if (clicker != null){
                    clicker.onItemClicked("");
                }
            }
        }
    }
}
