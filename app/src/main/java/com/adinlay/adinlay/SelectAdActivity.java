package com.adinlay.adinlay;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.constraint.motion.MotionLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.adinlay.adinlay.objects.Ad;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Noya on 7/25/2018.
 */

public class SelectAdActivity extends AppCompatActivity implements ItemClick{

    private static final String TAG = SelectAdActivity.class.getSimpleName();

    private ImageView mainIv = null;
    private RecyclerView recyclerView = null;
    private MotionLayout motionLayout;

    private GridLayoutManager layoutManager = null;

    SelectedAdViewModel viewModel = null;
    AdsRecyclerViewAdapter adapter = null;

    private String imagePath = null;
    private GlideRequests gr = null;

    public static int MISSING_MEDIA_RESULT = 23;

    private static final int SPAN_COUNT = 3;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_hub_start);

        setSupportActionBar(null);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= 23){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
        }

        // toolbar:
        findViewById(R.id.toolbar_right_tv).setVisibility(View.GONE);
        TextView btn1 = findViewById(R.id.toolbar_left_tv);
        btn1.setText(getResources().getString(R.string.arrow_and_back));
        btn1.setTextColor(Color.BLACK);
        findViewById(R.id.toolbar_left_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.toolbar_right_iv).setVisibility(View.VISIBLE);
        findViewById(R.id.toolbar_right_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });


        mainIv = findViewById(R.id.photo_hub_main_iv);
        recyclerView = findViewById(R.id.photo_hub_recycler);
        layoutManager = new GridLayoutManager(this, SPAN_COUNT);
        recyclerView.setLayoutManager(layoutManager);
        motionLayout = findViewById(R.id.photo_hub_layout_parent);

        viewModel = ViewModelProviders.of(this).get(SelectedAdViewModel.class);

        gr = GlideApp.with(SelectAdActivity.this);

        findViewById(R.id.photo_hub_next).setVisibility(View.INVISIBLE);
        findViewById(R.id.photo_hub_camera).setVisibility(View.GONE);
        findViewById(R.id.photo_hub_tab_bg).setVisibility(View.GONE);
        findViewById(R.id.photos_tab_btn).setVisibility(View.GONE);
        findViewById(R.id.challenge_tab_btn).setVisibility(View.GONE);
        findViewById(R.id.approved_tab_btn).setVisibility(View.GONE);
        findViewById(R.id.photo_hub_ads_shadow).setVisibility(View.VISIBLE);

        recyclerView.setPadding(0, 0, 0, 0);
        recyclerView.setHasFixedSize(true);

        Intent intent = getIntent();
        imagePath = intent.getStringExtra(PhotoHubActivity.SELECTED_IMAGE_PATH);
        if (imagePath == null){
            finish();
            return;
        }

        if (AyUtils.getMinRecyclerHeightDp() > 0){
            ConstraintLayout.LayoutParams recyclerLP = (ConstraintLayout.LayoutParams) recyclerView.getLayoutParams();
            int rvH = AyUtils.convertDpToPixel(AyUtils.getMinRecyclerHeightDp(), SelectAdActivity.this);
            recyclerLP.height = rvH;
            ConstraintLayout.LayoutParams lpIv = (ConstraintLayout.LayoutParams) mainIv.getLayoutParams();
            lpIv.dimensionRatio = "w,1:1";
            lpIv.bottomToTop = R.id.photo_hub_recycler;
            Timber.d("MAIN_RESIZE changed iv lp");
            recyclerView.requestLayout();
            mainIv.requestLayout();
            ConstraintSet startConstraintSet = motionLayout.getConstraintSet(R.layout.photo_hub_start);
            startConstraintSet.constrainHeight(R.id.photo_hub_recycler, rvH);
            startConstraintSet.setDimensionRatio(R.id.photo_hub_main_iv, "w,1:1");
            startConstraintSet.connect(R.id.photo_hub_main_iv, ConstraintSet.BOTTOM, R.id.photo_hub_recycler, ConstraintSet.TOP);
//            ConstraintSet endConstraintSet = motionLayout.getConstraintSet(R.layout.photo_hub_end); // changes to end set don't seem to have any affect
//            endConstraintSet.constrainMinHeight(R.id.photo_hub_recycler, rvH);
//            endConstraintSet.setDimensionRatio(R.id.photo_hub_main_iv, "w,1:1");
//            endConstraintSet.connect(R.id.photo_hub_main_iv, ConstraintSet.BOTTOM, R.id.photo_hub_recycler, ConstraintSet.TOP);
        }

        GlideApp.with(this)
                .load(imagePath)
                .centerCrop()
                .into(mainIv);


        viewModel.getPingAds().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (SelectedAdViewModel.FETCH_OBJECTS_OK.equals(s)){
                    ArrayList<Ad> ads = viewModel.getAdsArrayList();
                    if (ads == null){ // shouldn't happen!
                        ads = new ArrayList<>();
                    }
                    adapter = new AdsRecyclerViewAdapter(ads, gr, SelectAdActivity.this);
                    recyclerView.setAdapter(adapter);
                    // disable transition if not enough items:
                    if (ads.size() <= 2*SPAN_COUNT) {
                        motionLayout.setTransition(R.layout.photo_hub_start, R.layout.photo_hub_start);
                    } else {
                        motionLayout.setTransition(R.layout.photo_hub_start, R.layout.photo_hub_end);
                    }
                } else {
                    String message = s != null ? s : getResources().getString(R.string.something_wrong_get_ads);
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                }
            }
        });

        ArrayList<Ad> ads = viewModel.getAdsArrayList();
        if (ads == null){
            ads = new ArrayList<>();
            viewModel.getAvailableAds();
        }
        adapter = new AdsRecyclerViewAdapter(ads, gr, this);
        recyclerView.setAdapter(adapter);

        mainIv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 0) { // action Down
//                    Timber.i("mainIv onTouch");
                    return motionLayout.getCurrentState() == R.layout.photo_hub_start; // when mainIv is showing, swipe won't hide it, but if only partial, swipe on it can pull it down
                }
                return true;
            }
        });
    }

    @Override
    public void onItemClicked(Object object) {
        if (object instanceof Ad){
            Ad ad = (Ad) object;
            User user = AdinlayApp.getUser();
            boolean needToRunTest = user != null && user.needsToTestSocialMedia() && !ad.isTestingCampaign();

            if (!ad.isConnectedToMedia() || needToRunTest){

                Bundle args = new Bundle();
                AyDialogFragment.AyClickListener rightListener = null;
                if (!ad.isConnectedToMedia()) {
                    args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.connect_2_soc_med_allCaps);
                    String message = getString(R.string.ad_need2connect_2_soc_med, ad.getTargetMedia(), ad.getTargetMedia());
                    args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, message);
                    args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.later_allcaps);
                    args.putInt(AyDialogFragment.ARG_BUTTON_R_RES, R.string.log_in);
                    rightListener = new AyDialogFragment.AyClickListener() {
                        @Override
                        public void onButtonClicked(View v) {
                            Intent returnIntent = new Intent();
                            setResult(AyUtils.MISSING_MEDIA_RESULT, returnIntent);
                            finish();
                        }
                    };
                } else {
                    args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.test_required);
                    args.putInt(AyDialogFragment.ARG_MESSAGE_RES, R.string.choose_test_campaign);
                    args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
                }
                AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                if (rightListener != null){
                    dialogFragment.setListener(AyDialogFragment.VALUE_RIGHT, rightListener);
                }
                AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
                return;
            }


            AdinlayApp.setSelectedAd(ad);
            Timber.i("selected ad: %s", object);
            Intent intent = new Intent(SelectAdActivity.this, EditPostActivity.class);
            intent.putExtra(PhotoHubActivity.SELECTED_IMAGE_PATH, imagePath);
            startActivityForResult(intent, AyUtils.get16BitId(R.id.intent_share_new_post));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (AdinlayApp.getUser() == null){
            // lost state - let's let mainActivity deal with it
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case R.id.intent_share_new_post & 0x0000FFFF:
                if (resultCode == RESULT_OK){
//                    Timber.i("share_dbg");
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
                break;
        }
    }

    @Override
    public void finish() {
        super.finish();
        AdinlayApp.setSelectedAd(null);
    }
}
