package com.adinlay.adinlay;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.objects.ApprovedPost;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class ApprovedPostsRvAdapter extends RecyclerView.Adapter<ApprovedPostsRvAdapter.ApprovedPostViewHolder> {

    private ArrayList<ApprovedPost> approvedPosts;
    private WeakReference<ItemClick> wr_itemClickCallback;
    private WeakReference<GlideRequests> wr_glideWith = new WeakReference<GlideRequests>(null);
    private WeakReference<GlideRequest<Drawable>> wr_requestBuilder = new WeakReference<GlideRequest<Drawable>>(null);

    private int cellW = 0;

    public ApprovedPostsRvAdapter(@NonNull ArrayList<ApprovedPost> approvedPosts, GlideRequests glideRequests, ItemClick itemClickCallback) {
        this.approvedPosts = approvedPosts;
        this.wr_itemClickCallback = new WeakReference<>(itemClickCallback);
        wr_glideWith = new WeakReference<GlideRequests>(glideRequests);
        if (glideRequests != null){
            wr_requestBuilder = new WeakReference<GlideRequest<Drawable>>(glideRequests.asDrawable().transforms(new CenterCrop()));
        }
    }

    @NonNull
    @Override
    public ApprovedPostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_approved_post, parent, false);
        if (cellW <=0){
            cellW = parent.getMeasuredWidth() / 3;
        }
        return viewType == 1 ? new ApprovedInstPosrViewHolder(root) : new ApprovedPostViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull ApprovedPostViewHolder holder, int position) {

        ApprovedPost currPost = approvedPosts.get(position);

        Object taggedObject = holder.itemView.getTag(R.id.object_tag);
        if (taggedObject instanceof ApprovedPost && ((ApprovedPost) taggedObject).getAdinlayPostId().equals(currPost.getAdinlayPostId())){
            // same object
            return;
        }

        if (!currPost.getThumbnailUrl().isEmpty()){
            GlideRequests gr = wr_glideWith.get();
            if (gr != null){
                gr.clear(holder.postIv);
            }
            if (wr_requestBuilder.get() == null && gr != null){
                wr_requestBuilder = new WeakReference<GlideRequest<Drawable>>(gr.asDrawable().transforms(new CenterCrop()));
            }
            GlideRequest<Drawable> requestBuilder = wr_requestBuilder.get();
            if (requestBuilder != null){
                requestBuilder = requestBuilder.clone()
                        .load(currPost.getThumbnailUrl())
//                        .skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .thumbnail(0.2f);
                if (cellW > 0){
                    int overrideH = currPost.isFcbk() ? cellW * 630 / 1200 : cellW;
                    requestBuilder = requestBuilder.override(cellW, overrideH);
                }
                requestBuilder.into(holder.postIv);
            }

        }

        if (currPost.isFcbk()){
            holder.platformIv.setImageResource(R.drawable.facebook_approved);
        } else {
            holder.platformIv.setImageResource(R.drawable.inst_approved);
        }

        String expiresWhenUtc = currPost.getExpitrationDateString();
        if (!expiresWhenUtc.isEmpty()){
            holder.expiresTv.setText(AyUtils.parseUtcStringToDdMmYyyy(expiresWhenUtc));
        }

        holder.itemView.setOnClickListener(itemClickListener);
        holder.itemView.setTag(R.id.object_tag, currPost);

    }

    @Override
    public int getItemViewType(int position) {
        ApprovedPost currPost = approvedPosts.get(position);
        return currPost.isFcbk()? 0 :1;
    }

    @Override
    public int getItemCount() {
        return approvedPosts.size();
    }

    View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (wr_itemClickCallback.get() != null){
                wr_itemClickCallback.get().onItemClicked(v.getTag(R.id.object_tag));
            }
        }
    };

    public static class ApprovedPostViewHolder extends RecyclerView.ViewHolder{

        ImageView postIv;
        ImageView platformIv;
        TextView expiresTv;

        public ApprovedPostViewHolder(View itemView) {
            super(itemView);

            postIv = itemView.findViewById(R.id.iap_iv);
            platformIv = itemView.findViewById(R.id.iap_platform);
            expiresTv = itemView.findViewById(R.id.iap_expiration_date_tv);
        }
    }

    public static class ApprovedInstPosrViewHolder extends ApprovedPostViewHolder{

        ImageView postIv;
        ImageView platformIv;
        TextView expiresTv;

        public ApprovedInstPosrViewHolder(View itemView) {
            super(itemView);

            ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) postIv.getLayoutParams();
            lp.dimensionRatio = "1:1";
        }
    }
}
