package com.adinlay.adinlay.gui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;

public class AyDialogFragment extends DialogFragment {

    public interface AyClickListener{
        void onButtonClicked(View v);
    }

    public interface AyDismissListener{
        void onDismiss(DialogInterface dialog);
    }

    private AyClickListener leftOnClick = null;
    private AyClickListener rightOnClick = null;
    private AyClickListener middleOnClick = null;
    private AyDismissListener dismissListener = null;

    public static final String ARG_TITLE = "title";
    public static final String ARG_TITLE_RES = "titleRes";
    public static final String ARG_MESSAGE_CHAR_SEQ = "message";
    public static final String ARG_MESSAGE_RES = "messageRes";
    public static final String ARG_BUTTON_L = "buttonL";
    public static final String ARG_BUTTON_L_RES = "buttonResL";
    public static final String ARG_BUTTON_R = "buttonR";
    public static final String ARG_BUTTON_R_RES = "buttonResR";
    public static final String ARG_BUTTON_M = "buttonM";
    public static final String ARG_BUTTON_M_RES = "buttonResM";
    public static final String ARG_RESULT_ICON = "resultIcon";
    public static final String ARG_CANCELABLE_OUTSIDE = "cancelableOutside";
    public static final String ARG_CANCELABLE_BACK = "cancelableBack";

    public static final int VALUE_LEFT = 0;
    public static final int VALUE_RIGHT = 1;
    public static final int VALUE_MID = 2;
    public static final int VALUE_NULL = 0;
    public static final int VALUE_TRUE = 1;
    public static final int VALUE_FALSE = 2;

    public static AyDialogFragment newInstance(Bundle b){
        AyDialogFragment fragment = new AyDialogFragment();
        if (b != null){
            fragment.setArguments(b);
        }
        return fragment;
    }

    public AyDialogFragment() {
        setRetainInstance(true);
    }

    public void setListener(int which, AyClickListener listener){
        switch (which){
            case VALUE_LEFT:
                leftOnClick = listener;
                break;
            case VALUE_RIGHT:
                rightOnClick = listener;
                break;
            case VALUE_MID:
                middleOnClick = listener;
                break;
        }
    }

    public void setDismissListener(AyDismissListener dismissListener) {
        this.dismissListener = dismissListener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        Bundle args = getArguments();
        if (getActivity() != null && args != null){
            AdinlayDialogBuilder builder = new AdinlayDialogBuilder(getActivity());

            // message
            if (args.getInt(ARG_MESSAGE_RES, 0) > 0){
                builder.setCustomMessage(args.getInt(ARG_MESSAGE_RES, 0));
            } else {
                builder.setCustomMessage(args.getCharSequence(ARG_MESSAGE_CHAR_SEQ, null));
            }

            // title
            if (args.getString(ARG_TITLE, null) != null){
                builder.setCustomTitle(args.getString(ARG_TITLE, ""));
            } else if (args.getInt(ARG_TITLE_RES, 0) > 0){
                builder.setCustomTitle(args.getInt(ARG_TITLE_RES, 0));
            }

            // left button
            if (args.getString(ARG_BUTTON_L, null) != null){
                builder.setButtonL(args.getString(ARG_BUTTON_L, ""), leftClickListener);
            } else if (args.getInt(ARG_BUTTON_L_RES, 0) > 0){
                builder.setButtonL(args.getInt(ARG_BUTTON_L_RES), leftClickListener);
            }

            // right button
            if (args.getString(ARG_BUTTON_R, null) != null){
                builder.setButtonR(args.getString(ARG_BUTTON_R, ""), rightClickListener);
            } else if (args.getInt(ARG_BUTTON_R_RES, 0) > 0){
                builder.setButtonR(args.getInt(ARG_BUTTON_R_RES), rightClickListener);
            }

            // middle button
            if (args.getString(ARG_BUTTON_M, null) != null){
                builder.setButtonM(args.getString(ARG_BUTTON_M, ""), middleClickListener);
            } else if (args.getInt(ARG_BUTTON_M_RES, 0) > 0){
                builder.setButtonM(args.getInt(ARG_BUTTON_M_RES), middleClickListener);
            }

            // show icon:
            Boolean success = null;
            if (args.getInt(ARG_RESULT_ICON, 0) == VALUE_TRUE){
                success = true;
            } else if (args.getInt(ARG_RESULT_ICON, 0) == VALUE_FALSE){
                success = false;
            }
            if (success != null){
                builder.withResultIcon(success);
            }

            Dialog dialog = builder.create();

            if (args.getInt(ARG_CANCELABLE_OUTSIDE, 0) == VALUE_FALSE){
                dialog.setCanceledOnTouchOutside(false);
            }
            if (args.getInt(ARG_CANCELABLE_BACK, 0) == VALUE_FALSE){
                setCancelable(false);
            }

            return dialog;
        }

        return super.onCreateDialog(savedInstanceState);
    }

//    private boolean argsEnoughForBuilder(Bundle args){
//        return args != null && (args.getCharSequence(ARG_MESSAGE_CHAR_SEQ, null) != null ||
//                args.getInt(ARG_MESSAGE_RES, 0) > 0 ||
//                args.getString(ARG_TITLE, null) != null||
//                args.getInt(ARG_TITLE_RES, 0) > 0);
//    }

    View.OnClickListener leftClickListener  = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (leftOnClick != null){
                leftOnClick.onButtonClicked(view);
            }
            dismiss();
        }
    };

    View.OnClickListener rightClickListener  = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (rightOnClick != null){
                rightOnClick.onButtonClicked(view);
            }
            dismiss();
        }
    };

    View.OnClickListener middleClickListener  = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (middleOnClick != null){
                middleOnClick.onButtonClicked(view);
            }
            dismiss();
        }
    };

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (dismissListener != null){
            dismissListener.onDismiss(dialog);
        }
        super.onDismiss(dialog);
    }

    @Override
    public void onDestroy() {
        // handles https://code.google.com/p/android/issues/detail?id=17423
        Dialog dialog = getDialog();
        if (dialog != null && getRetainInstance()) {
            dialog.setDismissMessage(null);
        }

        super.onDestroy();
    }
}
