package com.adinlay.adinlay.gui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.adinlay.adinlay.AyUtils;
import com.adinlay.adinlay.R;


public class SignInDialogFragment extends DialogFragment {

    public interface SignInDialogListener{
        void signInClicked(View v, String email, String password);
    }

    private SignInDialogListener activityCallback = null;

    private TextView emailEt;
    private TextView passwordEt;

    public static SignInDialogFragment newInstance(Bundle b){
        SignInDialogFragment fragment = new SignInDialogFragment();
        if (b != null){
            fragment.setArguments(b);
        }
        return fragment;
    }

    public void setListener(SignInDialogListener listener){
        activityCallback = listener;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        if (getActivity() != null){

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View dialogLayout = inflater.inflate(R.layout.dialog_sign_in, null);
            emailEt = dialogLayout.findViewById(R.id.signin_d_email);
            passwordEt = dialogLayout.findViewById(R.id.signin_d_password);
            dialogLayout.findViewById(R.id.signin_d_btn).setOnClickListener(signInClickListener);
            builder.setView(dialogLayout);

            return builder.create();

        }

        return super.onCreateDialog(savedInstanceState);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (emailEt != null) {
            emailEt.removeTextChangedListener(emailEtTextWatcher);
            hideKeyboard();
        }
        super.onDismiss(dialog);
    }


    View.OnClickListener signInClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (emailEt == null || passwordEt == null){
                return;
            }

            String email = emailEt.getText().toString();

            if (email.isEmpty() || !email.matches(AyUtils.email_REGEX)){
                if (email.isEmpty()){
                    emailEt.setHintTextColor(getResources().getColor(R.color.redHint));
                    if (passwordEt.getText().toString().isEmpty()){
                        passwordEt.setHintTextColor(getResources().getColor(R.color.redHint));
                    }
                } else {
                    emailEt.setTextColor(getResources().getColor(R.color.red));
                    addTextWatcherToEmailEt();
                }
                if (getActivity() != null) {
                    Toast toast = Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.invalid_email), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 100);
                    toast.show();
                }
                return;
            }
            String password = passwordEt.getText().toString();
            if (password.isEmpty()){
                passwordEt.setHintTextColor(getResources().getColor(R.color.redHint));
                if (getActivity() != null) {
                    Toast toast = Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.toast_empty_password), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 100);
                    toast.show();
                }
                return;
            }

            if (activityCallback != null){
                hideKeyboard();
                activityCallback.signInClicked(v, email, password);
                dismiss();
            }
        }
    };

    TextWatcher emailEtTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (emailEt != null) {
                // return the text color to black
                emailEt.setTextColor(Color.BLACK);
                // remove the textWatcher
                emailEt.removeTextChangedListener(emailEtTextWatcher);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };



    private void addTextWatcherToEmailEt(){
        if (emailEt != null) {
            emailEt.removeTextChangedListener(emailEtTextWatcher);
            emailEt.addTextChangedListener(emailEtTextWatcher);
        }
    }

    private void hideKeyboard(){
        if (emailEt != null && getActivity() != null){
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null){
                imm.hideSoftInputFromWindow(emailEt.getWindowToken(), 0);
            }
        }
    }

}
