package com.adinlay.adinlay.gui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.R;

public class AdinlayDialogBuilder extends AlertDialog.Builder {

    private TextView titleTv;
    private TextView messageTv;
    private TextView button1;
    private TextView button2;
    private TextView buttonM;
    private ImageView resultIcon;
    private View btnsTopBorder;
    private View btnsMidLBorder;
    private View btnsMidRBorder;

    public AdinlayDialogBuilder(@NonNull Context context) {
        super(context);
        setAdinlayView(context);
    }

    public AdinlayDialogBuilder(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        setAdinlayView(context);
    }


    private void setAdinlayView(@NonNull Context context){
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.dialog_basic, null);
        titleTv = v.findViewById(R.id.dialog_title);
        messageTv = v.findViewById(R.id.dialog_text);
        button1 = v.findViewById(R.id.dialog_button1);
        button2 = v.findViewById(R.id.dialog_button2);
        buttonM = v.findViewById(R.id.dialog_buttonM);
        resultIcon = v.findViewById(R.id.dialog_icon);
        btnsTopBorder = v.findViewById(R.id.dialog_btns_top_border);
        btnsMidLBorder = v.findViewById(R.id.dialog_btns_mid_l_border);
        btnsMidRBorder = v.findViewById(R.id.dialog_btns_mid_r_border);
        setView(v);
    }

    public AdinlayDialogBuilder setCustomTitle(int titleRes){
        setTextToView(titleTv, titleRes);
        return this;
    }

    public AdinlayDialogBuilder setCustomTitle(String title){
        setTextToView(titleTv, title);
        return this;
    }

    public AdinlayDialogBuilder setCustomMessage(int messageRes){
        setTextToView(messageTv, messageRes);
        return this;
    }

    public AdinlayDialogBuilder setCustomMessage(CharSequence message){
        setTextToView(messageTv, message);
        return this;
    }

    public AdinlayDialogBuilder setButtonL(int stringRes, View.OnClickListener clickListener){
        return setButtonL(button1.getResources().getString(stringRes), clickListener);
    }

    public AdinlayDialogBuilder setButtonL(@NonNull String title, View.OnClickListener clickListener){
        button1.setVisibility(View.VISIBLE);
        button1.setText(title);
        button1.setOnClickListener(clickListener);
        btnsTopBorder.setVisibility(View.VISIBLE);
        if (buttonM.getVisibility() == View.VISIBLE){
            btnsMidLBorder.setVisibility(View.VISIBLE);
        }
        if (button2.getVisibility() == View.VISIBLE){
            btnsMidRBorder.setVisibility(View.VISIBLE);
        }
        return this;
    }

    public AdinlayDialogBuilder setButtonR(int stringRes, View.OnClickListener clickListener){
        return setButtonR(button2.getResources().getString(stringRes), clickListener);
    }

    public AdinlayDialogBuilder setButtonR(@NonNull String title, View.OnClickListener clickListener){
        button2.setVisibility(View.VISIBLE);
        button2.setText(title);
        button2.setOnClickListener(clickListener);
        btnsTopBorder.setVisibility(View.VISIBLE);
        if (button1.getVisibility() == View.VISIBLE || buttonM.getVisibility() == View.VISIBLE){
            btnsMidRBorder.setVisibility(View.VISIBLE);
        }
        return this;
    }

    public AdinlayDialogBuilder setButtonM(int stringRes, View.OnClickListener clickListener){
        return setButtonM(buttonM.getResources().getString(stringRes), clickListener);
    }

    public AdinlayDialogBuilder setButtonM(@NonNull String title, View.OnClickListener clickListener){
        buttonM.setVisibility(View.VISIBLE);
        buttonM.setText(title);
        buttonM.setOnClickListener(clickListener);
        btnsTopBorder.setVisibility(View.VISIBLE);
        if (button1.getVisibility() == View.VISIBLE){
            btnsMidLBorder.setVisibility(View.VISIBLE);
        }
        if (button2.getVisibility() == View.VISIBLE){
            btnsMidRBorder.setVisibility(View.VISIBLE);
        }
        return this;
    }

    private void setTextToView(TextView textView, CharSequence text){
        if (textView == null){
            return;
        }
        if (text == null){
            textView.setVisibility(View.GONE);
        } else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(text);
        }
    }

    private void setTextToView(TextView textView, int textRes){
        if (textView == null){
            return;
        }
        textView.setVisibility(View.VISIBLE);
        textView.setText(textRes);
    }

    public AdinlayDialogBuilder withResultIcon(boolean isSuccess){
        int res = isSuccess ? R.drawable.dialog_v : R.drawable.dialog_x;
        resultIcon.setVisibility(View.VISIBLE);
        resultIcon.setImageResource(res);
        return this;
    }

}
