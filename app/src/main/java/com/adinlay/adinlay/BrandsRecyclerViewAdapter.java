package com.adinlay.adinlay;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.adinlay.adinlay.objects.Brand;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Noya on 4/12/2018.
 */

public class BrandsRecyclerViewAdapter extends RecyclerView.Adapter<PostsRecyclerViewAdapter.MosaicViewHolder> {

    private final static String TAG = BrandsRecyclerViewAdapter.class.getSimpleName();
    private ArrayList<Brand> brandsList;
    private int spanCount;
    private GlideRequest<Drawable> requestBuilder;
    private WeakReference<ItemClick> wr_itemClickCallback;
    private int size = 0;

    public BrandsRecyclerViewAdapter(ArrayList<Brand> brandsList, int spanCount, GlideRequests glideRequests, ItemClick itemClickCalback) {
        this.brandsList = brandsList;
        this.spanCount = spanCount;
        requestBuilder = glideRequests.asDrawable().transforms(new CenterCrop());
        wr_itemClickCallback = new WeakReference<ItemClick>(itemClickCalback);
    }

    interface ItemClick{
        void onItemClicked(Object object);
    }

    @NonNull
    @Override
    public PostsRecyclerViewAdapter.MosaicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_just_iv, parent, false);

//        Timber.d( "parent height: %s parent width: %s", parent.getMeasuredHeight(), parent.getMeasuredWidth());
        GridLayoutManager.LayoutParams lp = (GridLayoutManager.LayoutParams) root.getLayoutParams();
        lp.height = parent.getMeasuredWidth() / spanCount;
        if (size <= 0){
            size = parent.getMeasuredWidth() / spanCount;
        }

        return new PostsRecyclerViewAdapter.MosaicViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull PostsRecyclerViewAdapter.MosaicViewHolder holder, int position) {
        Brand brand = brandsList.get(position);

        Object taggedObject = holder.itemView.getTag(R.id.object_tag);
        if (taggedObject instanceof Brand &&
                !brand.getId().isEmpty() &&
                brand.getId().equals(((Brand) taggedObject).getId())){
            // same object!
            return;
        }

        String logoUrl = brand.getLogoUrl().isEmpty() ? brand.getSmallestLogoUrl() : brand.getLogoUrl();
        if (!logoUrl.isEmpty()){
            int resize = holder.adIv.getWidth() > 0 ? holder.adIv.getWidth() : size > 0 ? size : 0;

            GlideRequest<Drawable> currRequest = requestBuilder.clone()
                    .load(logoUrl)
                    .fitCenter();
            if (resize > 0){
                currRequest = currRequest.override(resize, resize);
            }
            currRequest.into(holder.adIv);
        } else {
            holder.adIv.setImageResource(0);
        }

        holder.itemView.setOnClickListener(itemClickListener);

        holder.itemView.setTag(R.id.object_tag, brand);
    }

    @Override
    public int getItemCount() {
        return brandsList.size();
    }

    View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (wr_itemClickCallback.get() != null){
                wr_itemClickCallback.get().onItemClicked(v.getTag(R.id.object_tag));
            }
        }
    };

}
