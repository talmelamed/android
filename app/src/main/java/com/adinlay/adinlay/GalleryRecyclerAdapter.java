package com.adinlay.adinlay;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;

import com.bumptech.glide.ListPreloader;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;

import timber.log.Timber;

/**
 * Created on 4/21/2018.
 * a recyclerAdapter that implements Glide's preload callbacks for Gallery image objects
 */

public class GalleryRecyclerAdapter extends RecyclerView.Adapter<GalleryRecyclerAdapter.ViewHolder>
        implements ListPreloader.PreloadSizeProvider<GalleryImageObject>,
        ListPreloader.PreloadModelProvider<GalleryImageObject>{


    private static final String TAG = GalleryRecyclerAdapter.class.getSimpleName();
    private final int REGULAR = 1;
    private final int PLACEHOLDER = 2;

    private List<GalleryImageObject> data;
    private int spanCount;
    private int size = -1;
    private GlideRequest<Drawable> requestBuilder;
    private WeakReference<ItemClick> wr_itemClick = new WeakReference<ItemClick>(null);


    public GalleryRecyclerAdapter(List<GalleryImageObject> data, int spanCount, GlideRequests glideRequests) {
        this.data = data;
        this.spanCount = spanCount;

        requestBuilder = glideRequests.asDrawable().transform(new CenterCrop()); // .apply(new RequestOptions().transform(new RoundedCorners(20)))

//        setHasStableIds(true); // might be connected to a
    }

    public GalleryRecyclerAdapter(List<GalleryImageObject> data, int spanCount, GlideRequests glideRequests, int screenWidth, ItemClick clickCallback) {
        this(data, spanCount, glideRequests);

        if (screenWidth > 0) {
            size = screenWidth/spanCount;
        }
        wr_itemClick = new WeakReference<ItemClick>(clickCallback);
    }

    public void setSize(int screenW){
        if (screenW > 0) {
            size = screenW/spanCount;
        }
    }

    @Override
    public int getItemViewType(int position) {
        GalleryImageObject gio = data.get(position);
        return gio == null || gio.isPlaceholder() ? PLACEHOLDER : REGULAR;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        final View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_just_iv, parent, false);

        GridLayoutManager.LayoutParams lp = (GridLayoutManager.LayoutParams) root.getLayoutParams();
        lp.height = size > 0 ? size : parent.getMeasuredWidth() / spanCount;
        lp.width = size > 0 ? size : parent.getMeasuredWidth() / spanCount;
        if (size <= 0) {
//            Timber.d("MAIN_RESIZE onCreateViewHolder h: %s ", ( parent.getMeasuredWidth() / spanCount));
            size = parent.getWidth() /spanCount;
        }

        return viewType == REGULAR ? new ObjectViewHolder(root) : new PlaceholderVH(root);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GalleryImageObject galleryImageObject = data.get(position);

        int id = galleryImageObject == null ? GalleryImageObject.NO_ID : galleryImageObject.getId();

        if (size > 0 && (holder.itemView.getHeight() == 0 || holder.itemView.getWidth() == 0)){
            Timber.v("MAIN_RESIZE onBind h: %s  %s", holder.itemView.getHeight(), holder.itemView.getWidth());
            holder.itemView.getLayoutParams().height = size;
            holder.itemView.getLayoutParams().width = size;
        }

        if (holder instanceof PlaceholderVH){
            return;
        }
        if (holder instanceof ObjectViewHolder) {
            ObjectViewHolder objectHolder = (ObjectViewHolder) holder;
            // check if we need to redraw the item:
            if (id != GalleryImageObject.NO_ID && objectHolder.iv.getTag(R.id.image_id_tag) instanceof Integer &&
                    id == (Integer) objectHolder.iv.getTag(R.id.image_id_tag)){
    //            Timber.v("onBindViewHolder position %s got same id %s", position, id);
                return;
            }

            objectHolder.itemView.setTag(R.id.image_object_tag, galleryImageObject);
            objectHolder.iv.setTag(R.id.image_id_tag, id);
            objectHolder.itemView.setTag(R.id.position_tag, position);
            objectHolder.iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
            ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) objectHolder.iv.getLayoutParams();
            lp.setMargins(0,0,0,0);

            objectHolder.itemView.setOnClickListener(itemClickListener);

            if (galleryImageObject == null){
                Timber.d("GLD_DBG null galleryImageObject");
                return;
            } else if (galleryImageObject.getId() == GalleryImageObject.CAM_ID){
    //            holder.iv.setImageResource(android.R.drawable.ic_menu_camera);
                requestBuilder.clone()
                        .load(android.R.drawable.ic_menu_camera)
                        .skipMemoryCache(true)
                        .into(objectHolder.iv);
    //            Timber.v("GLD_DBG setting image for CAM");
            } else {
                int requestSize = objectHolder.iv.getWidth() > 0 ? objectHolder.iv.getWidth()*2/3 : size > 0 ? size*2/3 : 200;
                requestBuilder.clone()
                        .load(galleryImageObject.getFileUrl())
                        .skipMemoryCache(true)
                        .override(requestSize, requestSize)
                        .into(objectHolder.iv);
            }
        }

    }


    private View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Timber.d("IC_DBG got clicked %s", v.getTag((R.id.image_id_tag)));
            if (v.getTag((R.id.image_object_tag)) instanceof GalleryImageObject){
                GalleryImageObject gmi = (GalleryImageObject) v.getTag((R.id.image_object_tag));
                if (wr_itemClick.get() != null){
                    if (v.getTag((R.id.position_tag)) instanceof Integer){
                        Integer pos = (Integer) v.getTag((R.id.position_tag));
//                        Timber.d("MOTION scroll to pos: %s", pos);
                        gmi.setPosition(pos);
                    }
                    wr_itemClick.get().onItemClicked(gmi);
                }
//                if (gmi.getId() == GalleryImageObject.CAM_ID){
//                    Intent intent  = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    if (v.getContext() instanceof Activity) {
//                        File path = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
//                        Timber.i("CAM_APP_DBG have the external path %s", (path.exists()));
//                        String fileName = "A_IMG_" +
//                                new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date()) + ".jpg";
//                        File photo = new File(path, fileName);
//                        AyUtils.saveStringPreference(AyUtils.PREF_SAVED_IMAGE_FILE, "file:" + photo.getAbsolutePath());
////                        photoPath = "file:" + photo.getAbsolutePath();
//                        Timber.i("CAM_APP_DBG the file path: %s", photo.getAbsolutePath());
//                        if (Build.VERSION.SDK_INT > 23){
//                            Uri photoUri = FileProvider.getUriForFile(v.getContext(), BuildConfig.APPLICATION_ID + ".provider", photo);
//                            Timber.i( "CAM_APP_DBG provider uri is: %s", photoUri);
//                            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
//                        } else {
//                            Timber.i("CAM_APP_DBG not using provider");
//                            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
//                        }
//                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                        ((Activity) v.getContext()).startActivityForResult(intent, AyUtils.get16BitId(R.id.intent_take_photo));
//                    }
//                } else {
//
//                    // open edit screen
//                    if (v.getContext() instanceof AppCompatActivity) {
//                        Intent intent = new Intent(v.getContext(), EditShareActivity.class);
//                        intent.putExtra("image_path", gmi.getFileUrl());
//                        v.getContext().startActivity(intent);
//
//                    }
//                }
            }
        }
    };

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Nullable
    @Override
    public int[] getPreloadSize(@NonNull GalleryImageObject item, int adapterPosition, int perItemPosition) {
        return new int[]{size, size};
    }

    @NonNull
    @Override
    public List<GalleryImageObject> getPreloadItems(int position) {
        return Collections.singletonList(data.get(position));
    }

    @Nullable
    @Override
    public RequestBuilder<?> getPreloadRequestBuilder(@NonNull GalleryImageObject item) {
        if (item.getId() == GalleryImageObject.CAM_ID){
            return requestBuilder.clone().load(android.R.drawable.ic_menu_camera);
        }

        return requestBuilder.clone().load(item.getFileUrl());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);
        }
    }

    public static class ObjectViewHolder extends ViewHolder {
        ImageView iv;

        public ObjectViewHolder(View itemView) {
            super(itemView);
            this.iv = itemView.findViewById(R.id.item_iv);
        }
    }

    public static class PlaceholderVH extends ViewHolder {

        public PlaceholderVH(View itemView) {
            super(itemView);
        }
    }

}
