package com.adinlay.adinlay;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.adinlay.adinlay.adstickerview.DrawableSticker;
import com.adinlay.adinlay.adstickerview.Sticker;
import com.adinlay.adinlay.adstickerview.StickerView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Noya on 4/12/2018.
 */

public class AvailableAdsRecyclerViewAdapterOld extends RecyclerView.Adapter<AvailableAdsRecyclerViewAdapterOld.ViewHolder> {

    private final static String TAG = AvailableAdsRecyclerViewAdapterOld.class.getSimpleName();
    private ArrayList<Advertiser> advertisersList;
    WeakReference<StickerView> wr_stickerView;

    public AvailableAdsRecyclerViewAdapterOld(ArrayList<Advertiser> advertisersList, StickerView stickerView) {
        this.advertisersList = advertisersList;
        this.wr_stickerView = new WeakReference<StickerView>(stickerView);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ad_for_edit, parent, false);

        RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) root.getLayoutParams();

        lp.width = parent.getMeasuredWidth() / 3;
        lp.height = lp.width;
        Timber.i("onCreateViewHolder lp/width: %s", lp.width);

        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Advertiser advertiser = advertisersList.get(position);

        holder.iv.setImageResource(advertiser.getTempRes());
        holder.iv.setTag(R.id.image_id_tag, advertiser.getTempRes());
        holder.iv.setOnClickListener(adClickListener);

    }

    @Override
    public int getItemCount() {
        return advertisersList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv;

        public ViewHolder(View itemView) {
            super(itemView);
            this.iv = itemView.findViewById(R.id.ad_logo_iv);
        }
    }

    View.OnClickListener adClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            StickerView stickerView = wr_stickerView.get();
            if (stickerView == null){
                return;
            }

            if (stickerView.getStickerCount() > 0){
                return;
            }

            if (v.getTag(R.id.image_id_tag) instanceof Integer){
                int res = (int) v.getTag(R.id.image_id_tag);
                Drawable d = ContextCompat.getDrawable(v.getContext(), res);
                Sticker sticker = new DrawableSticker(d);
                stickerView.addSticker(sticker);
            }

        }
    };

}
