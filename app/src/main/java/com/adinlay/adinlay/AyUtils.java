package com.adinlay.adinlay;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.format.DateUtils;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;

import com.adinlay.adinlay.objects.Brand;
import com.adinlay.adinlay.objects.PostFilter;
import com.adinlay.adinlay.objects.ProfileTopic;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import org.json.JSONArray;
import org.json.JSONObject;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import timber.log.Timber;

/**
 * Created by Noya on 4/17/2018.
 */

public class AyUtils {

    private static final String TAG = AyUtils.class.getSimpleName();
    private static SharedPreferences _prefs = null;
    private static WeakReference<Context> _wrAppContext = new WeakReference<Context>(null);

    public static final String PREF_SAVED_IMAGE_FILE = "pref_saved_image_file";
    private static final String PREF_SERVER_JWT = "pref_server_jwt";
    public static final String PREF_FCM_TOKEN = "pref_firebase_msg_token";
    public static final String PREF_REPLACED_FCM_TOKEN = "pref_old_firebase_msg_token";
//    public static final String PREF_UPDATE_APPROVED = "pref_update_approved"; // old, unused String pref
    public static final String PREF_LAST_UPDATE_APPROVED_long = "pref_ast_update_approved_long";
    public static final String PREF_UPDATE_WALLET = "pref_update_wallet";
    public static final String PREF_UPDATE_FCM_REQUIRED = "pref_update_fcm_required";
    public static final String PREF_EXPLANATION_IMAGE_COUNTER = "pref_explanation_image_counter";
    public static final String PREF_DEEP_LINK = "pref_deep_link";
//    public static final String PREF_IG_LOGIN_IN_CLIENT = "IgLoginInClient_"; // to be used together with the userId
    public static final String UPDATE_VAL = "update";
    public static final String APPROVED_KEY = "approved";
    public static final String WALLET_KEY = "wallet";
    public static final String MEDIA_ID_KEY = "mediaId";
    public static final String IS_SINGLE_UPDATE = "isSingleUpdate";

    public static int MISSING_MEDIA_RESULT = 23;
    public static final String SELECTED_IMAGE_PATH = "selected_image_path";

    public static final long WAIT_INTERVAL_UPDATE_APPROVED = 1000*60*10; // 10 min

    private static String sessionToken = null;

    private static int minRecyclerHeightDp = -1;
    private static int screenWidthPixels = -1;
    private static int screenHeightPixels = -1;

    private final static int MAX_BITMAP_SIZE_DEFAULT = 1024; // todo: reduce size, based on available memory and add a for-loop checking oom

    private static SimpleDateFormat serverDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);

    public static final String email_REGEX =  "[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)?";

    private static HashMap<String, Brand> brandIdToName = new HashMap<>();
    private static HashMap<String, Integer> userMinAge = new HashMap<>();

    private static DateTimeFormatter ddmmyyFormatter = DateTimeFormatter.ofPattern("dd/MM/yy");

    public static final int DECIMAL_PLACES_CURRENCY = 2;
    public static final int DECIMAL_PLACES_DEFAULT = 2;

    private static boolean needUpdate = false;

    public static int get16BitId(int id){
        return id & 0x0000FFFF;
    }

    public static String stringifyBundle(Bundle bundle){
        if (bundle == null) {
            return "the bundle is null";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Bundle{");
        for (String key : bundle.keySet()) {
            sb.append(" ").
                    append(key).
                    append(" :: ").
                    append(bundle.get(key)).
                    append(";");
        }
       sb.append(" }");
        return sb.toString();
    }

    public static void init(Context context){
        _prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        _wrAppContext = new WeakReference<Context>(context.getApplicationContext());
//        Timber.v("we have prefs");
        serverDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        userMinAge.put("default", 18);
        ServerManager.initCookieJar(context);
    }

    public static Context getContext(){
        return _wrAppContext.get();
    }

    public static void saveStringPreference(String prefKey, String value){

        SharedPreferences.Editor editor = _prefs.edit();
        editor.putString(prefKey, value);
        editor.apply();
    }

    public static String getStringPref(String prefKey){
        return _prefs.getString(prefKey, null);
    }

    public static boolean getBooleanPref(String prefKey){
        return getBooleanPref(prefKey, false);
    }

    public static boolean getBooleanPref(String prefKey, boolean defValue){
        return _prefs.getBoolean(prefKey, defValue);
    }

    public static void setBooleanPref(String prefKey, boolean value){
        SharedPreferences.Editor editor = _prefs.edit();
        editor.putBoolean(prefKey, value);
        editor.apply();
    }

    public static void saveLongPref(String prefKey, long value){
        SharedPreferences.Editor editor = _prefs.edit();
        editor.putLong(prefKey, value);
        editor.apply();
    }

    public static long getLongPref(String prefKey, long defaultVal){
        return _prefs.getLong(prefKey, defaultVal);
    }

    public static void saveIntPref(String prefKey, int value){
        SharedPreferences.Editor editor = _prefs.edit();
        editor.putInt(prefKey, value);
        editor.apply();
    }

    public static int getIntPref(String prefKey, int defaultVal){
        return _prefs.getInt(prefKey, defaultVal);
    }

    /**
     * encrypts the token and saves it in sharedPrefernces
     * @param serverToken the token from the server, or null if we are logging out
     * @return true if encryption was successful (or if we get a null token)
     */
    public static boolean encryptAndSaveToken(String serverToken){
        sessionToken = serverToken;
        String encrypted = null;
        boolean ok  = false;
        if (serverToken != null){
            if (_wrAppContext.get() != null) {
                Cryptography crypt = new Cryptography(_wrAppContext.get());
                try {
                    encrypted = crypt.encryptData(serverToken);
                    ok = true;
                } catch (NoSuchPaddingException | NoSuchAlgorithmException | UnrecoverableEntryException |
                        CertificateException | KeyStoreException |IOException |
                        InvalidAlgorithmParameterException | InvalidKeyException | NoSuchProviderException |
                        BadPaddingException | IllegalBlockSizeException e) {
                    Timber.w(e);
                    e.printStackTrace();
                }

//                Timber.d("encrypted: %s", encrypted);
//                Timber.d("string:    %s", serverToken);

            }
        }
        _prefs.edit().putString(PREF_SERVER_JWT, encrypted).apply();
        return ok || serverToken == null;
    }

    public static String getDecryptedToken(){
        if (sessionToken != null){ // todo: should re-check if app goes to background
            return sessionToken;
        }
        String encrypted = _prefs.getString(PREF_SERVER_JWT, null);
        if (encrypted == null || _wrAppContext.get() == null){
            return null;
        }

        Cryptography crypt = new Cryptography(_wrAppContext.get());
        try {
            sessionToken = crypt.decryptData(encrypted);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | UnrecoverableEntryException |
                CertificateException | KeyStoreException | IOException |
                InvalidAlgorithmParameterException | InvalidKeyException | NoSuchProviderException |
                BadPaddingException | IllegalBlockSizeException e) {
            Timber.w(e, "getDecryptedToken");
            e.printStackTrace();
        }
//        Timber.d("decrypted: %s", sessionToken);
        return sessionToken;
    }

    public static void logoutUser(){
        AdinlayApp.setUser(null);
        AdinlayApp.setLoginStage(null);
        encryptAndSaveToken(null);
        // also clear the google+ client
        if (_wrAppContext.get() != null) { // logout from google+
            Context context = _wrAppContext.get();
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(context.getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build();
            GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(context, gso);
            googleSignInClient.signOut();
        }
        // also clear facebook
        LoginManager.getInstance().logOut();
        // and also clear the firebaseUser:
        SignInManager.clearFrbsUser();
        // clear server cookies:
        ServerManager.clearCookieJar();
    }

    public static int getMinAgeForLocation(){
        User user = AdinlayApp.getUser();
        if (user != null){
            JSONArray selectedArray = user.getProfileTopicSelection(ProfileTopic.LOCATION, true, true);
            String userLocation = selectedArray.length() > 0 ? selectedArray.optString(0) : "";
            int breakPoint = userLocation.indexOf(".");
            if (breakPoint > 0){
                userLocation = userLocation.substring(0, breakPoint);
            }
            Integer min = userMinAge.get(userLocation);
            if (min != null && min > 0){
                return min;
            }
        }
        return userMinAge.get("default") == null ? 18 : userMinAge.get("default");
    }

    public static void initMinAgeList(JSONObject object){
//        Timber.i("validation %s", object);
        JSONObject userValidationObject = object == null ? null : object.optJSONObject("user");
        if (userValidationObject == null){
            return;
        }
        int defaultMinAge = userMinAge.get("default") == null ? 18 : userMinAge.get("default");
        Iterator<String> iterator = userValidationObject.keys();
        while (iterator.hasNext()){
            String key = iterator.next();
            JSONObject locObject = userValidationObject.optJSONObject(key);
            if (locObject != null){
                int minAge = locObject.optInt("minUserAge", 0);
                if (minAge > 0){
                    userMinAge.put(key, minAge);
                    if (minAge > defaultMinAge){
                        defaultMinAge = minAge;
                        userMinAge.put("default", defaultMinAge);
                    }
                }
            }
        }
    }

    public static int convertDpToPixel(int dp, Context context){
        if (context == null){
            context = _wrAppContext.get();
            if (context == null){
                return dp;
            }
        }
        float resultPixel = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,dp,
                context.getResources().getDisplayMetrics());
        if (dp == 1 && resultPixel % 1 > 0f){
            Timber.i( "convertDpToPixel actual conversion: %s", resultPixel);
        }
        return (int) resultPixel;
    }

    public static int convertPixelToDp(int pixel, Context context){
        if (context == null){
            context = _wrAppContext.get();
            if (context == null){
                return pixel;
            }
        }
        int resultDp = (int) (pixel / context.getResources().getDisplayMetrics().density);
        return resultDp;
    }

    public static boolean isOnline(@NonNull Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm != null ? cm.getActiveNetworkInfo() : null;
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static String getStringResource(int resId){
        String ret = null;
        Context context = _wrAppContext.get();
        if (context != null){
            ret = context.getResources().getString(resId);
        }
        return ret;
    }

    public static String getStringResource(int resId, String param){
        if (param == null){
            return getStringResource(resId);
        }

        String ret = null;
        Context context = _wrAppContext.get();
        if (context != null){
            ret = context.getResources().getString(resId, param);
        }
        return ret;
    }

    public static String parseUtcStringToTimeAgo(String when){

        Date d = null;
        try {
            d = serverDateFormat.parse(when);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Timber.d("parseDte from : %s to: %s", when, d);
        long now = System.currentTimeMillis();
        if (d == null || d.getTime() > now){
            return "";
        }
        if (now - d.getTime() < 2*60*1000 && getContext() != null){
            return getContext().getResources().getString(R.string.just_now);
        }
        CharSequence ago = DateUtils.getRelativeTimeSpanString(d.getTime(), now, DateUtils.MINUTE_IN_MILLIS);
        // seems that DateUtils.getRelativeTimeSpanString returns as follows:
        // 0sec-59min: x minutes ago
        // 60min-[day?] : x hours ago
        // yesterday
        // over 1 day up to 7 days : x days ago
        // one week ond over : [date]
        return ago.toString();
    }

    public static String parseUtcStringToDdMmYyyy(@NonNull String when){

        String formatted = "";
        ZonedDateTime zdt = null;
        try {
            zdt = ZonedDateTime.parse(when).withZoneSameInstant(ZoneId.systemDefault());
            formatted = ddmmyyFormatter.format(zdt);
            if (formatted.startsWith("00") && when.length() >= 10){
                Timber.i("parseUtcStringToDdMmYyyy when: %s got zdt: %s and formatted: %s ddmmyyFormatter: %s",
                        when, zdt, formatted, ddmmyyFormatter);
                formatted = when.substring(0, 10).replaceAll("-", "/");
            }
        } catch (Exception e) { // for some devices (Motorola Moto G3 we sometimes got an exception
            Timber.w(e, "parseUtcStringToDdMmYyyy caught exception for: %s zdt: %s ddmmyyFormatter: %s",
                    when, zdt, ddmmyyFormatter.toString());
            e.printStackTrace();
            if (when.length() >= 10){
                formatted = when.substring(0, 10).replaceAll("-", "/");
            }
//            Instant instant = Instant.parse(when);
//            DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).withLocale(Locale.UK).withZone(ZoneId.systemDefault());
//            formatted = formatter.format(instant); // dd/mm/yyyy
        }

        return formatted;
    }

//    private String getStringValFromJson(JSONObject jsonObject, String key, String defVal){
//        if (jsonObject == null){
//            return defVal;
//        }
//        String val = defVal;
//        if (jsonObject.has(key)){
//            try {
//                val = jsonObject.getString(key);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//        return val;
//    }


    public static int getMinRecyclerHeightDp() {
        return minRecyclerHeightDp;
    }

    public static void setMinRecyclerHeightDp(int minRecyclerHeightDp, int screenWdthPixels, int screenHeightPixels) {
        AyUtils.minRecyclerHeightDp = minRecyclerHeightDp;
        AyUtils.screenWidthPixels = screenWdthPixels;
        AyUtils.screenHeightPixels = screenHeightPixels;
    }

    public static int getScreenWidthPixels() {
        return screenWidthPixels;
    }

    public static int getScreenHeightPixels() {
        return screenHeightPixels;
    }

    public static void loadSizedBitmapToIv(String path, ImageView iv, GPUImageFilter filter, int maxSize, int[] scaleSaver){
        Timber.d("OOM_DBG loadSizedBitmap starting");
        if (path == null || iv == null){
            return;
        }

        Bitmap bitmap = getSizedBitmap(path, maxSize, scaleSaver);
        if (bitmap == null){
            return;
        }
        if (filter == null){
            iv.setImageBitmap(bitmap);
        } else {

            GPUImage gpuImage = new GPUImage(iv.getContext());
            gpuImage.setImage(bitmap);
            gpuImage.setFilter(filter);
            Bitmap b = gpuImage.getBitmapWithFilterApplied();
            iv.setImageBitmap(b);
        }
        Timber.d("OOM_DBG loadSizedBitmap ending");
    }

    public static Bitmap getSizedBitmap(String path, int maxSize, int[] scaleSaver){
        if (path == null){
            return null;
        }
        Bitmap bitmap = null;
        File imgFile = new File(path);
        if (imgFile.exists()){
            InputStream is = null;
            try {
                // check the size of the file's bitmap
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                is = new FileInputStream(imgFile);
                BitmapFactory.decodeStream(is, null, options);

                int size = maxSize > 0 ? maxSize : MAX_BITMAP_SIZE_DEFAULT;
                // take the longer side, to ensure max size of the bitmap:
                int scaledLength = options.outWidth > options.outHeight ? options.outWidth : options.outHeight;
                int sampleSize = 1;
                while (scaledLength > size){
                    scaledLength /= 2;
                    sampleSize*= 2;
                }
                Timber.d("OOM_DBG decoding options width: %s height: %s sampleSize factor: %s final scaledLength: %s",
                        options.outWidth, options.outHeight, sampleSize, scaledLength);

                // make sure the bitmap is properly oriented:
                Matrix matrix = new Matrix();
                try {
                    ExifInterface exif = new ExifInterface(path);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
//                    Timber.v("calculating rotation matrix for orientation %s", orientation);
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_NORMAL:
                            break;
                        case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                            matrix.setScale(-1, 1);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            matrix.setRotate(180);
                            break;
                        case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                            matrix.setRotate(180);
                            matrix.postScale(-1, 1);
                            break;
                        case ExifInterface.ORIENTATION_TRANSPOSE:
                            matrix.setRotate(90);
                            matrix.postScale(-1, 1);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            matrix.setRotate(90);
                            break;
                        case ExifInterface.ORIENTATION_TRANSVERSE:
                            matrix.setRotate(-90);
                            matrix.postScale(-1, 1);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            matrix.setRotate(-90);
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Timber.i(e, "OOM_DBG problem orientating the chosen photo");
                }
                BitmapFactory.Options options2 = new BitmapFactory.Options();
                options2.inSampleSize = sampleSize;

                if (scaleSaver != null && scaleSaver.length > 0){
                    scaleSaver[0] = sampleSize;
                }

                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                is = new FileInputStream(imgFile);

                bitmap = BitmapFactory.decodeStream(is,null, options2);

//                if (Build.VERSION.SDK_INT >= 19) {
//                    Timber.v("OOM_DBG scaled bitmap byteCount: %s width: %s height: %s", + bitmap.getByteCount(), bitmap.getWidth(),bitmap.getHeight());
//                }

                if (bitmap != null) {
                    bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                }
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Timber.e(e, "OOM_DBG problem reading the chosen photo %s", path);
            } finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Timber.d("OOM_DBG getSizedBitmap ending");
        if (bitmap == null){
            bitmap = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888); // return an empty bitmap
        }
        return bitmap;
    }

    // get the original dimensions of image stored at "path"
    public static Rect getOriginalImageSize(String path)
    {
        Rect ImageDimensions = null;
        File imgFile = new File(path);
        if (imgFile.exists()){
            InputStream is = null;
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                is = new FileInputStream(imgFile);
                BitmapFactory.decodeStream(is, null, options);
                // now read the options:
                int h = options.outHeight;
                int w = options.outWidth;

                ImageDimensions = new Rect(0,0,w,h);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return ImageDimensions;
    }

    public static Bitmap getBitmapRegion(String path, Rect cropRegion, Context context)
    {
        if (path == null)
        {
            return null;
        }
        Bitmap bitmap = null;
        File imgFile = new File(path);
        if (imgFile.exists())
        {
            Rect sizeOrig = getOriginalImageSize(path);
            // make sure the bitmap is properly oriented:
            Matrix matrix = new Matrix();
            int orientation = ExifInterface.ORIENTATION_NORMAL;
            try {
                ExifInterface exif = new ExifInterface(path);
                orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
//                    Timber.v("calculating rotation matrix for orientation %s", orientation);
                switch (orientation) {
                    case ExifInterface.ORIENTATION_NORMAL:
                        break;
                    case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                        matrix.setScale(-1, 1);
                        cropRegion = new Rect(sizeOrig.right - cropRegion.right ,cropRegion.top ,sizeOrig.right - cropRegion.left, cropRegion.bottom); // FLIP_HORIZONTAL
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        matrix.setRotate(180);
                        cropRegion = new Rect(sizeOrig.right - cropRegion.right,  sizeOrig.bottom - cropRegion.bottom, sizeOrig.right - cropRegion.left, sizeOrig.bottom - cropRegion.top);
                        break;
                    case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                        matrix.setRotate(180);
                        matrix.postScale(-1, 1);
                        cropRegion = new Rect(sizeOrig.right - cropRegion.right,  sizeOrig.bottom - cropRegion.bottom, sizeOrig.right - cropRegion.left, sizeOrig.bottom - cropRegion.top);
                        cropRegion = new Rect(sizeOrig.right - cropRegion.right ,cropRegion.top ,sizeOrig.right - cropRegion.left, cropRegion.bottom); // FLIP_HORIZONTAL
                        break;
                    case ExifInterface.ORIENTATION_TRANSPOSE:
                        matrix.setRotate(90);
                        matrix.postScale(-1, 1);
                        cropRegion = new Rect(cropRegion.top, cropRegion.left, cropRegion.bottom,  cropRegion.right);
                        // no flip required in this case
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        matrix.setRotate(90);
                        cropRegion = new Rect(cropRegion.top, sizeOrig.bottom - cropRegion.right, cropRegion.bottom,  sizeOrig.bottom - cropRegion.left);
                        break;
                    case ExifInterface.ORIENTATION_TRANSVERSE:
                        matrix.setRotate(-90);
                        matrix.postScale(-1, 1);
                        cropRegion = new Rect(cropRegion.top,  sizeOrig.bottom - cropRegion.right, cropRegion.bottom, sizeOrig.bottom - cropRegion.left);
                        cropRegion = new Rect(sizeOrig.right - cropRegion.right ,cropRegion.top ,sizeOrig.right - cropRegion.left, cropRegion.bottom); // FLIP_HORIZONTAL
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        matrix.setRotate(-90);
                        cropRegion = new Rect(cropRegion.top,  sizeOrig.bottom - cropRegion.right, cropRegion.bottom, sizeOrig.bottom - cropRegion.left);
                        cropRegion = new Rect(sizeOrig.right - cropRegion.right,  sizeOrig.bottom - cropRegion.bottom, sizeOrig.right - cropRegion.left, sizeOrig.bottom - cropRegion.top); // FLIP_VERTICAL
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
                Timber.i(e, "OOM_DBG problem orientating the chosen photo %s", path);
            }

            BitmapFactory.Options optionsCrop = new BitmapFactory.Options();

            boolean readImageTry = true;
            optionsCrop.inSampleSize = 1; // by default try no scaling

            // Eliyahu: gradually reduce isSampleSize in case of OutOfMemoryError
            while (readImageTry && optionsCrop.inSampleSize<16)
            {
                try
                {
                    BitmapRegionDecoder decoder = BitmapRegionDecoder.newInstance(path, false); // Eliyahu: with stream returns null, with pathname works, so got rid of InputStream
                    bitmap = decoder.decodeRegion(cropRegion, optionsCrop);
                    if (orientation != ExifInterface.ORIENTATION_NORMAL) // manual treatment of orientation required
                    {
                        // apply matrix to cropped bitmap (also protected from OutOfMemoryError)
                        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                    }

                    // apply filter:
                    PostFilter postFilter = AdinlayApp.getDraftSelectedFilter();
                    if (context != null && postFilter != null){
                        bitmap = applyFilterToBitmap(bitmap, postFilter.getGpuImageFilter(context), context);
                    }

                    readImageTry = false;
                }
                catch (IOException | OutOfMemoryError e)
                {
                    e.printStackTrace();
                    optionsCrop.inSampleSize *= 2;
                    readImageTry = true;
                    Timber.d("OOM_DBG OutOfMemoryError caught in getBitmapRegion reducing scaling to inSampleSize=%s", optionsCrop.inSampleSize);
                }
            }
        }
        Timber.d("OOM_DBG getBitmapRegion ending");

        return bitmap;
    }

    public static Bitmap applyFilterToBitmap(Bitmap bitmap, GPUImageFilter filter, Context context){

        if (bitmap == null || filter == null || context == null){
            return bitmap;
        }

        GPUImage gpuImage = new GPUImage(context);
        gpuImage.setImage(bitmap);
        gpuImage.setFilter(filter);
        return gpuImage.getBitmapWithFilterApplied();
    }

    @NonNull
    public static String getBrandName(String id){
        if (id == null){
            return "";
        }
        Brand brand = brandIdToName.get(id);
        String name = brand == null ? "" : brand.getName();
        return name;
    }

    public static Brand getBrand(String id){
        return id == null ? null : brandIdToName.get(id);
    }

   public static void addBrandNameToList(@NonNull String id, @NonNull Brand brand){
        brandIdToName.put(id, brand);
   }

   public static Spannable getSpannable(@NonNull String fullText, @NonNull String highlightText, @NonNull String color){

       Spannable spannable = new SpannableString(fullText);
       int substringStart=0;
       int start = fullText.indexOf(highlightText, substringStart);
       while(start >=0){
           spannable.setSpan(
                   new ForegroundColorSpan(Color.parseColor(color)),
                   start,start + highlightText.length(),
                   Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
           );
           substringStart = start + highlightText.length();
           start = fullText.indexOf(highlightText, substringStart);
       }
       return spannable;
   }

//   public static String getJsonNumberAsString(JSONObject jsonObject, String key, int decimalPlaces){
//       if (jsonObject == null){
//           String ret = "0";
//           for (int i = 0; i < decimalPlaces; i ++){
//               if (i == 0){
//                   ret += ".";
//               }
//               ret += "0";
//           }
//           return ret;
//       }
//
//       double amountDouble = jsonObject.optDouble(key, 0);
//       BigDecimal val = BigDecimal.valueOf(amountDouble);
//       return "" + val.setScale(decimalPlaces, BigDecimal.ROUND_HALF_UP);
//   }

   @NonNull
    public static String getStringAmountWithCurrencySymbol(@NonNull BigDecimal serverAmount, int decimalPlaces, Resources resources){
       User user = AdinlayApp.getUser();
       boolean isILS = user != null && user.isCurrencyIls();

       BigDecimal exchangeRate = user == null ? BigDecimal.valueOf(1) : user.getUserExchangeRate();
       BigDecimal relevantAmount = !isILS ? serverAmount : serverAmount.multiply(exchangeRate);

       int symbolRes = isILS ? R.string.formatted_price_ils : R.string.formatted_price_dollar;

       return resources.getString(symbolRes, "" + relevantAmount.setScale(decimalPlaces, BigDecimal.ROUND_DOWN));
   }

   public static void setOddEvenBackgroundColor(int position, View view){
        if (view == null){
            return;
        }
       int color = position % 2 == 0 ? view.getResources().getColor(R.color.color_rv_even) :
               view.getResources().getColor(R.color.color_rv_odd);
       view.setBackgroundColor(color);
   }

    public static boolean needsUpdate() {
        return needUpdate;
    }

    public static void setNeedUpdate(boolean needUpdate) {
        AyUtils.needUpdate = needUpdate;
    }

    public static void setResToStar(ImageView star, int pos, int strength){
        if (star == null){
            return;
        }
        int src = strength > pos ? R.drawable.star_s : R.drawable.star_copy_4_s;
        star.setImageResource(src);
    }

    public static void showDialogFragment(@NonNull DialogFragment fragment, FragmentManager fragmentManager){
        if (fragmentManager != null) {
            Fragment priorFrag = fragmentManager.findFragmentByTag("dialog");
            if (priorFrag instanceof DialogFragment){
                ((DialogFragment) priorFrag).dismiss();
            }
            fragment.show(fragmentManager, "dialog");
        }
    }
}
