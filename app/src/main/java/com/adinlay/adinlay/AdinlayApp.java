package com.adinlay.adinlay;

import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;

import com.adinlay.adinlay.objects.Ad;
import com.adinlay.adinlay.objects.ApprovedPost;
import com.adinlay.adinlay.objects.DraftPost;
import com.adinlay.adinlay.objects.PostFilter;
import com.adinlay.adinlay.objects.ProfileTopic;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;
import com.jakewharton.threetenabp.AndroidThreeTen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

/**
 * Created by Noya on 5/30/2018.
 */

public class AdinlayApp extends MultiDexApplication {

    private static String loginStage;
    private static User user = null;
//    private static final String SERVER_TYPE_KEY = "serverTypeKey";

    private static DraftPost draftPost = null;
    private static ArrayList<ProfileTopic> userProfileTopics;
    private static ArrayList<ProfileTopic> followersProfileTopics;
    private static ArrayList<ApprovedPost> approvedPosts = new ArrayList<>();
    private static int userTopicPos = -1;
    private static int followersTopicPos = -1;
    private static ProfileTopic socialMediaSettings;
    private static HashMap<String, String> locationsMap = null;

    private static boolean informOneApprovedPostRemoved = false;

    @Override
    public void onCreate() {
        super.onCreate();
        configCrashlytics();
        AndroidThreeTen.init(this);
        AyUtils.init(this);
        loginStage = "unknown";
        Timber.d("AdinlayApp onCreate"); // no logs before configCrashlytics!!!

//        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//                .detectLeakedSqlLiteObjects()
//                .detectLeakedClosableObjects()
//                .detectActivityLeaks()
//                .penaltyLog()
////                .penaltyDeath()
//                .build());
        // todo: test detectNonSdkApiUsage()
    }

    private void configCrashlytics(){
        CrashlyticsCore crashlyticsCore = new CrashlyticsCore.Builder()
                .disabled(BuildConfig.DEBUG)
                .build();
        Fabric.with(this, new Crashlytics.Builder().core(crashlyticsCore).build());

        if (BuildConfig.DEBUG){
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashlyticsTree());
        }
    }

    public static String getLoginStage() {
        return loginStage == null? "unknown" : loginStage;
    }

    public static void setLoginStage(String loginStage) {
        AdinlayApp.loginStage = loginStage;
    }

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        AdinlayApp.user = user;
    }

    public static String getUserNameUppercase(){
        String name = user == null ? "" : user.getShortName();
        // todo: max length? check for spaces?
        name = name.toUpperCase(); // this is user input, so default locale should be ok
        return name;
    }

    @NonNull
    public static String getUserVcfName(){
        return user == null ? "" : user.getVcfName();
    }

    @NonNull
    public static String getUserEmail(){
        return user == null ? "" : user.geteMail();
    }

    @NonNull
    public static String getUserPhone(){
        return user == null ? "" : user.getPhoneNumber();
    }

    public static int getUserStrength(){
        return user == null ? 0 : user.getStrength();
    }

    @NonNull
    public static String getUserId(){
        return user == null ? "" : user.getId() == null ? "" : user.getId() ;
    }

//    public static boolean isProdServer(){
//        return AyUtils.getBooleanPref(SERVER_TYPE_KEY);
//    }

    public static Ad getSelectedAd() {
        return draftPost == null ? null : draftPost.getSelectedAd();
    }

    public static void setSelectedAd(Ad selectedAd) {
        if (draftPost == null){
            return;
        }
        draftPost.setSelectedAd(selectedAd);
    }

    public static DraftPost getDraftPost() {
        return draftPost;
    }

    public static void clearDraftBitmap(){
        if (draftPost != null){
            draftPost.clearDraftPostBitmap();
            draftPost.setSelectedFilter(null);
        }
    }

    public static PostFilter getDraftSelectedFilter(){
        if (draftPost == null){
            return null;
        }
        return draftPost.getSelectedFilter();
    }

    public static void setDraftPost(DraftPost draftPost) {
        AdinlayApp.draftPost = draftPost;
    }

    public static String getBaseImage() {
        return draftPost == null ? null : draftPost.getImagePath();
    }

    public static void setBaseImage(String imagePath) {
        if (draftPost == null){
            return;
        }
        draftPost.setImagePath(imagePath);
    }

    public static ArrayList<ProfileTopic> getUserProfileTopics() {
        return userProfileTopics;
    }

    public static ArrayList<ProfileTopic> getFollowersProfileTopics() {
        return followersProfileTopics;
    }

    public static void setProfileSettings(JSONObject settingsObject) {
        if (settingsObject == null){
            return;
        }

        setProfileTopics(settingsObject.optJSONObject("user"), true);
        setProfileTopics(settingsObject.optJSONObject("followers"), false);


        JSONObject socialMediaJson = settingsObject.optJSONObject("socialMedia");
//        Timber.d("socialMedia: %s", socialMediaJson);
        if (socialMediaJson != null){
            JSONArray jArr = socialMediaJson.optJSONArray("topics");
            if (jArr != null && jArr.length() > 0){
                JSONObject scSettings = jArr.optJSONObject(0);
                if (scSettings != null){
                    socialMediaSettings = new ProfileTopic(scSettings);
                }
            }
        }

    }

    private static void setProfileTopics(JSONObject jsonObject, boolean isUser){
        if (jsonObject == null){
            return;
        }

//        Timber.v("topic_dbg setProfileTopics with json: %s isUser? %s", jsonObject, isUser);
        ArrayList<ProfileTopic> currTopicsArr = new ArrayList<>();
        ProfileTopic location = null;
        if (jsonObject.optJSONArray("topics") != null){
            JSONArray jsonTopics = jsonObject.optJSONArray("topics");
            for (int i = 0; i < jsonTopics.length(); i++){
                JSONObject currTopic = jsonTopics.optJSONObject(i);
                if (currTopic != null){
                    currTopicsArr.add(new ProfileTopic(currTopic));
                    if (isUser && ProfileTopic.LOCATION.equals(currTopic.optString("id"))){
                        location = new ProfileTopic(currTopic);
                    }
                }
            }
        }
        if (isUser){
            userProfileTopics = currTopicsArr;
            if (location != null){
                // init locationsMap in separate thread, not that location is a big item
                final ProfileTopic f_location = location;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        locationsMap = f_location.mapItemNames();
                    }
                }).start();
            }
        } else {
            followersProfileTopics = currTopicsArr;
        }
    }

    public static String getLocationName(String id){
        if (locationsMap != null && id != null){
            return locationsMap.get(id);
        }
        return null;
    }

    public static int getUserTopicPos() {
        return userTopicPos;
    }

    public static void setUserTopicPos(int userTopicPos) {
        AdinlayApp.userTopicPos = userTopicPos;
    }

    public static int getFollowersTopicPos() {
        return followersTopicPos;
    }

    public static void setFollowersTopicPos(int followersTopicPos) {
        AdinlayApp.followersTopicPos = followersTopicPos;
    }

    public static int getAllTopicsLength() {
        int userTopicsNum = userProfileTopics == null ? 0 : userProfileTopics.size();
        int followersTopicsNum = followersProfileTopics == null ? 0 : followersProfileTopics.size();
        return userTopicsNum + followersTopicsNum;
    }

    public static ProfileTopic getCurrUserTopic(){
//        Timber.i("topic_dbg pos: " + userTopicPos + " list" + ((userProfileTopics == null)? " is null" : " length: " + userProfileTopics.size()));
        if (userProfileTopics != null && userTopicPos >= 0 &&
                userTopicPos < userProfileTopics.size()){
            return userProfileTopics.get(userTopicPos);
        }
        return null;
    }

    public static ProfileTopic getCurrFollowersTopic(){
        if (followersProfileTopics != null && followersTopicPos >= 0 &&
                followersTopicPos < followersProfileTopics.size()){
            return followersProfileTopics.get(followersTopicPos);
        }
        return null;
    }

    @NonNull
    public static ProfileTopic getSocialMediaSettings(){
        if (socialMediaSettings == null){
            socialMediaSettings = new ProfileTopic(new JSONObject());
        }
        return socialMediaSettings;
    }

    public static ArrayList<ApprovedPost> getApprovedPosts() {
        return approvedPosts;
    }

    public static int getNumberOfApprovedPosts(){
        return approvedPosts == null ? 0 : approvedPosts.size();
    }

    public static void setApprovedPosts(String serverResponse){
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(serverResponse);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONArray jsonArray = jsonObject == null ? null : jsonObject.optJSONArray("posts");
        if (jsonArray != null){
            ArrayList<ApprovedPost> newApprovals = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++){

                JSONObject currJson = jsonArray.optJSONObject(i);
                if (currJson != null){
                    newApprovals.add(new ApprovedPost(currJson));
                }
            }
            AdinlayApp.approvedPosts.clear();
            AdinlayApp.approvedPosts.addAll(newApprovals);
            setIsOneApprovedPostRemoved(false);
        }

    }

    public static void updateUserSocMediaConnections(JSONObject jsonObject){
        if (user == null){
            return;
        }
        JSONObject instJson = jsonObject.optJSONObject("instagram");
        boolean instConnected = instJson != null && instJson.optBoolean("connection", false);
        int followers = instConnected ? instJson.optInt("followers") : 0;
        String igUsername = instConnected ? instJson.optString("username") : "";
        String igUserId = instConnected ? instJson.optString("instagramAccountId") : "";
        user.setConnectedToInstagram(instConnected);
        user.setInstaFollowers(followers);
        user.setIgUsername(igUsername);
        user.setIgUserId(igUserId);

        JSONObject fbJson = jsonObject.optJSONObject("facebook");
        boolean fbConnected = fbJson != null && fbJson.optBoolean("connection", false);
        int friends = fbConnected ? fbJson.optInt("friends") : 0;
        String fbUsername = fbConnected ? fbJson.optString("displayName") : "";
        String fbUserId = fbConnected ? fbJson.optString("facebookUserId") : "";
        user.setConnectedToFacebook(fbConnected);
        user.setFacebookFriends(friends);
        user.setFbUsername(fbUsername);
        user.setFbUserId(fbUserId);
//        Timber.i("SOC_MED fcbk %s %s friends, inst %s %s followers", fbConnected, friends, instConnected, followers);
    }

    public static boolean isOneApprovedPostRemoved() {
        return informOneApprovedPostRemoved;
    }

    public static void setIsOneApprovedPostRemoved(boolean informOneApprovedPostRemoved) {
        AdinlayApp.informOneApprovedPostRemoved = informOneApprovedPostRemoved;
    }
}
