package com.adinlay.adinlay;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.adinlay.adinlay.adstickerview.BitmapStickerIcon;
import com.adinlay.adinlay.adstickerview.DeleteIconEvent;
import com.adinlay.adinlay.adstickerview.StickerView;
import com.adinlay.adinlay.adstickerview.ZoomIconEvent;
import com.adinlay.adinlay.gui.TouchImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import timber.log.Timber;

/**
 * Created by Noya on 4/26/2018.
 */

public class EditImageFrag extends Fragment {

    private final static String TAG = EditImageFrag.class.getSimpleName();

    String imagePath = null;
    TouchImageView photo_iv = null;
    StickerView stickerView = null;
    ImageView resultIv = null;

    public void setImageFilePath(String path){
        imagePath = path;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_edit, container, false);

        resultIv = root.findViewById(R.id.edit_result);
        stickerView = root.findViewById(R.id.ad_sticker_view);
        photo_iv = root.findViewById(R.id.iv_edit_photo);

        Drawable zoomD = ContextCompat.getDrawable(inflater.getContext(), android.R.drawable.ic_menu_rotate);
        BitmapStickerIcon zoomIcon = new BitmapStickerIcon(zoomD, BitmapStickerIcon.RIGHT_BOTOM);
        zoomIcon.setIconEvent(new ZoomIconEvent());
        Drawable deleteD = ContextCompat.getDrawable(inflater.getContext(), android.R.drawable.ic_menu_close_clear_cancel);
        BitmapStickerIcon deleteIcon = new BitmapStickerIcon(deleteD, BitmapStickerIcon.LEFT_TOP);
        deleteIcon.setIconEvent(new DeleteIconEvent());
        stickerView.setIcons(Arrays.asList(deleteIcon, zoomIcon));
        stickerView.setConstrained(true);
        if (imagePath != null){

            loadSizedBitmap(imagePath);
            // attempt 1 load image into bgImageView, properly zoomed:
//            iv.setImageURI(Uri.parse(imagePath));
//            iv.setZoom(1f);

//            // attempt 2 - also loads image properly, but doesn't scale the underlying bmp:
//            SimpleTarget<Drawable> drawableSimpleTarget = new SimpleTarget<Drawable>() {
//                @Override
//                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
//                    photo_iv.setImageDrawable(resource);
//                    photo_iv.setZoom(1f);
//                    Timber.d("GLD_DBG  setting image");
//                }
//            };
//            Timber.d("GLD_DBG  calling load");
//            GlideApp.with(EditImageFrag.this).asDrawable().load(imagePath).into(drawableSimpleTarget);


        } // todo: handle else - should never happen!!!

        final RecyclerView recyclerView = root.findViewById(R.id.edit_ads_recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        RecyclerView.Adapter adapter = new AvailableAdsRecyclerViewAdapterOld(getMockUpAdvertisers(), stickerView);

        int filterIvSize = AyUtils.convertDpToPixel(40, inflater.getContext());

        GlideApp.with(EditImageFrag.this).load(imagePath).override(filterIvSize).into((ImageView) root.findViewById(R.id.iv_filter_normal));
        GlideApp.with(EditImageFrag.this).load(imagePath).override(filterIvSize).into((ImageView) root.findViewById(R.id.iv_filter_blend));
        GlideApp.with(EditImageFrag.this).load(imagePath).override(filterIvSize).into((ImageView) root.findViewById(R.id.iv_filter_70));
        GlideApp.with(EditImageFrag.this).load(imagePath).override(filterIvSize).into((ImageView) root.findViewById(R.id.iv_filter_40));


        root.findViewById(R.id.edit_filter_normal).setOnClickListener(filterClickListener);
        root.findViewById(R.id.edit_filter_blend).setOnClickListener(filterClickListener);
        root.findViewById(R.id.edit_filter_70).setOnClickListener(filterClickListener);
        root.findViewById(R.id.edit_filter_40).setOnClickListener(filterClickListener);

        resultIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resultIv.setVisibility(View.GONE);
            }
        });


        recyclerView.setAdapter(adapter);


        return root;
    }

    View.OnClickListener filterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (photo_iv != null && photo_iv.getDrawable() instanceof BitmapDrawable
                    && stickerView != null && stickerView.getStickerCount() == 1){

                Bitmap bmp = ((BitmapDrawable) photo_iv.getDrawable()).getBitmap().copy(Bitmap.Config.ARGB_8888, true);
//                Timber.v("OOM_DBG bmp bye count: %s", bmp.getByteCount()); // or check allocated byte count, if Build.VERSION.SDK_INT >= 19
//                Timber.v("BMP_DBG photo_iv zoomed rect pxl: %s", photo_iv.getZoomedRectPxl()); // may need for openCV
                // crop the needed part:
                Bitmap cropped = Bitmap.createBitmap(bmp, (int) photo_iv.getZoomedRectPxl().left,
                        (int) photo_iv.getZoomedRectPxl().top,
                        (int) photo_iv.getZoomedRectPxl().width(),
                        (int) photo_iv.getZoomedRectPxl().height());

                // draw the background to the overlay (result) bitmap
                Bitmap overlay = Bitmap.createBitmap(cropped.getWidth(), cropped.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas  = new Canvas(overlay);
                canvas.drawBitmap(cropped, new Matrix(), null);

                if (stickerView.getCurrentSticker() != null && //fixme: perhaps also take stickers[0]
                        stickerView.getCurrentSticker().getDrawable() instanceof BitmapDrawable){
                    // get a mutable bitmap for the ad:
                    Bitmap stickerBmp = ((BitmapDrawable) stickerView.getCurrentSticker().getDrawable()).getBitmap().isMutable() ?
                            ((BitmapDrawable) stickerView.getCurrentSticker().getDrawable()).getBitmap() :
                            ((BitmapDrawable) stickerView.getCurrentSticker().getDrawable()).getBitmap().copy(Bitmap.Config.ARGB_8888, true);

                    // FIXME: 5/10/2018 may need to check size of sticker (ad)

                    // add alpha if needed:
                    if (v.getId() == R.id.edit_filter_40 || v.getId() == R.id.edit_filter_70){
                        Canvas canvasSticker = new Canvas(stickerBmp);
                        int percentOf255 = (v.getId() == R.id.edit_filter_40) ? 102 : 178;
                        int color = (percentOf255 & 0xFF) <<24;
                        canvasSticker.drawColor(color, PorterDuff.Mode.DST_IN);
                    }

                    // add the ad bitmap:
                    Matrix matrix  = new Matrix();
                    matrix.set(stickerView.getCurrentSticker().getMatrix());

                    float scale = 1.0f *cropped.getWidth() / stickerView.getWidth();
                    Timber.v( "BMP_DBG matrix %s scale: %s", matrix, scale);

                    matrix.postScale(scale, scale);
                    Timber.v("BMP_DBG matrix post-scale %s", matrix);

                    // add a paint with blend - if needed
                    Paint paint = null;
                    if (v.getId() == R.id.edit_filter_blend){
                        paint = new Paint();
                        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.OVERLAY)); // also consider the DARKEN as option
                    }

                    canvas.drawBitmap(stickerBmp, matrix, paint);

                    // display the result in resultIv:
                    resultIv.setVisibility(View.VISIBLE);
                    if (resultIv.getDrawable() instanceof BitmapDrawable &&
                            ((BitmapDrawable) resultIv.getDrawable()).getBitmap() != null){
                        ((BitmapDrawable) resultIv.getDrawable()).getBitmap().recycle();
                    }
                    resultIv.setImageBitmap(overlay);

                    cropped.recycle();
                    cropped = null;
                    stickerBmp.recycle();
                    stickerBmp = null;

                }
            }
        }
    };

    private void loadSizedBitmap(String path){
        Timber.d("OOM_DBG loadSizedBitmap starting");
        File imgFile = new File(path);
        if (imgFile.exists()){
            InputStream is = null;
            try {
                // check the size of the file's bitmap
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                is = new FileInputStream(imgFile);
                BitmapFactory.decodeStream(is, null, options);

                int size = 1024; //  tbd required size
                // take the longer side, to ensure max size of the bitmap:
                int scaledLength = options.outWidth > options.outHeight ? options.outWidth : options.outHeight;
                int sampleSize = 1;
                while (scaledLength > size){
                    scaledLength /= 2;
                    sampleSize*= 2;
                }
                Timber.d("OOM_DBG decoding options width: %s height: %s SampleSize factor:: %s final scaledLength: %s",
                        options.outWidth, options.outHeight, sampleSize, scaledLength);

                // make sure the bitmap is properly oriented:
                Matrix matrix = new Matrix();
                try {
                    ExifInterface exif = new ExifInterface(path);
                    int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                    Timber.v("calculating rotation matrix for orientation %s", orientation);
                    switch (orientation) {
                        case ExifInterface.ORIENTATION_NORMAL:
                            break;
                        case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                            matrix.setScale(-1, 1);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            matrix.setRotate(180);
                            break;
                        case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                            matrix.setRotate(180);
                            matrix.postScale(-1, 1);
                            break;
                        case ExifInterface.ORIENTATION_TRANSPOSE:
                            matrix.setRotate(90);
                            matrix.postScale(-1, 1);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            matrix.setRotate(90);
                            break;
                        case ExifInterface.ORIENTATION_TRANSVERSE:
                            matrix.setRotate(-90);
                            matrix.postScale(-1, 1);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_270:
                            matrix.setRotate(-90);
                            break;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    Timber.e(e, " problem orientating the chosen photo %s", path);
                }
                BitmapFactory.Options options2 = new BitmapFactory.Options();
                options2.inSampleSize = sampleSize;

                is = new FileInputStream(imgFile);

                Bitmap bitmap = BitmapFactory.decodeStream(is,null, options2);

//                if (Build.VERSION.SDK_INT >= 19) {
//                    Timber.v(TAG, "OOM_DBG scaled bitmap byteCount: %s w: %s h: %s", bitmap.getByteCount(), bitmap.getWidth(), bitmap.getHeight());
//                }

                bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);

                photo_iv.setImageBitmap(bitmap);
                photo_iv.setZoom(1f);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Timber.e(e, "OOM_DBG problem reading the chosen photo %s", path);
            } finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Timber.d("OOM_DBG loadSizedBitmap ending");
    }

    private ArrayList<Advertiser> getMockUpAdvertisers(){
        ArrayList<Advertiser> list = new ArrayList<>();
        list.add(new Advertiser(R.drawable.rectangle_4_a));
        list.add(new Advertiser(R.drawable.rectangle_4_a));
        list.add(new Advertiser(R.drawable.rectangle_4_a));
        list.add(new Advertiser(R.drawable.rectangle_4_a));
        list.add(new Advertiser(R.drawable.rectangle_4_a));
        list.add(new Advertiser(R.drawable.rectangle_4_a));
        list.add(new Advertiser(R.drawable.rectangle_4_a));
        list.add(new Advertiser(R.drawable.rectangle_4_a));
        list.add(new Advertiser(R.drawable.rectangle_4_a));
        list.add(new Advertiser(R.drawable.rectangle_4_a));
        list.add(new Advertiser(R.drawable.rectangle_4_a));
        list.add(new Advertiser(R.drawable.rectangle_4_a));
        list.add(new Advertiser(R.drawable.rectangle_4_a));
        list.add(new Advertiser(R.drawable.rectangle_4_a));
        list.add(new Advertiser(R.drawable.rectangle_4_a));
        list.add(new Advertiser(R.drawable.rectangle_4_a));
        return list;
    }
}
