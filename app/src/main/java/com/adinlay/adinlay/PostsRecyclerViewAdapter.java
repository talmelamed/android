package com.adinlay.adinlay;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ablanco.zoomy.TapListener;
import com.ablanco.zoomy.Zoomy;
import com.adinlay.adinlay.objects.Post;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Noya on 4/10/2018.
 */

public class PostsRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = PostsRecyclerViewAdapter.class.getSimpleName();

    private ArrayList<Post> postsList;
//    private GlideRequest<Drawable> requestBuilder;
    private int displayMode;

    private WeakReference<ItemClick> wr_ItemClickCallback = new WeakReference<ItemClick>(null);
    private WeakReference<Activity> wr_Activity = new WeakReference<Activity>(null);
    private WeakReference<GlideRequests> wr_glideWith = new WeakReference<GlideRequests>(null);
    private WeakReference<GlideRequest<Drawable>> wr_requestBuilder = new WeakReference<GlideRequest<Drawable>>(null);

    private int fullWidth = 0;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({HomeFrag.ALL_ADS_MOSAIC, HomeFrag.ALL_ADS, HomeFrag.MY_ADS})
    public @interface DisplayMode{}


    public PostsRecyclerViewAdapter(ArrayList<Post> postsList, @DisplayMode int mode,
                                    GlideRequests glideRequests, ItemClick callback, Activity activity) {
        this.postsList = postsList;
        this.displayMode = mode;

        wr_glideWith = new WeakReference<GlideRequests>(glideRequests);
        if (glideRequests != null){
            wr_requestBuilder = new WeakReference<GlideRequest<Drawable>>(glideRequests.asDrawable().transforms(new CenterCrop()));
        }
        wr_ItemClickCallback = new WeakReference<ItemClick>(callback);
        wr_Activity = new WeakReference<Activity>(activity);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (fullWidth == 0){
            fullWidth = parent.getMeasuredWidth();
        }

        if (displayMode == HomeFrag.ALL_ADS_MOSAIC){
            View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_just_iv, parent, false);
            return new MosaicViewHolder(root);
        }
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false);
        return new PostViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        Post currPost = postsList.get(position);
        boolean isFcbk = currPost.checkPlatform(Post.PLATFORM_FACEBOOK);
        boolean isFcbkLinkDimen = currPost.isFacebookLinkSize();
//        Timber.v("onBindViewHolder position %s", position);

        // check if don't we need to redraw the item:
        if (holder.itemView.getTag(R.id.post_tag) instanceof Post &&
                !currPost.getPostId().isEmpty() &&
                currPost.getPostId().equals(((Post) holder.itemView.getTag(R.id.post_tag)).getPostId())){
//            Timber.v("onBindViewHolder position %s got same item ", position, currPost.getPostId());
            return;
        }

        ImageView iv = holder instanceof PostViewHolder ? ((PostViewHolder) holder).adIv :
                holder instanceof MosaicViewHolder ? ((MosaicViewHolder) holder).adIv : null;
        GlideRequests gr = wr_glideWith.get();
        if (iv != null && gr != null){
            String imageUrl =  holder instanceof PostViewHolder ? currPost.getPhotoUrl() : currPost.getThmbnailUrl(); // trying: full size image in MyAds
            boolean isFullSizeUrl = false;
            if (imageUrl.isEmpty()){
                imageUrl = currPost.getPhotoUrl();
                isFullSizeUrl = true;
            }
            if (!imageUrl.isEmpty()) {
                gr.clear(iv);
                if (holder instanceof PostViewHolder){
                    ConstraintLayout.LayoutParams lpIv = (ConstraintLayout.LayoutParams) ((PostViewHolder)holder).adIv.getLayoutParams();
                    lpIv.dimensionRatio = isFcbkLinkDimen? "1200:630" : "1:1";
                }
                GlideRequest<Drawable> ivRequest;
                if (wr_requestBuilder.get() == null){
                    wr_requestBuilder = new WeakReference<GlideRequest<Drawable>>(gr.asDrawable().transforms(new CenterCrop()));
                    ivRequest = gr.asDrawable().transforms(new CenterCrop());
                } else {
                    ivRequest = wr_requestBuilder.get().clone();
                }

                if (holder instanceof MosaicViewHolder){
                    ivRequest = ivRequest.diskCacheStrategy(DiskCacheStrategy.ALL);
                    ((MosaicViewHolder) holder).adIv.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }

//                int overrideW = iv.getWidth();
                int overrideW = holder instanceof PostViewHolder || isFcbk || currPost.getMosaicSize() ==  Post.BIG_INST_SIZE ? fullWidth : fullWidth/2;
                if (overrideW <= 0){
                    if (holder instanceof MosaicViewHolder && isFullSizeUrl){
                        overrideW = overrideW/2;
                    }
                }
                int overrideH = isFcbkLinkDimen ? overrideW * 630 / 1200 : overrideW;
                int downSizeDivider = holder instanceof PostViewHolder || !isFullSizeUrl ? 1 : 1; // now disabled, used to use half in case we don't have a thumbnail url

                if (overrideW > 0 && overrideH > 0){
                    ivRequest.load(imageUrl)
//                            .skipMemoryCache(true)
                            .error(android.R.drawable.ic_menu_gallery) // too small!!!
                            .override(overrideW/downSizeDivider, overrideH/downSizeDivider)
                            .centerCrop()
                            .downsample(DownsampleStrategy.AT_MOST)
                            .thumbnail(0.2f)
                            .into(iv);
                } else {
                    ivRequest.load(imageUrl)
//                            .skipMemoryCache(true)
                            .error(android.R.drawable.ic_menu_gallery) // too small!!!
                            .centerCrop()
                            .downsample(DownsampleStrategy.AT_MOST)
                            .thumbnail(0.2f)
                            .into(iv);
                }
            } else { // shouldn't happen!!!
                iv.setImageResource(0);
            }

            if (wr_Activity.get() != null && holder instanceof PostViewHolder) {
                Zoomy.Builder zoomyBuilder = new Zoomy.Builder(wr_Activity.get()).target(iv);
//                if (holder instanceof MosaicViewHolder) { // disable zoom in mosaic
//                    zoomyBuilder.tapListener(mosaicTapListener);
//                }
                zoomyBuilder.tapListener(mosaicTapListener);
                zoomyBuilder.register();
            }
        }

        if (holder instanceof PostViewHolder){

            PostViewHolder pvHolder = (PostViewHolder) holder;
            pvHolder.publisherFrame.setVisibility(View.GONE); // todo: if we get publisher image - use this, not title
//        if (isMyAds){
//            holder.publisherFrame.setVisibility(View.GONE);
//        } else {
//            holder.publisherFrame.setVisibility(View.GONE);
////            holder.publisherFrame.setVisibility(View.VISIBLE);
////            holder.publisherName.setText(currPost.getPublisher().getName());
////            holder.publisherIcon.setImageResource(currPost.getPublisher().getMockUpRes()); // manage some placeholder
//        }

            if (displayMode == HomeFrag.ALL_ADS) {
//                pvHolder.brandName.setText(currPost.getDisplayName());
            }

            int platformRes = currPost.checkPlatform(Post.PLATFORM_INSTAGRAM) ? R.drawable.page_1_c :
                    isFcbk ? R.drawable.facebook_c : 0;
            pvHolder.mediaIndicator.setImageResource(platformRes);

            String text = currPost.getText().trim();
            if (!text.isEmpty()) {
                pvHolder.adDescription.setOnClickListener(tvReadMoreListener);
                text = text.replaceAll("(?m)^[ \t]*\r?\n", "");
                pvHolder.adDescription.setText(text);
                pvHolder.adDescription.setVisibility(View.VISIBLE);
                int maxLines = currPost.isShowingAllText() ? Integer.MAX_VALUE : 2;
                pvHolder.adDescription.setMaxLines(maxLines);
            } else {
                pvHolder.adDescription.setVisibility(View.GONE);
            }

            if (displayMode == HomeFrag.MY_ADS){
                pvHolder.value.setVisibility(View.VISIBLE);
                String price = AyUtils.getStringAmountWithCurrencySymbol(currPost.getPrice(),
                        AyUtils.DECIMAL_PLACES_DEFAULT, pvHolder.value.getResources());
                pvHolder.value.setText(price);
            } else {
                pvHolder.value.setVisibility(View.GONE);
            }

            String when = AyUtils.parseUtcStringToTimeAgo(currPost.getDateString());
            pvHolder.whenPublished.setText(when);

            if(isFcbk){
                pvHolder.likeIv.setVisibility(View.INVISIBLE);
                pvHolder.likesNum.setVisibility(View.INVISIBLE);
            } else {
                pvHolder.likeIv.setVisibility(View.VISIBLE);
                pvHolder.likesNum.setVisibility(View.VISIBLE);
                String likes = "" + currPost.getLikes();
                pvHolder.likesNum.setText(likes);
            }

            pvHolder.brandName.setText(AyUtils.getBrandName(currPost.getBrandId()));

            pvHolder.adIv.setTag(R.id.post_tag, currPost);

        } else if (holder instanceof MosaicViewHolder){
            holder.itemView.setOnClickListener(mosaicClickListener);

            // display/hide border based on position
            int topVisibility = position == 0 ? View.GONE : View.VISIBLE;
            ((MosaicViewHolder) holder).topBorder.setVisibility(topVisibility);
            int bottomVisibility = position == getItemCount() - 1 ? View.GONE : View.VISIBLE;
            ((MosaicViewHolder) holder).bottomBorder.setVisibility(bottomVisibility);
            // middle borders:
            if (currPost.getMosaicSize() == 1){
                // small inst, check if other smallInst is before or after:
                if (postsList.size() > position + 1 && postsList.get(position+1) != null &&
                        postsList.get(position+1).getMosaicSize() == 1){
                    ((MosaicViewHolder) holder).leftBorder.setVisibility(View.GONE);
                    ((MosaicViewHolder) holder).rightBorder.setVisibility(View.VISIBLE);
                } else {
                    ((MosaicViewHolder) holder).leftBorder.setVisibility(View.VISIBLE);
                    ((MosaicViewHolder) holder).rightBorder.setVisibility(View.GONE);
                }
            } else {
                ((MosaicViewHolder) holder).leftBorder.setVisibility(View.GONE);
                ((MosaicViewHolder) holder).rightBorder.setVisibility(View.GONE);
            }
        }
        holder.itemView.setTag(R.id.post_tag, currPost);
        holder.itemView.setTag(R.id.position_tag, position);

    }

    private View.OnClickListener mosaicClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Post post = null;
            if (v.getTag(R.id.post_tag) instanceof Post){
                post = (Post) v.getTag(R.id.post_tag);
            }
            Timber.v("clicked on post: %s", post);
            if (wr_ItemClickCallback.get() != null && post != null){
                wr_ItemClickCallback.get().onMosaicItemClicked(post);
            }
        }
    };

    private TapListener mosaicTapListener = new TapListener() {
        @Override
        public void onTap(View v) {
            Post post = null;
            if (v.getTag(R.id.post_tag) instanceof Post){
                post = (Post) v.getTag(R.id.post_tag);
            }
            Timber.v("clicked on post: %s", post);
            if (wr_ItemClickCallback.get() != null && post != null){
                wr_ItemClickCallback.get().onMosaicItemClicked(post);
            }
        }
    };

//    @Override
//    public void onViewRecycled(@NonNull RecyclerView.ViewHolder holder) {
//        super.onViewRecycled(holder);
//        GlideRequests gr = wr_glideWith.get();
//        ImageView iv = holder instanceof PostViewHolder ? ((PostViewHolder) holder).adIv :
//                holder instanceof MosaicViewHolder ? ((MosaicViewHolder) holder).adIv : null;
//        if (gr != null && iv != null){
//            gr.clear(iv);
//        }
//    }

    interface ItemClick{
        void onMosaicItemClicked(Post post);
    }

    @Override
    public int getItemCount() {
        return postsList.size();
    }

    private View.OnClickListener tvReadMoreListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v instanceof TextView){
                boolean isOpen = false;
                if (((TextView) v).getMaxLines() > 2){
                    ((TextView) v).setMaxLines(2);
                } else {
                    ((TextView) v).setMaxLines(Integer.MAX_VALUE);
                    isOpen = true;
                }
                if (v.getParent() instanceof View && ((View) v.getParent()).getTag(R.id.post_tag) instanceof Post){
                    Post post = (Post) ((View) v.getParent()).getTag(R.id.post_tag);
                    post.setShowingAllText(isOpen);
                }
            }
        }
    };

    public static class PostViewHolder extends RecyclerView.ViewHolder {

        TextView brandName;
        ImageView adIv;
        TextView adDescription;
        TextView adinlayLikesNum;
        TextView likesNum;
        ImageView likeIv;
        TextView value;
        TextView whenPublished;
//        ImageView publisherIcon;
//        TextView publisherName;
        View publisherFrame;
        ImageView mediaIndicator;


        public PostViewHolder(View itemView) {
            super(itemView);

            brandName = itemView.findViewById(R.id.brand_name);
            adIv = itemView.findViewById(R.id.ad_iv);
            adDescription = itemView.findViewById(R.id.ad_desc);
            adinlayLikesNum = itemView.findViewById(R.id.post_ay_like_count);
            likesNum = itemView.findViewById(R.id.ad_like_count);
            likeIv = itemView.findViewById(R.id.ad_like_btn);
            value = itemView.findViewById(R.id.price_tv);
            whenPublished = itemView.findViewById(R.id.ad_timestamp);
            publisherFrame = itemView.findViewById(R.id.publisher_frame);
            mediaIndicator = itemView.findViewById(R.id.social_indicators);
//            publisherIcon = itemView.findViewById(R.id.publisher_photo);
//            publisherName = itemView.findViewById(R.id.publisher_name);
        }
    }

    public static class MosaicViewHolder extends RecyclerView.ViewHolder {

        ImageView adIv;
        View topBorder;
        View leftBorder;
        View rightBorder;
        View bottomBorder;

        public MosaicViewHolder(View itemView) {
            super(itemView);

            adIv = itemView.findViewById(R.id.item_iv);
            topBorder = itemView.findViewById(R.id.item_top_divider);
            leftBorder = itemView.findViewById(R.id.item_left_divider);
            rightBorder = itemView.findViewById(R.id.item_right_divider);
            bottomBorder = itemView.findViewById(R.id.item_bottom_divider);
        }
    }

}
