package com.adinlay.adinlay;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adinlay.adinlay.adstickerview.BitmapStickerIcon;
import com.adinlay.adinlay.adstickerview.DrawableSticker;
import com.adinlay.adinlay.adstickerview.Sticker;
import com.adinlay.adinlay.adstickerview.StickerIconEvent;
import com.adinlay.adinlay.adstickerview.StickerView;
import com.adinlay.adinlay.adstickerview.ZoomIconEvent;
import com.adinlay.adinlay.gui.TouchImageView;
import com.adinlay.adinlay.objects.Ad;
import com.adinlay.adinlay.objects.DraftPost;
import com.adinlay.adinlay.objects.OverlayInfo;
import com.adinlay.adinlay.objects.PostFilter;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.Arrays;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import timber.log.Timber;


/**
 * Created by Noya on 8/8/2018.
 */

public class EditPostActivity extends AppCompatActivity implements ItemClick {

    private static final String TAG = EditPostActivity.class.getSimpleName();
    private static final String INSTRUCTIONS_TAG = "instructions_tag";


    private TouchImageView mainIv = null;
    private ImageView mainAdIv = null;
    private ImageView filterOrLayoutButton = null;
    private StickerView stickerView = null;

    protected RecyclerView recyclerView = null;
    private ContentLoadingProgressBar spinner = null;
    private View spinnerFrame = null;

    EditPostViewModel model = null;
    private boolean isfcbk = false;
    private boolean isProcessingImage = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_post);

        setSupportActionBar(null);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= 23){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
        }

        // toolbar:
        findViewById(R.id.toolbar_right_tv).setVisibility(View.GONE);
        TextView btn1 = findViewById(R.id.toolbar_left_tv);
        btn1.setText(getResources().getString(R.string.arrow_and_back));
        btn1.setTextColor(Color.BLACK);
        findViewById(R.id.toolbar_left_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.toolbar_right_iv).setVisibility(View.VISIBLE);
        findViewById(R.id.toolbar_right_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });

        model = ViewModelProviders.of(this).get(EditPostViewModel.class);

        mainIv = findViewById(R.id.ep_main_iv);
        mainAdIv = findViewById(R.id.ep_ad_iv);
        View rvFrame = findViewById(R.id.ep_rv_frag_frame);
        filterOrLayoutButton = findViewById(R.id.ep_filter);
        ImageView nextButton = findViewById(R.id.ep_next);
        spinnerFrame = findViewById(R.id.ep_spinner_frame);
        spinner = findViewById(R.id.ep_spinner);
        stickerView = findViewById(R.id.ep_sticker);

        if (AyUtils.getMinRecyclerHeightDp() > 0){
            // adjust frame for recyclerView:
            ConstraintLayout.LayoutParams fragLP = (ConstraintLayout.LayoutParams) rvFrame.getLayoutParams();
            fragLP.height = AyUtils.convertDpToPixel(AyUtils.getMinRecyclerHeightDp(), EditPostActivity.this);
            // adjust mainView:
            ConstraintLayout.LayoutParams lpIv = (ConstraintLayout.LayoutParams) mainIv.getLayoutParams();
            lpIv.dimensionRatio = "w,1:1";
            lpIv.bottomToTop = R.id.ep_rv_frag_frame;
            Timber.d("MAIN_RESIZE changed iv lp");
            // requestLayout:
            rvFrame.requestLayout();
            mainIv.requestLayout();
        }

        Intent intent = getIntent();
        String imagePath = intent.getStringExtra(PhotoHubActivity.SELECTED_IMAGE_PATH);
        if (imagePath == null || AdinlayApp.getSelectedAd() == null){
            finish();
        }
        model.setImagePath(imagePath);
        model.fetchFilters();

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        InstructionsFrag instructionsFrag = new InstructionsFrag();
        instructionsFrag.setItemClickCallback(this);
        ft.replace(R.id.ep_instructions_frame, instructionsFrag, INSTRUCTIONS_TAG);

        AdLayoutsFrag frag = new AdLayoutsFrag();
        frag.setItemClickCallback(this);
        ft.replace(R.id.ep_rv_frag_frame, frag, TAG);

        ft.commit();


        mainIv.setMaxZoom(5f);
        Bitmap bitmap = AyUtils.getSizedBitmap(model.getImagePath(), -1, model.getScaleFactorSaver());
//        Timber.v("model.getScaleFactorSaver : %s", model.getScaleFactorSaver()[0]);
        model.setBaseImageBitmap(bitmap.copy(Bitmap.Config.ARGB_8888, true));
        mainIv.setImageBitmap(bitmap);

        Ad selectedAd = AdinlayApp.getSelectedAd();
        if (selectedAd != null){

            if (!selectedAd.isStickerCampaign()) {
                stickerView.setVisibility(View.GONE);
                GlideApp.with(getApplicationContext())
                        .load(selectedAd.getSelectedLayoutUrl(true))
                        .thumbnail(0.2f)
                        .into(mainAdIv);
            } else {
                Drawable zoomD = ContextCompat.getDrawable(this, android.R.drawable.ic_menu_rotate);
                BitmapStickerIcon zoomIcon = new BitmapStickerIcon(zoomD, BitmapStickerIcon.RIGHT_BOTOM);
                zoomIcon.setIconEvent(new ZoomIconEvent());
                Drawable deleteD = ContextCompat.getDrawable(this, android.R.drawable.ic_menu_close_clear_cancel);
                BitmapStickerIcon deleteIcon = new BitmapStickerIcon(deleteD, BitmapStickerIcon.LEFT_TOP);
                deleteIcon.setIconEvent(new StickerIconEvent() {
                    @Override
                    public void onActionDown(StickerView stickerView, MotionEvent event) {}

                    @Override
                    public void onActionMove(StickerView stickerView, MotionEvent event) {}

                    @Override
                    public void onActionUp(StickerView stickerView, MotionEvent event) {
                        stickerView.removeCurrentSticker();
                        if (stickerView.getStickerCount() == 0){
                            mainIv.setDisableScale(false);
                        }
                    }
                });
                stickerView.setIcons(Arrays.asList(deleteIcon, zoomIcon));
                stickerView.setConstrained10percent(true);
//        stickerView.setOnStickerOperationListener(stickerListener);

                mainIv.setDisableScale(true);
                mainIv.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {
                    @Override
                    public boolean onSingleTapConfirmed(MotionEvent e) {
                        if (stickerView.getStickerCount() > 0){
                            Sticker sticker = stickerView.checkForStickerAndLock(e.getX(), e.getY()); // this seems to work, now disable touchImageView while sticker is operational
                            boolean isOnSticker = sticker != null;
                            mainIv.setDisableScale(isOnSticker);
//                    Timber.i("STICK_ main onSingleTapConfirmed we are now locked? %s, isOnSticker? %s", stickerView.isLocked(), isOnSticker);
                            return true;
                        }
                        return false;
                    }

                    @Override
                    public boolean onDoubleTap(MotionEvent e) {
                        // fixme_sticker: currently the doubleTap of this listener doesn't consume the action, tbd desired behavior
                        return false;
                    }

                    @Override
                    public boolean onDoubleTapEvent(MotionEvent e) {
                        return false;
                    }
                });

                GlideApp.with(getApplicationContext())
                        .load(selectedAd.getSelectedLayoutUrl(true))
                        .into(stickerTarget);
            }

//            if (selectedAd.needsApproval()){
//                nextButton.setImageResource(R.drawable.rectangle_send2approve);
//            }
            if ("facebook".equalsIgnoreCase(selectedAd.getTargetMedia())){
                isfcbk = true;
                if (!selectedAd.isStickerCampaign()) {
                    findViewById(R.id.ep_top_crop_shader).setVisibility(View.VISIBLE);
                    findViewById(R.id.ep_bottom_crop_shader).setVisibility(View.VISIBLE);
                    findViewById(R.id.ep_ad_frame).setVisibility(View.VISIBLE);
                    mainIv.post(new Runnable() {
                        @Override
                        public void run() {
                            View shader = findViewById(R.id.ep_bottom_crop_shader);
                            int h = shader.getHeight();
                            if (h > 0){
                                mainIv.setPadding(0, h,0, h);
                                mainIv.setCropToPadding(false);
                            }
                        }
                    });
                }

            }

        }


        nextButton.setVisibility(View.VISIBLE);
        filterOrLayoutButton.setVisibility(View.VISIBLE);

        filterOrLayoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceRecyclerFrag();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Timber.i("click on next next next isProcessingImage? %s", isProcessingImage);
                if (isProcessingImage){
                    return;
                }
                if (mainIv.getDrawable() instanceof BitmapDrawable){
                    isProcessingImage = true;
                    model.getSpinnerLiveData().setValue(true);
                    Ad selectedAd = AdinlayApp.getSelectedAd();
                    boolean isSticker = selectedAd != null && selectedAd.isStickerCampaign();
                    if (isSticker){
                        fetchFullSizeStickerAndRenderPost();
                        return;
                    }
                    fetchFullSizeAdAndRenderPost();
                } else {
                    isProcessingImage = false;
                }
            }
        });

        model.getSpinnerLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean show) {
                if (show == null || !show){
                    spinner.hide();
                    spinnerFrame.setVisibility(View.GONE);
                } else {
                    spinnerFrame.setVisibility(View.VISIBLE);
                    spinner.show();
                }
            }
        });
        spinnerFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do nothing, prevents clicks
//                Timber.i("click on spinnerFrame spinnerFrame spinnerFrame");
            }
        });

        model.getNotifyErrorProcessingImage().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (s != null && !s.isEmpty()){
                    Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    SimpleTarget<Drawable> stickerTarget = new SimpleTarget<Drawable>() {
        @Override
        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
            if (stickerView.getStickerCount() > 0){
                stickerView.removeAllStickers();
            }
            Sticker sticker = new DrawableSticker(resource);
            stickerView.addSticker(sticker);
            stickerView.setLocked(false);
            mainIv.setDisableScale(true);
        }
    };

    StickerView.OnStickerOperationListener stickerListener = new StickerView.OnStickerOperationListener() {
        @Override
        public void onStickerAdded(@NonNull Sticker sticker) {}

        @Override
        public void onStickerClicked(@NonNull Sticker sticker) {
            if (stickerView.getStickerCount() > 0){
//                stickerView.setLocked(true);
//                Timber.i("STICK_ we are now locked? %s", stickerView.isLocked());
            }
        }

        @Override
        public void onStickerDeleted(@NonNull Sticker sticker) {}

        @Override
        public void onStickerDragFinished(@NonNull Sticker sticker) {}

        @Override
        public void onStickerTouchedDown(@NonNull Sticker sticker) {}

        @Override
        public void onStickerZoomFinished(@NonNull Sticker sticker) {}

        @Override
        public void onStickerFlipped(@NonNull Sticker sticker) {}

        @Override
        public void onStickerDoubleTapped(@NonNull Sticker sticker) {
//            if (stickerView.getStickerCount() > 0){
//                stickerView.setLocked(true);
////                Timber.i("STICK_ we are now locked? %s", stickerView.isLocked());
//            }
        }
    };

    private void fetchFullSizeAdAndRenderPost(){
        SimpleTarget<Bitmap> adTarget = new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                Timber.i("TARGET_ onResourceReady, h: %s w: %s", resource.getHeight(), resource.getWidth());
                if (mainIv.getDrawable() instanceof BitmapDrawable){
                    shareFullSizeAdAsync(resource);

                } else { // shouldn't happen!
                    model.getSpinnerLiveData().postValue(false);
                    isProcessingImage = false;
                    Timber.w("TARGET_ mainIv doesn't have bitmapDrawable?!?");
                }
            }
        };
        Ad selectedAd = AdinlayApp.getSelectedAd();
        String url = selectedAd != null ? selectedAd.getSelectedLayoutUrl(true) : "";
//        Timber.i("TARGET_ start");
        GlideApp.with(EditPostActivity.this).asBitmap().load(url).into(adTarget);
    }

    private void shareFullSizeAdAsync(@NonNull final Bitmap resource){
        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean success = false;
                if (mainIv.getDrawable() instanceof BitmapDrawable){

                    // DONE Eliyahu: switch to sampleSize, which is saved in model.getScaleFactorSaver()[0]
                    double scaleWidth = model.getScaleFactorSaver()[0];
                    double scaleHeight = model.getScaleFactorSaver()[0];

                    /* // ELIYAHU: deprecated manual estimation of scale factor from image's dimensions.
                    Bitmap bmp = ((BitmapDrawable) mainIv.getDrawable()).getBitmap().copy(Bitmap.Config.ARGB_8888, true);
                    //Timber.i("OOM_DBG bmp byte count: %s", bmp.getByteCount()); // or check allocated byte count, if Build.VERSION.SDK_INT >= 19
                    //Timber.i("BMP_DBG photo_iv zoomed rect pxl: %s", mainIv.getZoomedRectPxl()); // may need for openCV

                    int bmpWidth = bmp.getWidth();
                    int bmpHeight = bmp.getHeight();

                    Rect imSizeOrig = AyUtils.getOriginalImageSize(model.getImagePath());

                    double scaleWidth = Math.round((double) imSizeOrig.right / (double) Math.max(bmpWidth, bmpHeight));
                    double scaleHeight = Math.round((double) imSizeOrig.bottom / (double) Math.min(bmpWidth, bmpHeight));

                    if (scaleWidth!=scaleHeight) // in case of +/-90 rotations - reverse
                    {
                        scaleWidth = Math.round((double) imSizeOrig.right / (double) Math.min(bmpWidth, bmpHeight));
                        scaleHeight = Math.round((double) imSizeOrig.bottom / (double) Math.max(bmpWidth, bmpHeight));
                    }
                                        bmp.recycle();
                    */

                    RectF zoomedRectPxl = mainIv.getZoomedRectPxl();
                    // rect should be (X,Y,X+W,Y+H) for BitmapRegionDecoder ( 4 corners coordinates!!! )
                    Rect cropRectOrig = new Rect((int) (zoomedRectPxl.left * scaleWidth),
                            (int) (zoomedRectPxl.top * scaleHeight),
                            (((int) (zoomedRectPxl.width() * scaleWidth))+((int)(zoomedRectPxl.left * scaleWidth))),
                            (((int) (zoomedRectPxl.height() * scaleHeight))+ ((int)(zoomedRectPxl.top * scaleHeight))) );

                    Bitmap croppedOrig =  AyUtils.getBitmapRegion(model.getImagePath(), cropRectOrig, EditPostActivity.this);
                    if (croppedOrig == null){
                        model.getNotifyErrorProcessingImage().postValue(getString(R.string.something_wrong_processing_image));
                        model.getSpinnerLiveData().postValue(false);
                        isProcessingImage = false;
                        Timber.d("shareFullSizeAdAsync getBitmapRegion returned null");
                        return;
                    }


                    // draw the background to the overlay (result) bitmap
                    Bitmap adBitmap = resource;
                    if (isfcbk){

                        adBitmap = Bitmap.createBitmap(resource, 0,
                                (int) (0.5f * (resource.getHeight() - (630f/1200f * resource.getHeight()))),
                                resource.getWidth(),
                                (int) (630f/1200f * resource.getHeight()));
//                        Timber.i( "fb_dbg ck size: %s : %s = %s", adBitmap.getWidth(), adBitmap.getHeight(), (1f * adBitmap.getWidth() / adBitmap.getHeight()));
                    }

                    int scale = 1;
                    while (!success && scale <= 4){
                        try {

                            Bitmap overlay = Bitmap.createBitmap(adBitmap.getWidth() / scale,  adBitmap.getHeight() / scale, Bitmap.Config.ARGB_8888);

                            Canvas canvas  = new Canvas(overlay);
//                            canvas.drawBitmap(cropped, null, new Rect(0,0, overlay.getWidth(), overlay.getHeight()), null);
                            canvas.drawBitmap(croppedOrig, null, new Rect(0,0, overlay.getWidth(), overlay.getHeight()), null);
                            canvas.drawBitmap(adBitmap, null, new Rect(0,0, overlay.getWidth(), overlay.getHeight()), null);

                            DraftPost selected = AdinlayApp.getDraftPost();
                            if (selected != null){
                                selected.setDraftPostBitmap(overlay);
                                Intent shareIntent = new Intent(EditPostActivity.this, SharePostActivity.class);
                                startActivityForResult(shareIntent, AyUtils.get16BitId(R.id.intent_share_new_post));
                            }
                            success = true;
                        } catch (OutOfMemoryError e){
                            scale *= 2;
                            Timber.d("shareFullSizeAdAsync will now try to scale down by: %s", scale);
                        }
                    }

                    croppedOrig.recycle();
                }
                model.getSpinnerLiveData().postValue(false);
                if (!success){
                    model.getNotifyErrorProcessingImage().postValue(getString(R.string.something_wrong_processing_image));
                    isProcessingImage = false;
                }
                Timber.i("TARGET_ done");
            }
        }).start();
    }

    private void fetchFullSizeStickerAndRenderPost(){
        // if no sticker:
        if (stickerView.getStickerCount() == 0){
            shareFullSizeStickerAsync(null);
            return;
        }
        SimpleTarget<Bitmap> adTarget = new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                Timber.i("TARGET_ onResourceReady, h: %s w: %s", resource.getHeight(), resource.getWidth());
                if (mainIv.getDrawable() instanceof BitmapDrawable){
                    shareFullSizeStickerAsync(resource);

                } else { // shouldn't happen!
                    model.getSpinnerLiveData().postValue(false);
                    isProcessingImage = false;
                    Timber.w("TARGET_ mainIv doesn't have bitmapDrawable?!?");
                }
            }
        };
        Ad selectedAd = AdinlayApp.getSelectedAd();
        String url = selectedAd != null ? selectedAd.getSelectedLayoutUrl(true) : "";
//        Timber.i("TARGET_ start");
        GlideApp.with(EditPostActivity.this).asBitmap().load(url).into(adTarget);
    }


    private void shareFullSizeStickerAsync(final Bitmap resource){
        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean success = false;
                if (mainIv.getDrawable() instanceof BitmapDrawable){

                    // get the background image scale (sampleSize), which was  saved in model.getScaleFactorSaver()[0]
                    double scaleWidth = model.getScaleFactorSaver()[0];
                    double scaleHeight = model.getScaleFactorSaver()[0];


                    RectF zoomedRectPxl = mainIv.getZoomedRectPxl();
                    // rect should be (X,Y,X+W,Y+H) for BitmapRegionDecoder ( 4 corners coordinates!!! )
                    Rect cropRectOrig = new Rect((int) (zoomedRectPxl.left * scaleWidth),
                            (int) (zoomedRectPxl.top * scaleHeight),
                            (((int) (zoomedRectPxl.width() * scaleWidth))+((int)(zoomedRectPxl.left * scaleWidth))),
                            (((int) (zoomedRectPxl.height() * scaleHeight))+ ((int)(zoomedRectPxl.top * scaleHeight))) );

                    Bitmap croppedOrig =  AyUtils.getBitmapRegion(model.getImagePath(), cropRectOrig, EditPostActivity.this);
                    if (croppedOrig == null){
                        model.getNotifyErrorProcessingImage().postValue(getString(R.string.something_wrong_processing_image));
                        model.getSpinnerLiveData().postValue(false);
                        isProcessingImage = false;
                        Timber.d("shareFullSizeStickerAsync getBitmapRegion returned null");
                        return;
                    }


                    // draw the background to the overlay (result) bitmap
                    int resultW =  resource != null? resource.getWidth() : 1024;
                    int resultH =  resource != null? resource.getHeight() : 1024;
                    float difY = 0f;

//                    Timber.i( "fb_dbg stick_ ck size: %s : %s, difY: %s", resultW, resultH, difY);

                    int scale = 1;
                    while (!success && scale <= 4){
                        try {

                            Bitmap overlay = Bitmap.createBitmap(resultW / scale,  resultH / scale, Bitmap.Config.ARGB_8888);

                            Canvas canvas  = new Canvas(overlay);

                            canvas.drawBitmap(croppedOrig, null, new Rect(0,0, overlay.getWidth(), overlay.getHeight()), null);

                            Matrix matrix  = new Matrix();
                            Sticker sticker = stickerView.getAdSticker();
                            if (sticker != null){
                                float[] mappedF = sticker.getMappedBoundPoints();
                                int viewWidth = stickerView.getWidth() > 0 ? stickerView.getWidth() : 1;
                                float scaleW = 1.0f / scale * resultW / viewWidth;
//                                Timber.i( "BMP_DBG matrix mappedF bound is %s", Arrays.toString(mappedF));
//                                Timber.i( "BMP_DBG matrix sticker is       %s", sticker.getMatrix());
//                                Timber.i( "BMP_DBG matrix scaleW is        %s", scaleW);
//                                Timber.i( "BMP_DBG sticker current width is %s", sticker.getCurrentWidth());
//                                Timber.i( "BMP_DBG sticker current scale is %s", sticker.getCurrentScale());
//                                Timber.i( "BMP_DBG sticker current angle is %s", sticker.getCurrentAngle());

                                // step 1: first we scale
                                matrix.postScale(sticker.getCurrentScale(), sticker.getCurrentScale());
                                matrix.postScale(scaleW, scaleW);
//                                Timber.i( "BMP_DBG postScale matrix is     %s", matrix);

                                // step 2: then rotate
                                matrix.postRotate(sticker.getCurrentAngle());
//                                Timber.i( "BMP_DBG postRotate matrix is    %s", matrix);

                                // step 3: and only now we translate
                                matrix.postTranslate(mappedF[0] *scaleW, mappedF[1] *scaleW  - difY/scale);
//                                Timber.i( "BMP_DBG postTranslate matrix is %s", matrix.toString());

                            }

                            if (resource != null) {
                                canvas.drawBitmap(resource, matrix, null); // we use  adBitmap to crop and get the matrix, but draw with resource
                            }

                            DraftPost selected = AdinlayApp.getDraftPost();
                            if (selected != null){
                                selected.setDraftPostBitmap(overlay);
                                Intent shareIntent = new Intent(EditPostActivity.this, SharePostActivity.class);
                                startActivityForResult(shareIntent, AyUtils.get16BitId(R.id.intent_share_new_post));
                            }
                            success = true;
                        } catch (OutOfMemoryError e){
                            scale *= 2;
                            Timber.d("shareFullSizeStickerAsync will now try to scale down by: %s", scale);
                        }
                    }

                    croppedOrig.recycle();
                }
                model.getSpinnerLiveData().postValue(false);
                if (!success){
                    model.getNotifyErrorProcessingImage().postValue(getString(R.string.something_wrong_processing_image));
                    isProcessingImage = false;
                }
                Timber.i("TARGET_ done");
            }
        }).start();
    }


    private void replaceRecyclerFrag(){
        FragmentManager fm = getSupportFragmentManager();

        Fragment currFrag = fm.findFragmentByTag(TAG);
        Fragment newFrag = null;
        if (currFrag instanceof AdLayoutsFrag){
            newFrag = new FiltersFrag();
            ((FiltersFrag)newFrag).setItemClickCallback(this);
            filterOrLayoutButton.setImageResource(R.drawable.ic_filters_ylw);
        } else if (currFrag instanceof FiltersFrag){
            newFrag = new AdLayoutsFrag();
            ((AdLayoutsFrag)newFrag).setItemClickCallback(this);
            filterOrLayoutButton.setImageResource(R.drawable.ic_filters_wt);
        }
        if (newFrag == null){
            return;
        }
        FragmentTransaction ft = fm.beginTransaction();
        if (currFrag instanceof AdLayoutsFrag){
            ft.setCustomAnimations(R.anim.slide_in_right_slower, R.anim.slide_out_left);
        } else {
            ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        }
        ft.replace(R.id.ep_rv_frag_frame, newFrag, TAG);
        ft.commit();
    }


    @Override
    public void onItemClicked(Object object) {
        if (object instanceof OverlayInfo && ((OverlayInfo) object).getUrl() != null){
            String urlString = ((OverlayInfo) object).getUrl();
            Timber.d( "Switch ad layout: %s", urlString);

            GlideRequest<Drawable> request = GlideApp.with(this)
                    .load(urlString);
            Ad ad = AdinlayApp.getSelectedAd();
            String thumbUrl = ad == null ? "" : ad.getSelectedLayoutUrl(false);
            boolean isSticker = ad != null && ad.isStickerCampaign();
            if (!isSticker && !thumbUrl.isEmpty() && !urlString.equalsIgnoreCase(thumbUrl)){
                request = request.thumbnail(GlideApp.with(this).load(thumbUrl));
            } else if (!isSticker){
                request = request.thumbnail(0.2f);
            }
            if (!isSticker) {
                request.into(mainAdIv);
            } else {
                request.into(stickerTarget);
            }
        }

        if (object instanceof OverlayInfo || object == null){
            FragmentManager fm = getSupportFragmentManager();
            Fragment instructionsFrag = fm.findFragmentByTag(INSTRUCTIONS_TAG);
            if (instructionsFrag != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ft.remove(instructionsFrag);
                ft.commit();
            }
        }

        if (object instanceof PostFilter){
            Timber.v("click on filter: %s", ((PostFilter) object).getName());
            GPUImageFilter filter = ((PostFilter) object).getGpuImageFilter(this);
            Bitmap bmp = AyUtils.applyFilterToBitmap(
                    model.getBaseImageBitmap().copy(Bitmap.Config.ARGB_8888, true),
                    filter, this);
            DraftPost draft = AdinlayApp.getDraftPost();
            if (draft != null){
                draft.setSelectedFilter((PostFilter) object);
            }
            if (bmp == null){
                AyUtils.loadSizedBitmapToIv(model.getImagePath(), mainIv, filter, -1, model.getScaleFactorSaver());
            } else {
//                if (filter == null){
//                    mainIv.setImageBitmap(bmp);
//                } else {
//                    // todo Eliyahu: vignette filter - get the params for mainIv - center, croppedRect
//                    GPUImage gpuImage = new GPUImage(this);
//                    gpuImage.setImage(bmp);
//                    gpuImage.setFilter(((PostFilter) object).getGpuImageFilter(this));
//                    Bitmap b = gpuImage.getBitmapWithFilterApplied();
//                }
                mainIv.setImageBitmap(bmp);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isProcessingImage = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (AdinlayApp.getUser() == null || AdinlayApp.getDraftPost() == null){
            // lost state - let's let mainActivity deal with it
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case R.id.intent_share_new_post & 0x0000FFFF:
                if (resultCode == RESULT_OK){
//                    Timber.v("share_dbg");
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
                break;
        }
    }

    @Override
    public void finish() {
        super.finish();
        AdinlayApp.clearDraftBitmap();
        model.recycleBaseImageBitmap();
        Timber.v("clearing bitmap");
    }
}
