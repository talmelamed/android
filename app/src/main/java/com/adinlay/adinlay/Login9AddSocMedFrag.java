package com.adinlay.adinlay;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.adinlay.adinlay.objects.CheckableObject;
import com.adinlay.adinlay.objects.ProfileTopic;
import com.adinlay.adinlay.objects.SocialMediaRvItem;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.ArrayList;
import java.util.Arrays;

import timber.log.Timber;


/**
 * Created by Noya on 6/5/2018.
 */

public class Login9AddSocMedFrag extends LoginFrag implements ItemClick{

    private static final String TAG = Login9AddSocMedFrag.class.getSimpleName();

    LoginViewModel viewModel;

    CallbackManager fcbkCallbackManager = null;

    SocMedRvAdapter adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            viewModel = ViewModelProviders.of(getActivity()).get(LoginViewModel.class);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_login9_connect2media, container, false);

        if (viewModel == null&& getActivity() != null) {
            viewModel = ViewModelProviders.of(getActivity()).get(LoginViewModel.class);
        }

        viewModel.updateFlowData(R.drawable.group_next_profile, getResources().getString(R.string.next_allcaps), false, false);

        RecyclerView counterRv = root.findViewById(R.id.soc_med_counter_rv);
        initProgressRecycler(counterRv, SignInManager.getTotalStages(), SignInManager.getTotalStages() - 1);

        ProfileTopic socialMediaTopic = AdinlayApp.getSocialMediaSettings();
        ArrayList<CheckableObject> socMedItems = socialMediaTopic.getItems(false);
        if (!socialMediaTopic.getImageUrl().isEmpty()){
            GlideApp.with(this).load(socialMediaTopic.getImageUrl())
                    .into((ImageView) root.findViewById(R.id.soc_med_iv));
        }
        boolean hasInst = false;
        boolean hasFcbk = false;
        for (CheckableObject item: socMedItems){
            String itemId = item.getId();
            hasInst = hasInst || "instagram".equalsIgnoreCase(itemId);
            hasFcbk = hasFcbk || "facebook".equalsIgnoreCase(itemId);
        }
        ArrayList<SocialMediaRvItem> items = new ArrayList<>();
        if (hasInst) {
            items.add(new SocialMediaRvItem(SocialMediaRvItem.SOCIAL_MEDIA_IG, "Instagram", R.drawable.page_1_c, R.drawable.page_1_gray));
        }
        if (hasFcbk) {
            items.add(new SocialMediaRvItem(SocialMediaRvItem.SOCIAL_MEDIA_FB, "Facebook", R.drawable.facebook_c, R.drawable.facebook_gray));
        }

        RecyclerView itemsRv = root.findViewById(R.id.soc_med_recycler);
        itemsRv.setLayoutManager(new LinearLayoutManager(getActivity()));
        itemsRv.setHasFixedSize(true);
        adapter = new SocMedRvAdapter(items, this);
        itemsRv.setAdapter(adapter);

        fcbkCallbackManager = CallbackManager.Factory.create();

        viewModel.getServerRequestLiveData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Timber.i("onChanged server request %s", s);

                refreshMediaConnections();
                if (LoginViewModel.FCBK_LOGOUT_REQ_OK.equals(s)){
                    LoginManager.getInstance().logOut();
                }
            }
        });

        // fcbk connect:
        LoginManager.getInstance().registerCallback(fcbkCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Timber.i("FacebookCallback onSuccess accessToken.token    %s", loginResult.getAccessToken().getToken());
                // send server the token, get number of followers:
                viewModel.connectToFcbk(loginResult.getAccessToken().getToken());
            }

            @Override
            public void onCancel() {
                viewModel.getSpinnerLiveData().postValue(false);
                refreshMediaConnections();
            }

            @Override
            public void onError(FacebookException error) {
                viewModel.getSpinnerLiveData().postValue(false);
                refreshMediaConnections();
            }
        });

        // change the text
        TextView titleTv = root.findViewById(R.id.soc_med_title);
        titleTv.setText(socialMediaTopic.getTitle());
        TextView oneLinerTv = root.findViewById(R.id.soc_med_one_liner);
        oneLinerTv.setText(socialMediaTopic.getDescription());

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshMediaConnections();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        fcbkCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
//        Timber.v("requestCode: %s", requestCode);
    }

    @Override
    public boolean onMainClicked() {
        // since connection to social media no longer a blocker, click is handled by mainActivity
        return true;
    }

    @Override
    public boolean onBackClicked() {
        return false;
    }

    private void refreshMediaConnections(){

        adapter.notifyDataSetChanged();
        viewModel.getIsMainEnabledLiveData().setValue(canContinue());

    }

    private boolean onInst(){
        return AdinlayApp.getUser() != null && AdinlayApp.getUser().isConnectedToInstagram();
    }

    private boolean onFcbk(){
        return AdinlayApp.getUser() != null && AdinlayApp.getUser().isConnectedToFacebook();
    }

    /**
     * @return currently, true if connected to *any* media (at least one)
     */
    private boolean canContinue(){
        return onInst() || onFcbk();
    }

    @Override
    protected String getFragStageName() {
        return "social_socialmedia";
    }

    @Override
    public void onItemClicked(Object object) {
        if (object instanceof Integer){
            switch ((Integer)object){
                case SocialMediaRvItem.SOCIAL_MEDIA_IG:
                    onClickIg();
                    break;
                case SocialMediaRvItem.SOCIAL_MEDIA_FB:
                    onClickFb();
                    break;
            }
        }
    }

    private void onClickIg(){
        if (AdinlayApp.getUser() != null){
            if (!AdinlayApp.getUser().isConnectedToInstagram()){
                // need to connect to instagram:
                Intent intent = new Intent(getActivity(), ConnectToInstagramActivity.class);
                startActivityForResult(intent, AyUtils.get16BitId(R.id.intent_instagram_signin));
//                        Timber.v("starting with requestCode: %s", AyUtils.get16BitId(R.id.intent_instagram_signin));
            } else if (getActivity() != null){
                // disconnect from instagram:
                Bundle args = new Bundle();
                args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.logout_instagram_camel);
                args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.cancel);
                args.putInt(AyDialogFragment.ARG_BUTTON_R_RES, R.string.logout);
                AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                dialogFragment.setListener(AyDialogFragment.VALUE_RIGHT, new AyDialogFragment.AyClickListener() {
                    @Override
                    public void onButtonClicked(View v) {
                        viewModel.logoutFromInstagram();
                    }
                });
                AyUtils.showDialogFragment(dialogFragment, getFragmentManager());
            }
        }
    }

    private void onClickFb(){
        User user = AdinlayApp.getUser();
        if (user != null){
            if (user.isConnectedToFacebook()){
                if (User.PROVIDER_FACEBOOK.equals(user.getLoginProvider())){
                    Timber.d("not letting user to disconnect from facebook - the login method!");
                    return;
                }
                Bundle args = new Bundle();
                args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.logout_fcbk_camel);
                args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.cancel);
                args.putInt(AyDialogFragment.ARG_BUTTON_R_RES, R.string.logout);
                AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                dialogFragment.setListener(AyDialogFragment.VALUE_RIGHT, new AyDialogFragment.AyClickListener() {
                    @Override
                    public void onButtonClicked(View v) {
                        viewModel.logoutFromFcbk();
                    }
                });
                AyUtils.showDialogFragment(dialogFragment, getFragmentManager());
            } else {
                // send call to facebook.loginManager to sign-in:
                LoginManager.getInstance().logInWithReadPermissions(Login9AddSocMedFrag.this, Arrays.asList("public_profile", "email", "user_friends", "user_posts"));
            }
        }
    }
}
