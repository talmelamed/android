package com.adinlay.adinlay;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.adinlay.adinlay.objects.Brand;
import com.adinlay.adinlay.objects.Post;
import com.adinlay.adinlay.objects.WalletEntry;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Noya on 6/27/2018.
 */

public class MainViewModel extends ViewModel {

    private static final String TAG = MainViewModel.class.getSimpleName();
    public static final String FETCH_POSTS_OK = "fetchPostsOk";
    public static final String LOGOUT_INSTAGRAM_OK = "logoutInstagramOk";
    public static final String LOGOUT_FCBK_OK = "logoutFcbkOk";
    public static final String LOGOUT_MEDIA_FAIL_PREFIX = "logoutMediaFailPrefix";
    public static final String LOGIN_FCBK_FAIL_PREFIX = "loginFcbkFailPrefix";
    public static final String WALLET_LIST_PREFIX = "walletlistprefix";
    public static final String WALLET_HEADERS_PREFIX = "walletheadersprefix";
    public static final String RESEND_VERIFICATION_ERROR = "resendVerificationError";
    public static final String RESEND_VERIFICATION_ERROR2 = "resendVerificationError2";
    public static final String DELETE_FCM_OK = "deleteFcmOk";

    private ArrayList<Post> allAdsArr = null;
    private ArrayList<Post[]> mosaicPostsList = null;
    private ArrayList<Post> myAdsArr = null;
    private ArrayList<WalletEntry> walletEntriesArr = null;
    private MutableLiveData<String> pingAllAds;
    private MutableLiveData<String> pingMyAds;
    private ArrayList<Brand> brandsArray = null;
    private MutableLiveData<String> pingBrands;
    private MutableLiveData<String> pingInstagramLogout;
    private MutableLiveData<String> pingWalletError;
    private MutableLiveData<String> pingRecheckEmailVerified;
    private MutableLiveData<String> pingResendEmailVerification;
    private MutableLiveData<String> pingFcmDelete;
    private MutableLiveData<String> pingFcbkLogin;
    private MutableLiveData<BigDecimal> balanceAmount;
    private MutableLiveData<BigDecimal> pendingAmount;
    private MutableLiveData<BigDecimal> redeemedAmount;
    private MutableLiveData<String> pingWalletList;
    private MutableLiveData<Boolean> spinnerLiveData;
    private MutableLiveData<String> pingApprovedPosts;
    private int allAdsPos = 0;
    private int myAdsPos = 0;
    private int brandsPos = 0;
    private boolean needToRefreshAllAds = false;
    private boolean needToRefreshMyAds = false;
    private float balance = 0;
    private boolean padFor2tabstrips = true;

    private String filterByBrand = null;

    public MutableLiveData<String> getPingAllAds(){
        if (pingAllAds == null){
            pingAllAds = new MutableLiveData<>();
        }
        return pingAllAds;
    }

    public MutableLiveData<String> getPingMyAds(){
        if (pingMyAds == null){
            pingMyAds = new MutableLiveData<>();
        }
        return pingMyAds;
    }

    public MutableLiveData<String> getPingBrands(){
        if (pingBrands == null){
            pingBrands = new MutableLiveData<>();
        }
        return pingBrands;
    }

    public MutableLiveData<String> getPingInstagramLogout(){
        if (pingInstagramLogout == null){
            pingInstagramLogout = new MutableLiveData<>();
        }
        return pingInstagramLogout;
    }

    public MutableLiveData<BigDecimal> getBalanceAmountLD(){
        if (balanceAmount == null){
            balanceAmount = new MutableLiveData<>();
        }
        return balanceAmount;
    }

    public MutableLiveData<BigDecimal> getPendingAmountLD(){
        if (pendingAmount == null){
            pendingAmount = new MutableLiveData<>();
        }
        return pendingAmount;
    }

    public MutableLiveData<BigDecimal> getRedeemedAmountLD(){
        if (redeemedAmount == null){
            redeemedAmount = new MutableLiveData<>();
        }
        return redeemedAmount;
    }

    public MutableLiveData<String> getPingWalletList(){
        if (pingWalletList == null){
            pingWalletList = new MutableLiveData<>();
        }
        return pingWalletList;
    }

    public MutableLiveData<String> getPingWalletError(){
        if (pingWalletError == null){
            pingWalletError = new MutableLiveData<>();
        }
        return pingWalletError;
    }

    public MutableLiveData<String> getPingFcbkLogin(){
        if (pingFcbkLogin == null){
            pingFcbkLogin = new MutableLiveData<>();
        }
        return pingFcbkLogin;
    }

    public MutableLiveData<Boolean> getSpinnerLiveData(){
        if (spinnerLiveData == null){
            spinnerLiveData = new MutableLiveData<>();
        }
        return spinnerLiveData;
    }

    public MutableLiveData<String> getPingRecheckEmailVerified(){
        if (pingRecheckEmailVerified == null){
            pingRecheckEmailVerified = new MutableLiveData<>();
        }
        return pingRecheckEmailVerified;
    }

    public MutableLiveData<String> getPingResendEmailVerification(){
        if (pingResendEmailVerification == null){
            pingResendEmailVerification = new MutableLiveData<>();
        }
        return pingResendEmailVerification;
    }

    public MutableLiveData<String> getPingFcmDelete(){
        if (pingFcmDelete == null){
            pingFcmDelete = new MutableLiveData<>();
        }
        return pingFcmDelete;
    }

    public MutableLiveData<String> getPingApprovedPosts(){
        if (pingApprovedPosts == null){
            pingApprovedPosts = new MutableLiveData<>();
        }
        return pingApprovedPosts;
    }

    @Nullable
    public ArrayList<Post> getAllAdsArrayList(){
        return allAdsArr;
    }

    @Nullable
    public ArrayList<Brand> getBrandsArrayList(){
        return brandsArray;
    }

    @Nullable
    public ArrayList<Post> getMyAdsArrayList(){
        return myAdsArr;
    }

    public ArrayList<Post[]> getMosaicPostsList() {
        return mosaicPostsList;
    }

    @Nullable
    public ArrayList<WalletEntry> getWalletEntriesArrayList(){
        return walletEntriesArr;
    }

    public void getPosts(final boolean myPosts){
        ServerManager.queryServer(myPosts? ServerManager.GET_MY_POSTS : filterByBrand == null ?
                ServerManager.GET_ALL_POSTS : ServerManager.POSTS_BY_BRANDS,
                myPosts ? null : filterByBrand, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                // {"posts": [{...},{...}]}
                JSONObject outerJson = null;
                try {
                    outerJson = new JSONObject(responseBody);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (outerJson == null){
                    onError("Unexpected response");
                    return;
                }

                JSONArray postsArr = outerJson.optJSONArray("posts");
                if (postsArr != null){
                    ArrayList<Post> adsList = new ArrayList<>();
                    for (int i = 0; i < postsArr.length(); i++){
                        JSONObject currPost = postsArr.optJSONObject(i);
                        if (currPost != null){
                            adsList.add(new Post(currPost));
                        }
                    }

                    if (!myPosts) {
                        // analyze the order:
                        ArrayList<Post[]> placedPosts = new ArrayList<>();

                        for (int i = 0; i < adsList.size(); i++){
                            Post post = adsList.get(i);
                            if (post != null && post.isFacebookLinkSize()){
                                // CASE 1: fb
//                                Timber.v("MOSAIC_ i %s case 1", i);
                                post.setMosaicSize(Post.FB_SIZE);
                                placedPosts.add(new Post[]{post});
                            } else if (post != null && adsList.size() > i +1 && adsList.get(i+1) != null){
                                Post nextPost = adsList.get(i+1);
                                // CASE 2: inst followed by fb
                                if (nextPost.isFacebookLinkSize()){
//                                    Timber.v("MOSAIC_ i %s case 2", i);
                                    post.setMosaicSize(Post.BIG_INST_SIZE);
                                    placedPosts.add(new Post[]{post});
                                    i++;
                                    nextPost.setMosaicSize(Post.FB_SIZE);
                                    placedPosts.add(new Post[]{nextPost});
                                } else {
                                    // 2 inst, let's see what follows:
                                    if (adsList.size() > i + 2 && adsList.get(i+2) != null){
                                        Post thirdP = adsList.get(i+2);
                                        if (!thirdP.isFacebookLinkSize()){
                                            // CASE 3: 3 inst
//                                            Timber.v("MOSAIC_ i %s case 3", i);
                                            post.setMosaicSize(Post.BIG_INST_SIZE);
                                            placedPosts.add(new Post[]{post});
                                            i++;
                                            nextPost.setMosaicSize(Post.SMALL_INST_SIZE);
                                            i++;
                                            placedPosts.add(new Post[]{nextPost, thirdP});
                                            thirdP.setMosaicSize(Post.SMALL_INST_SIZE);
                                        }else {
                                            // CASE 4: 2 inst followed by fb
//                                            Timber.v("MOSAIC_ i %s case 4", i);
                                            post.setMosaicSize(Post.SMALL_INST_SIZE);
                                            i++;
                                            nextPost.setMosaicSize(Post.SMALL_INST_SIZE);
                                            placedPosts.add(new Post[]{post, nextPost});
                                            i++;
                                            thirdP.setMosaicSize(Post.FB_SIZE);
                                            placedPosts.add(new Post[]{thirdP});
                                        }
                                    } else {
                                        // CASE 5: list ends with 2 inst
//                                        Timber.v("MOSAIC_ i %s case 5", i);
                                        post.setMosaicSize(Post.SMALL_INST_SIZE);
                                        i++;
                                        nextPost.setMosaicSize(Post.SMALL_INST_SIZE);
                                        placedPosts.add(new Post[]{post, nextPost});
                                    }
                                }
                            } else if (post != null){
                                // CASE 6: list ends with 1 inst
//                                Timber.v("MOSAIC_ i %s case 6", i);
                                post.setMosaicSize(Post.BIG_INST_SIZE);
                                placedPosts.add(new Post[]{post});
                            } else {
                                // shouldn't happen!!!!!
//                                Timber.v("MOSAIC_ i %s case 7", i);
                            }
                        }
                        mosaicPostsList = placedPosts;
                    }

                    if (myPosts && myAdsArr == null){
                        myAdsArr = new ArrayList<>();
                    } else if (!myPosts && allAdsArr == null){
                        allAdsArr = new ArrayList<>();
                    }
                    ArrayList<Post> mList = myPosts? myAdsArr : allAdsArr;
                    mList.clear();
                    mList.addAll(adsList);
                    MutableLiveData<String> liveData = myPosts? getPingMyAds() : getPingAllAds();
                    liveData.postValue(FETCH_POSTS_OK);
                } else {
                    onError("Unexpected response");
                }

            }

            @Override
            public void onError(String message) {
                MutableLiveData<String> liveData = myPosts? getPingMyAds() : getPingAllAds();
                liveData.postValue(message);
            }
        });
    }

    public int getAllAdsPos() {
        return allAdsPos;
    }

    public void setAllAdsPos(int allAdsPos) {
//        Timber.v("setAllAdsPos from: %s to: %s", this.allAdsPos, allAdsPos);
        this.allAdsPos = allAdsPos;
    }

    public int getMyAdsPos() {
        return myAdsPos;
    }

    public void setMyAdsPos(int myAdsPos) {
        this.myAdsPos = myAdsPos;
    }


    public float getBalance(){
        return balance;
    }

    public boolean isPadFor2tabstrips() {
        return padFor2tabstrips;
    }

    public void setPadFor2tabstrips(boolean padFor2tabstrips) {
        this.padFor2tabstrips = padFor2tabstrips;
    }

    public void getBrands(){
        ServerManager.queryServer(ServerManager.BRANDS, null, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                JSONArray jArr = null;
                try {
                    jArr = new JSONArray(responseBody);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jArr != null){
                    ArrayList<Brand> brands = new ArrayList<>();
                    for (int i = 0; i < jArr.length(); i++) {
                        JSONObject currJson = jArr.optJSONObject(i);
                        if (currJson != null){
                            Brand brand = new Brand(currJson);
                            brands.add(brand);
                            AyUtils.addBrandNameToList(brand.getId(), brand);
                        }
                    }
                    if(brandsArray == null){
                        brandsArray = new ArrayList<>();
                    }
                    brandsArray.clear();
                    brandsArray.addAll(brands);
                    getPingBrands().postValue(FETCH_POSTS_OK);
                } else {
                    onError("Unexpected response");
                }

            }

            @Override
            public void onError(String message) {
                getPingBrands().postValue(message);
            }
        });
    }

    public void fetchApprovedPosts(){
        ServerManager.queryServer(ServerManager.FCBK_APPROVED_POSTS, null, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                // [{"pageUrl":"https://dev-api.adinlay.com/client/social/webpage?title=Adinlay%20sports%20is%20coming%20soon...&image=https%3A%2F%2Fadinlay-published-photos-dev.s3.eu-central-1.amazonaws.com%2Ffacebook%2F5b682f4b33d7547887bcc555%2FGgoUVMHUtr2Rj3afDHas79YH.jpg&link=https%3A%2F%2Fdev-api.adinlay.com%2Fpublic%2Fengagement%2Ffacebook%2Fclick%2F5c3c5f543bdb3e00172493eb","postId":"GgoUVMHUtr2Rj3afDHas79YH","photoUrl":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/GgoUVMHUtr2Rj3afDHas79YH.jpg","photoUrl_small":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/GgoUVMHUtr2Rj3afDHas79YH_small.jpg"},{"pageUrl":"https://dev-api.adinlay.com/client/social/webpage?title=Adinlay%20sports%20is%20coming%20soon...&image=https%3A%2F%2Fadinlay-published-photos-dev.s3.eu-central-1.amazonaws.com%2Ffacebook%2F5b682f4b33d7547887bcc555%2FU8EK1aPlwYAcOUDyX7Kt9q9Y.jpg&link=https%3A%2F%2Fdev-api.adinlay.com%2Fpublic%2Fengagement%2Ffacebook%2Fclick%2F5c3c62c7a839b700173f377e","postId":"U8EK1aPlwYAcOUDyX7Kt9q9Y","photoUrl":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/U8EK1aPlwYAcOUDyX7Kt9q9Y.jpg","photoUrl_small":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/U8EK1aPlwYAcOUDyX7Kt9q9Y_small.jpg"},{"pageUrl":"https://dev-api.adinlay.com/client/social/webpage?title=Adinlay%20sports%20is%20coming%20soon...&image=https%3A%2F%2Fadinlay-published-photos-dev.s3.eu-central-1.amazonaws.com%2Ffacebook%2F5b682f4b33d7547887bcc555%2F5tVn7dGI8L3U5lvdo5dgCZYL.jpg&link=https%3A%2F%2Fdev-api.adinlay.com%2Fpublic%2Fengagement%2Ffacebook%2Fclick%2F5c3c6385a839b700173f3781","postId":"5tVn7dGI8L3U5lvdo5dgCZYL","photoUrl":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/5tVn7dGI8L3U5lvdo5dgCZYL.jpg","photoUrl_small":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/5tVn7dGI8L3U5lvdo5dgCZYL_small.jpg"},{"pageUrl":"https://dev-api.adinlay.com/client/social/webpage?title=Adinlay%20sports%20is%20coming%20soon...&image=https%3A%2F%2Fadinlay-published-photos-dev.s3.eu-central-1.amazonaws.com%2Ffacebook%2F5b682f4b33d7547887bcc555%2F2xYgE0wfnHR9i1A18CYhItC3.jpg&link=https%3A%2F%2Fdev-api.adinlay.com%2Fpublic%2Fengagement%2Ffacebook%2Fclick%2F5c3c64aaa839b700173f3782","postId":"2xYgE0wfnHR9i1A18CYhItC3","photoUrl":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/2xYgE0wfnHR9i1A18CYhItC3.jpg","photoUrl_small":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/2xYgE0wfnHR9i1A18CYhItC3_small.jpg"}]
                AdinlayApp.setApprovedPosts(responseBody);
                getPingApprovedPosts().postValue(FETCH_POSTS_OK);
            }

            @Override
            public void onError(String message) {
                getPingApprovedPosts().postValue(message);
            }
        });
    }

    public int getBrandsPos() {
        return brandsPos;
    }

    public void setBrandsPos(int brandsPos) {
        this.brandsPos = brandsPos;
    }

    public String getFilterByBrand() {
        return filterByBrand;
    }

    public void setFilterByBrand(String filterByBrand) {
        this.filterByBrand = filterByBrand;
    }

    public void logoutFromInstagram(){
        getSpinnerLiveData().setValue(true);
        ServerManager.logoutInstagram(new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                getPingInstagramLogout().postValue(LOGOUT_INSTAGRAM_OK);
                getSpinnerLiveData().postValue(false);
            }

            @Override
            public void onError(String message) {
                String str = message == null || message.isEmpty() ? LOGOUT_MEDIA_FAIL_PREFIX : LOGOUT_MEDIA_FAIL_PREFIX + message;
                getPingInstagramLogout().postValue(str);
                getSpinnerLiveData().postValue(false);
            }
        });
    }

    public void connectToFcbk(String fcbkToken){
        ServerManager.connectToFcbk(fcbkToken, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                getPingFcbkLogin().postValue(FETCH_POSTS_OK);
                getSpinnerLiveData().postValue(false);
            }

            @Override
            public void onError(String message) {
                String str = message == null || message.isEmpty() ? LOGIN_FCBK_FAIL_PREFIX : LOGIN_FCBK_FAIL_PREFIX + message;
                getPingFcbkLogin().postValue(str);
                getSpinnerLiveData().postValue(false);
            }
        });
    }

    public void logoutFcbk(){
        getSpinnerLiveData().setValue(true);
        ServerManager.disconnectFromFcbk(new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                getPingFcbkLogin().postValue(LOGOUT_FCBK_OK);
                getSpinnerLiveData().postValue(false);
            }

            @Override
            public void onError(String message) {
                String str = message == null || message.isEmpty() ? LOGOUT_MEDIA_FAIL_PREFIX : LOGOUT_MEDIA_FAIL_PREFIX + message;
                getPingFcbkLogin().postValue(str);
                getSpinnerLiveData().postValue(false);
            }
        });
    }

    public void setRefreshPosts(){
        setNeedToRefreshAllAds(true);
        setNeedToRefreshMyAds(true);
        setAllAdsPos(0);
        setMyAdsPos(0);
    }

    public void setNeedToRefreshAllAds(boolean needToRefreshAllAds) {
        this.needToRefreshAllAds = needToRefreshAllAds;
    }

    public void setNeedToRefreshMyAds(boolean needToRefreshMyAds) {
        this.needToRefreshMyAds = needToRefreshMyAds;
    }

    public boolean isNeedToRefreshAllAds() {
        return needToRefreshAllAds;
    }

    public boolean isNeedToRefreshMyAds() {
        return needToRefreshMyAds;
    }

    public void fetchBalanceInfo(){
        ServerManager.queryServer(ServerManager.BALANCE_REQUEST, null, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(responseBody);
                } catch (JSONException e) {
                    e.printStackTrace();
                    onError(e.getMessage());
                    return;
                }
                double dBalance = jsonObject.optDouble("balance", 0);
                BigDecimal val = BigDecimal.valueOf(dBalance);
                getBalanceAmountLD().postValue(val);
                getPendingAmountLD().postValue(BigDecimal.valueOf(jsonObject.optDouble("pending", 0)));
                getRedeemedAmountLD().postValue(BigDecimal.valueOf(jsonObject.optDouble("redeemption", 0)));
                balance = val.floatValue();
            }

            @Override
            public void onError(String message) {
                String pingMessage = message == null ? WALLET_HEADERS_PREFIX : WALLET_HEADERS_PREFIX + message;
                getPingWalletError().postValue(pingMessage);
                Timber.i("WALLET_ query balance onError: %s", message);
            }
        });

    }

    public void reFetchFcmToken(){
        AyUtils.saveStringPreference(AyUtils.PREF_UPDATE_FCM_REQUIRED, null);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
//                            Timber.i(task.getException(),"getInstanceId_ failed");
                            return;
                        }

                        // Get new Instance ID token
                        if (task.getResult() != null){
                            String token = task.getResult().getToken();
                            AyUtils.saveStringPreference(AyUtils.PREF_FCM_TOKEN, token);
//                            Timber.d("getInstanceId_ NTFY_ got token: %s", token);
                            if (AdinlayApp.getUser() != null){
                                ServerManager.sendFcmTokenToServer();
                            }
                        }

                    }
                });
            }
        }).start();
    }

    public void fetchWalletInfo(){

        fetchBalanceInfo();

        ServerManager.queryServer(ServerManager.WALLET_LIST, null, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                // [{...},{...}]
                JSONArray jArr = null;
                try {
                    jArr = new JSONArray(responseBody);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (jArr != null){
                    ArrayList<WalletEntry> serverEntriesList = new ArrayList<>();
                    serverEntriesList.add(WalletEntry.getTitleEntry());
                    for (int i = 0; i < jArr.length(); i++) {
                        JSONObject currJson = jArr.optJSONObject(i);
                        if (currJson != null){
                            WalletEntry entry = new WalletEntry(currJson);
                            serverEntriesList.add(entry);
                        }
                    }
                    if (walletEntriesArr == null){
                        walletEntriesArr = new ArrayList<>();
                    }
                    walletEntriesArr.clear();
                    walletEntriesArr.addAll(serverEntriesList);
                    getPingWalletList().postValue(FETCH_POSTS_OK);
                } else {
                    onError("Unexpected response");
                }
            }

            @Override
            public void onError(String message) {
                Timber.i("WALLET_ query walletList onError: %s", message);
                String pingMessage = message == null ? WALLET_LIST_PREFIX : WALLET_LIST_PREFIX + message;
                getPingWalletError().postValue(pingMessage);
            }
        });
    }

    public void recheckEmailVerified(){
        ServerManager.recheckEmailVerified(new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                getPingRecheckEmailVerified().postValue(FETCH_POSTS_OK);
            }

            @Override
            public void onError(String message) {
                getPingRecheckEmailVerified().postValue(message);
            }
        });
    }

    public void resendEmailVerification(){
        FirebaseUser firebaseUser = SignInManager.getFirebaseUser();
        if (firebaseUser != null){
            firebaseUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        Timber.i("verify_ task is successful");
                        getPingResendEmailVerification().postValue(FETCH_POSTS_OK);
                    } else {
                        Timber.w(task.getException(), "resendEmailVerification task not successful. Canceled? %s", task.isCanceled());
                        getPingResendEmailVerification().postValue(RESEND_VERIFICATION_ERROR);
                    }
                }
            });
        } else {
//            Timber.i("verify_ no firebase user");
            getPingResendEmailVerification().postValue(RESEND_VERIFICATION_ERROR2);
        }
    }

    public void refetchUserJson(){
        if (AdinlayApp.getUser() == null){
            ServerManager.getUpdatedUserInfo(null);
        }
    }

    public void deleteFcmToken(){
        getSpinnerLiveData().postValue(true);
        ServerManager.deleteFcmTokenFromServer(new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                getPingFcmDelete().postValue(DELETE_FCM_OK);
                getSpinnerLiveData().postValue(false);
            }

            @Override
            public void onError(String message) {
                getPingFcmDelete().postValue(message);
                getSpinnerLiveData().postValue(false);
            }
        });
    }

}
