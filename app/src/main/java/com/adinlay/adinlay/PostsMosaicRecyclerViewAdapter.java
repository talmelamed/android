package com.adinlay.adinlay;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.adinlay.adinlay.objects.Post;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Noya on 4/10/2018.
 */

public class PostsMosaicRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = PostsMosaicRecyclerViewAdapter.class.getSimpleName();

    private ArrayList<Post[]> postsList;

    private WeakReference<PostsRecyclerViewAdapter.ItemClick> wr_ItemClickCallback = new WeakReference<PostsRecyclerViewAdapter.ItemClick>(null);
    private WeakReference<Activity> wr_Activity = new WeakReference<Activity>(null);
    private WeakReference<GlideRequests> wr_glideWith = new WeakReference<GlideRequests>(null);
    private WeakReference<GlideRequest<Drawable>> wr_requestBuilder = new WeakReference<GlideRequest<Drawable>>(null);

    private int fullWidth = 0;


    public PostsMosaicRecyclerViewAdapter(ArrayList<Post[]> postsList, GlideRequests glideRequests,
                                          PostsRecyclerViewAdapter.ItemClick callback, Activity activity) {
        this.postsList = postsList;

        wr_glideWith = new WeakReference<GlideRequests>(glideRequests);
        if (glideRequests != null){
            wr_requestBuilder = new WeakReference<GlideRequest<Drawable>>(glideRequests.asDrawable().transforms(new CenterCrop()));
        }
        wr_ItemClickCallback = new WeakReference<PostsRecyclerViewAdapter.ItemClick>(callback);
        wr_Activity = new WeakReference<Activity>(activity);
    }

    @Override
    public int getItemViewType(int position) {
        Post[] currPosts = postsList.get(position);
        if (currPosts != null && currPosts.length == 2){
            return 2;
        } else if (currPosts != null && currPosts.length == 1){
            Post post = currPosts[0];
            if (post.isFacebookLinkSize()){
                return 1;
            }
        }
        return 0;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (fullWidth == 0){
            fullWidth = parent.getMeasuredWidth();
        }

        if (viewType == 2){
            View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_two_iv, parent, false);
            RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) root.getLayoutParams();
            lp.height = parent.getMeasuredWidth() / 2;
            return new MosaicTwoImageViewHolder(root);
        } else {
            View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_just_iv, parent, false);
            RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) root.getLayoutParams();

            if (viewType == 1){
                lp.height = parent.getMeasuredWidth() * 630/1200;
                return new MosaicOneFbViewHolder(root);
            } else {
                lp.height = parent.getMeasuredWidth();
                return new MosaicOneImageViewHolder(root);
            }
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        Post[] postsArr = postsList.get(position);

        int topVisibility = position == 0 ? View.GONE : View.VISIBLE;
        int bottomVisibility = position == getItemCount() - 1 ? View.GONE : View.VISIBLE;
        if (holder instanceof MosaicOneImageViewHolder){
            MosaicOneImageViewHolder holder1 = (MosaicOneImageViewHolder) holder;
            Post currPost = postsArr.length > 0 ? postsArr[0] : null;
            loadImageToIv(currPost, holder1.adIv, false);
            holder1.topBorder.setVisibility(topVisibility);
            holder1.bottomBorder.setVisibility(bottomVisibility);
            holder1.leftBorder.setVisibility(View.GONE);
            holder1.rightBorder.setVisibility(View.GONE);
        } else if (holder instanceof MosaicTwoImageViewHolder){
            MosaicTwoImageViewHolder holder2 = (MosaicTwoImageViewHolder) holder;
            Post currPost = postsArr.length > 0 ? postsArr[0] : null;
            loadImageToIv(currPost, holder2.adIvStart, true);
            currPost = postsArr.length > 1 ? postsArr[1] : null;
            loadImageToIv(currPost, holder2.adIvEnd, true);
            holder2.topBorder.setVisibility(topVisibility);
            holder2.bottomBorder.setVisibility(bottomVisibility);
        }


    }

    private void loadImageToIv(Post currPost, ImageView iv, boolean isHalf){

        GlideRequests gr = wr_glideWith.get();
        if (iv != null && gr != null && currPost != null){

            // check in case we don't need to redraw the item:
            if (iv.getTag(R.id.post_tag) instanceof Post && !currPost.getPostId().isEmpty() &&
                    currPost.getPostId().equals(((Post) iv.getTag(R.id.post_tag)).getPostId())){
//            Timber.v("onBindViewHolder position %s got same item ", position, currPost.getPostId());
                return;
            }

            String imageUrl = currPost.getThmbnailUrl();
            boolean isFullSizeUrl = false;
            boolean isFcbkLink = currPost.isFacebookLinkSize();
            if (imageUrl.isEmpty()){
                imageUrl = currPost.getPhotoUrl();
                isFullSizeUrl = true;
            }
            if (!imageUrl.isEmpty()) {
                gr.clear(iv);

                GlideRequest<Drawable> ivRequest;
                if (wr_requestBuilder.get() == null){
                    wr_requestBuilder = new WeakReference<GlideRequest<Drawable>>(gr.asDrawable().transforms(new CenterCrop()));
                    ivRequest = gr.asDrawable().transforms(new CenterCrop());
                } else {
                    ivRequest = wr_requestBuilder.get().clone();
                }

                ivRequest = ivRequest.diskCacheStrategy(DiskCacheStrategy.ALL);
                iv.setScaleType(ImageView.ScaleType.CENTER_CROP);

//                int overrideW = iv.getWidth();
                int overrideW = isFcbkLink || !isHalf ? fullWidth : fullWidth/2;
                if (overrideW <= 0 && isFullSizeUrl){
                    overrideW = overrideW/2;
                }
                int overrideH = isFcbkLink ? overrideW * 630 / 1200 : overrideW;

                if (overrideW > 0 && overrideH > 0){
                    ivRequest.load(imageUrl)
//                            .skipMemoryCache(true)
                            .error(android.R.drawable.ic_menu_gallery) // too small!!!
                            .override(overrideW, overrideH)
                            .centerCrop()
                            .downsample(DownsampleStrategy.AT_MOST)
                            .thumbnail(0.2f)
                            .into(iv);
                } else {
                    ivRequest.load(imageUrl)
//                            .skipMemoryCache(true)
                            .error(android.R.drawable.ic_menu_gallery) // too small!!!
                            .centerCrop()
                            .downsample(DownsampleStrategy.AT_MOST)
                            .thumbnail(0.2f)
                            .into(iv);
                }
            } else { // shouldn't happen!!!
                iv.setImageResource(0);
            }

        }

        // set tag on iv, and also clickListener
        if (iv != null){
            iv.setTag(R.id.post_tag, currPost);
            iv.setOnClickListener(mosaicClickListener);
        }
    }

    private View.OnClickListener mosaicClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Post post = null;
            if (v.getTag(R.id.post_tag) instanceof Post){
                post = (Post) v.getTag(R.id.post_tag);
            }
            Timber.v("clicked on post: %s", post);
            if (wr_ItemClickCallback.get() != null && post != null){
                wr_ItemClickCallback.get().onMosaicItemClicked(post);
            }
        }
    };


    @Override
    public int getItemCount() {
        return postsList.size();
    }



    public static class MosaicOneImageViewHolder extends RecyclerView.ViewHolder {

        ImageView adIv;
        View topBorder;
        View leftBorder;
        View rightBorder;
        View bottomBorder;

        public MosaicOneImageViewHolder(View itemView) {
            super(itemView);

            adIv = itemView.findViewById(R.id.item_iv);
            topBorder = itemView.findViewById(R.id.item_top_divider);
            leftBorder = itemView.findViewById(R.id.item_left_divider);
            rightBorder = itemView.findViewById(R.id.item_right_divider);
            bottomBorder = itemView.findViewById(R.id.item_bottom_divider);
        }
    }

    public static class MosaicOneFbViewHolder extends MosaicOneImageViewHolder {


        public MosaicOneFbViewHolder(View itemView) {
            super(itemView);
        }
    }

    public static class MosaicTwoImageViewHolder extends RecyclerView.ViewHolder {

        ImageView adIvStart;
        ImageView adIvEnd;
        View topBorder;
        View bottomBorder;
        View mid1Border;
        View mid2Border;


        public MosaicTwoImageViewHolder(View itemView) {
            super(itemView);

            adIvStart = itemView.findViewById(R.id.item_iv_start);
            adIvEnd = itemView.findViewById(R.id.item_iv_end);
            topBorder = itemView.findViewById(R.id.double_item_top_divider);
            bottomBorder = itemView.findViewById(R.id.double_item_bottom_divider);
            mid1Border = itemView.findViewById(R.id.middle_divider_start);
            mid2Border = itemView.findViewById(R.id.middle_divider_end);
        }
    }


}
