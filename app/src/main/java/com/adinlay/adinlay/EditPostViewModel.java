package com.adinlay.adinlay;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.graphics.Bitmap;

import com.adinlay.adinlay.objects.PostFilter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Noya on 8/8/2018.
 */

public class EditPostViewModel extends ViewModel {

    private static String TAG = EditPostViewModel.class.getSimpleName();
    public static String FETCH_OK = "fetch_ok";

    private String imagePath = null;
    private ArrayList<PostFilter> filters = null;
    private Bitmap baseImageBitmap = null;
    private int[] scaleFactorSaver = new int[1];

    private MutableLiveData<String> pingFiltersReady;
    private MutableLiveData<Boolean> spinnerLiveData;
    private MutableLiveData<String> notifyErrorProcessingImage;
    private MutableLiveData<String> imagePathLiveData;

    private List<GalleryImageObject> photosList = new ArrayList<>();

    public String getImagePath() {
        return imagePath;
    }

    public ArrayList<PostFilter> getFiltersList() {
        if (filters == null){
            JSONObject noFiltersFilter = new JSONObject();
            try {
                noFiltersFilter.put(PostFilter.NAME_KEY, "Original");
                noFiltersFilter.put(PostFilter.SUB_FILTERS_KEY, new JSONArray());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ArrayList<PostFilter> tempList = new ArrayList<>();
            tempList.add(new PostFilter(noFiltersFilter));
            return tempList;
        }
        return filters;
    }

    public MutableLiveData<String> getPingFilters(){
        if (pingFiltersReady == null){
            pingFiltersReady = new MutableLiveData<>();
        }
        return pingFiltersReady;
    }

    public MutableLiveData<Boolean> getSpinnerLiveData() {
        if (spinnerLiveData == null){
            spinnerLiveData = new MutableLiveData<>();
        }
        return spinnerLiveData;
    }

    public MutableLiveData<String> getNotifyErrorProcessingImage(){
        if (notifyErrorProcessingImage == null){
            notifyErrorProcessingImage = new MutableLiveData<>();
        }
        return notifyErrorProcessingImage;
    }

    public MutableLiveData<String> getImagePathLiveData(){
        if (imagePathLiveData == null){
            imagePathLiveData = new MutableLiveData<>();
        }
        return imagePathLiveData;
    }


    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void fetchFilters(){
        ServerManager.queryServer(ServerManager.FILTERS, null, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                // [{},{}...]
                JSONArray jArr = null;
                try {
                    jArr = new JSONArray(responseBody);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jArr != null){
                    ArrayList<PostFilter> fetchedFilters = new ArrayList<>();
                    for (int i = 0; i < jArr.length(); i++) {
                        JSONObject currJson = jArr.optJSONObject(i);
                        if (currJson != null){
                            PostFilter filter = new PostFilter(currJson);
                            fetchedFilters.add(filter);
                        }
                    }

                    if (filters == null){
                        filters = new ArrayList<>();
                    }
                    filters.clear();
                    filters.addAll(fetchedFilters);
                    finalizeFiltersList(filters);

                    getPingFilters().postValue(FETCH_OK);

                }
            }

            @Override
            public void onError(String message) {
                if (filters == null){
                    filters = new ArrayList<>();
                }
                filters.clear();
                finalizeFiltersList(filters);
                getPingFilters().postValue(FETCH_OK);
            }
        });
    }

    private void finalizeFiltersList(ArrayList<PostFilter> filtersArr){
        if (filtersArr == null){
            return;
        }
        if (filtersArr.size() == 0){
            JSONObject noFiltersFilter = new JSONObject();
            try {
                noFiltersFilter.put(PostFilter.NAME_KEY, "Original");
                noFiltersFilter.put(PostFilter.SUB_FILTERS_KEY, new JSONArray());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            filtersArr.add(new PostFilter(noFiltersFilter));
        }

    }

    public Bitmap getBaseImageBitmap() {
        return baseImageBitmap;
    }

    public void setBaseImageBitmap(Bitmap baseImageBitmap) {
        this.baseImageBitmap = baseImageBitmap;
    }

    public void recycleBaseImageBitmap(){
        if (baseImageBitmap != null){
            baseImageBitmap.recycle();
        }
        baseImageBitmap = null;
    }

    public int[] getScaleFactorSaver() {
        return scaleFactorSaver;
    }

    public List<GalleryImageObject> getPhotosList() {
        return photosList;
    }

    public void setPhotosList(List<GalleryImageObject> photosList) {
        if (photosList != null) {
            this.photosList = photosList;
        }
    }
}
