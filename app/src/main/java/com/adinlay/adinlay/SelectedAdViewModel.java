package com.adinlay.adinlay;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.support.annotation.Nullable;

import com.adinlay.adinlay.objects.Ad;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Noya on 6/27/2018.
 */

public class SelectedAdViewModel extends ViewModel {

    private static final String TAG = SelectedAdViewModel.class.getSimpleName();
    public static final String FETCH_OBJECTS_OK = "fetchObjectsOk";
    public static final String FETCH_APPROVED_OK = "fetchApprovedOk";

    private ArrayList<Ad> adsArray = null;
    private MutableLiveData<String> pingAds;
    private MutableLiveData<String> pingApprovedPosts;
    private int adsPos = 0;
    private String fileUrl = null;


    public MutableLiveData<String> getPingAds(){
        if (pingAds == null){
            pingAds = new MutableLiveData<>();
        }
        return pingAds;
    }

    public MutableLiveData<String> getPingApprovedPosts(){
        if (pingApprovedPosts == null){
            pingApprovedPosts = new MutableLiveData<>();
        }
        return pingApprovedPosts;
    }

    @Nullable
    public ArrayList<Ad> getAdsArrayList(){
        return adsArray;
    }



    public void getAvailableAds(){
        ServerManager.queryServer(ServerManager.GALLERY_ADS, null, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                // {"social_media":{"facebook":{"connection":true,"friends":10},"instagram":{"connection":true,"followers":11}},"ads":[{},{}...], "user":{}}
                JSONArray jArr = null;
                JSONObject socMedJson = null;
                try {
                    JSONObject jsonObject = new JSONObject(responseBody);
                    JSONObject socMediaObject = jsonObject.optJSONObject("social_media");
                    AdinlayApp.updateUserSocMediaConnections(socMediaObject);
                    jArr = jsonObject.optJSONArray("ads");
                    socMedJson = jsonObject.optJSONObject("user");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (jArr != null){
                    ArrayList<Ad> ads = new ArrayList<>();
                    for (int i = 0; i < jArr.length(); i++) {
                        JSONObject currJson = jArr.optJSONObject(i);
                        if (currJson != null){
                            Ad ad = new Ad(currJson);
                            ads.add(ad);
                        }
                    }
                    if(adsArray == null){
                        adsArray = new ArrayList<>();
                    }
                    adsArray.clear();
                    adsArray.addAll(ads);
                    getPingAds().postValue(FETCH_OBJECTS_OK);
                } else {
                    onError("Unexpected response");
                }

                if (AdinlayApp.getUser() != null){
                    AdinlayApp.getUser().updateSocMedStatus(socMedJson);
                }

            }

            @Override
            public void onError(String message) {
                getPingAds().postValue(message);
            }
        });
    }

    public int getAdsPos() {
        return adsPos;
    }

    public void setAdsPos(int adsPos) {
//        Timber.v("setAllAdsPos from: %s to: %s", this.allAdsPos, allAdsPos);
        this.adsPos = adsPos;
    }

    public void fetchFirstImage(final Context context){
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<GalleryImageObject> data = new ArrayList<>();
                Cursor cursor = context.getContentResolver()
                        .query(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                new String[] {MediaStore.Images.ImageColumns._ID, MediaStore.MediaColumns.DATA},
                                null, null, "_id DESC LIMIT 1");

                if (cursor == null){
                    return;
                }

                while (cursor.moveToNext()){
                    GalleryImageObject gim = new GalleryImageObject(cursor);
                    fileUrl = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DATA));
                }

                cursor.close();
            }
        }).start();

    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void fetchApprovedPosts(){
        ServerManager.queryServer(ServerManager.FCBK_APPROVED_POSTS, null, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                // [{"pageUrl":"https://dev-api.adinlay.com/client/social/webpage?title=Adinlay%20sports%20is%20coming%20soon...&image=https%3A%2F%2Fadinlay-published-photos-dev.s3.eu-central-1.amazonaws.com%2Ffacebook%2F5b682f4b33d7547887bcc555%2FGgoUVMHUtr2Rj3afDHas79YH.jpg&link=https%3A%2F%2Fdev-api.adinlay.com%2Fpublic%2Fengagement%2Ffacebook%2Fclick%2F5c3c5f543bdb3e00172493eb","postId":"GgoUVMHUtr2Rj3afDHas79YH","photoUrl":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/GgoUVMHUtr2Rj3afDHas79YH.jpg","photoUrl_small":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/GgoUVMHUtr2Rj3afDHas79YH_small.jpg"},{"pageUrl":"https://dev-api.adinlay.com/client/social/webpage?title=Adinlay%20sports%20is%20coming%20soon...&image=https%3A%2F%2Fadinlay-published-photos-dev.s3.eu-central-1.amazonaws.com%2Ffacebook%2F5b682f4b33d7547887bcc555%2FU8EK1aPlwYAcOUDyX7Kt9q9Y.jpg&link=https%3A%2F%2Fdev-api.adinlay.com%2Fpublic%2Fengagement%2Ffacebook%2Fclick%2F5c3c62c7a839b700173f377e","postId":"U8EK1aPlwYAcOUDyX7Kt9q9Y","photoUrl":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/U8EK1aPlwYAcOUDyX7Kt9q9Y.jpg","photoUrl_small":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/U8EK1aPlwYAcOUDyX7Kt9q9Y_small.jpg"},{"pageUrl":"https://dev-api.adinlay.com/client/social/webpage?title=Adinlay%20sports%20is%20coming%20soon...&image=https%3A%2F%2Fadinlay-published-photos-dev.s3.eu-central-1.amazonaws.com%2Ffacebook%2F5b682f4b33d7547887bcc555%2F5tVn7dGI8L3U5lvdo5dgCZYL.jpg&link=https%3A%2F%2Fdev-api.adinlay.com%2Fpublic%2Fengagement%2Ffacebook%2Fclick%2F5c3c6385a839b700173f3781","postId":"5tVn7dGI8L3U5lvdo5dgCZYL","photoUrl":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/5tVn7dGI8L3U5lvdo5dgCZYL.jpg","photoUrl_small":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/5tVn7dGI8L3U5lvdo5dgCZYL_small.jpg"},{"pageUrl":"https://dev-api.adinlay.com/client/social/webpage?title=Adinlay%20sports%20is%20coming%20soon...&image=https%3A%2F%2Fadinlay-published-photos-dev.s3.eu-central-1.amazonaws.com%2Ffacebook%2F5b682f4b33d7547887bcc555%2F2xYgE0wfnHR9i1A18CYhItC3.jpg&link=https%3A%2F%2Fdev-api.adinlay.com%2Fpublic%2Fengagement%2Ffacebook%2Fclick%2F5c3c64aaa839b700173f3782","postId":"2xYgE0wfnHR9i1A18CYhItC3","photoUrl":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/2xYgE0wfnHR9i1A18CYhItC3.jpg","photoUrl_small":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/2xYgE0wfnHR9i1A18CYhItC3_small.jpg"}]
                AdinlayApp.setApprovedPosts(responseBody);
                getPingApprovedPosts().postValue(FETCH_APPROVED_OK);
            }

            @Override
            public void onError(String message) {
            }
        });
    }
}
