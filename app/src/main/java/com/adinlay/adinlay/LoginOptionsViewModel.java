package com.adinlay.adinlay;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

/**
 * Created by Noya on 6/3/2018.
 */

public class LoginOptionsViewModel extends ViewModel {

    private static final String TAG = LoginOptionsViewModel.class.getSimpleName();

    private MutableLiveData<String> serverRequestLiveData;
    private MutableLiveData<Boolean> spinnerLiveData;

    public MutableLiveData<String> getServerRequestLiveData(){
        if (serverRequestLiveData == null){
            serverRequestLiveData = new MutableLiveData<>();
        }
        return serverRequestLiveData;
    }

    public MutableLiveData<Boolean> getSpinnerLiveData() {
        if (spinnerLiveData == null){
            spinnerLiveData = new MutableLiveData<>();
        }
        return spinnerLiveData;
    }

    public void signInUser(String email, String password){
        getSpinnerLiveData().setValue(true);
        SignInManager.signInUser(email, password, signInCallback);
    }

    public void signInWithFacebookToken(AccessToken facebookAccessToken){
        getSpinnerLiveData().setValue(true);
        SignInManager.handleFacebookSignIn(facebookAccessToken, signInCallback);
    }

    public void signInWithGoogleAccount(GoogleSignInAccount account){
        getSpinnerLiveData().setValue(true);
        SignInManager.handleGoogleSignIn(account, signInCallback);
    }

    private ServerManager.Callback signInCallback = new ServerManager.Callback() {
        @Override
        public void onResponseOk(String responseBody) {
            getSpinnerLiveData().postValue(false);
            if (AdinlayApp.getUser() != null){
                getServerRequestLiveData().postValue("ready");
            } else { // shouldn't happen
                getServerRequestLiveData().postValue(AyUtils.getStringResource(R.string.something_wrong_adinlay_login));
            }
        }

        @Override
        public void onError(String message) {
            getSpinnerLiveData().postValue(false);
            getServerRequestLiveData().postValue(message);
        }
    };

    private ServerManager.Callback instaSignInCallback = new ServerManager.Callback() {
        @Override
        public void onResponseOk(String responseBody) {
            getSpinnerLiveData().postValue(false);
            if (AdinlayApp.getUser() != null){
                getServerRequestLiveData().postValue("ready");
            } else { // shouldn't happen
                getServerRequestLiveData().postValue(AyUtils.getStringResource(R.string.something_wrong_inst_login));
            }
        }

        @Override
        public void onError(String message) {
            getSpinnerLiveData().postValue(false);
            getServerRequestLiveData().postValue(message);
        }
    };

    public void connectToInstagram(String userName, String password){
        getSpinnerLiveData().setValue(true);
        if (AdinlayApp.getUser() != null && AdinlayApp.getUser().isIgLoginViaClient()) {
            ServerManager.igConnectViaClient(userName, password, instaSignInCallback);
        } else {
            ServerManager.connectToInstagram(userName, password, instaSignInCallback);
        }
    }

}
