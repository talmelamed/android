package com.adinlay.adinlay;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;

import timber.log.Timber;

/**
 * Created by Noya on 8/8/2018.
 */

public class FiltersFrag extends Fragment {

    private static final String TAG = FiltersFrag.class.getSimpleName();

    private GridLayoutManager layoutManager = null;
    private FiltersRecyclerViewAdapter adapter = null;
    protected RecyclerView recyclerView = null;

    EditPostViewModel model = null;
    private WeakReference<ItemClick> wr_itemClick = new WeakReference<ItemClick>(null);

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_recyclerview, container, false);

        if (model == null && getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(EditPostViewModel.class);
        }

        recyclerView = root.findViewById(R.id.recycler);
        recyclerView.setPadding(0, 0,0,0);
        recyclerView.setHasFixedSize(true);

        root.findViewById(R.id.refresh_layout).setEnabled(false);

        layoutManager = new GridLayoutManager(inflater.getContext(), 3);
        recyclerView.setLayoutManager(layoutManager);

        if (model.getFiltersList() == null){
            model.getPingFilters().observe(this, new Observer<String>() {
                @Override
                public void onChanged(@Nullable String s) {
                    setAdapter();
                }
            });
            // FIXME: 8/8/2018 perhaps should fetch filters again?
            Timber.d("FF list is null");
        } else {
            setAdapter();
            Timber.d("FF list is ready");
        }
        return root;
    }

    private void setAdapter(){
        GlideRequests gr = getActivity() == null ? GlideApp.with(this) : GlideApp.with(getActivity());
        adapter = new FiltersRecyclerViewAdapter(model.getImagePathLiveData().getValue(), model.getFiltersList(),
                AdinlayApp.getSelectedAd(), 3, gr, wr_itemClick.get());
        recyclerView.setAdapter(adapter);
    }

    public void setItemClickCallback(ItemClick callback){
        wr_itemClick = new WeakReference<ItemClick>(callback);
    }

}
