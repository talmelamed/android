package com.adinlay.adinlay;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.adinlay.adinlay.objects.ApprovedPost;

import java.util.ArrayList;


/**
 * Created by Noya on 8/15/2018.
 */

public class ApprovedPostViewModel extends ViewModel {

    public static String SHARE_OK = "shareOk";
    public static String DELETE_OK = "deleteOk";
    public static String SHARE_FAIL = "shareFail_";
    public static String DELETE_FAIL = "deleteFail";
    public static String FETCH_OK = "fetchOk";


    private MutableLiveData<String> pingServerResponse;
//    private MutableLiveData<String> pingReportFBsuccess;
    private MutableLiveData<Boolean> spinnerLiveData;

    public MutableLiveData<String> getPingServerResponse(){
        if (pingServerResponse == null){
            pingServerResponse = new MutableLiveData<>();
        }
        return pingServerResponse;
    }

//    public MutableLiveData<String> getPingReportFBsuccess(){
//        if (pingReportFBsuccess == null){
//            pingReportFBsuccess = new MutableLiveData<>();
//        }
//        return pingReportFBsuccess;
//    }

    public MutableLiveData<Boolean> getSpinnerLiveData() {
        if (spinnerLiveData == null){
            spinnerLiveData = new MutableLiveData<>();
        }
        return spinnerLiveData;
    }



    public void reportFcbkPostResult(String postId, boolean isSuccess){
        ServerManager.Callback successCallback = new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                getSpinnerLiveData().postValue(false);
                getPingServerResponse().postValue(SHARE_OK);
                if (AdinlayApp.getUser() != null && AdinlayApp.getUser().socialMediaTestIncomplete()){
                    ServerManager.getUpdatedUserInfo(null);
                }
            }

            @Override
            public void onError(String message) {
                getSpinnerLiveData().postValue(false);
                String msg =  message == null ? SHARE_FAIL : SHARE_FAIL + message;
                getPingServerResponse().postValue(msg);
            }
        };
        getSpinnerLiveData().postValue(isSuccess);
        ServerManager.reportFacebookPostResult(postId, isSuccess, isSuccess? successCallback : null);

    }

    public void deleteFcbkPost(String postId){
        ServerManager.Callback callback = new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                getSpinnerLiveData().postValue(false);
                getPingServerResponse().postValue(DELETE_OK);
                if (AdinlayApp.getUser() != null && AdinlayApp.getUser().socialMediaTestIncomplete()){
                    ServerManager.getUpdatedUserInfo(null);
                }
            }

            @Override
            public void onError(String message) {
                getSpinnerLiveData().postValue(false);
                String msg =  message == null ? DELETE_FAIL : DELETE_FAIL + message;
                getPingServerResponse().postValue(msg);
            }
        };
        getSpinnerLiveData().postValue(true);
        ServerManager.reportFacebookPostResult(postId, false, callback);

    }

    public void fetchApprovedPosts(){
        ServerManager.queryServer(ServerManager.FCBK_APPROVED_POSTS, null, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                getSpinnerLiveData().postValue(false);
                // [{"pageUrl":"https://dev-api.adinlay.com/client/social/webpage?title=Adinlay%20sports%20is%20coming%20soon...&image=https%3A%2F%2Fadinlay-published-photos-dev.s3.eu-central-1.amazonaws.com%2Ffacebook%2F5b682f4b33d7547887bcc555%2FGgoUVMHUtr2Rj3afDHas79YH.jpg&link=https%3A%2F%2Fdev-api.adinlay.com%2Fpublic%2Fengagement%2Ffacebook%2Fclick%2F5c3c5f543bdb3e00172493eb","postId":"GgoUVMHUtr2Rj3afDHas79YH","photoUrl":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/GgoUVMHUtr2Rj3afDHas79YH.jpg","photoUrl_small":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/GgoUVMHUtr2Rj3afDHas79YH_small.jpg"},{"pageUrl":"https://dev-api.adinlay.com/client/social/webpage?title=Adinlay%20sports%20is%20coming%20soon...&image=https%3A%2F%2Fadinlay-published-photos-dev.s3.eu-central-1.amazonaws.com%2Ffacebook%2F5b682f4b33d7547887bcc555%2FU8EK1aPlwYAcOUDyX7Kt9q9Y.jpg&link=https%3A%2F%2Fdev-api.adinlay.com%2Fpublic%2Fengagement%2Ffacebook%2Fclick%2F5c3c62c7a839b700173f377e","postId":"U8EK1aPlwYAcOUDyX7Kt9q9Y","photoUrl":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/U8EK1aPlwYAcOUDyX7Kt9q9Y.jpg","photoUrl_small":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/U8EK1aPlwYAcOUDyX7Kt9q9Y_small.jpg"},{"pageUrl":"https://dev-api.adinlay.com/client/social/webpage?title=Adinlay%20sports%20is%20coming%20soon...&image=https%3A%2F%2Fadinlay-published-photos-dev.s3.eu-central-1.amazonaws.com%2Ffacebook%2F5b682f4b33d7547887bcc555%2F5tVn7dGI8L3U5lvdo5dgCZYL.jpg&link=https%3A%2F%2Fdev-api.adinlay.com%2Fpublic%2Fengagement%2Ffacebook%2Fclick%2F5c3c6385a839b700173f3781","postId":"5tVn7dGI8L3U5lvdo5dgCZYL","photoUrl":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/5tVn7dGI8L3U5lvdo5dgCZYL.jpg","photoUrl_small":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/5tVn7dGI8L3U5lvdo5dgCZYL_small.jpg"},{"pageUrl":"https://dev-api.adinlay.com/client/social/webpage?title=Adinlay%20sports%20is%20coming%20soon...&image=https%3A%2F%2Fadinlay-published-photos-dev.s3.eu-central-1.amazonaws.com%2Ffacebook%2F5b682f4b33d7547887bcc555%2F2xYgE0wfnHR9i1A18CYhItC3.jpg&link=https%3A%2F%2Fdev-api.adinlay.com%2Fpublic%2Fengagement%2Ffacebook%2Fclick%2F5c3c64aaa839b700173f3782","postId":"2xYgE0wfnHR9i1A18CYhItC3","photoUrl":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/2xYgE0wfnHR9i1A18CYhItC3.jpg","photoUrl_small":"https://adinlay-published-photos-dev.s3.eu-central-1.amazonaws.com/facebook/5b682f4b33d7547887bcc555/2xYgE0wfnHR9i1A18CYhItC3_small.jpg"}]
                AdinlayApp.setApprovedPosts(responseBody);
                getPingServerResponse().postValue(FETCH_OK);
            }

            @Override
            public void onError(String message) {
                getSpinnerLiveData().postValue(false);
//                getPingServerResponse().postValue(message);
            }
        });
    }

    public void sendPromoToServer(String promo, final boolean isAdd){
        getSpinnerLiveData().postValue(true);
        ServerManager.sendPromoToServer(promo, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                getSpinnerLiveData().postValue(false);
                getPingServerResponse().postValue(isAdd ? SHARE_OK : DELETE_OK);
            }

            @Override
            public void onError(String message) {
                getSpinnerLiveData().postValue(false);
                String prefix = isAdd ? SHARE_FAIL : DELETE_FAIL;
                String msg =  message == null ? prefix : prefix + message;
                getPingServerResponse().postValue(msg);
            }
        }, isAdd);
    }

}
