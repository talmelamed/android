package com.adinlay.adinlay;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;

/**
 * Created by Noya on 8/8/2018.
 */

public class AdLayoutsFrag extends Fragment {

    private static final String TAG = AdLayoutsFrag.class.getSimpleName();

    private GridLayoutManager layoutManager = null;
    private PreviewRecyclerViewAdapter adapter = null;
    protected RecyclerView recyclerView = null;

    EditPostViewModel model = null;
    private WeakReference<ItemClick> wr_itemClick = new WeakReference<ItemClick>(null);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(EditPostViewModel.class);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_recyclerview, container, false);

        if (model == null && getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(EditPostViewModel.class);
        }

        recyclerView = root.findViewById(R.id.recycler);
        recyclerView.setPadding(0, 0,0,0);
        recyclerView.setHasFixedSize(true);

        root.findViewById(R.id.refresh_layout).setEnabled(false);

        layoutManager = new GridLayoutManager(inflater.getContext(), 3);
        recyclerView.setLayoutManager(layoutManager);

        model.getImagePathLiveData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (s != null){
                    setAdapter(s);
                }
            }
        });

        setAdapter(model.getImagePathLiveData().getValue());

//        root.findViewById(R.id.recycler_frag_message).setVisibility(View.GONE); // already gone

        return root;
    }

    public void setItemClickCallback(ItemClick callback){
        wr_itemClick = new WeakReference<ItemClick>(callback);
    }

    private void setAdapter(String path){
        if (AdinlayApp.getSelectedAd() != null) {
            GlideRequests gr = getActivity() == null ? GlideApp.with(this) : GlideApp.with(getActivity());
            adapter = new PreviewRecyclerViewAdapter(path, AdinlayApp.getSelectedAd(), 3, gr, wr_itemClick.get());
            recyclerView.setAdapter(adapter);
        }
    }

}
