package com.adinlay.adinlay;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adinlay.adinlay.objects.PromoItem;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class PromoCodesRvAdapter extends RecyclerView.Adapter<PromoCodesRvAdapter.ViewHolder> {

    private WeakReference<ItemClick> wr_callback;
    private ArrayList<PromoItem> items;

    public PromoCodesRvAdapter(ArrayList<PromoItem> items, ItemClick callback) {
        this.items = items;
        wr_callback = new WeakReference<>(callback);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_promo_code, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PromoItem promoItem = items.get(position);

        holder.nameTv.setText(promoItem.getName());
        holder.codeTv.setText(promoItem.getCode());

        holder.deleteButton.setTag(R.id.object_tag, promoItem);
        holder.deleteButton.setOnClickListener(itemClickListener);

        AyUtils.setOddEvenBackgroundColor(position, holder.itemView);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView nameTv;
        TextView codeTv;
        View deleteButton;

        public ViewHolder(View itemView) {
            super(itemView);

            nameTv = itemView.findViewById(R.id.item_promo_name_tv);
            codeTv = itemView.findViewById(R.id.item_promo_code_tv);
            deleteButton = itemView.findViewById(R.id.item_promo_button);
        }
    }

    View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (wr_callback.get() != null){
                wr_callback.get().onItemClicked(v.getTag(R.id.object_tag));
            }
        }
    };

}
