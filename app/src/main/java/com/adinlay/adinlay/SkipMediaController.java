package com.adinlay.adinlay;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.MediaController;
import android.widget.TextView;

/**
 * Created by Noya on 6/21/2018.
 */

public class SkipMediaController extends MediaController {
    private OnClickListener skipListener = null;


    public SkipMediaController(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SkipMediaController(Context context, boolean useFastForward) {
        super(context, useFastForward);
    }

    public SkipMediaController(Context context) {
        super(context);
    }

    @Override
    public void setAnchorView(View view) {
        super.setAnchorView(view);

//        Button skip = new Button(getContext());
        TextView skip = new TextView(getContext());
        skip.setText("X");
        skip.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        skip.setTextColor(Color.BLACK);
        skip.setTypeface(skip.getTypeface(), Typeface.BOLD_ITALIC);

        skip.setBackgroundResource(R.drawable.btn_bg_grayish);
        skip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (skipListener != null){
                    skipListener.onClick(v);
                }
            }
        });
        int pad = AyUtils.convertDpToPixel(4, getContext());
        skip.setPadding(2*pad, pad, 2*pad, pad);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.LEFT;
        params.setMargins(2*pad, 2*pad,0,pad);
        addView(skip, params);
    }

    public void setSkipListener(OnClickListener skipListener){
        this.skipListener = skipListener;
    }
}
