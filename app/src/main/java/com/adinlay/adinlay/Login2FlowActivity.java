package com.adinlay.adinlay;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.google.firebase.analytics.FirebaseAnalytics;

import timber.log.Timber;

public class Login2FlowActivity extends AppCompatActivity {

    final private static String TAG = Login2FlowActivity.class.getSimpleName();

    TextView mainButtonOld = null;
    ImageView mainButtonIv = null;
    View logo;
    TextView termsLinkTv;
    View spinnerFrame;
    View backButton;
    TextView backButtonTv;

    private LoginViewModel model;

    private FirebaseAnalytics mFirebaseAnalytics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2_flow);

        mainButtonOld = findViewById(R.id.button_main);
        mainButtonIv = findViewById(R.id.login_flow_main_button);
        logo = findViewById(R.id.toolbar_logo);
        termsLinkTv = findViewById(R.id.tv_link_terms_and_conds);
        spinnerFrame = findViewById(R.id.login_spinner_frame);
        spinnerFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do nothing - prevent clicks
            }
        });

        // light background to status bar:
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setNavigationBarColor(getResources().getColor(R.color.colorPrimaryDark)); // need to decide on color
            if (Build.VERSION.SDK_INT >= 23){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
        }

        //toolbar:
        backButton = findViewById(R.id.toolbar_left_btn);
        backButtonTv = findViewById(R.id.toolbar_left_tv);
        backButtonTv.setText(getResources().getString(R.string.arrow_and_back));
        backButtonTv.setTextColor(Color.BLACK);
        findViewById(R.id.toolbar_right_tv).setVisibility(View.GONE);

        model = ViewModelProviders.of(this).get(LoginViewModel.class);
        model.setIsSingleUpdate(getIntent() != null && getIntent().hasExtra(AyUtils.IS_SINGLE_UPDATE));
        model.setSocialMediaId(getIntent() == null ? null : getIntent().getStringExtra(AyUtils.MEDIA_ID_KEY));
        if (model.isSingleUpdate() && AdinlayApp.getUser() != null){
            AdinlayApp.getUser().resetPendingJson(); // clear any changes that didn't go through
        }

        // add home button
        if (model.isSingleUpdate()){
            findViewById(R.id.toolbar_right_iv).setVisibility(View.VISIBLE);
            findViewById(R.id.toolbar_right_btn).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setResult(RESULT_OK, new Intent());
                    finish();
                }
            });
        }

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        model.getLoginFlowLiveData().observe(this, new Observer<LoginFlowData>() {
            @Override
            public void onChanged(@Nullable LoginFlowData loginFlowData) {
                if (loginFlowData == null){
                    return;
                }
                if (!model.isSingleUpdate()) {
                    mainButtonIv.setImageResource(loginFlowData.getMainButtonRes());
                    mainButtonIv.setContentDescription(loginFlowData.getMainButtonText());
                } else {
                    mainButtonIv.setImageResource(R.drawable.group_update);
                    mainButtonIv.setContentDescription(getString(R.string.update));
                }
                if (loginFlowData.getMainButtonRes() == 0 && loginFlowData.getMainButtonText() != null){
                    mainButtonOld.setVisibility(View.VISIBLE);
                    mainButtonOld.setText(loginFlowData.getMainButtonText());
                    findViewById(R.id.login_flow_bottom_pad).setVisibility(View.GONE);
                } else {
                    mainButtonOld.setVisibility(View.GONE);
                    findViewById(R.id.login_flow_bottom_pad).setVisibility(View.VISIBLE);
                }
                int visibility = loginFlowData.showLogo() ? View.VISIBLE : View.GONE;
                logo.setVisibility(visibility);
                visibility = loginFlowData.isShowTermsLink() && !model.isSingleUpdate() ? View.VISIBLE : View.GONE;
                termsLinkTv.setVisibility(visibility);
//                visibility = loginFlowData.showBackButton() || model.isSingleUpdate() ? View.VISIBLE : View.GONE;
                visibility = SignInManager.LETS_START_STAGE.equals(AdinlayApp.getLoginStage()) ? View.GONE: View.VISIBLE;
                backButtonTv.setVisibility(visibility);
//                View.OnClickListener listener = loginFlowData.showBackButton() || model.isSingleUpdate() ? backClickListener : null;
                backButton.setOnClickListener(backClickListener);
            }
        });

        model.getSpinnerLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean show) {
                int visibility = show == null || !show ? View.GONE : View.VISIBLE;
                spinnerFrame.setVisibility(visibility);
            }
        });

        model.getServerRequestLiveData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                Timber.i("onChanged server request %s", s);
                if (LoginViewModel.UPDATE_REQ_OK.equals(s) || LoginViewModel.SIGN_UP_REQ_OK.equals(s)){
                    if (model.isSingleUpdate()){
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.profile_updated), Toast.LENGTH_SHORT).show();
                        AdinlayApp.setLoginStage(SignInManager.COMPLETE_STAGE);
                        finish();
                        AyUtils.setNeedUpdate(true);
                        return;
                    }
                    if (LoginViewModel.UPDATE_REQ_OK.equals(s) && SignInManager.isLastTopic()){
                        AyUtils.saveStringPreference(AyUtils.PREF_DEEP_LINK, null); // clear deep link, if any
                    }
                    // load next frag:
                    loadNextStageFrag();
                } else if (LoginViewModel.UPDATE_EMAIL_REQ_OK.equals(s)) {
                    // we updated the account info, including email, and user needs to verify it
                    // verification email was sent and we display dialog,
                    // ok in dialog loads the next stage/ finishes update and goes back to profile
                    if (model.isSingleUpdate()){
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.profile_updated), Toast.LENGTH_SHORT).show();
                        AdinlayApp.setLoginStage(SignInManager.COMPLETE_STAGE);
                    }
                    String message = getString(R.string.verification_email_was_sent, AdinlayApp.getUserEmail());
                    Bundle args = new Bundle();
                    args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.confirm_your_email);
                    args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, message);
                    args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
                    AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                    dialogFragment.setDismissListener(new AyDialogFragment.AyDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            if (model.isSingleUpdate()){
                                finish();
                                return;
                            }
                            // load next frag:
                            loadNextStageFrag();
                        }
                    });

                    if (!isFinishing()) {
                        AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
                    }
                    return;
                } else if (LoginViewModel.INST_LOGOUT_REQ_OK.equals(s)) {
                    // do nothing - this is not an error message, and handled by the frag
                    return;
                } else if (LoginViewModel.DELETE_USER_DONE.equals(s)) {
                    // collect analytics info
                    Bundle params = new Bundle();
                    Fragment currFrag = getSupportFragmentManager().findFragmentByTag(TAG);
                    String stageName = currFrag instanceof LoginFrag ?
                            ((LoginFrag) currFrag).getFragStageName() : "";
                    params.putString("stage", stageName);

                    goBackToLoginOptions();

                    mFirebaseAnalytics.logEvent("start_over", params);
                    return;
                } else {
                    String message = s;
                    if (message != null && (message.startsWith(LoginViewModel.INST_LOGOUT_REQ_PREFIX) ||
                            message.startsWith(LoginViewModel.FCBK_LOGOUT_REQ_PREFIX) ||
                            message.startsWith(LoginViewModel.FB_LOGIN_FAIL_PREFIX))){
                        // this was an attempt to logout from instagram, just show the message:
                        message = message.replace(LoginViewModel.INST_LOGOUT_REQ_PREFIX, "");
                        if (message.isEmpty()){
                            message = getResources().getString(R.string.something_wrong_inst_logout);
                        }
                        message = message.replace(LoginViewModel.FB_LOGIN_FAIL_PREFIX, "");
                        if (message.isEmpty()){
                            message = getResources().getString(R.string.something_wrong_fcbk_connect);
                        }
                        message = message.replace(LoginViewModel.FCBK_LOGOUT_REQ_PREFIX, "");
                        if (message.isEmpty()){
                            message = getResources().getString(R.string.something_wrong_fcbk_logout);
                        }
                        showErrorDialog(message);
                        return;
                    }

                    // clean the message:
                    String messageDefault = AdinlayApp.getLoginStage().equals(SignInManager.SIGN_UP_STAGE) ?
                            getResources().getString(R.string.something_wrong_adinlay_register) :
                            model.isSingleUpdate() ?
                            getResources().getString(R.string.something_wrong_try_later) :
                            getResources().getString(R.string.something_wrong_adinlay_finish_signin);
                    message = message == null || message.isEmpty() ? messageDefault : message;
                    if (message.startsWith(ServerManager.USER_INFO_TRY_FROM_SCRATCH)){
                        message = message.replace(ServerManager.USER_INFO_TRY_FROM_SCRATCH, "");
                        if (message.isEmpty()) {
                            message = messageDefault;
                        }
                    } else if (message.startsWith(ServerManager.POOR_CONNECTIVITY)){
                        message = getResources().getString(R.string.something_wrong_connectivity);
                    }
                    if (/*AdinlayApp.getLoginStage().equals(SignInManager.SOCIAL_MEDIA_STAGE) ||*/
                            (AdinlayApp.getLoginStage().equals(SignInManager.SIGN_UP_STAGE) && AyUtils.getDecryptedToken() != null)){
                        // should offer the user to logout and start from scratch:
                        showErrorDialog(message, true);
                    } else {
                        // this should be sign-up stage before we got a token, just show error:
                        showErrorDialog(message);
                    }
                }
            }
        });

        model.getIsMainEnabledLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isEnabled) {
                if (isEnabled == null || isEnabled){
                    mainButtonOld.setTextColor(Color.BLACK);
                    mainButtonOld.setBackgroundResource(R.drawable.btn_bg_clear);
                } else {
                    mainButtonOld.setTextColor(getResources().getColor(R.color.disabled_black));
                    mainButtonOld.setBackgroundResource(R.drawable.btn_bg_clear_disabled);
                }
            }
        });

        mainButtonOld.setOnClickListener(mainClickListener);
        mainButtonIv.setOnClickListener(mainClickListener);

        Fragment fragment = getFragForStage();
        if (fragment != null){
            android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.login_flow_main_frame, fragment, TAG);
            ft.commit();
        } else {
            Timber.w("oncreate and got null fragment, stage is: %s", AdinlayApp.getLoginStage());
            finish();
        }

        findViewById(R.id.login2_con_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Timber.v("CLK layout");
                hideKeyboard();
            }
        });

        final View layoutRoot = findViewById(R.id.login2_con_layout);
        layoutRoot.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect rect = new Rect();
                layoutRoot.getWindowVisibleDisplayFrame(rect);

                int heightDiff = layoutRoot.getRootView().getHeight() - (rect.bottom - rect.top);
//                Timber.i("heightDiff: %s root.h: %s rect.h_dp %s", heightDiff, layoutRoot.getRootView().getHeight(), AyUtils.convertPixelToDp((rect.bottom - rect.top), layoutRoot.getContext()));
//                Timber.i("1dp: %s", AyUtils.convertDpToPixel(1, Login2FlowActivity.this));
                boolean open = false;
                if (heightDiff > AyUtils.convertDpToPixel(100, layoutRoot.getContext())){
                    open = true;
                }
                int visibility = open? View.GONE : View.VISIBLE;

                // temp fix for confirm screen:
                setViewVisibility(findViewById(R.id.l4_chain_spacer0), open);
                setViewVisibility(findViewById(R.id.l4_chain_spacer1), open);
                setViewVisibility(findViewById(R.id.l4_chain_spacer2), open);
                setViewVisibility(findViewById(R.id.l4_chain_spacer3), open);

                // in smaller screens, also hide logo:
                View logo = findViewById(R.id.login_toolbar);
                int logoVisibility = AyUtils.convertPixelToDp((rect.bottom - rect.top), layoutRoot.getContext()) > 370 ?
                        View.VISIBLE : visibility;
                if (logo != null){
                    logo.setVisibility(logoVisibility);
                }
            }
        });

        setTermsText();

    }

    private void setViewVisibility(View v, boolean shouldHide){
        if (v != null){
            int visibility = shouldHide ? View.GONE : View.VISIBLE;
            v.setVisibility(visibility);
        }
    }

    private void loadNextStageFrag(){
        // update LoginStage
        SignInManager.setNextStage();
        // get the frag:
        Fragment nextFrag = getFragForStage();
        if (nextFrag != null) {
            loadFragment(nextFrag);
        } else {
            // todo: else?!?
        }
    }

    private void loadFragment(@NonNull Fragment fragment){
        hideKeyboard();
        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.login_flow_main_frame, fragment, TAG);
        ft.commit();
    }

    View.OnClickListener mainClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            hideKeyboard();
            android.support.v4.app.FragmentManager fm = getSupportFragmentManager();

            boolean doneWithFrag = false;
            Fragment currFrag = fm.findFragmentByTag(TAG);
            Timber.i("mainClickListener stage: %s gotLoginFrag? %s", AdinlayApp.getLoginStage(), (currFrag instanceof LoginFrag));
            if (currFrag instanceof LoginFrag){
                doneWithFrag = ((LoginFrag) currFrag).onMainClicked();
            }
            String user =  AdinlayApp.getUser() == null ? "null" :  AdinlayApp.getUser().toString();
            Timber.i("mainClickListener user: %s", user);

            if (doneWithFrag){
                // handle special cases:
                if (SignInManager.SIGN_UP_STAGE.equals(AdinlayApp.getLoginStage())){
                    // show spinner, create user, then continue - always returns false, frag sends info to server
                    return;
                } else if (/*(SignInManager.SOCIAL_MEDIA_STAGE.equals(AdinlayApp.getLoginStage()) && isUserConnected())*/
                        SignInManager.isLastTopic() || model.isSingleUpdate()){
                    // send the updates to the server:
                    if (AdinlayApp.getUser() != null) {
                        if (!model.isSingleUpdate()) {
                            AdinlayApp.getUser().changePendingJsonField(User.REG_STATUS_KEY, "completed");
                            String referral = AyUtils.getStringPref(AyUtils.PREF_DEEP_LINK);
                            if (referral != null){
                                AdinlayApp.getUser().changePendingJsonField("referralLink", referral);
//                                AyUtils.saveStringPreference(AyUtils.PREF_DEEP_LINK, null); // clearing pref after response from server
//                                Timber.d("DL__ sending deepLink to server %s", referral);
                            }
                        }
                        model.sendUserUpdateToServer();
                    }
                    return;
                } /*else if (SignInManager.SOCIAL_MEDIA_STAGE.equals(AdinlayApp.getLoginStage()) && !isUserConnected()){
                    // show dialog:
                    if (messageDialog != null && messageDialog.isShowing()){
                        messageDialog.dismiss();
                    }
                    AdinlayDialogBuilder builder = new AdinlayDialogBuilder(Login2FlowActivity.this);
                    builder.setCustomTitle(R.string.skip_title)
                            .setCustomMessage(R.string.skip_soc_med_explain)
                            .setButtonR(R.string.login_button, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (messageDialog != null){
                                        messageDialog.dismiss();
                                    }
                                }
                            })
                            .setButtonL(R.string.later_allcaps, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (messageDialog != null){
                                        messageDialog.dismiss();
                                    }
                                    if (AdinlayApp.getUser() != null) {
                                        if (!model.isSingleUpdate()) { // should always be true
                                            AdinlayApp.getUser().changePendingJsonField(User.REG_STATUS_KEY, "completed");
                                        }
                                        model.sendUserUpdateToServer();
                                    }
                                }
                            });

                    messageDialog = builder.create();
                    messageDialog.show();

                    return;
                } */else if (SignInManager.LETS_START_STAGE.equals(AdinlayApp.getLoginStage())){
                    // add event:
                    Bundle params = null;
                    long flowStart = model.getFlowStart();
                    if (flowStart > 0){
                        params = new Bundle();
                        long delta = System.currentTimeMillis() - flowStart;
                        params.putLong("flow_duration", delta);
                    }
                    mFirebaseAnalytics.logEvent("registration_complete", params);
                    // intent to movie, and finish here
                    AdinlayApp.setLoginStage(SignInManager.COMPLETE_STAGE);
                    Bundle args = new Bundle();
                    args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.heads_up);
                    args.putInt(AyDialogFragment.ARG_MESSAGE_RES, R.string.will_need_to_do_test_post);
                    args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
                    AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                    dialogFragment.setDismissListener(new AyDialogFragment.AyDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            Intent intent = new Intent(Login2FlowActivity.this, VideoActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                    AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
                    return;
                }


                loadNextStageFrag();
            }


        }
    };

    private boolean isUserConnected (){
        User user = AdinlayApp.getUser();
        return user != null && (user.isConnectedToInstagram() || user.isConnectedToFacebook());
    }

    @Override
    public void onBackPressed() {
        if (SignInManager.SIGN_UP_STAGE.equals(AdinlayApp.getLoginStage())){
            // go back to login-options:
            goBackToLoginOptions();
            return;
        }
        FragmentManager fm = getSupportFragmentManager();
        Fragment currFrag = fm.findFragmentByTag(TAG);
        if (currFrag instanceof LoginFrag && ((LoginFrag) currFrag).onBackClicked()){
            return;
        }
        if (!model.isSingleUpdate()){
            if (SignInManager.LETS_START_STAGE.equals(AdinlayApp.getLoginStage())){
                super.onBackPressed(); // just leave the app, user registration is complete, so no need to offer start-over
                return;
            }
            int message = SignInManager.EXPLAIN_INCOMPLETE_STAGE.equals(AdinlayApp.getLoginStage()) ||
                    SignInManager.CONFIRM_STAGE.equals(AdinlayApp.getLoginStage()) ? R.string.exit_or_start_over :
                    R.string.exit_or_start_over_long;
            Bundle args = new Bundle();
            args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.what_to_do);
            args.putInt(AyDialogFragment.ARG_MESSAGE_RES, message);
            args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.cancel);
            args.putInt(AyDialogFragment.ARG_BUTTON_R_RES, R.string.exit);
            args.putInt(AyDialogFragment.ARG_BUTTON_M_RES, R.string.start_over);
            AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
            dialogFragment.setListener(AyDialogFragment.VALUE_RIGHT, new AyDialogFragment.AyClickListener() {
                @Override
                public void onButtonClicked(View v) {
                    finish();
                }
            });
            dialogFragment.setListener(AyDialogFragment.VALUE_MID, new AyDialogFragment.AyClickListener() {
                @Override
                public void onButtonClicked(View v) {
                    model.onStartOver();
                }
            });
            AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
            return;
        } else {
            AdinlayApp.setLoginStage(SignInManager.COMPLETE_STAGE);
            if (model.getSpinnerLiveData().getValue() != null &&
                    !model.getSpinnerLiveData().getValue() && AdinlayApp.getUser() != null){
                AdinlayApp.getUser().resetPendingJson(); // back from single update and not waiting for server - clear pending
            }
        }
        super.onBackPressed();
    }

    View.OnClickListener backClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressed();
        }
    };

    private void goBackToLoginOptions(){
        AdinlayApp.setLoginStage(null);
        AyUtils.logoutUser();
        Intent intent = new Intent(Login2FlowActivity.this, Login1Activity.class);
        startActivity(intent);
        finish();
    }

    private void setTermsText(){
        String tcCamelStr = getResources().getString(R.string.terms_and_conditions_camel);
        SpannableStringBuilder spanBuilder = new SpannableStringBuilder(
                getResources().getString(R.string.press_terms_and_conditions_text));
        spanBuilder.append(tcCamelStr);
        spanBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                Intent intent = new Intent(Login2FlowActivity.this, TermsActivity.class);
                startActivity(intent);
            }
        }, spanBuilder.length() - tcCamelStr.length(), spanBuilder.length(), 0);
        int firstSpanLength = spanBuilder.length();
        spanBuilder.append(getResources().getString(R.string._and_));
        spanBuilder.setSpan(new ForegroundColorSpan(Color.BLACK), firstSpanLength, spanBuilder.length(), 0);
        String ppCamel = getResources().getString(R.string.privacy_policy_camel);
        spanBuilder.append(ppCamel);
        spanBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                Intent intent = new Intent(Login2FlowActivity.this, TermsActivity.class);
                intent.putExtra("privacy", "privacy");
                startActivity(intent);
            }
        }, spanBuilder.length() - ppCamel.length(), spanBuilder.length(), 0);
        termsLinkTv.setMovementMethod(LinkMovementMethod.getInstance());
        termsLinkTv.setText(spanBuilder, TextView.BufferType.SPANNABLE);
    }

    private void hideKeyboard(){

        if (mainButtonOld != null){
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(mainButtonOld.getWindowToken(), 0);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data); // ensures a frag that called startActivityForResult will get the result

    }

    private void showErrorDialog(String message){
        showErrorDialog(message, false);
    }

    private void showErrorDialog(String message, boolean withLogout){

        if (message == null || message.isEmpty()){
            return;
        }
        Bundle args = new Bundle();
        args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.error);
        args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, message);
        args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
        if (withLogout){
            args.putInt(AyDialogFragment.ARG_BUTTON_R_RES, R.string.start_over);
        }
        AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
        if (withLogout) {
            dialogFragment.setListener(AyDialogFragment.VALUE_RIGHT, new AyDialogFragment.AyClickListener() {
                @Override
                public void onButtonClicked(View v) {
                    goBackToLoginOptions();
                }
            });
        }
        if (!isFinishing()) {
            AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (AdinlayApp.getUser() == null && !(SignInManager.SIGN_UP_STAGE.equals(AdinlayApp.getLoginStage()))){
            // lost state - back to splash
            Intent intent = new Intent(Login2FlowActivity.this, SplashActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private Fragment getFragForStage(){

        Fragment frag = null;

        switch (AdinlayApp.getLoginStage()){
            case SignInManager.SIGN_UP_STAGE:
                frag = new SignUpFrag();
                break;
            case SignInManager.CONFIRM_STAGE:
            case SignInManager.UPDATE_CONFIRM_STAGE:
                frag = new Login4ConfirmFrag();
                break;
            case SignInManager.EXPLAIN_INCOMPLETE_STAGE:
            case SignInManager.EXPLAIN_YOU_STAGE:
            case SignInManager.EXPLAIN_FOLLOWERS_STAGE:
                frag = new Login5ExplainFrag();
                break;
            case SignInManager.YOU_STAGE:
            case SignInManager.FOLLOWERS_STAGE:
            case SignInManager.UPDATE_YOU_STAGE:
            case SignInManager.UPDATE_FOLLOWERS_STAGE:
                frag = new ProfileTopicFrag();
                ((ProfileTopicFrag) frag).setIsAboutUser(
                        AdinlayApp.getLoginStage().equals(SignInManager.YOU_STAGE) ||
                                AdinlayApp.getLoginStage().equals(SignInManager.UPDATE_YOU_STAGE),
                        model.isSingleUpdate(), model.getSocialMediaId());
                break;
            case SignInManager.SOCIAL_MEDIA_STAGE:
                frag = new Login9AddSocMedFrag();
                break;
//            case SignInManager.CONNECT_INSTA_STAGE: // separate activity
//                frag = new PlaceholderFrag();
//                break;
            case SignInManager.LETS_START_STAGE:
                frag = new Login13LetsStartFrag();
                break;
        }

        return frag;
    }

}
