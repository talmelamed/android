package com.adinlay.adinlay;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.adinlay.adinlay.objects.ProfileTopic;
import com.adinlay.adinlay.objects.SocialMediaRvItem;

import java.util.ArrayList;

public class MediaPrefsListActivity extends BasicActivity implements ItemClick {

    String mediaId = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int media = 0;
        Intent createIntent = getIntent();
        if (createIntent != null){
            media = createIntent.getIntExtra("media", 0);
        }
        User user = AdinlayApp.getUser();
        if (media == SocialMediaRvItem.SOCIAL_MEDIA_IG && user != null &&
                user.isConnectedToInstagram()){
            mediaId = user.getIgUserId();
        } else if (media == SocialMediaRvItem.SOCIAL_MEDIA_FB && user != null &&
                user.isConnectedToFacebook()){
            mediaId = user.getFbUserId();
        }
        if (mediaId == null || mediaId.isEmpty() || user == null){
            finish();
            return;
        }

        // header
        FrameLayout headerFrame = findViewById(R.id.profile_vcf_frame);
        headerFrame.addView(getLayoutInflater().inflate(R.layout.profile_media_header, headerFrame, false));

        ImageView iconIv = findViewById(R.id.pm_logo_iv);
        int res = media == SocialMediaRvItem.SOCIAL_MEDIA_IG ? R.drawable.inst_header : R.drawable.facebook_header;
        iconIv.setImageResource(res);
        TextView socMedDisplayName = findViewById(R.id.pm_display_name);
        String name = media == SocialMediaRvItem.SOCIAL_MEDIA_IG ? user.getIgUsername() : user.getFbUsername();
        socMedDisplayName.setText(name);
        TextView socMedFollowers = findViewById(R.id.pm_followers);
        String num = media == SocialMediaRvItem.SOCIAL_MEDIA_IG ? "" + user.getInstaFollowers() : "" + user.getFacebookFriends();
        String followers = media == SocialMediaRvItem.SOCIAL_MEDIA_IG ? getResources().getString(R.string.number_of_followers, num) :
                getResources().getString(R.string.number_of_friends, num);
        socMedFollowers.setText(followers);

        String titleStr = name.toUpperCase() + " " + getResources().getString(R.string.preferences_allcaps).toUpperCase();
        TextView titleTv = findViewById(R.id.profile_prefs_title_tv);
        titleTv.setText(titleStr);
        TextView tv = findViewById(R.id.profile_prefs_subtitle_tv);
        tv.setText(R.string.stronger_profile_who_follows_u);

        // list
        RecyclerView rv = findViewById(R.id.profile_prefs_rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rv.setLayoutManager(layoutManager);
        rv.setHasFixedSize(true);
        ArrayList<ProfileTopic> followersTopics = AdinlayApp.getFollowersProfileTopics();
        TopicsRvAdapter topicsAdapter = new TopicsRvAdapter(followersTopics, false, this);
        rv.setAdapter(topicsAdapter);

    }

    @Override
    int getLayoutResourceId() {
        return R.layout.activity_profile_prefs;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode){
            case R.id.intent_update_profile & 0x0000FFFF:
                if (resultCode == RESULT_OK){ // we send this if the user clicks "home" from next screen
                    setResult(RESULT_OK, new Intent());
                    finish();
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onItemClicked(Object object) {
        if (object instanceof Integer){
            int objectId = ((Integer)object);
            switch (objectId) {
                case R.string.preferences_allcaps:

                    // make sure we have the necessary info:
                    User user = AdinlayApp.getUser();
                    if (user != null && !user.hasSocialMediaProfile(mediaId)){ // a highly-unlikely edge case
                        Bundle args = new Bundle();
                        args.putInt(AyDialogFragment.ARG_MESSAGE_RES, R.string.something_wrong_try_later);
                        args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
                        AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                        AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());

                        // also clean up the stage:
                        AdinlayApp.setLoginStage(SignInManager.COMPLETE_STAGE);

                        return;
                    }

                    Intent intent = new Intent(MediaPrefsListActivity.this, Login2FlowActivity.class);
                    intent.putExtra(AyUtils.IS_SINGLE_UPDATE, AyUtils.IS_SINGLE_UPDATE);
                    intent.putExtra(AyUtils.MEDIA_ID_KEY, mediaId);
                    startActivityForResult(intent, AyUtils.get16BitId(R.id.intent_update_profile));
                    break;
            }
        }
    }
}
