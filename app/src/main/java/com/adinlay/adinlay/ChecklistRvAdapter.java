package com.adinlay.adinlay;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adinlay.adinlay.objects.CheckableObject;
import com.adinlay.adinlay.objects.ProfileTopic;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by Noya on 9/23/2018.
 */

public class ChecklistRvAdapter extends RecyclerView.Adapter<ChecklistRvAdapter.CheckItemViewHolder>{

    private ArrayList<CheckableObject> itemsList;
    private int singleChoicePos = -1;
    private boolean isSingleChoice;
    private boolean mustChooseOne;
    private ArrayList<String> chosenItemsIds = new ArrayList<>();
    private ArrayList<String> cleanAllItemsIds = new ArrayList<>();
    private WeakReference<ItemClick> wr_itemCallback = new WeakReference<ItemClick>(null);
    private boolean isHierarchic;

    public ChecklistRvAdapter(@NonNull ArrayList<CheckableObject> itemsList, boolean isSingleChoice, boolean mustChooseOne){
        this(itemsList, isSingleChoice, mustChooseOne, null, false);
    }

    public ChecklistRvAdapter(@NonNull ArrayList<CheckableObject> itemsList, boolean isSingleChoice,
                              boolean mustChooseOne, ItemClick itemCallback, boolean isHierarchic) {
        this.itemsList = itemsList;
        this.isSingleChoice = isSingleChoice;
        this.mustChooseOne = mustChooseOne;
        wr_itemCallback = new WeakReference<ItemClick>(itemCallback);
        this.isHierarchic = isHierarchic;
        if (isSingleChoice){
            initSingleChoicePos();
        } else if (!isHierarchic){
            initMultipleSelection();
        }
    }

    public int getSingleChoicePos() {
        return singleChoicePos;
    }

    @Nullable
    public String getItemTitle(int pos){
        if (pos < 0 || pos >= getItemCount()){
            return null;
        }
        return itemsList.get(pos).getTitle();
    }

    @NonNull
    @Override
    public CheckItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile_info, parent, false);
        return new CheckItemViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull CheckItemViewHolder holder, int position) {
        CheckableObject currObject = itemsList.get(position);

        holder.title.setText(currObject.getTitle());

        if (isSingleChoice){
            holder.expandButton.setVisibility(View.INVISIBLE);
            holder.switchView.setChecked(position == singleChoicePos);
        } else {
            holder.switchView.setChecked(currObject.isChecked());
            int expandVisibility = currObject.hasSubItems() ? View.VISIBLE : View.INVISIBLE;
            holder.expandButton.setVisibility(expandVisibility);
        }

        holder.switchView.setEnabled(false); // use click on wider area
        holder.mainClickArea.setTag(R.id.position_tag, position);
        holder.mainClickArea.setContentDescription(currObject.getTitle());
        holder.mainClickArea.setOnClickListener(selectItemClickListener);

//        AyUtils.setOddEvenBackgroundColor(position, holder.itemView); // not on topics!
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public static class CheckItemViewHolder extends RecyclerView.ViewHolder{

        View expandButton;
        TextView title;
        SwitchCompat switchView;
        View mainClickArea;

        public CheckItemViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.pii_title);
            expandButton = itemView.findViewById(R.id.pii_expand_button);
            switchView = itemView.findViewById(R.id.pii_switcher);
            mainClickArea = itemView.findViewById(R.id.pii_main_click_area);
        }
    }

    private View.OnClickListener selectItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag(R.id.position_tag) instanceof Integer &&
                    v.getTag(R.id.position_tag) != null){
                int clickedPos = (int) v.getTag(R.id.position_tag);
                if (isSingleChoice){
                    int oldChoice = singleChoicePos;
                    if (oldChoice == clickedPos && mustChooseOne){
                        return;
                    }
                    if (oldChoice == clickedPos){
                        singleChoicePos = -1;
                    } else {
                        singleChoicePos = clickedPos;
                        setObjectIsSelected(clickedPos, true);
                    }
                    setObjectIsSelected(oldChoice, false);
                } else {
                    boolean isSelected = clickedPos > -1 && clickedPos < getItemCount() &&
                            !itemsList.get(clickedPos).isChecked();
                    setObjectIsSelected(clickedPos, isSelected);
                    // todo: handle mustChooseOne scenario
                }
            }
        }
    };


    private void setObjectIsSelected(int pos, boolean isSelected){
        if (pos > -1 && pos < getItemCount()){
            CheckableObject item = itemsList.get(pos);

            // handle DEFAULT_ALL_ID item:
            if (ProfileTopic.DEFAULT_ALL_ID.equals(item.getId())){
                if (item.isChecked()){
                    // DEFAULT_ALL_ID is already selected and user re-clicked on it - do nothing
                    return;
                }
                // DEFAULT_ALL_ID isn't checked: choose it, and un-check all other items:
                for (CheckableObject currItem : itemsList){
                    currItem.setChecked(ProfileTopic.DEFAULT_ALL_ID.equals(currItem.getId()));
                }
                notifyDataSetChanged();
                chosenItemsIds.clear();
                chosenItemsIds.add(ProfileTopic.DEFAULT_ALL_ID);

                // for now, not calling wr_itemCallback, since irrelevant - just in hierarchic topics

                return;
            }


            item.setChecked(isSelected);
            notifyItemChanged(pos);
            if (isSelected){
                chosenItemsIds.add(item.getId());
                if (wr_itemCallback.get() != null){
                    wr_itemCallback.get().onItemClicked(item); // for now, just hierarchic topics
                }

                // if we have a DEFAULT_ALL_ID, make sure it isn't selected
                CheckableObject defaultItem = itemsList.get(0);
                if (defaultItem != null && ProfileTopic.DEFAULT_ALL_ID.equals(defaultItem.getId()) &&
                        defaultItem.isChecked()){
                    defaultItem.setChecked(false);
                    chosenItemsIds.remove(ProfileTopic.DEFAULT_ALL_ID);
                    notifyItemChanged(0);
                }
            } else {
                chosenItemsIds.remove(item.getId());

                // if no item is selected, and we have a DEFAULT_ALL_ID item, turn it on:
                if (chosenItemsIds.isEmpty() && !isSingleChoice && !isHierarchic){
                    CheckableObject defaultItem = itemsList.get(0);
                    if (ProfileTopic.DEFAULT_ALL_ID.equals(defaultItem.getId())){
                        defaultItem.setChecked(true);
                        notifyItemChanged(0);
                        chosenItemsIds.add(ProfileTopic.DEFAULT_ALL_ID);
                    }
                }
            }
        }
    }


    @NonNull
    public ArrayList<String> getChosenItemsIds() {

        if (isSingleChoice && !isHierarchic){
            if (chosenItemsIds.size() <= 1){
                return chosenItemsIds;
            }
            chosenItemsIds.clear();
            if (singleChoicePos > -1 && singleChoicePos < getItemCount()){
                chosenItemsIds.add(itemsList.get(singleChoicePos).getId());
            }
            return chosenItemsIds;
        }

        // fixme: this is an UGLY hack to get hierarchic path, need proper design for sub-items
        if (isHierarchic && chosenItemsIds.isEmpty() && itemsList.size() > 0 && !itemsList.get(0).getIdPath().isEmpty()){
            ArrayList<String> currList = new ArrayList<>();
            String path = itemsList.get(0).getIdPath();
            if (path.endsWith(".")){
                path = path.substring(0, path.length() - 1);
            }
            currList.add(path);
            return currList;
        }

        if (chosenItemsIds.contains(ProfileTopic.DEFAULT_ALL_ID)){
            return new ArrayList<String>();
        }

        return chosenItemsIds;
    }

    private void initSingleChoicePos(){
        for (int i = 0; i < itemsList.size(); i++){
            if (itemsList.get(i).isChecked()){
                singleChoicePos = i;
                chosenItemsIds.add(0, itemsList.get(i).getId());
            }
        }
    }

    private void initMultipleSelection(){
        for (int i = 0; i < itemsList.size(); i++){
            CheckableObject object = itemsList.get(i);
            if (object.isChecked()){
                chosenItemsIds.add(object.getId());
            }
            if (!ProfileTopic.DEFAULT_ALL_ID.equals(object.getId())){
                cleanAllItemsIds.add(object.getId());
            }
        }
    }
}
