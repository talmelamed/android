package com.adinlay.adinlay;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import timber.log.Timber;


/**
 * Created by Noya on 5/24/2018.
 */

public class SplashViewModel extends ViewModel {

    private static final String TAG = SplashViewModel.class.getSimpleName();

    public static final String VERSION_OK = "versionOK_";

    public static final int STAGE_UNSET = 0;
    public static final int CHECKING_APP_VERSION = 1;
    public static final int APP_VERSION_UNSUPPORTED = 2;
    public static final int APP_VERSION_OK = 3;

    private MutableLiveData<Boolean> spinnerLiveData;
    private MutableLiveData<String> serverRequestLiveData;
    private MutableLiveData<String> appVersionCheckLiveData;
    private int stage = STAGE_UNSET;

    private boolean busy = false;


    public MutableLiveData<Boolean> getSpinnerLiveData() {
        if (spinnerLiveData == null){
            spinnerLiveData = new MutableLiveData<>();
        }
        return spinnerLiveData;
    }

    public MutableLiveData<String> getServerRequestLiveData(){
        if (serverRequestLiveData == null){
            serverRequestLiveData = new MutableLiveData<>();
        }
        return serverRequestLiveData;
    }

    public MutableLiveData<String> getAppVersionCheckLiveData(){
        if (appVersionCheckLiveData == null){
            appVersionCheckLiveData = new MutableLiveData<>();
        }
        return appVersionCheckLiveData;
    }


    public void checkUserInfo(){
        ServerManager.getUserInfo(new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                if (AdinlayApp.getUser() != null){
                    getSpinnerLiveData().postValue(false);
                    getServerRequestLiveData().postValue("ready");
                } else {
                    // something went wrong, let's clear the jwt and have the user sign-in again
                    onError(ServerManager.USER_INFO_TRY_FROM_SCRATCH);
                }
            }

            @Override
            public void onError(String message) {
                getSpinnerLiveData().postValue(false);
                if (message != null && message.startsWith(ServerManager.USER_INFO_TRY_FROM_SCRATCH)){
                    // clear the user and jwt:
                    AyUtils.logoutUser();
                }
                getServerRequestLiveData().postValue(message);
                Timber.i("checking user info got bad ret %s", message);
            }
        }, true, null);
    }

    public void validateAppVersion(){

        int versionCode = BuildConfig.VERSION_CODE;
        ServerManager.checkAppVersion(versionCode, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                stage = APP_VERSION_OK;
                getAppVersionCheckLiveData().postValue(VERSION_OK + responseBody);
            }

            @Override
            public void onError(String message) {
                Timber.d("checking app version got bad ret %s", message);
                stage = message != null && message.startsWith(ServerManager.UNSUPPORTED_APP_VERSION) ?
                        APP_VERSION_UNSUPPORTED : STAGE_UNSET;
                getAppVersionCheckLiveData().postValue(message);
            }
        });

    }

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
    }

    @NonNull
    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public void checkInstagramConnect(){
        ServerManager.checkSocMedConnections(null);
    }
}
