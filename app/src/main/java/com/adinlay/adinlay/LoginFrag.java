package com.adinlay.adinlay;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by Noya on 6/3/2018.
 */

public abstract class LoginFrag extends Fragment {

    FirebaseAnalytics mFirebaseAnalytics = null;
    /**
     * called when main button is clicked, frag to save relevant user info
     * or do any
     * @return true if info was collected info and flow can continue
     */
    public abstract boolean onMainClicked();

    public abstract boolean onBackClicked(); // true if no further action is needed

    protected void initProgressRecycler(RecyclerView counterRv, int total, int stage){
        if (counterRv == null || total <= 0){
            return;
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        counterRv.setLayoutManager(layoutManager);
        counterRv.setHasFixedSize(true);

        CounterRecyclerViewAdapter counterAdapter = new CounterRecyclerViewAdapter(total, stage);
        counterRv.setAdapter(counterAdapter);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    @Override
    public void onResume() {
        super.onResume();
        Activity activity = getActivity();
        if (mFirebaseAnalytics != null && activity != null && shouldChangeScreenName()){
            mFirebaseAnalytics.setCurrentScreen(activity, getFragStageName(), null);
        }
    }

    protected abstract String getFragStageName();

    protected boolean shouldChangeScreenName(){
        return true;
    }
}
