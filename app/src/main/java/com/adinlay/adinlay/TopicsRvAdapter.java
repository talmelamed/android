package com.adinlay.adinlay;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.objects.ProfileTopic;

import org.json.JSONArray;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by Noya on 10/9/2018.
 */

public class TopicsRvAdapter extends RecyclerView.Adapter<TopicsRvAdapter.TopicListItemViewHolder>{

    private ArrayList<ProfileTopic> items;
    private boolean isAboutUser; // if false => is followers
    private WeakReference<ItemClick> wr_itemClick;

    public TopicsRvAdapter(@NonNull ArrayList<ProfileTopic> items, boolean isAboutUser, ItemClick itemClick) {
        this.items = items;
        this.isAboutUser = isAboutUser;
        wr_itemClick = new WeakReference<>(itemClick);
    }

    @NonNull
    @Override
    public TopicListItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_topic_entry, parent, false);
        return new TopicListItemViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull TopicListItemViewHolder holder, int position) {
        ProfileTopic topic = items.get(position);
        String title = topic.getTitle();
        if (topic.isHierarchic()){
            User user = AdinlayApp.getUser();
            if (user != null) {
                JSONArray selectedArray = user.getProfileTopicSelection(topic.getId(), isAboutUser);
                String selectionString = selectedArray.length() > 0 ? selectedArray.optString(0) : "";
                if (!selectionString.isEmpty()){
                    if (ProfileTopic.LOCATION.equals(topic.getId())){
                        String selectionText = AdinlayApp.getLocationName(selectionString);
                        selectionString = selectionText != null ? selectionText.replace("\\", ", ") : selectionString;
                    }
                    title += ": " + selectionString; // this is the ID, but will do for now
                }
            }
        }
        holder.tv.setText(title);
        holder.button.setTag(R.id.position_tag, position);
        holder.button.setOnClickListener(itemClickListener);

        AyUtils.setOddEvenBackgroundColor(position, holder.itemView);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag(R.id.position_tag) instanceof Integer &&
                    v.getTag(R.id.position_tag) != null){
                int clickedPos = (int) v.getTag(R.id.position_tag);
                if (isAboutUser){
                    AdinlayApp.setUserTopicPos(clickedPos);
                    AdinlayApp.setLoginStage(SignInManager.UPDATE_YOU_STAGE);
                } else {
                    AdinlayApp.setFollowersTopicPos(clickedPos);
                    AdinlayApp.setLoginStage(SignInManager.UPDATE_FOLLOWERS_STAGE);
                }

                if (wr_itemClick.get() != null){
                    // using the itemClick to call startActivityForResult(), since v.getContext
                    // sometimes returns AppCompatActivity instead of Activity
                    wr_itemClick.get().onItemClicked(R.string.preferences_allcaps);
                    return;
                }
                if (v.getContext() != null){
//                    Timber.i("context ok");
                    Intent intent = new Intent(v.getContext(), Login2FlowActivity.class);
                    intent.putExtra(AyUtils.IS_SINGLE_UPDATE, AyUtils.IS_SINGLE_UPDATE);
                    v.getContext().startActivity(intent);
                }

            }
        }
    };

    public static class TopicListItemViewHolder extends RecyclerView.ViewHolder {

        TextView tv;
        ImageView button;

        public TopicListItemViewHolder(View itemView) {
            super(itemView);
            tv = itemView.findViewById(R.id.item_topic_name_tv);
            button = itemView.findViewById(R.id.item_topic_button);
        }
    }
}
