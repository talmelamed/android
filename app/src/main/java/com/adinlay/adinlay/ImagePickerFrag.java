package com.adinlay.adinlay;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader;

import java.util.List;

import timber.log.Timber;

/**
 * Created by Noya on 4/8/2018.
 */

public class ImagePickerFrag extends android.support.v4.app.Fragment implements LoaderManager.LoaderCallbacks<List<GalleryImageObject>> {

    private static final String TAG  = ImagePickerFrag.class.getSimpleName();

    private RecyclerView recyclerView = null;
    GalleryRecyclerAdapter adapter = null;
    RecyclerViewPreloader<GalleryImageObject> preloader = null;
//    private AsyncGalleryAdapter adapter = null;
    private LinearLayoutManager layoutManager = null;

    private static final int SPAN_COUNT = 3;
    int firstVisible = -1;
    int firstVisibleOffset = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_advertisers, container, false);

        recyclerView = root.findViewById(R.id.recycler);

        layoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        return root;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLoaderManager().initLoader(R.id.image_loader_id, null, this);
    }


    @NonNull
    @Override
    public Loader<List<GalleryImageObject>> onCreateLoader(int id, @Nullable Bundle args) {
        return new GalleryDataLoader(getActivity());
    }

    @Override
    public void onLoadFinished(@NonNull Loader<List<GalleryImageObject>> loader, List<GalleryImageObject> data) {
        GlideRequests gr = GlideApp.with(this);
        if (data != null && (data.size() == 0 ||
                data.get(0) != null && data.get(0).getId() != GalleryImageObject.CAM_ID)){
            data.add(0, new GalleryImageObject(GalleryImageObject.CAM_ID, ""));
        }

        // remove previous pre-loaders:
        if (adapter != null && preloader != null){
            recyclerView.removeOnScrollListener(preloader);
        }

        adapter = new GalleryRecyclerAdapter(data, SPAN_COUNT, gr);
        preloader = new RecyclerViewPreloader<GalleryImageObject>(gr, adapter, adapter, 20);
        recyclerView.addOnScrollListener(preloader);
        recyclerView.setAdapter(adapter);

        // move the
        if (firstVisible >= 0){
            final int f_pos = firstVisible;
//            Timber.i("SCRL_ onLoadFinished 1st visible: %s", firstVisible);
            layoutManager.scrollToPositionWithOffset(f_pos, firstVisibleOffset);
            firstVisible = -1;
            firstVisibleOffset = 0;
        }

    }

    @Override
    public void onLoaderReset(@NonNull Loader<List<GalleryImageObject>> loader) {
    }

    @Override
    public void onPause() {
        firstVisible = layoutManager.findFirstVisibleItemPosition();
        View v = layoutManager.getChildAt(0);
        firstVisibleOffset = (v == null) ? 0 : (v.getTop() - layoutManager.getPaddingTop());
//        Timber.i("SCRL_ onPause 1st visible: %s offset: %s", firstVisible, firstVisibleOffset);
        super.onPause();
    }

}


