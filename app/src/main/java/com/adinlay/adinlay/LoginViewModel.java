package com.adinlay.adinlay;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import timber.log.Timber;

/**
 * Created by Noya on 5/24/2018.
 */

public class LoginViewModel extends ViewModel {

    private static final String TAG = LoginViewModel.class.getSimpleName();
    public static final String INST_LOGOUT_REQ_OK = "instLogoutRequestOk";
    public static final String FCBK_LOGOUT_REQ_OK = "fcbkLogoutRequestOk";
    public static final String UPDATE_REQ_OK = "updateRequestOk";
    public static final String UPDATE_EMAIL_REQ_OK = "updateEmailRequestOk";
    public static final String SIGN_UP_REQ_OK = "signUpRequestOk";
    public static final String INST_LOGOUT_REQ_PREFIX = "instLogoutRequestPrefix";
    public static final String FCBK_LOGOUT_REQ_PREFIX = "fcbkLogoutRequestPrefix";
    public static final String FB_LOGIN_FAIL_PREFIX = "facebookLoginFailPrefix";
    public static final String DELETE_USER_DONE = "deleteUserDone";

    private MutableLiveData<LoginFlowData> loginFlowLiveData;
    private MutableLiveData<String> serverRequestLiveData;
    private MutableLiveData<Boolean> spinnerLiveData; // if null or false - hide, if tru - show
    private MutableLiveData<Boolean> isMainEnabledLiveData; // if null or true - enables, if false - disable

    private boolean isSingleUpdate = false;
    private boolean sentEmailUpdate = false;
    private long flowStart = -1;
    private String socialMediaId = null;

    public void updateFlowData(int mainButtonRes, String mainButtonText, boolean showBack, boolean showLogo){
        updateFlowData(mainButtonRes, mainButtonText, showBack, showLogo, false);
    }

    public void updateFlowData(int mainButtonRes, String mainButtonText, boolean showBack, boolean showLogo, boolean showTermsLink){

        // or check https://stackoverflow.com/questions/48020377/livedata-update-on-object-field-change
        // can also just have the live data as stage (String I think), and rest of the elements just be reg data we keep here with simple get?!?
        LoginFlowData data = new LoginFlowData(mainButtonRes, mainButtonText, showBack, showLogo, showTermsLink);

        getLoginFlowLiveData().postValue(data);
    }

    public MutableLiveData<LoginFlowData> getLoginFlowLiveData() {
        if (loginFlowLiveData == null) {
            loginFlowLiveData = new MutableLiveData<>();
        }
        return loginFlowLiveData;
    }

    public MutableLiveData<String> getServerRequestLiveData(){
        if (serverRequestLiveData == null){
            serverRequestLiveData = new MutableLiveData<>();
        }
        return serverRequestLiveData;
    }

    public MutableLiveData<Boolean> getSpinnerLiveData(){
        if (spinnerLiveData == null){
            spinnerLiveData = new MutableLiveData<>();
        }
        return spinnerLiveData;
    }

    public MutableLiveData<Boolean> getIsMainEnabledLiveData(){
        if (isMainEnabledLiveData == null){
            isMainEnabledLiveData = new MutableLiveData<>();
        }
        return isMainEnabledLiveData;
    }

    public void logoutFromInstagram(){
        getSpinnerLiveData().setValue(true);
        ServerManager.logoutInstagram(new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                onServerResponse(INST_LOGOUT_REQ_OK);
            }

            @Override
            public void onError(String message) {
                String ret = message == null? INST_LOGOUT_REQ_PREFIX : INST_LOGOUT_REQ_PREFIX + message;
                onServerResponse(ret);
            }
        });
    }

    public void logoutFromFcbk(){
        getSpinnerLiveData().setValue(true);
        ServerManager.disconnectFromFcbk(new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                onServerResponse(FCBK_LOGOUT_REQ_OK);
            }

            @Override
            public void onError(String message) {
                String ret = message == null? FCBK_LOGOUT_REQ_PREFIX : FCBK_LOGOUT_REQ_PREFIX + message;
                onServerResponse(ret);
            }
        });
    }


    public void connectToFcbk(String fcbkToken){
        getSpinnerLiveData().setValue(true);
        ServerManager.connectToFcbk(fcbkToken, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                onServerResponse(INST_LOGOUT_REQ_OK);
                getSpinnerLiveData().postValue(false);
            }

            @Override
            public void onError(String message) {
                String str = message == null ? FB_LOGIN_FAIL_PREFIX : FB_LOGIN_FAIL_PREFIX + message;
                onServerResponse(str);
                getSpinnerLiveData().postValue(false);
            }
        });
    }

    public void onServerResponse(String message){
        // we should be on another thread, so posting (not setting) the values:
        getSpinnerLiveData().postValue(false);
        getServerRequestLiveData().postValue(message);
    }

    public void sendUserUpdateToServer(){
        getSpinnerLiveData().setValue(true);
        ServerManager.Callback callback = new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                onServerResponse(UPDATE_REQ_OK);
            }

            @Override
            public void onError(String message) {
                onServerResponse(message);
            }
        };

        if (socialMediaId == null || socialMediaId.isEmpty()) {
            ServerManager.updateUserInfo(callback, false);
        } else {
            ServerManager.updateSocialMediaProfile(callback);
        }
    }

    public void sendSignUpRequest(String email, String password){
        getSpinnerLiveData().setValue(true);
        SignInManager.createNewUser(email, password, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                onServerResponse(SIGN_UP_REQ_OK);
            }

            @Override
            public void onError(String message) {
                onServerResponse(message);
            }
        });
    }

    public void sendAccountUpdate(){
        getSpinnerLiveData().setValue(true);
//        boolean test = true;
//        if (test){
//            onServerResponse(UPDATE_EMAIL_REQ_OK);
//            return;
//        }
        ServerManager.updateUserInfo(new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                User user = AdinlayApp.getUser();
                // if we are in the confirm stage (not single update, or if user changed the email but it was never verified:
                if ((isSentEmailUpdate() || !isSingleUpdate) && user != null && !user.isEmailVerified()){

                    // need to send a verification email:
                    FirebaseUser firebaseUser = SignInManager.getFirebaseUser();
                    if (firebaseUser != null){
                        firebaseUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Timber.i("verify_ task is successful");
                                    onServerResponse(UPDATE_EMAIL_REQ_OK);
                                } else {
                                    Timber.w(task.getException(), "sendEmailVerification task not successful. Canceled? %s", task.isCanceled());
                                    onServerResponse(UPDATE_REQ_OK);
                                }
                            }
                        });
                        return;
                    }
                }
                onServerResponse(UPDATE_REQ_OK);
            }

            @Override
            public void onError(String message) {
                onServerResponse(message);
            }
        }, true);
    }

    public boolean isSingleUpdate() {
        return isSingleUpdate;
    }

    public void setIsSingleUpdate(boolean singleUpdate) {
        isSingleUpdate = singleUpdate;
    }

    public String getSocialMediaId() {
        return socialMediaId;
    }

    public void setSocialMediaId(String socialMediaId) {
        this.socialMediaId = socialMediaId;
    }

    public boolean isSentEmailUpdate() {
        return sentEmailUpdate;
    }

    public void setSentEmailUpdate(boolean sentEmailUpdate) {
        this.sentEmailUpdate = sentEmailUpdate;
    }

    public long getFlowStart() {
        return flowStart;
    }

    public void setFlowStart(long flowStart) {
        this.flowStart = flowStart;
    }

    public void onStartOver(){
        getSpinnerLiveData().postValue(true);
        if (FirebaseAuth.getInstance().getCurrentUser() != null){
            final String fbUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
            FirebaseAuth.getInstance().getCurrentUser().delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    Timber.i("DEL_ after deleting firebase user was ok? %s", task.isSuccessful());
                    String error = task.isSuccessful() ? null : "onStartOver failed to delete Firebase user " + fbUser + " for: " + task.getException();
                    deleteUser(error);
                }
            });
        } else {
            deleteUser("onStartOver Firebase user is null");
        }
    }

    private void deleteUser(final String errorMessage){
        final String userId = AdinlayApp.getUserId();
        ServerManager.deletePartialUser(new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                deleteUserDone(errorMessage);
            }

            @Override
            public void onError(String message) {
                String currError = "onStartOver failed to delete Adinlay user " + userId + " for: " + message;
                String combinedErrorMsg = errorMessage == null? currError : errorMessage + ". Also " + currError;
                deleteUserDone(combinedErrorMsg);
            }
        });
    }

    private void deleteUserDone(String errorMessage){
        onServerResponse(DELETE_USER_DONE);
        if (errorMessage != null && !errorMessage.isEmpty()){
            Timber.w(errorMessage);
        }
    }

}
