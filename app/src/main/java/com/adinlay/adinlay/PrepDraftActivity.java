package com.adinlay.adinlay;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.constraint.motion.MotionLayout;
import android.support.constraint.motion.MotionScene;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.Loader;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adinlay.adinlay.adstickerview.BitmapStickerIcon;
import com.adinlay.adinlay.adstickerview.DrawableSticker;
import com.adinlay.adinlay.adstickerview.Sticker;
import com.adinlay.adinlay.adstickerview.StickerIconEvent;
import com.adinlay.adinlay.adstickerview.StickerView;
import com.adinlay.adinlay.adstickerview.ZoomIconEvent;
import com.adinlay.adinlay.gui.AyDialogFragment;
import com.adinlay.adinlay.gui.TouchImageView;
import com.adinlay.adinlay.objects.Ad;
import com.adinlay.adinlay.objects.DraftPost;
import com.adinlay.adinlay.objects.OverlayInfo;
import com.adinlay.adinlay.objects.PostFilter;
import com.adinlay.adinlay.objects.TabLauncher;
import com.adinlay.adinlay.objects.TlFilters;
import com.adinlay.adinlay.objects.TlLayouts;
import com.adinlay.adinlay.objects.TlPhotos;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import timber.log.Timber;


public class PrepDraftActivity extends BasicActivity implements
        ItemClick, LoaderManager.LoaderCallbacks<List<GalleryImageObject>> {

    private static final String TAG = PrepDraftActivity.class.getSimpleName();
    private static final String INSTRUCTIONS_TAG = "instructions_tag";
    private static final int SPAN_COUNT = 3;

    TabStrip tabStrip;

    private TouchImageView mainIv = null;
    private ImageView mainAdIv = null;
    private ImageView photoHubIv = null;
    private StickerView stickerView = null;
    private ImageView editorExplainIv = null;
    View hideExplainImage = null;

    protected RecyclerView recyclerView = null;
    private ContentLoadingProgressBar spinner = null;
    private View spinnerFrame = null;
    private MotionLayout motionLayout = null;
    RecyclerView.Adapter adapter = null;
    GridLayoutManager layoutManager = null;
    WeakReference<PreviewRecyclerViewAdapter> wr_AdsAdapter = new WeakReference<>(null);
    WeakReference<GalleryRecyclerAdapter> wr_GalleryAdapter = new WeakReference<>(null);
    WeakReference<FiltersRecyclerViewAdapter> wr_FiltersAdapter = new WeakReference<>(null);

    private String flagPosForTab = null;
    private TabLauncher delayedTab = null;

    EditPostViewModel model = null;
    private boolean isfcbk = false;
    private boolean isSticker = false;
    private boolean isProcessingImage = false;
    private String newImageUrl = null; // if not null - load this image when loader is ready

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // home button:
        findViewById(R.id.toolbar_right_iv).setVisibility(View.VISIBLE);
        findViewById(R.id.toolbar_right_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });

        model = ViewModelProviders.of(this).get(EditPostViewModel.class);

        mainIv = findViewById(R.id.ep_main_iv);
        mainAdIv = findViewById(R.id.ep_ad_iv);
        photoHubIv = findViewById(R.id.ep_photos_iv);
        ImageView nextButton = findViewById(R.id.ep_next);
        spinnerFrame = findViewById(R.id.ep_spinner_frame);
        spinner = findViewById(R.id.ep_spinner);
        stickerView = findViewById(R.id.ep_sticker);
        motionLayout = findViewById(R.id.prep_draft_parent);
        recyclerView = findViewById(R.id.ep_recycler);
        editorExplainIv = findViewById(R.id.ep_extra_instructions);
        hideExplainImage = findViewById(R.id.ep_extra_instructions_remove);

        Ad selectedAd = AdinlayApp.getSelectedAd();
        if (selectedAd == null){
            finish();
            return;
        }
        isSticker = selectedAd.isStickerCampaign();

        initTabStrip();
        // cam icon:
        findViewById(R.id.ep_cam_container).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // make sure mainIv is fully visible:
                if (motionLayout.getProgress() > 0f){
                    motionLayout.transitionToStart();
                }

                // open camera
                Intent intent  = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File path = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
                Timber.v("CAM_APP_DBG have the external path? %s", (path.exists()));
                String fileName = "A_IMG_" +
                        new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date()) + ".jpg";
                File photo = new File(path, fileName);
                AyUtils.saveStringPreference(AyUtils.PREF_SAVED_IMAGE_FILE, "file:" + photo.getAbsolutePath());

                Timber.v("CAM_APP_DBG the string: file: %s", photo.getAbsolutePath());
                if (Build.VERSION.SDK_INT > 23){
                    Uri photoUri = FileProvider.getUriForFile(view.getContext(), BuildConfig.APPLICATION_ID + ".provider", photo);
                    Timber.v("CAM_APP_DBG provider uri is: %s", photoUri);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                } else {
                    Timber.v("CAM_APP_DBG not using provider");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                }
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivityForResult(intent, AyUtils.get16BitId(R.id.intent_take_photo));
            }
        });
        findViewById(R.id.ep_toolbar).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        recyclerView.setPadding(0, 0,0,0);
        recyclerView.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(this, SPAN_COUNT);
        recyclerView.setLayoutManager(layoutManager);

        int netH = AyUtils.getScreenHeightPixels() - getResources().getDimensionPixelSize(R.dimen.draft_post_strip_h) - getResources().getDimensionPixelSize(R.dimen.toolbar_h);
        int neededH = Math.min(AyUtils.getScreenWidthPixels(), getResources().getDimensionPixelSize(R.dimen.max_w_centralizer)) *4/3;
        if (AyUtils.getScreenHeightPixels() > 0 && netH < neededH){
            View centralizer = findViewById(R.id.ep_centralizer);
            int newWidth = AyUtils.getScreenHeightPixels() *2/3;
            ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams) centralizer.getLayoutParams();
            lp.width = newWidth;
            motionLayout.requestLayout();
        }

        Intent intent = getIntent();
        final String imagePath = intent.getStringExtra(PhotoHubActivity.SELECTED_IMAGE_PATH);
        if (imagePath != null) {
            model.getImagePathLiveData().setValue(imagePath);
        }
        model.fetchFilters();

        model.getPingFilters().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (EditPostViewModel.FETCH_OK.equals(s)){
                    wr_FiltersAdapter = new WeakReference<>(null);
//                    Timber.i("FiltersAdapter set to null");
                    TabLauncher currTab = tabStrip.getSelectedTab();
                    if (currTab != null && TabLauncher.FILTERS_TAG.equals(currTab.getLauncherTag())){
                        adapter = getFiltersAdapter();
                        if (recyclerView.getAdapter() instanceof FiltersRecyclerViewAdapter) {
                            recyclerView.swapAdapter(adapter, false);
                        } else {
                            recyclerView.setAdapter(adapter);
                        }
                        onAdapterChanged();
                    }
                }
            }
        });

        motionLayout.setTransitionListener(new MotionLayout.TransitionListener() {
            boolean wasMainLocked = false;
            boolean initialized = false;
            @Override
            public void onTransitionStarted(MotionLayout motionLayout, int i, int i1) {
//                Timber.i("TRAN_ onTransitionStarted");
                if (!initialized){
                    initialized = true;
//                    wasMainLocked = mainIv.isDisableScale();
                }
//                mainIv.setDisableScale(true);
                if (isSticker){
                    stickerView.freezeStickersMatrix(true);
                }
            }

            @Override
            public void onTransitionChange(MotionLayout motionLayout, int i, int i1, float v) {
//                if (flagPosForTab != null){
//                    Timber.d("TRAN_ onTransitionChange float is: %s, i: %s, i1: %s", v, i, i1);
//                    Timber.i("onTransitionChange float is: %s, last position: %s, selected: %s", v, layoutManager.findLastVisibleItemPosition(), selectedPosition);
//                }
                if (!initialized){
                    initialized = true;
//                    wasMainLocked = mainIv.isDisableScale();
                }
//                mainIv.setDisableScale(true);
                if (isSticker){
                    stickerView.freezeStickersMatrix(true);
                }
            }

            @Override
            public void onTransitionCompleted(MotionLayout motionLayout, int i) {

//                Timber.i("TRAN_ onTransitionCompleted i is: %s, start? %s", i, (i == R.layout.photo_hub_start));
                if (i == R.layout.activity_prep_draft_1 && delayedTab != null){
                    selectTab(delayedTab, false);
                } else if (i == R.layout.activity_prep_draft_1 && flagPosForTab != null) { // flag - so we use this code only after click, not other scrolls
//                    Timber.i("MOTION scroll onTransitionCompleted selectedPosition is: %s, firstPos: %s, last: %s",
//                            selectedPosition, first, layoutManager.findLastVisibleItemPosition());
                    final String tabTag = flagPosForTab;
                    recyclerView.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            // make sure we still see the selected item in the recyclerView
                            TabLauncher currTab = tabStrip.getSelectedTab();
                            Timber.i("MOT_ tabTag %s, launcher %s", tabTag, currTab);
                            if (currTab != null && tabTag.equals(currTab.getLauncherTag())){
                                int pos = currTab.getPosition();
                                Timber.i("MOT_ pos %s, childCount %s", pos, layoutManager.getItemCount());
                                if (pos >= 0 && pos < layoutManager.getItemCount()) {
//                                    recyclerView.smoothScrollToPosition(pos); // sort of ok, but the scroll just makes sure the item is seen (so it could be at bottom), plus could cause glitch in layout change
//                                    layoutManager.scrollToPositionWithOffset(pos,0);
                                    if (layoutManager.findFirstCompletelyVisibleItemPosition() > pos ||
                                            layoutManager.findLastCompletelyVisibleItemPosition() < pos) {
                                        layoutManager.scrollToPositionWithOffset(pos,0);
                                    }
                                }
                            }

                        }
                    }, 50);

                }
                flagPosForTab = null;
                delayedTab = null;
//                final boolean unlockMain = wasMainLocked;
                initialized = false;
                wasMainLocked = false;
                mainIv.postDelayed(new Runnable() {
                    @Override
                    public void run() {
//                        mainIv.setDisableScale(unlockMain);
                        if (isSticker){
                            stickerView.freezeStickersMatrix(false);
                        }
                    }
                }, 10);
            }

            @Override
            public void onTransitionTrigger(MotionLayout motionLayout, int i, boolean b, float v) {
//                Timber.i("TRAN_ onTransitionTrigger");
            }

            @Override
            public boolean allowsTransition(MotionScene.Transition transition) {
                return true;
            }
        });

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        InstructionsFrag instructionsFrag = new InstructionsFrag();
        instructionsFrag.setItemClickCallback(this);
        ft.replace(R.id.ep_spinner_frame, instructionsFrag, INSTRUCTIONS_TAG);

        ft.commit();

        // initiate adapters - seems unnecessary
//        getAdsAdapter();
//        getGalleryAdapter();
//        getFiltersAdapter();

        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                selectTab(tabStrip.getTabs().get(TabLauncher.LAYOUTS_TAG), true); // need rv to know its height, so moving selectTab to post
            }
        });

        // mainIv:
        mainIv.setMaxZoom(5f);
//        setUpMainImage(imagePath); // in stickers, since image didn't always show, moved to after we set up ad/sticker

        // Ad-or-Sticker ImageViews:
        if (!isSticker) {
            stickerView.setVisibility(View.GONE);
            GlideApp.with(getApplicationContext())
                    .load(selectedAd.getSelectedLayoutUrl(true))
                    .thumbnail(0.2f)
                    .into(mainAdIv);
            setUpMainImage(imagePath);
        } else {
            Drawable zoomD = ContextCompat.getDrawable(this, android.R.drawable.ic_menu_rotate);
            BitmapStickerIcon zoomIcon = new BitmapStickerIcon(zoomD, BitmapStickerIcon.RIGHT_BOTOM);
            zoomIcon.setIconEvent(new ZoomIconEvent());
            Drawable deleteD = ContextCompat.getDrawable(this, android.R.drawable.ic_menu_close_clear_cancel);
            BitmapStickerIcon deleteIcon = new BitmapStickerIcon(deleteD, BitmapStickerIcon.LEFT_TOP);
            deleteIcon.setIconEvent(new StickerIconEvent() {
                @Override
                public void onActionDown(StickerView stickerView, MotionEvent event) {}

                @Override
                public void onActionMove(StickerView stickerView, MotionEvent event) {}

                @Override
                public void onActionUp(StickerView stickerView, MotionEvent event) {
                    stickerView.removeCurrentSticker();
                    if (stickerView.getStickerCount() == 0){
                        mainIv.setDisableScale(false);
                    }
                }
            });
            stickerView.setIcons(Arrays.asList(deleteIcon, zoomIcon));
            stickerView.setConstrained10percent(true);

            mainIv.post(new Runnable() {
                @Override
                public void run() {
                    mainIv.setDisableScale(true);
                    mainIv.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {
                        @Override
                        public boolean onSingleTapConfirmed(MotionEvent e) {
                            if (stickerView.getStickerCount() > 0){
                                Sticker sticker = stickerView.checkForStickerAndLock(e.getX(), e.getY()); // this seems to work, now disable touchImageView while sticker is operational
                                boolean isOnSticker = sticker != null;
                                mainIv.setDisableScale(isOnSticker);
//                    Timber.i("STICK_ main onSingleTapConfirmed we are now locked? %s, isOnSticker? %s", stickerView.isLocked(), isOnSticker);
                                return true;
                            }
                            return false;
                        }

                        @Override
                        public boolean onDoubleTap(MotionEvent e) {
                            // fixme_sticker: currently the doubleTap of this listener doesn't consume the action, tbd desired behavior
                            return false;
                        }

                        @Override
                        public boolean onDoubleTapEvent(MotionEvent e) {
                            return false;
                        }
                    });
                    setUpMainImage(imagePath);
                }
            });

            GlideApp.with(getApplicationContext())
                    .load(selectedAd.getSelectedLayoutUrl(true))
                    .into(stickerTarget);
        }

        if ("facebook".equalsIgnoreCase(selectedAd.getTargetMedia())){
            isfcbk = true;
            if (!isSticker) {
                ConstraintSet startConstraintSet = motionLayout.getConstraintSet(R.layout.activity_prep_draft_1);
                startConstraintSet.setVisibility(R.id.ep_top_crop_shader, ConstraintSet.VISIBLE);
                startConstraintSet.setVisibility(R.id.ep_bottom_crop_shader, ConstraintSet.VISIBLE);
                startConstraintSet.setVisibility(R.id.ep_ad_frame, ConstraintSet.VISIBLE);
                View bottomShader = findViewById(R.id.ep_bottom_crop_shader);
                bottomShader.setBackgroundColor(getResources().getColor(R.color.black_fifty_percent));
                View framer = findViewById(R.id.ep_ad_frame);
                framer.setBackgroundResource(R.drawable.bg_clear_yellow_frame);
                mainIv.post(new Runnable() {
                    @Override
                    public void run() {
                        View shader = findViewById(R.id.ep_bottom_crop_shader);
                        int h = shader.getHeight();
                        if (h > 0){
                            mainIv.setPadding(0, h,0, h);
                            mainIv.setCropToPadding(false);
                            mainIv.measure(View.MeasureSpec.makeMeasureSpec(mainIv.getWidth(), View.MeasureSpec.EXACTLY),
                                    View.MeasureSpec.makeMeasureSpec(mainIv.getHeight(), View.MeasureSpec.EXACTLY));
                            mainIv.resetZoom(); // trigger the fitImageToView() method
                        }
                    }
                });
            }

        }

        if (AyUtils.getIntPref(AyUtils.PREF_EXPLANATION_IMAGE_COUNTER, 0) < 3){
            editorExplainIv.setVisibility(View.VISIBLE);
            editorExplainIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // do nothing, prevent other clicks
                }
            });
            int resId = AdinlayApp.getUser() != null && AdinlayApp.getUser().isCurrencyIls() ? // fixme: should check the locale
                    R.drawable.editor_tabs_explain_heb : R.drawable.editor_tabs_explain_eng;
            GlideApp.with(this).load(resId).into(editorExplainIv);
            hideExplainImage.setVisibility(View.VISIBLE);
            hideExplainImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeEditorExplanationIfNeeded();
                }
            });
        }

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (removeEditorExplanationIfNeeded()){
                    return;
                }
                if (model.getImagePathLiveData().getValue() == null){
                    showSelectPhotoToast();
                    return;
                }
                if (isProcessingImage){
                    return;
                }
                isProcessingImage = true;
                model.getSpinnerLiveData().setValue(true);
                if (isSticker){
                    fetchFullSizeStickerAndRenderPost();
                    return;
                }
                fetchFullSizeAdAndRenderPost();
            }
        });

        model.getSpinnerLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean show) {
//                Timber.i("mot_ live data spinnerFrame spinnerFrame spinnerFrame show? %s", show);
                if (show == null || !show){
                    spinner.hide();
                } else {
                    spinner.show();
                }
            }
        });

        spinnerFrame.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0){
                    return isProcessingImage;
                }
                return false;
            }
        });

        model.getNotifyErrorProcessingImage().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (s != null && !s.isEmpty()){
                    Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
                }
            }
        });

        LoaderManager.getInstance(PrepDraftActivity.this).initLoader(R.id.image_loader_id, null, PrepDraftActivity.this);

    }

    @Override
    int getLayoutResourceId() {
        return R.layout.activity_prep_draft_frame;
    }

    private void initTabStrip(){
        TabLauncher galleryTl = new TlPhotos(0);
        changeTabAsset(galleryTl, false);
        HashMap<String, TabLauncher> tabs = new HashMap<>();
        tabs.put(TabLauncher.LAYOUTS_TAG, new TlLayouts(0));
        tabs.put(TabLauncher.PHOTOS_TAG, galleryTl);
        tabs.put(TabLauncher.FILTERS_TAG, new TlFilters(0));
        tabStrip = new TabStrip(tabs, TabLauncher.LAYOUTS_TAG);

        View layoutsTab = findViewById(R.id.ep_layouts_container);
        View photosTab = findViewById(R.id.ep_photos_container);
        View filtersTab = findViewById(R.id.ep_filters_container);

        layoutsTab.setTag(R.id.tab_launcher_tag, tabStrip.getTabs().get(TabLauncher.LAYOUTS_TAG));
        photosTab.setTag(R.id.tab_launcher_tag, tabStrip.getTabs().get(TabLauncher.PHOTOS_TAG));
        filtersTab.setTag(R.id.tab_launcher_tag, tabStrip.getTabs().get(TabLauncher.FILTERS_TAG));

        layoutsTab.setOnClickListener(tabsLauncherListener);
        photosTab.setOnClickListener(tabsLauncherListener);
        filtersTab.setOnClickListener(tabsLauncherListener);
    }

    View.OnClickListener tabsLauncherListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getTag(R.id.tab_launcher_tag) instanceof TabLauncher){
                // remember position
                TabLauncher currLauncher = tabStrip.getSelectedTab();
                if (currLauncher != null && layoutManager != null && adapter.getItemCount() > 0){
                    currLauncher.setPosition(layoutManager.findFirstCompletelyVisibleItemPosition());
                }
                // change tab:
                final TabLauncher tl = (TabLauncher) view.getTag(R.id.tab_launcher_tag);
                if (motionLayout.getProgress() > 0f) { // need to scroll down, fully show mainIV
                    motionLayout.post(new Runnable() {
                        @Override
                        public void run() {
                            flagPosForTab = null;
                            delayedTab = tl;
                            motionLayout.transitionToStart();
                        }
                    });
                } else {
//                    final boolean wasMainLocked = mainIv.isDisableScale(); // for some reason - mainIv and sticker sometimes lose position
//                    mainIv.setDisableScale(true);
                    if (isSticker){ // for some reason - sticker sometimes loses position
                        stickerView.freezeStickersMatrix(true);
                    }
                    selectTab(tl, false);
                    delayedTab = null;
                    mainIv.postDelayed(new Runnable() {
                        @Override
                        public void run() {
//                            mainIv.setDisableScale(wasMainLocked);
                            if (isSticker){
                                stickerView.freezeStickersMatrix(false);
                            }
                        }
                    }, 10);
                }
            }
        }
    };

    private void selectTab(@NonNull TabLauncher newTab, boolean forceSelect){
        TabLauncher oldTab = tabStrip.getSelectedTab();
         if (!forceSelect && removeEditorExplanationIfNeeded()){
             // continue, we still want to change tab
         }
        if (newTab.equals(oldTab)&& !forceSelect){
//            Timber.i("FLOW_ current tab %s", tabStrip.getSelectedTab().getLauncherTag());
            return;
        }

        // special case, should make generic
        if (model.getImagePathLiveData().getValue() == null && TabLauncher.FILTERS_TAG.equals(newTab.getLauncherTag())){
            showSelectPhotoToast();
            return;
        }

        tabStrip.setSelectedTab(newTab);

        // show which button is selected:
        if (!newTab.equals(oldTab)){
            changeTabAsset(oldTab, false);
        }
        changeTabAsset(newTab, true);

        switch (newTab.getLauncherTag()){
            case TabLauncher.LAYOUTS_TAG:
                if (AdinlayApp.getSelectedAd() != null) {
                    adapter = getAdsAdapter();
                }
                break;
            case TabLauncher.PHOTOS_TAG:
                adapter = getGalleryAdapter();
                break;
            case TabLauncher.FILTERS_TAG:
                adapter = getFiltersAdapter();
                break;
        }
        recyclerView.setAdapter(adapter);
        onAdapterChanged();
        int pos = newTab.getPosition();
        if (pos > 0 && adapter != null && adapter.getItemCount() > pos){
            layoutManager.scrollToPositionWithOffset(pos, 0);
        }
    }

    @NonNull
    private PreviewRecyclerViewAdapter getAdsAdapter(){
        PreviewRecyclerViewAdapter adsAdapter = wr_AdsAdapter.get();
        if (adsAdapter == null){
//            Timber.i("getAdsAdapter was null");
            adsAdapter = new PreviewRecyclerViewAdapter(model.getImagePathLiveData().getValue(),
                    AdinlayApp.getSelectedAd(), SPAN_COUNT, GlideApp.with(this), this);
            wr_AdsAdapter = new WeakReference<>(adsAdapter);
        }
        return adsAdapter;
    }

    @NonNull
    private GalleryRecyclerAdapter getGalleryAdapter(){
        GalleryRecyclerAdapter galleryAdapter = wr_GalleryAdapter.get();
        if (galleryAdapter == null){
//            Timber.i("getGalleryAdapter was null");
            galleryAdapter = new GalleryRecyclerAdapter(model.getPhotosList(), SPAN_COUNT,
                    GlideApp.with(this), -1, this);
            wr_GalleryAdapter = new WeakReference<>(galleryAdapter);
        }
        return galleryAdapter;
    }

    @NonNull
    private FiltersRecyclerViewAdapter getFiltersAdapter(){
        FiltersRecyclerViewAdapter filtersAdapter = wr_FiltersAdapter.get();
        if (filtersAdapter == null){
//            Timber.i("getFiltersAdapter was null");
            filtersAdapter = new FiltersRecyclerViewAdapter(model.getImagePathLiveData().getValue(), model.getFiltersList(),
                    AdinlayApp.getSelectedAd(), SPAN_COUNT, GlideApp.with(this), this);
            wr_FiltersAdapter = new WeakReference<>(filtersAdapter);
        }
        return filtersAdapter;
    }

    private void updatePathInFiltersAdapter(@NonNull String path){
        FiltersRecyclerViewAdapter filtersAdapter = wr_FiltersAdapter.get();
        if (filtersAdapter != null){
            filtersAdapter.updateImagePath(path);
        }
        PreviewRecyclerViewAdapter adsAdapter = wr_AdsAdapter.get();
        if (adsAdapter != null){
            adsAdapter.updateImagePath(path);
        }
    }

    private void showSelectPhotoToast(){
        Toast.makeText(getApplicationContext(), getResources().getString(R.string.must_select_image), Toast.LENGTH_SHORT).show();
    }

    private void changeTabAsset(@NonNull TabLauncher tl, boolean isSelected){
        ImageView iv = findViewById(tl.getIvId());
        if (iv != null){
            int res = isSelected? tl.getSelectedSrcId() : tl.getSrcId();
            if (tl.isChangeBackground()) {
                iv.setBackgroundResource(res);
                int filterColorInt = isSelected ? Color.argb(0, 255, 255, 255) : Color.argb(130, 0, 0, 0);
                iv.setColorFilter(filterColorInt);
            } else {
                iv.setImageResource(res);
            }
        }
        TextView tv = findViewById(tl.getTvId());
        if (tv != null){
            int color = isSelected ? R.color.colorAdinlayYellow : R.color.color_tab_unselected;
            tv.setTextColor(getResources().getColor(color));
        }
    }

    private void onAdapterChanged(){
        if (adapter == null || adapter.getItemCount() <= 2*SPAN_COUNT){
            motionLayout.setTransition(R.layout.activity_prep_draft_1, R.layout.activity_prep_draft_1);
        } else {
            motionLayout.setTransition(R.layout.activity_prep_draft_1, R.layout.activity_prep_draft_2);
        }
    }

    private void setUpMainImage(String path){
        if (path == null){
            return;
        }
        Bitmap bitmap = AyUtils.getSizedBitmap(path, -1, model.getScaleFactorSaver());
//        Timber.v("model.getScaleFactorSaver : %s", model.getScaleFactorSaver()[0]);
        if (bitmap != null) {
            String oldPath = model.getImagePathLiveData().getValue();
            model.getImagePathLiveData().setValue(path);
            model.setBaseImageBitmap(bitmap.copy(Bitmap.Config.ARGB_8888, true));
            mainIv.resetZoom();
            mainIv.setImageBitmap(bitmap);
            updatePathInFiltersAdapter(path);

            int size = getResources().getDimensionPixelSize(R.dimen.prep_ic);
            GlideRequest<Drawable> request = GlideApp.with(this)
                    .load(path)
                    .override(size, size)
                    .centerCrop();
            if (oldPath != null){
                request = request.thumbnail(GlideApp.with(this).load(oldPath).override(size, size));
            }
            request.into(photoHubIv);
        }
    }

    private boolean removeEditorExplanationIfNeeded(){
        if (editorExplainIv.getVisibility() == View.VISIBLE){
//            Timber.i("inst_ removeEditorExplanationIfNeeded iv to be gone");
            editorExplainIv.setVisibility(View.GONE);
            editorExplainIv.setImageResource(0);
            editorExplainIv.setOnClickListener(null);
            int val = AyUtils.getIntPref(AyUtils.PREF_EXPLANATION_IMAGE_COUNTER, 0) + 1;
            AyUtils.saveIntPref(AyUtils.PREF_EXPLANATION_IMAGE_COUNTER, val);
            hideExplainImage.setOnClickListener(null);
            hideExplainImage.setVisibility(View.GONE);
            return true;
        }
        return false;
    }

    private SimpleTarget<Drawable> stickerTarget = new SimpleTarget<Drawable>() {
        @Override
        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
            if (stickerView.getStickerCount() > 0){
                stickerView.removeAllStickers();
            }
            Sticker sticker = new DrawableSticker(resource);
            stickerView.addSticker(sticker);
            stickerView.setLocked(false);
            mainIv.setDisableScale(true);
        }
    };

    private void fetchFullSizeAdAndRenderPost(){
        SimpleTarget<Bitmap> adTarget = new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                Timber.i("TARGET_ onResourceReady, h: %s w: %s", resource.getHeight(), resource.getWidth());
                if (mainIv.getDrawable() instanceof BitmapDrawable){
                    shareFullSizeAdAsync(resource);

                } else { // shouldn't happen!
                    model.getSpinnerLiveData().postValue(false);
                    isProcessingImage = false;
                    Timber.w("TARGET_ mainIv doesn't have bitmapDrawable?!?");
                }
            }
        };
        Ad selectedAd = AdinlayApp.getSelectedAd();
        String url = selectedAd != null ? selectedAd.getSelectedLayoutUrl(true) : "";
//        Timber.i("TARGET_ start");
        GlideApp.with(PrepDraftActivity.this).asBitmap().load(url).into(adTarget);
    }

    private void shareFullSizeAdAsync(@NonNull final Bitmap resource){
        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean success = false;
                if (mainIv.getDrawable() instanceof BitmapDrawable){

                    // DONE Eliyahu: switch to sampleSize, which is saved in model.getScaleFactorSaver()[0]
                    double scaleWidth = model.getScaleFactorSaver()[0];
                    double scaleHeight = model.getScaleFactorSaver()[0];

                    /* // ELIYAHU: deprecated manual estimation of scale factor from image's dimensions.
                    Bitmap bmp = ((BitmapDrawable) mainIv.getDrawable()).getBitmap().copy(Bitmap.Config.ARGB_8888, true);
                    //Timber.i("OOM_DBG bmp byte count: %s", bmp.getByteCount()); // or check allocated byte count, if Build.VERSION.SDK_INT >= 19
                    //Timber.i("BMP_DBG photo_iv zoomed rect pxl: %s", mainIv.getZoomedRectPxl()); // may need for openCV

                    int bmpWidth = bmp.getWidth();
                    int bmpHeight = bmp.getHeight();

                    Rect imSizeOrig = AyUtils.getOriginalImageSize(model.getImagePath());

                    double scaleWidth = Math.round((double) imSizeOrig.right / (double) Math.max(bmpWidth, bmpHeight));
                    double scaleHeight = Math.round((double) imSizeOrig.bottom / (double) Math.min(bmpWidth, bmpHeight));

                    if (scaleWidth!=scaleHeight) // in case of +/-90 rotations - reverse
                    {
                        scaleWidth = Math.round((double) imSizeOrig.right / (double) Math.min(bmpWidth, bmpHeight));
                        scaleHeight = Math.round((double) imSizeOrig.bottom / (double) Math.max(bmpWidth, bmpHeight));
                    }
                                        bmp.recycle();
                    */

                    RectF zoomedRectPxl = mainIv.getZoomedRectPxl();
                    // rect should be (X,Y,X+W,Y+H) for BitmapRegionDecoder ( 4 corners coordinates!!! )
                    Rect cropRectOrig = new Rect((int) (zoomedRectPxl.left * scaleWidth),
                            (int) (zoomedRectPxl.top * scaleHeight),
                            (((int) (zoomedRectPxl.width() * scaleWidth))+((int)(zoomedRectPxl.left * scaleWidth))),
                            (((int) (zoomedRectPxl.height() * scaleHeight))+ ((int)(zoomedRectPxl.top * scaleHeight))) );

                    Bitmap croppedOrig =  AyUtils.getBitmapRegion(model.getImagePathLiveData().getValue(), cropRectOrig, PrepDraftActivity.this);
                    if (croppedOrig == null){
                        model.getNotifyErrorProcessingImage().postValue(getString(R.string.something_wrong_processing_image));
                        model.getSpinnerLiveData().postValue(false);
                        isProcessingImage = false;
                        Timber.d("shareFullSizeAdAsync getBitmapRegion returned null");
                        return;
                    }

                    // draw the background to the overlay (result) bitmap
                    Bitmap adBitmap = resource;
                    if (isfcbk){

                        adBitmap = Bitmap.createBitmap(resource, 0,
                                (int) (0.5f * (resource.getHeight() - (630f/1200f * resource.getHeight()))),
                                resource.getWidth(),
                                (int) (630f/1200f * resource.getHeight()));
//                        Timber.i( "fb_dbg ck size: %s : %s = %s", adBitmap.getWidth(), adBitmap.getHeight(), (1f * adBitmap.getWidth() / adBitmap.getHeight()));
                    }

                    int scale = 1;
                    while (!success && scale <= 4){
                        try {

                            Bitmap overlay = Bitmap.createBitmap(adBitmap.getWidth() / scale,  adBitmap.getHeight() / scale, Bitmap.Config.ARGB_8888);

                            Canvas canvas  = new Canvas(overlay);
//                            canvas.drawBitmap(cropped, null, new Rect(0,0, overlay.getWidth(), overlay.getHeight()), null);
                            canvas.drawBitmap(croppedOrig, null, new Rect(0,0, overlay.getWidth(), overlay.getHeight()), null);
                            canvas.drawBitmap(adBitmap, null, new Rect(0,0, overlay.getWidth(), overlay.getHeight()), null);

                            DraftPost selected = AdinlayApp.getDraftPost();
                            if (selected != null){
                                selected.setDraftPostBitmap(overlay);
                                Intent shareIntent = new Intent(PrepDraftActivity.this, SharePostActivity.class);
                                startActivityForResult(shareIntent, AyUtils.get16BitId(R.id.intent_share_new_post));
                            }
                            success = true;
                        } catch (OutOfMemoryError e){
                            scale *= 2;
                            Timber.d("shareFullSizeAdAsync will now try to scale down by: %s", scale);
                        }
                    }

                    croppedOrig.recycle();
                }
                model.getSpinnerLiveData().postValue(false);
                if (!success){
                    model.getNotifyErrorProcessingImage().postValue(getString(R.string.something_wrong_processing_image));
                    isProcessingImage = false;
                }
                Timber.i("TARGET_ done");
            }
        }).start();
    }

    private void fetchFullSizeStickerAndRenderPost(){
        // if no sticker:
        if (stickerView.getStickerCount() == 0){
            shareFullSizeStickerAsync(null);
            return;
        }
        SimpleTarget<Bitmap> adTarget = new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//                Timber.i("TARGET_ onResourceReady, h: %s w: %s", resource.getHeight(), resource.getWidth());
                if (mainIv.getDrawable() instanceof BitmapDrawable){
                    shareFullSizeStickerAsync(resource);

                } else { // shouldn't happen!
                    model.getSpinnerLiveData().postValue(false);
                    isProcessingImage = false;
                    Timber.w("TARGET_ mainIv doesn't have bitmapDrawable?!?");
                }
            }
        };
        Ad selectedAd = AdinlayApp.getSelectedAd();
        String url = selectedAd != null ? selectedAd.getSelectedLayoutUrl(true) : "";
//        Timber.i("TARGET_ start");
        GlideApp.with(PrepDraftActivity.this).asBitmap().load(url).into(adTarget);
    }


    private void shareFullSizeStickerAsync(final Bitmap resource){
        new Thread(new Runnable() {
            @Override
            public void run() {
                boolean success = false;
                if (mainIv.getDrawable() instanceof BitmapDrawable){

                    // get the background image scale (sampleSize), which was  saved in model.getScaleFactorSaver()[0]
                    double scaleWidth = model.getScaleFactorSaver()[0];
                    double scaleHeight = model.getScaleFactorSaver()[0];


                    RectF zoomedRectPxl = mainIv.getZoomedRectPxl();
                    // rect should be (X,Y,X+W,Y+H) for BitmapRegionDecoder ( 4 corners coordinates!!! )
                    Rect cropRectOrig = new Rect((int) (zoomedRectPxl.left * scaleWidth),
                            (int) (zoomedRectPxl.top * scaleHeight),
                            (((int) (zoomedRectPxl.width() * scaleWidth))+((int)(zoomedRectPxl.left * scaleWidth))),
                            (((int) (zoomedRectPxl.height() * scaleHeight))+ ((int)(zoomedRectPxl.top * scaleHeight))) );

                    Bitmap croppedOrig =  AyUtils.getBitmapRegion(model.getImagePathLiveData().getValue(), cropRectOrig, PrepDraftActivity.this);
                    if (croppedOrig == null){
                        model.getNotifyErrorProcessingImage().postValue(getString(R.string.something_wrong_processing_image));
                        model.getSpinnerLiveData().postValue(false);
                        isProcessingImage = false;
                        Timber.d("shareFullSizeStickerAsync getBitmapRegion returned null");
                        return;
                    }


                    // draw the background to the overlay (result) bitmap
                    int resultW =  resource != null? resource.getWidth() : 1024;
                    int resultH =  resource != null? resource.getHeight() : 1024;
                    float difY = 0f;

//                    Timber.i( "fb_dbg stick_ ck size: %s : %s, difY: %s", resultW, resultH, difY);

                    int scale = 1;
                    while (!success && scale <= 4){
                        try {

                            Bitmap overlay = Bitmap.createBitmap(resultW / scale,  resultH / scale, Bitmap.Config.ARGB_8888);

                            Canvas canvas  = new Canvas(overlay);

                            canvas.drawBitmap(croppedOrig, null, new Rect(0,0, overlay.getWidth(), overlay.getHeight()), null);

                            Matrix matrix  = new Matrix();
                            Sticker sticker = stickerView.getAdSticker();
                            if (sticker != null){
                                float[] mappedF = sticker.getMappedBoundPoints();
                                int viewWidth = stickerView.getWidth() > 0 ? stickerView.getWidth() : 1;
                                float scaleW = 1.0f / scale * resultW / viewWidth;

                                // step 1: first we scale
                                matrix.postScale(sticker.getCurrentScale(), sticker.getCurrentScale());
                                matrix.postScale(scaleW, scaleW);
//                                Timber.i( "BMP_DBG postScale matrix is     %s", matrix);

                                // step 2: then rotate
                                matrix.postRotate(sticker.getCurrentAngle());
//                                Timber.i( "BMP_DBG postRotate matrix is    %s", matrix);

                                // step 3: and only now we translate
                                matrix.postTranslate(mappedF[0] *scaleW, mappedF[1] *scaleW  - difY/scale);
//                                Timber.i( "BMP_DBG postTranslate matrix is %s", matrix.toString());

                            }

                            if (resource != null) {
                                canvas.drawBitmap(resource, matrix, null); // we use  adBitmap to crop and get the matrix, but draw with resource
                            }

                            DraftPost selected = AdinlayApp.getDraftPost();
                            if (selected != null){
                                selected.setDraftPostBitmap(overlay);
                                Intent shareIntent = new Intent(PrepDraftActivity.this, SharePostActivity.class);
                                startActivityForResult(shareIntent, AyUtils.get16BitId(R.id.intent_share_new_post));
                            }
                            success = true;
                        } catch (OutOfMemoryError e){
                            scale *= 2;
                            Timber.d("shareFullSizeStickerAsync will now try to scale down by: %s", scale);
                        }
                    }

                    croppedOrig.recycle();
                }
                model.getSpinnerLiveData().postValue(false);
                if (!success){
                    model.getNotifyErrorProcessingImage().postValue(getString(R.string.something_wrong_processing_image));
                    isProcessingImage = false;
                }
                Timber.i("TARGET_ done");
            }
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isProcessingImage = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (AdinlayApp.getUser() == null || AdinlayApp.getDraftPost() == null){
            // lost state - let's let mainActivity deal with it
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case R.id.intent_share_new_post & 0x0000FFFF:
                if (resultCode == RESULT_OK){
//                    Timber.v("share_dbg");
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
                break;
            case R.id.intent_take_photo & 0x0000FFFF:
                if (resultCode == RESULT_OK){
                    String dataInfo = "is null";
                    if (data != null){
                        dataInfo = "" + data;
                        if (data.getExtras() != null){
                            dataInfo += " extras: " + AyUtils.stringifyBundle(data.getExtras());
                        }
                    }

                    // scan the new file, so that contentProvider will know about it:
                    String savedImagePath = AyUtils.getStringPref(AyUtils.PREF_SAVED_IMAGE_FILE);
                    Timber.v("CAM_DBG onActivityResult got ok, data: %s prefPath: %s", dataInfo, savedImagePath);
                    if (savedImagePath != null) {
                        Uri photoUri = Uri.parse(savedImagePath);
                        MediaScannerConnection.scanFile(this, new String[]{photoUri.getPath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                            @Override
                            public void onScanCompleted(String path, Uri uri) {
                                newImageUrl = path;
                                Timber.d("CAM_DBG finished scan for %s", path);
                            }
                        });
                        // make sure rv is showing photoGallery, and position is 0 (less animation when data is re-loaded, following the scan
                        TabLauncher photosTab = tabStrip.getTabs().get(TabLauncher.PHOTOS_TAG);
                        if (photosTab != null){
                            photosTab.setPosition(0);
                        }
                        findViewById(R.id.ep_photos_container).callOnClick();
                    } else {
                        Timber.d("CAM_DBG onActivityResult path is null");
                    }
                } else{
                    Timber.d("CAM_DBG onActivityResult got bad result: %s", resultCode);
                }
                break;
        }
    }

    @Override
    public void finish() {
        super.finish();
        AdinlayApp.clearDraftBitmap();
        model.recycleBaseImageBitmap();
        Timber.v("clearing bitmap");
    }


    @Override
    public void onItemClicked(Object object) {

        if (object == null){
            FragmentManager fm = getSupportFragmentManager();
            Fragment instructionsFrag = fm.findFragmentByTag(INSTRUCTIONS_TAG);
            if (instructionsFrag != null) {
                FragmentTransaction ft = fm.beginTransaction();
                ft.remove(instructionsFrag);
                ft.commit();
            }
            return;
        }

        if (object instanceof String){
            // something went wrong loading instructions - NOTE: this isn't an actual click, we may have left the activity!!!
            Bundle args = new Bundle();
            args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.error);
            args.putInt(AyDialogFragment.ARG_MESSAGE_RES, R.string.something_wrong_load_instructions);
            args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
            AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
            if (!isFinishing()) {
                AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
            }
            return;
        }

        String launcherTag = null;
        int pos = -1;

        if (object instanceof OverlayInfo && ((OverlayInfo) object).getUrl() != null){
            removeEditorExplanationIfNeeded();
            String urlString = ((OverlayInfo) object).getUrl();
            Timber.d( "Switch ad layout: %s", urlString);

            GlideRequest<Drawable> request = GlideApp.with(this)
                    .load(urlString);
            Ad ad = AdinlayApp.getSelectedAd();
            String thumbUrl = ad == null ? "" : ad.getSelectedLayoutUrl(false);
            if (!isSticker && !thumbUrl.isEmpty() && !urlString.equalsIgnoreCase(thumbUrl)){
                request = request.thumbnail(GlideApp.with(this).load(thumbUrl));
            } else if (!isSticker){
                request = request.thumbnail(0.2f);
            }
            if (!isSticker) {
                request.into(mainAdIv);
            } else {
                request.into(stickerTarget);
            }

            launcherTag = TabLauncher.LAYOUTS_TAG;
            pos = ((OverlayInfo) object).getAdapterPosition();
        }

        if (object instanceof PostFilter){
            removeEditorExplanationIfNeeded();
            Timber.v("click on filter: %s", ((PostFilter) object).getName());
            GPUImageFilter filter = ((PostFilter) object).getGpuImageFilter(this);
            Bitmap bmp = AyUtils.applyFilterToBitmap(
                    model.getBaseImageBitmap().copy(Bitmap.Config.ARGB_8888, true),
                    filter, this);
            DraftPost draft = AdinlayApp.getDraftPost();
            if (draft != null){
                draft.setSelectedFilter((PostFilter) object);
            }
            if (bmp == null){
                AyUtils.loadSizedBitmapToIv(model.getImagePathLiveData().getValue(), mainIv, filter, -1, model.getScaleFactorSaver());
            } else {
                mainIv.setImageBitmap(bmp, true);
            }

            pos = ((PostFilter) object).getAdapterPosition();
            launcherTag = TabLauncher.FILTERS_TAG;
        }

        if (object instanceof GalleryImageObject){ // todo test performance
            removeEditorExplanationIfNeeded();
            GalleryImageObject gmi = (GalleryImageObject) object;
            if (gmi.getFileUrl() != null) {
                setUpMainImage(gmi.getFileUrl());
                DraftPost draft = AdinlayApp.getDraftPost();
                if (draft != null){
                    draft.setSelectedFilter(null);
                }
                pos = gmi.getPosition();
                launcherTag = TabLauncher.PHOTOS_TAG;
            }
        }

        // set the position
        if (launcherTag != null && pos >= 0){
            TabLauncher tabLauncher = tabStrip.getTabs().get(launcherTag);
            if (tabLauncher != null){
                tabLauncher.setPosition(pos);
                onPositionSelected(launcherTag, pos);
            }
        }

    }

    private void onPositionSelected(@NonNull final String launcherTag, final int pos){
        if (pos < 0){
            return;
        }
        if (motionLayout.getProgress() > 0f) {
            layoutManager.scrollToPositionWithOffset(pos,0); // before the transition - move the selected item to the top (still need to handle items from end of list)
            motionLayout.post(new Runnable() {
                @Override
                public void run() {
                    flagPosForTab = launcherTag;
                    motionLayout.transitionToStart();
                }
            });
        } else {
            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    // make sure we still see the relevant item (due to issues after fling)
                    TabLauncher currTab = tabStrip.getSelectedTab();
                    if (currTab != null && launcherTag.equals(currTab.getLauncherTag())){
                        if (layoutManager.findFirstVisibleItemPosition() > pos ||
                                layoutManager.findLastVisibleItemPosition() < pos) {
                            layoutManager.scrollToPositionWithOffset(pos,0);
                        }
                    }
                }
            });
        }
    }

    @NonNull
    @Override
    public Loader<List<GalleryImageObject>> onCreateLoader(int i, @Nullable Bundle bundle) {
        return new GalleryDataLoader(this);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<List<GalleryImageObject>> loader, List<GalleryImageObject> galleryImageObjects) {
        if (galleryImageObjects == null){
            galleryImageObjects = new ArrayList<>();
        }
        if (galleryImageObjects.size() > 2*SPAN_COUNT){
            int toAdd = galleryImageObjects.size() % SPAN_COUNT == 0 ? 3*SPAN_COUNT: 4*SPAN_COUNT - galleryImageObjects.size() % SPAN_COUNT;
            for (int i = 0; i < toAdd; i++){
                galleryImageObjects.add(new GalleryImageObject(true));
            }
        }

        model.setPhotosList(galleryImageObjects);
        if (model.getImagePathLiveData().getValue() == null &&
                galleryImageObjects.size() > 0 && galleryImageObjects.get(0) != null){
            setUpMainImage(galleryImageObjects.get(0).getFileUrl());
        } else if (newImageUrl != null &&  galleryImageObjects.size() > 0 &&
                newImageUrl.equals(galleryImageObjects.get(0).getFileUrl())){
            setUpMainImage(galleryImageObjects.get(0).getFileUrl());
            newImageUrl = null;
            TabLauncher photosTab = tabStrip.getTabs().get(TabLauncher.PHOTOS_TAG);
            if (photosTab != null){
                photosTab.setPosition(0);
            }
        }
        wr_GalleryAdapter = new WeakReference<>(null);
//        Timber.i("GalleryAdapter set to null");
        // check if we need to update the adapter:
        TabLauncher currTab = tabStrip.getSelectedTab();
        if (TabLauncher.PHOTOS_TAG.equals(currTab.getLauncherTag())){
            adapter = getGalleryAdapter();
            if (recyclerView.getAdapter() instanceof GalleryRecyclerAdapter) {
                recyclerView.swapAdapter(adapter, false);
            } else {
                recyclerView.setAdapter(adapter);
            }
            onAdapterChanged();
            int pos = currTab.getPosition();
            if (pos >= 0 && pos < galleryImageObjects.size()){
                layoutManager.scrollToPositionWithOffset(pos, 0);
            }
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<List<GalleryImageObject>> loader) {
    }
}
