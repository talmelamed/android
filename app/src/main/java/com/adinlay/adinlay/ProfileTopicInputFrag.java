package com.adinlay.adinlay;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import org.threeten.bp.Instant;
import org.threeten.bp.LocalDate;
import org.threeten.bp.YearMonth;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZonedDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Noya on 11/15/2018.
 */

public class ProfileTopicInputFrag extends ProfileTopicItemsFrag {

    private static final String TAG = ProfileTopicInputFrag.class.getSimpleName();

    private EditText inputEt = null;
    private TextView inputTv = null;
    private DatePickerDialog dpDialog = null;
    private String input = null;
    private String inputType = "";
    private String title = "";
    int minAge = 18;


    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_et_input, container, false);

        inputEt = root.findViewById(R.id.topic_input_et);
        inputTv = root.findViewById(R.id.topic_input_tv);

        if (!isDateInput()){
            if (input != null){
                inputEt.setText(input);
            }
        } else {
            inputEt.setVisibility(View.GONE);
            inputTv.setVisibility(View.VISIBLE);
            inputTv.requestFocus();

            if (input != null && !input.isEmpty()){
                try {
                    BigDecimal bigDecimal = new BigDecimal(input);
                    Instant inputInstant = Instant.ofEpochMilli(bigDecimal.longValue());
                    setInputTvText(inputInstant);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }

            inputTv.setContentDescription(title);
            inputTv.setHint(R.string.enter_birth_date);
            minAge = AyUtils.getMinAgeForLocation();
            TextView minExplain = root.findViewById(R.id.topic_input_explain);
            minExplain.setText(getResources().getString(R.string.min_age, "" + minAge));
            minExplain.setVisibility(View.VISIBLE);
            inputTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (dpDialog != null && dpDialog.isShowing()){
                        // probably a double-click, dialog already showing
                        return;
                    }

//                    Timber.i("inputTv_ is clicked");
                    // add min, max and existing input
                    ZonedDateTime zdtNow = ZonedDateTime.now();
                    ZonedDateTime zdtOldest = zdtNow.minusYears(100);
                    ZonedDateTime zdtYoungest = zdtNow.minusYears(minAge);
                    ZonedDateTime zdtDefault = zdtYoungest;
                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP){ // bug in datePickerDialog in older builds
                        zdtOldest = zdtOldest.toLocalDate().atStartOfDay(zdtOldest.getZone()); // min - take start of day
                        zdtYoungest = zdtYoungest.toLocalDate().atStartOfDay(zdtYoungest.getZone()).plusDays(1).minusSeconds(1); // max - take end of day
                        zdtDefault = zdtDefault.minusSeconds(1);
                    }
//                    Timber.d("DATE_INPUT have: %s", input);
                    if (input != null){
                        try {
                            BigDecimal bigDecimal = new BigDecimal(input);
                            zdtDefault = ZonedDateTime.ofInstant(Instant.ofEpochMilli(bigDecimal.longValue()), ZoneId.systemDefault());
                            // in case a user chose the min date (oldest age) and now wants to update it, prevent crash in older os:
                            if (zdtDefault.isBefore(zdtOldest)){
                                zdtDefault = zdtOldest.plusSeconds(1);
                            } else if (zdtDefault.isAfter(zdtYoungest)){
                                zdtDefault = zdtYoungest.minusSeconds(1);
                            }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                    }

                    Context context = getActivity() != null ? getActivity() : v.getContext();
                    if (context == null){
                        return;
                    }

                    dpDialog = new DatePickerDialog(context, R.style.CustomDatePickerDialogTheme, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            dpDialog = null;

                            LocalDate ld = YearMonth.of(year, month + 1).atDay(dayOfMonth);
                            long epochDate = ld.atStartOfDay(ZoneId.systemDefault()).toEpochSecond() * 1000;

                            input = "" + epochDate;
                            Instant selectedInstant = Instant.ofEpochMilli(epochDate);
                            setInputTvText(selectedInstant);

                        }
                    }, zdtDefault.getYear(), zdtDefault.getMonth().getValue() -1, zdtDefault.getDayOfMonth());
                    dpDialog.getDatePicker().setMaxDate(zdtYoungest.toEpochSecond()* 1000);
                    dpDialog.getDatePicker().setMinDate(zdtOldest.toEpochSecond()* 1000);
                    dpDialog.show();

                }
            });
        }

        return root;
    }

    private void setInputTvText(Instant inputZdtInstant){
        ZonedDateTime zdt =  ZonedDateTime.ofInstant(inputZdtInstant, ZoneId.systemDefault());
        String pattern = "MMMM dd, yyyy";
        String formatted = DateTimeFormatter.ofPattern(pattern).format(zdt);
        inputTv.setText(formatted);
    }

    public void setTopicInfo(String inputType, String existingInput, String topicTitle){
        this.inputType = inputType;
        input = existingInput;
        title = topicTitle == null ? "" : topicTitle;
    }

    private Date checkLastDayOdMonth(long dateMillis){
        Instant instant = Instant.ofEpochMilli(dateMillis);
        LocalDate ld = instant.atZone(ZoneId.systemDefault()).toLocalDate();
//        Timber.i("PICKER_ millis: %s, ld: %s, endOfMonth: %s", dateMillis, ld, YearMonth.of(ld.getYear(), ld.getMonth()).atEndOfMonth());
        if (ld.getDayOfMonth() > 28) {
            ld = ld.minusDays(ld.getDayOfMonth() - 28);
//            Timber.i("PICKER_ changed ld to : %s", ld);
        }
//        Timber.i("PICKER_ returning date: %s", new Date(ld.atStartOfDay(ZoneId.systemDefault()).toEpochSecond() * 1000));
        return new Date(ld.atStartOfDay(ZoneId.systemDefault()).toEpochSecond() * 1000);
    }


    @NonNull
    public ArrayList<String> getSelection(){
        ArrayList<String> selection = new ArrayList<String>();
        if (isDateInput() && input != null){
            selection.add(input);
        } else if (!isDateInput()){
            input = inputEt.getText().toString();
            selection.add(input);
        }
        return selection;
    }

    @Override
    public boolean consumeBackClick() {
        return clearPicker();
    }

    public boolean clearPicker(){
        boolean ret = false;
        if (dpDialog != null){
            ret = dpDialog.isShowing();
            if (ret){
                dpDialog.dismiss();
            }
            dpDialog = null;
        }
        return ret;
    }

    @Override
    public void onDestroyView() {
        clearPicker();
        super.onDestroyView();
    }

    private boolean isDateInput(){
        return "date".equalsIgnoreCase(inputType);
    }

}
