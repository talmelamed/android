package com.adinlay.adinlay;

import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class TermsActivity extends AppCompatActivity {

    private final static String TAG = TermsActivity.class.getSimpleName();

    WebView webView = null;

    public static final String TERMS_URL = "https://s3.eu-central-1.amazonaws.com/adinlay-resources/T%26C_Ambassadors_files/index.html";
    public static final String PRIVACY_POLICY_URL = "https://s3.eu-central-1.amazonaws.com/adinlay-resources/ADinlay+Privacy+Policy/index.htm";
//    public static final String DISPLAY_PDF_TERMS_URL = "http://drive.google.com/viewerng/viewer?embedded=true&url=" + TERMS_URL; // if the link (TERMS_URL) leads to a pdf
    // alternative urls for google docs display pdf: "http://docs.google.com/gview?embedded=true&url=" || "http://docs.google.com/viewer?url="

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        findViewById(R.id.toolbar_right_tv).setVisibility(View.GONE);
        TextView btn1 = findViewById(R.id.toolbar_left_tv);
        btn1.setText(getResources().getString(R.string.arrow_and_back));
        btn1.setTextColor(Color.BLACK);
        findViewById(R.id.toolbar_left_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        webView = findViewById(R.id.terms_webview);
        if (Build.VERSION.SDK_INT >= 19) {
            WebView.setWebContentsDebuggingEnabled(false);
        }
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        String url = getIntent() != null && getIntent().hasExtra("privacy") ?
                PRIVACY_POLICY_URL : TERMS_URL;
        webView.loadUrl(url);


//        webView.getSettings().setJavaScriptEnabled(true); // can add @SuppressLint("SetJavaScriptEnabled") to top of method
//        webView.setWebViewClient(new WebViewClient()); // needed for urls with redirect - otherwise the system opens browser
        // below: attempts to understand why and fix problem when using the DISPLAY_PDF_TERMS_URL - that sometimes the webView stays blank
        // my latest thought is that this is a problem @googleDocs, and this is why we don't get any indication that something went wrong
//        webView.setWebViewClient(new WebViewClient(){ // note: in clean version we already set up a webViewClient, this one ads logs
//            @Override
//            public void onPageStarted(WebView view, String url, Bitmap favicon) {
//                super.onPageStarted(view, url, favicon);
//                Timber.i("TC_ onPageStarted");
//            }
//
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                super.onPageFinished(view, url);
//                Timber.i("TC_ onPageFinished %s %s", view.getHeight(), view.getWidth());
//            }
//
//            @Override
//            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
//                super.onReceivedError(view, request, error);
//                Timber.i(error, "TC_ onReceivedError view: %s request: %s", view, request); // do we need to check error not null?
//            }
//
//            @Override
//            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
//                super.onReceivedSslError(view, handler, error);
//                Timber.i(error, "TC_ onReceivedSslError view: %s", view); // do we need to check error not null?
//            }
//
//
//            @Override
//            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
//                super.onReceivedHttpError(view, request, errorResponse);
//                Timber.i("TC_ onReceivedHttpError view: %s + errorResponse: %s", view, errorResponse);
//            }
//
//            @Override
//            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//                super.onReceivedError(view, errorCode, description, failingUrl);
//                Timber.i("TC_ onReceivedSslError2 view: %s errorCode: %s description: %s", view, errorCode, description);
//            }
//        });
//        webView.getSettings().setAllowFileAccessFromFileURLs(true);
//        webView.getSettings().setAllowUniversalAccessFromFileURLs(true);
//        webView.getSettings().setBuiltInZoomControls(true);
//        webView.setWebChromeClient(new WebChromeClient());
//        webView.getSettings().setDomStorageEnabled(true);


//        webView.loadUrl(DISPLAY_PDF_TERMS_URL);
        // also tried the alternative urls:
//        webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + "https://s3.eu-central-1.amazonaws.com/adinlay-resources/T%26C+Ambassadors.pdf");
//        webView.loadUrl("http://docs.google.com/viewer?url=" + "https://s3.eu-central-1.amazonaws.com/adinlay-resources/T%26C+Ambassadors.pdf"); // only url that showed errors


    }



}
