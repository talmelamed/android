package com.adinlay.adinlay;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adinlay.adinlay.objects.Ad;

/**
 * Created by Noya on 8/15/2018.
 */

public class ShareApprovalProcessFrag extends Fragment {

    private static final String TAG = ShareApprovalProcessFrag.class.getSimpleName();


//    EditPostViewModel model = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (getActivity() != null){
//            model = ViewModelProviders.of(getActivity()).get(EditPostViewModel.class);
//        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_share_approval_process, container, false);

        Ad ad = AdinlayApp.getSelectedAd();
        boolean isFcbk = ad != null && "facebook".equalsIgnoreCase(ad.getTargetMedia());
        boolean isFirst = ad != null && ad.isTestingCampaign();
        int textRes = isFcbk && isFirst ? R.string.approval_process_test_fb :
                isFcbk ? R.string.approval_process_fcbk :
                        isFirst ? R.string.approval_process_test_ig :
                                R.string.approval_process;
        TextView mainTv = root.findViewById(R.id.approval_explain_tv);
        mainTv.setText(textRes);

//        if (model == null && getActivity() != null){
//            model = ViewModelProviders.of(getActivity()).get(EditPostViewModel.class);
//        }

        return root;
    }



}
