package com.adinlay.adinlay;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.constraint.motion.MotionLayout;
import android.support.constraint.motion.MotionScene;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.objects.DraftPost;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import timber.log.Timber;

/**
 * Created by Noya on 4/22/2018.
 */

public class PhotoHubActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<GalleryImageObject>>, ItemClick {

    private static final String TAG = PhotoHubActivity.class.getSimpleName();
    public static final String SELECTED_IMAGE_PATH = "selected_image_path";

    private ImageView mainIv = null;
    private View nextButton = null;
    private TextView approvedButton = null;
    private TextView approvedBadge = null;
    private MotionLayout motionLayout = null;

    private RecyclerView recyclerView = null;
    private GalleryRecyclerAdapter adapter = null;
//    private RecyclerViewPreloader<GalleryImageObject> preloader = null;
    private GridLayoutManager layoutManager = null;
    private static final int SPAN_COUNT = 3;
    private int screenW = -1;

    private GalleryImageObject mainGio = null;
    private String newImageUrl = null; // if not null - load this image when loader is ready
    private int selectedPosition = 0;
    private boolean flagOnItemClick = false;

    private ApprovedPostViewModel viewModel = null;
    BroadcastReceiver fcmReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent == null ? null : intent.getExtras();
//            Timber.i("NFT_ update? %s", (extras != null && "update".equals(extras.getString("approved"))));
            if (extras != null && AyUtils.UPDATE_VAL.equals(extras.getString(AyUtils.APPROVED_KEY))){
                viewModel.fetchApprovedPosts();
            }
        }
    };


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
//        Timber.d("onCreate of PhotoHubActivity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_hub_start);

        setSupportActionBar(null);

//        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null){
//            actionBar.setDisplayShowHomeEnabled(false);
//            actionBar.setDisplayHomeAsUpEnabled(false);
//            actionBar.setDisplayShowCustomEnabled(true);
//            actionBar.setDisplayShowTitleEnabled(false);
//        }

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= 23){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
        }

        // toolbar:
        findViewById(R.id.toolbar_right_tv).setVisibility(View.GONE);
        TextView btn1 = findViewById(R.id.toolbar_left_tv);
        btn1.setText(getResources().getString(R.string.arrow_and_back));
        btn1.setTextColor(Color.BLACK);
        findViewById(R.id.toolbar_left_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mainIv = findViewById(R.id.photo_hub_main_iv);
        recyclerView = findViewById(R.id.photo_hub_recycler);
        nextButton = findViewById(R.id.photo_hub_next);
        motionLayout = findViewById(R.id.photo_hub_layout_parent);

        approvedButton = findViewById(R.id.approved_tab_btn);
        approvedBadge = findViewById(R.id.counter_badge_approved);
        approvedButton.setVisibility(View.VISIBLE);

        layoutManager = new GridLayoutManager(this, SPAN_COUNT);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        motionLayout.setTransitionListener(new MotionLayout.TransitionListener() {
            @Override
            public void onTransitionStarted(MotionLayout motionLayout, int i, int i1) {
//                Timber.i("TRAN_ onTransitionStarted");
            }

            @Override
            public void onTransitionChange(MotionLayout motionLayout, int i, int i1, float v) {
                if (flagOnItemClick){
//                    Timber.d("TRAN_ onTransitionChange float is: %s, i: %s, i1: %s", v, i, i1);
//                    Timber.i("onTransitionChange float is: %s, last position: %s, selected: %s", v, layoutManager.findLastVisibleItemPosition(), selectedPosition);
                }
            }

            @Override
            public void onTransitionCompleted(MotionLayout motionLayout, int i) {

//                Timber.i("TRAN_ onTransitionCompleted i is: %s, start? %s", i, (i == R.layout.photo_hub_start));
                if (i == R.layout.photo_hub_start && flagOnItemClick) { // flag - so we use this code only after click, not other scrolls
                    flagOnItemClick = false;
//                    Timber.i("MOTION scroll onTransitionCompleted selectedPosition is: %s, firstPos: %s, last: %s",
//                            selectedPosition, first, layoutManager.findLastVisibleItemPosition());
                    recyclerView.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // make sure we still see the selected item in the recyclerView
                            recyclerView.smoothScrollToPosition(selectedPosition); // sort of ok, but the scroll just makes sure the item is seen (so it could be at bottom)
                        }
                    }, 50);

                }
                flagOnItemClick = false;
            }

            @Override
            public void onTransitionTrigger(MotionLayout motionLayout, int i, boolean b, float v) {
//                Timber.i("TRAN_ onTransitionTrigger");
            }

            @Override
            public boolean allowsTransition(MotionScene.Transition transition) {
                return true;
            }
        });

        mainIv.post(new Runnable() {
            @Override
            public void run() {
                WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
                if (wm == null){
                    getSupportLoaderManager().initLoader(R.id.image_loader_id, null, PhotoHubActivity.this);
                    return;
                }
                Display display = wm.getDefaultDisplay();
                DisplayMetrics metrics = new DisplayMetrics();
                display.getMetrics(metrics);
                int screenHeight = metrics.heightPixels;
                screenW = metrics.widthPixels;
                if (recyclerView.getAdapter() instanceof GalleryRecyclerAdapter){
                    ((GalleryRecyclerAdapter) recyclerView.getAdapter()).setSize(screenW);
                }

                int[] loc = new int[2];
                mainIv.getLocationOnScreen(loc);
                int h = loc[1] + mainIv.getHeight();
                int delta = screenHeight - h;
                int dpDelta = AyUtils.convertPixelToDp(delta, getApplicationContext());
                int desiredDeltaDp = AyUtils.convertPixelToDp(screenW/3 + getResources().getDimensionPixelSize(R.dimen.draft_post_strip_h), PhotoHubActivity.this);
                Timber.d("MAIN_RESIZE screen-height: %s iv.h: %s delta: %s in dp %s desired dpDelta: %s", screenHeight, h, delta, dpDelta, desiredDeltaDp);
                if (dpDelta < desiredDeltaDp){
                    ConstraintLayout.LayoutParams recyclerLP = (ConstraintLayout.LayoutParams) recyclerView.getLayoutParams();
                    int rvH = AyUtils.convertDpToPixel(desiredDeltaDp, PhotoHubActivity.this);
                    recyclerLP.height = rvH;

                    ConstraintLayout.LayoutParams lpIv = (ConstraintLayout.LayoutParams) mainIv.getLayoutParams();
                    lpIv.dimensionRatio = "w,1:1";
                    lpIv.bottomToTop = R.id.photo_hub_recycler;
                    Timber.d("MAIN_RESIZE changed iv lp");
                    recyclerView.requestLayout();
                    mainIv.requestLayout();
                    AyUtils.setMinRecyclerHeightDp(desiredDeltaDp, screenW, screenHeight);
                    ConstraintSet startConstraintSet = motionLayout.getConstraintSet(R.layout.photo_hub_start);
                    startConstraintSet.constrainHeight(R.id.photo_hub_recycler, rvH);
                    startConstraintSet.setDimensionRatio(R.id.photo_hub_main_iv, "w,1:1");
                    startConstraintSet.connect(R.id.photo_hub_main_iv, ConstraintSet.BOTTOM, R.id.photo_hub_recycler, ConstraintSet.TOP);
//                    ConstraintSet endConstraintSet = motionLayout.getConstraintSet(R.layout.photo_hub_end); // changes to end set don't seem to have any affect
//                    endConstraintSet.constrainMinHeight(R.id.photo_hub_recycler, rvH);
//                    endConstraintSet.setDimensionRatio(R.id.photo_hub_main_iv, "w,1:1");
//                    endConstraintSet.connect(R.id.photo_hub_main_iv, ConstraintSet.BOTTOM, R.id.photo_hub_recycler, ConstraintSet.TOP);
                }else {
                    AyUtils.setMinRecyclerHeightDp(0, screenW, screenHeight);
                }

                getSupportLoaderManager().initLoader(R.id.image_loader_id, null, PhotoHubActivity.this);
                recyclerView.setHasFixedSize(true);
            }
        });

        mainIv.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == 0) { // action Down
                    Timber.i("mainIv onTouch");
                    return motionLayout.getCurrentState() == R.layout.photo_hub_start; // when mainIv is showing, swipe won't hide it, but if only partial, swipe on it can pull it down
                }
                return true;
            }
        });

        findViewById(R.id.photo_hub_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File path = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
                Timber.d("CAM_APP_DBG have the external path? %s", (path.exists()));
                String fileName = "A_IMG_" +
                        new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date()) + ".jpg";
                File photo = new File(path, fileName);
                AyUtils.saveStringPreference(AyUtils.PREF_SAVED_IMAGE_FILE, "file:" + photo.getAbsolutePath());

                Timber.d("CAM_APP_DBG the string: file: %s", photo.getAbsolutePath());
                if (Build.VERSION.SDK_INT > 23){
                    Uri photoUri = FileProvider.getUriForFile(v.getContext(), BuildConfig.APPLICATION_ID + ".provider", photo);
                    Timber.d("CAM_APP_DBG provider uri is: %s", photoUri);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                } else {
                    Timber.d("CAM_APP_DBG not using provider");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
                }
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivityForResult(intent, AyUtils.get16BitId(R.id.intent_take_photo));
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mainGio == null){
                    return;
                }
                AdinlayApp.setDraftPost(new DraftPost(mainGio.getFileUrl()));
                Intent intent = new Intent(PhotoHubActivity.this, SelectAdActivity.class);
                intent.putExtra(SELECTED_IMAGE_PATH, mainGio.getFileUrl());
                startActivityForResult(intent, AyUtils.get16BitId(R.id.intent_share_new_post));
            }
        });

        findViewById(R.id.photo_hub_tab_bg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do nothing - prevent clicks below
            }
        });

        viewModel = ViewModelProviders.of(this).get(ApprovedPostViewModel.class);
        viewModel.getPingServerResponse().observe(PhotoHubActivity.this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (ApprovedPostViewModel.FETCH_OK.equals(s)){
                    updateApprovedButton();
                }
            }
        });

//        Bundle extras = getIntent() == null ? null : getIntent().getExtras();
//        if (extras != null && AyUtils.UPDATE_VAL.equals(extras.getString(AyUtils.APPROVED_KEY))){
//            Intent intent = new Intent(PhotoHubActivity.this, ApprovedPostActivity.class);
//            intent.putExtra(AyUtils.APPROVED_KEY, AyUtils.UPDATE_VAL);
//            startActivityForResult(intent, AyUtils.get16BitId(R.id.intent_share_new_post));
//            getIntent().removeExtra(AyUtils.APPROVED_KEY);
//        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        long now = System.currentTimeMillis();
        if (now - AyUtils.getLongPref(AyUtils.PREF_LAST_UPDATE_APPROVED_long, 0) >
                AyUtils.WAIT_INTERVAL_UPDATE_APPROVED){
            viewModel.fetchApprovedPosts();
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(fcmReceiver,
                new IntentFilter(getString(R.string.filter_approvals_action)));
        updateApprovedButton();
        if (motionLayout.getCurrentState() == R.layout.photo_hub_end){
            motionLayout.transitionToStart(); // return to start position, fixes bug: user returns to screen when state was at end, then scrolls up: mainIv showed for a sec
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (AdinlayApp.getUser() == null){
            // lost state - let's let MainActivity deal with it
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(fcmReceiver);
        super.onPause();
    }

    private void updateApprovedButton(){
        int waitingPosts = AdinlayApp.getNumberOfApprovedPosts();
        if (waitingPosts < 1){
            approvedButton.setTextColor(getResources().getColor(R.color.disabled_black));
            approvedButton.setOnClickListener(null);
            approvedBadge.setVisibility(View.GONE);
        } else {
            approvedButton.setTextColor(Color.BLACK);
            approvedButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(PhotoHubActivity.this, ApprovedPostActivity.class);
                    startActivityForResult(intent, AyUtils.get16BitId(R.id.intent_share_new_post));
                }
            });
            approvedBadge.setVisibility(View.VISIBLE);
            String numStr = waitingPosts < 10 ? " " + waitingPosts + " " : "" + waitingPosts;
            approvedBadge.setText(numStr);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case R.id.intent_take_photo & 0x0000FFFF:
                if (resultCode == RESULT_OK){
                    String dataInfo = "is null";
                    if (data != null){
                        dataInfo = "" + data;
                        if (data.getExtras() != null){
                            dataInfo += " extras: " + AyUtils.stringifyBundle(data.getExtras());
                        }
                    }

                    // scan the new file, so that contentProvider will know about it:
                    String savedImagePath = AyUtils.getStringPref(AyUtils.PREF_SAVED_IMAGE_FILE);
                    Timber.d("CAM_DBG onActivityResult got ok, data: %s prefPath: %s", dataInfo, savedImagePath);
                    if (savedImagePath != null) {
                        Uri photoUri = Uri.parse(savedImagePath);
                        MediaScannerConnection.scanFile(this, new String[]{photoUri.getPath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                            @Override
                            public void onScanCompleted(String path, Uri uri) {
                                newImageUrl = path;
                                Timber.d("CAM_DBG finished scan for %s", path);
                            }
                        });
                    } else {
                        Timber.d("CAM_DBG onActivityResult path is null");
                    }
                } else{
                    Timber.d("CAM_DBG onActivityResult got bad result: %s", resultCode);
                }
                break;
            case R.id.intent_share_new_post & 0x0000FFFF:
                if (resultCode == RESULT_OK || resultCode == SelectAdActivity.MISSING_MEDIA_RESULT){
//                    Timber.i("share_dbg");
                    Intent returnIntent = new Intent();
                    setResult(resultCode, returnIntent);
                    finish();
                }
                break;
        }

    }

    @Override
    public void finish() {
        super.finish();
        AdinlayApp.setDraftPost(null);
    }

    private void loadMainImage(GalleryImageObject gio){
        if (gio == null){
            return;
        }
        if (mainGio == null){
            GlideApp.with(this)
                    .load(gio.getFileUrl())
                    .thumbnail(GlideApp.with(this)
                            .load(gio.getFileUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .override(75,75)
                            .centerCrop()
                    )
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .centerCrop()
                    .into(mainIv);
        } else {
            // prevent flicker by adding thumbnail of oldUrl with same "result" (size, transformations) as main request
            String oldPath = mainGio.getFileUrl();
            GlideApp.with(this)
                    .load(gio.getFileUrl())
                    .thumbnail(GlideApp.with(this)
                            .load(oldPath)
                            .centerCrop()
                    )
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .centerCrop()
                    .into(mainIv);
        }
        mainGio = gio;

    }

    @NonNull
    @Override
    public Loader<List<GalleryImageObject>> onCreateLoader(int id, @Nullable Bundle args) {
        return new GalleryDataLoader(this);
    }

    @Override
    public void onLoadFinished(@NonNull Loader<List<GalleryImageObject>> loader, List<GalleryImageObject> data) {

        // remove previous pre-loaders:
//        if (adapter != null && preloader != null){
//            recyclerView.removeOnScrollListener(preloader);
//        }

        GlideRequests gr = GlideApp.with(this);
        adapter = new GalleryRecyclerAdapter(data, SPAN_COUNT, gr, screenW, this);
//        preloader = new RecyclerViewPreloader<GalleryImageObject>(gr, adapter, adapter, 6);
//        recyclerView.addOnScrollListener(preloader);
        recyclerView.setAdapter(adapter);

        // move to selectedPosition
        if (selectedPosition > 0 && data != null && selectedPosition < data.size()){
            layoutManager.scrollToPositionWithOffset(selectedPosition,0);
        }

        if (data != null && data.size() <= 2*SPAN_COUNT) {
            motionLayout.setTransition(R.layout.photo_hub_start, R.layout.photo_hub_start);
        } else {
            motionLayout.setTransition(R.layout.photo_hub_start, R.layout.photo_hub_end);
        }

        if (data != null && data.size() > 0 && data.get(0) != null){
            Timber.v("CAM_DBG onLoadFinished, newImageUrl: %s data.get(0) %s", newImageUrl, data.get(0).getFileUrl());
            if (newImageUrl != null && newImageUrl.equals(data.get(0).getFileUrl())){
                loadMainImage(data.get(0));
                newImageUrl = null;
            } else if (mainGio == null) {
                loadMainImage(data.get(0));
            }

            if (nextButton.getVisibility() == View.GONE) {
                nextButton.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onLoaderReset(@NonNull Loader<List<GalleryImageObject>> loader) {
    }

    @Override
    public void onItemClicked(Object object) {
//        Timber.v("MOTION click");
        if (object instanceof GalleryImageObject){
            GalleryImageObject gmi = (GalleryImageObject) object;
            loadMainImage(gmi);
            int pos = gmi.getPosition();
            if (pos >= 0){
                selectedPosition = pos;
//                Timber.d("TRAN_ click progress: %s, selected: %s, state: %s", motionLayout.getProgress(), selectedPosition, motionLayout.getCurrentState());
                if (motionLayout.getProgress() > 0f) {
                    layoutManager.scrollToPositionWithOffset(selectedPosition,0); // before the transition - move the selected item to the top (still need to handle items from end of list)
                    motionLayout.post(new Runnable() {
                        @Override
                        public void run() {
                            flagOnItemClick = true;
                            motionLayout.transitionToStart();
                        }
                    });
                } else {
                    recyclerView.post(new Runnable() {
                        @Override
                        public void run() {
                            // make sure we still see the relevant item (due to issues after fling)
                            if (layoutManager.findFirstVisibleItemPosition() > selectedPosition ||
                                    layoutManager.findLastVisibleItemPosition() < selectedPosition) {
                                layoutManager.scrollToPositionWithOffset(selectedPosition,0);
                            }
                        }
                    });
                }
            }
        }
    }
}
