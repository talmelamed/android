package com.adinlay.adinlay;

import android.database.Cursor;
import android.provider.MediaStore;

/**
 * Created by Noya on 4/17/2018.
 */

public class GalleryImageObject {

    private int id;
//    private Uri uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + id); // should also be able to load with uri...
    private String file_url;
    private int position = -1;
    private boolean isPlaceholder = false;

    public static final int NO_ID = -1;
    public static final int CAM_ID = NO_ID -1;
    public static final int PLACEHOLDER_ID = NO_ID -1;


    public GalleryImageObject(Cursor cursor) {
        this.id = -1;
        this.file_url = "";
        if (cursor != null){
            id = cursor.getInt(cursor.getColumnIndex("_id"));
            file_url = cursor.getString(cursor.getColumnIndex(MediaStore.MediaColumns.DATA));
        }
    }

    public GalleryImageObject(int id, String file_url) {
        this.id = id;
        this.file_url = file_url;
    }

    public GalleryImageObject(boolean isPlaceholder) {
        this.isPlaceholder = isPlaceholder;
        this.id = PLACEHOLDER_ID;
        this.file_url = "";
    }

    public String getFileUrl() {
        return file_url;
    }

    public int getId() {
        return id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isPlaceholder() {
        return isPlaceholder;
    }
}
