package com.adinlay.adinlay;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import timber.log.Timber;

public class SplashActivity extends AppCompatActivity {


    ProgressBar spinner;
    SplashViewModel model;
    boolean hasStopped = false;
    private boolean gotUser = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        FirebaseAnalytics.getInstance(this);
        FirebaseDynamicLinks.getInstance().getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
//                            Bundle bundle = pendingDynamicLinkData.zzc(); // bundle was empty
//                            Timber.i("DL__ got link bundle %s", AyUtils.stringifyBundle(bundle));
                        }
                        if (deepLink != null && !gotUser) {
                            AyUtils.saveStringPreference(AyUtils.PREF_DEEP_LINK, deepLink.toString());
                        }
//                        Timber.d("DL__ got deepLink %s", deepLink);
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Timber.i("DL__ onFailure %s", e.getMessage());
                    }
        });

        if (!isTaskRoot()){ // false if app (main) open and the notification clicked!!!!
            finish();
            // see if we need any refreshing done, for now:
            Bundle extras = getIntent() == null ? null : getIntent().getExtras();
            if (extras != null){

                if (AyUtils.UPDATE_VAL.equals(extras.getString(AyUtils.APPROVED_KEY))){
//                    Timber.i("NFT_ update approved");
                    AyUtils.saveLongPref(AyUtils.PREF_LAST_UPDATE_APPROVED_long, 0); // trigger a new fetch
                    if (AdinlayApp.getUser() != null && AdinlayApp.getUser().socialMediaTestIncomplete()){
                        ServerManager.getUpdatedUserInfo(null);
                    }
                } else if (AyUtils.UPDATE_VAL.equals(extras.getString(AyUtils.WALLET_KEY))){
                    AyUtils.saveStringPreference(AyUtils.PREF_UPDATE_WALLET, "" + System.currentTimeMillis());
//                    Timber.i("NFT_ update wallet");
                }
            }
            return;
        }

        // light background to status bar:
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= 23){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
        }

        spinner = findViewById(R.id.splash_spinner);

        model = ViewModelProviders.of(this).get(SplashViewModel.class);

        // check if we have a saved token - if so, the service will handle any changes to it.
        // If not, we need to get the token - save it and send to the server:
        if (AyUtils.getStringPref(AyUtils.PREF_FCM_TOKEN) == null){

            FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                @Override
                public void onComplete(@NonNull Task<InstanceIdResult> task) {
                    if (!task.isSuccessful()) {
                        Timber.d(task.getException(),"getInstanceId failed");
                        return;
                    }

                    // Get new Instance ID token
                    if (task.getResult() != null){
                        String token = task.getResult().getToken();
                        AyUtils.saveStringPreference(AyUtils.PREF_FCM_TOKEN, token);
                        Timber.d("NTFY_ got token: %s", token);
                        // just in case (highly unlikely!), if we have user, send the token now
                        if (AdinlayApp.getUser() != null){
                            ServerManager.sendFcmTokenToServer();
                        }
                    }

                }
            });
        } else {
            Timber.v("NTFY_ got saved pref: %s", AyUtils.getStringPref(AyUtils.PREF_FCM_TOKEN));
        }

        // check if notification added info to the intent:
//        Intent intent = getIntent();
//        Timber.i("NTFY_ intent bundle: %s", AyUtils.stringifyBundle(intent.getExtras()));


        model.getSpinnerLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean show) {
                int visibility = show == null || !show ? View.GONE : View.VISIBLE;
                spinner.setVisibility(visibility);
            }
        });

        model.getServerRequestLiveData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if ("ready".equals(s) && AdinlayApp.getUser() != null){
                    // have model send server request to check connection to Instagram:
                    model.checkInstagramConnect();
                    gotUser = true;
//                    Timber.d("DL__ got user");
                    if ("completed".equals(AdinlayApp.getUser().getRegistrationStatus())){
                        // go to main:
                        AdinlayApp.setLoginStage(SignInManager.COMPLETE_STAGE);
                        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
//                        Bundle extras = getIntent() == null ? null : getIntent().getExtras();
//                        if (extras != null && AyUtils.UPDATE_VAL.equals(extras.getString(AyUtils.APPROVED_KEY))){
//                            intent.putExtra(AyUtils.APPROVED_KEY, AyUtils.UPDATE_VAL);// fixme: need a separate val for approve and for reject!!!!
//                            getIntent().removeExtra(AyUtils.APPROVED_KEY);
//                        }
                        startActivity(intent);
                        finish();
                        AyUtils.saveStringPreference(AyUtils.PREF_DEEP_LINK, null); // clear any DL we may have
                    } else {
                        // continue the login flow process:
                        AdinlayApp.setLoginStage(SignInManager.EXPLAIN_INCOMPLETE_STAGE);
                        Intent intent = new Intent(SplashActivity.this, Login2FlowActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    // handle special error messages:
                    if (s != null && s.startsWith(ServerManager.POOR_CONNECTIVITY)){
                        showErrorDialog(getResources().getString(R.string.check_internet_try_again),
                                getResources().getString(R.string.poor_connectivity_title), new AyDialogFragment.AyClickListener() {
                                    @Override
                                    public void onButtonClicked(View v) {
                                        model.setBusy(false);
                                        checkForUser();
                                    }
                                });
                        model.setBusy(false);
                        return;
                    }

                    // couldn't use existing jwt, login from scratch:
                    // clear jwt
                    AyUtils.logoutUser();

                    // go to login1Activity login-options
                    Intent intent = new Intent(SplashActivity.this, Login1Activity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        model.getAppVersionCheckLiveData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                if (s != null && s.startsWith(SplashViewModel.VERSION_OK)){
                    // version is OK!
                    checkForUser();
                    // check if we should recommend an update
                    String jStr = s.replace(SplashViewModel.VERSION_OK, "").trim();
                    if (!jStr.isEmpty() && jStr.startsWith("{") && jStr.endsWith("}")){
                        try {
                            JSONObject jsonObject = new JSONObject(jStr);
                            int recommended = jsonObject.optInt("recommended"); // should be getting {"recommended":"27"}, so if it doesn't fit int we'll get 0
//                            Timber.v("recommended version: %s", recommended);
                            if (recommended > 0 && recommended > BuildConfig.VERSION_CODE){
                                Toast toast = Toast.makeText(getApplicationContext(), R.string.update_recommended,
                                        Toast.LENGTH_LONG);
                                toast.setGravity(Gravity.CENTER, 0,0);
                                toast.show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    return;
                }

                boolean appNeedsUpdate = s != null && s.startsWith(ServerManager.UNSUPPORTED_APP_VERSION);
                if (appNeedsUpdate){
                    showErrorDialog(s.replace(ServerManager.UNSUPPORTED_APP_VERSION, ""),
                            getResources().getString(R.string.update_required),
                            new AyDialogFragment.AyClickListener() {
                                @Override
                                public void onButtonClicked(View v) {
                                    openGooglePlay();
                                    finish();
                                }
                            },
                            getResources().getString(R.string.ok_allcaps));
                } else {
                    showErrorDialog(s, getResources().getString(R.string.error), recheckAppVersionListener);
                }
                model.getSpinnerLiveData().postValue(false);
            }
        });

//        SignInManager.clearFrbsUser();

    }


    AyDialogFragment.AyClickListener recheckAppVersionListener = new AyDialogFragment.AyClickListener() {
        @Override
        public void onButtonClicked(View v) {
            model.setStage(SplashViewModel.STAGE_UNSET);
            validateAppVersion();
        }
    };

    private void openGooglePlay(){
        String appId = getPackageName();

        // while app is only for alpha testers: open google play in the browser
//        String testersUrlStr = "https://play.google.com/apps/testing/" + appId;
//        Intent alphaWebIntent = new Intent(Intent.ACTION_VIEW,
//                Uri.parse(testersUrlStr));
//        startActivity(alphaWebIntent);


        Intent gpIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("market://details?id=" + appId));
        boolean playAppFound = false;

        // check all applications able to handle the intent:
        final List<ResolveInfo> availableIntentApps = getPackageManager()
                .queryIntentActivities(gpIntent, 0);

        for (ResolveInfo resolveInfo: availableIntentApps){
            if (resolveInfo.activityInfo.applicationInfo.packageName
                    .equals("com.android.vending")){
                ActivityInfo gpAppActivity = resolveInfo.activityInfo;
                ComponentName componentName = new ComponentName(
                        gpAppActivity.applicationInfo.packageName,
                        gpAppActivity.name);
                gpIntent.setComponent(componentName);
                startActivity(gpIntent);
                playAppFound = true;
                break;
            }
        }

        if (!playAppFound){
            // need to open browser
            String urlStr = "https://play.google.com/store/apps/details?id="+appId;
            Intent webIntent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse(urlStr));
            startActivity(webIntent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Timber.i("onResume, hasStopped? %s busy? %s stage: %s", hasStopped, model.isBusy(), model.getStage());
        hasStopped = false;
        if (!AyUtils.isOnline(this) && model.getStage() == SplashViewModel.STAGE_UNSET){
            showErrorDialog(getResources().getString(R.string.internet_required),
                    getResources().getString(R.string.no_internet_title), recheckAppVersionListener);
            return;
        }

        validateAppVersion();

    }

    private void validateAppVersion(){
        if (model.getStage() >= SplashViewModel.CHECKING_APP_VERSION){
            return;
        }
        model.setStage(SplashViewModel.CHECKING_APP_VERSION);
        model.getSpinnerLiveData().setValue(true);
        model.validateAppVersion();
    }

    private void checkForUser(){
        if (model.isBusy()){
            return;
        }
        model.setBusy(true);

        String key = AyUtils.getDecryptedToken();
        Timber.v("key is: %s", key);
        if (key != null){
            model.getSpinnerLiveData().setValue(true);
            model.checkUserInfo();
            return;

        }
        // code for 1 sec delay, then move to login-options screen
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                Timber.i("onResume in postDelayed, hasStopped? %s", hasStopped);
                // check if app is still active
                if (hasStopped){
                    model.setBusy(false);
                    return;
                }

                Intent intent = new Intent(SplashActivity.this, Login1Activity.class);
                startActivity(intent);
                finish();
            }
        }, 1000);
        ServerManager.clearCookieJar();
    }

    private void showErrorDialog(String message, String title, AyDialogFragment.AyClickListener clickListener){
        showErrorDialog(message, title, clickListener, getResources().getString(R.string.retry));
    }


    private void showErrorDialog(String message, String title, AyDialogFragment.AyClickListener clickListener, String buttonText){
        Bundle args = new Bundle();
        args.putString(AyDialogFragment.ARG_TITLE, title);
        args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, message);
        if (clickListener != null && buttonText != null){
            args.putString(AyDialogFragment.ARG_BUTTON_L, buttonText);
        }
        AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
        if (clickListener != null){
            dialogFragment.setListener(AyDialogFragment.VALUE_LEFT, clickListener);
        }
        if (!isFinishing()) {
            AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        hasStopped = true;
//        Timber.i("onStop, setting hasStopped to true");
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
