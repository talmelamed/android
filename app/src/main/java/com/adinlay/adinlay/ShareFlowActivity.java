package com.adinlay.adinlay;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import timber.log.Timber;


/**
 * Created by Noya on 7/25/2018.
 */

public abstract class ShareFlowActivity extends AppCompatActivity {

    private static final String TAG = ShareFlowActivity.class.getSimpleName();

    protected View mainView = null;
    protected RecyclerView recyclerView = null;

//    protected View buttonsFrame = null;
//    protected View bottomRightBtn = null;
//    protected View bottomLeftBtn = null;
//    protected View bottomCenterBtn = null;
//    protected View topLeftBtn = null;
//    protected View instructionsBtn = null;
//    protected View instructionsCloseBtn = null;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());

        setSupportActionBar(null);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= 23){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else if (Build.VERSION.SDK_INT >= 21){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
        }

        // toolbar:
        findViewById(R.id.toolbar_right_tv).setVisibility(View.GONE);
        TextView btn1 = findViewById(R.id.toolbar_left_tv);
        btn1.setText(getResources().getString(R.string.arrow_and_back));
        btn1.setTextColor(Color.BLACK);
        findViewById(R.id.toolbar_left_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        mainView = findViewById(getMainDisplayId());
        recyclerView = findViewById(getRecyclerId());

        if (AyUtils.getMinRecyclerHeightDp() > 0){
            // adjust recyclerView:
            ConstraintLayout.LayoutParams recyclerLP = (ConstraintLayout.LayoutParams) recyclerView.getLayoutParams();
            recyclerLP.height = AyUtils.convertDpToPixel(AyUtils.getMinRecyclerHeightDp(), ShareFlowActivity.this);
            // adjust mainView:
            ConstraintLayout.LayoutParams lpIv = (ConstraintLayout.LayoutParams) mainView.getLayoutParams();
            lpIv.dimensionRatio = "w,1:1";
            lpIv.bottomToTop = getRecyclerId();
            Timber.d("MAIN_RESIZE changed iv lp");
            // requestLayout:
            recyclerView.requestLayout();
            mainView.requestLayout();
        }

    }

    public abstract int getLayoutId();
    public abstract int getToolbarId();
    public abstract int getMainDisplayId();
    public abstract int getRecyclerId();

}
