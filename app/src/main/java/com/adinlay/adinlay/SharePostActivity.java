package com.adinlay.adinlay;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.adinlay.adinlay.objects.Ad;
import com.adinlay.adinlay.objects.DraftPost;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONException;
import org.json.JSONObject;

import timber.log.Timber;

/**
 * Created by Noya on 4/26/2018.
 */

public class SharePostActivity extends AppCompatActivity {

    private static final String TAG = SharePostActivity.class.getSimpleName();
    private static final String APPROVAL_NOTICE_PREF = "approvalNoticePref";

    private EditText userEt;
    private SharePostViewModel model;
    private ContentLoadingProgressBar spinner = null;
    private View spinnerFrame = null;
    private boolean needsApproval = false;

    private boolean isFcbk = false;
    private boolean isFree = false;
    private boolean isFcbkLink = false;
    private boolean isFirst = false;
    private ShareDialog shareDialog = null;
    private CallbackManager fcbkCallbackManager = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_post);

        setSupportActionBar(null);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= 23){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
        }

        // toolbar:
        findViewById(R.id.toolbar_right_tv).setVisibility(View.GONE);
        TextView btn1 = findViewById(R.id.toolbar_left_tv);
        btn1.setText(getResources().getString(R.string.arrow_and_back));
        btn1.setTextColor(Color.BLACK);
        findViewById(R.id.toolbar_left_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPostReleaseTime();
                finish();
            }
        });
        findViewById(R.id.toolbar_right_iv).setVisibility(View.VISIBLE);
        findViewById(R.id.toolbar_right_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                resetPostReleaseTime();
                finish();
            }
        });

        model = ViewModelProviders.of(this).get(SharePostViewModel.class);
        userEt = findViewById(R.id.ds_user_edit_text);
        ImageView mainIv = findViewById(R.id.ds_main_iv);
        spinner = findViewById(R.id.draft_spinner);
        spinnerFrame = findViewById(R.id.draft_spinner_frame);
        View fragFrame = findViewById(R.id.ds_bottom_sizer);



        Bitmap bmp = AdinlayApp.getDraftPost() == null || AdinlayApp.getDraftPost().getDraftPostBitmap() == null ? null :
                AdinlayApp.getDraftPost().getDraftPostBitmap().copy(Bitmap.Config.ARGB_8888, true);
        if (bmp == null || AdinlayApp.getSelectedAd() == null){
            finish();
            return;
        }
//        Timber.d("fb_dbg bmp size: %s X %s", bmp.getWidth(), bmp.getHeight());

        Ad ad = AdinlayApp.getSelectedAd();
        isFcbk = ad != null && "facebook".equalsIgnoreCase(ad.getTargetMedia());
        isFree = ad != null && ad.isFreeCampaign();
        isFirst = ad != null && ad.isTestingCampaign();
        isFcbkLink = isFcbk && (!isFree || isFirst); // this decides what we post on fcbk: a link from the server, or the image itself (which we then also upload to the server)
        boolean fcbkNoTag = isFcbk && ad.getInstagramTag().isEmpty();
        int dif = (int) ((1.0f * bmp.getWidth() / bmp.getHeight() * 630) - 1200); // check if ratio is similar to fcbk link ratio
        boolean isFcbkLinkSize = isFcbk && dif < 5 && dif > -5;
        if (isFcbkLinkSize && !getResources().getBoolean(R.bool.isTablet)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        }
        if (isFcbk){
            if (isFcbkLinkSize) {
                ConstraintLayout.LayoutParams lpIv = (ConstraintLayout.LayoutParams) mainIv.getLayoutParams();
                lpIv.dimensionRatio = "1200:630";
                mainIv.requestLayout();
                findViewById(R.id.ds_rotate_to_watch_tv).setVisibility(View.VISIBLE);
            }
            userEt.setVisibility(View.GONE);
            ImageView platformIcon = findViewById(R.id.ds_platform_icon);
            platformIcon.setImageResource(R.drawable.facebook_share);
            fcbkCallbackManager = CallbackManager.Factory.create();
            shareDialog = new ShareDialog(this);
//            if (fcbkNoTag) { // show the frag with "no added text" message
//                findViewById(R.id.draft_inlay_text_tab_btn).setVisibility(View.GONE);
//            }
            // can also register a callback to the dialog:
            // shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() { ... });
            // see documentation at https://developers.facebook.com/docs/sharing/android/
        }
        int netH = AyUtils.getScreenHeightPixels() - getResources().getDimensionPixelSize(R.dimen.draft_post_strip_h) -
                getResources().getDimensionPixelSize(R.dimen.toolbar_h) - getResources().getDimensionPixelSize(R.dimen.upper_bar_h);
        int actualWidth = Math.min(AyUtils.getScreenWidthPixels(), getResources().getDimensionPixelSize(R.dimen.max_w_centralizer));
        int neededH = actualWidth *4/3;
        if ((!isFcbk || !isFcbkLinkSize) && AyUtils.getScreenHeightPixels() > 0 && netH < neededH){
            // adjust frame for recyclerView:
            ConstraintLayout.LayoutParams fragLP = (ConstraintLayout.LayoutParams) fragFrame.getLayoutParams();
            fragLP.height = actualWidth / 3 + AyUtils.convertDpToPixel(8, SharePostActivity.this);
            // adjust mainView:
            ConstraintLayout.LayoutParams lpIv = (ConstraintLayout.LayoutParams) mainIv.getLayoutParams();
            lpIv.dimensionRatio = "w,1:1";
            lpIv.bottomToTop = R.id.ds_btns_frame;
            View shareButtonsFrame = findViewById(R.id.ds_btns_frame);
            ConstraintLayout.LayoutParams lpShareBtns = (ConstraintLayout.LayoutParams) shareButtonsFrame.getLayoutParams();
            lpShareBtns.bottomToTop = R.id.ds_bottom_sizer;

            Timber.d("MAIN_RESIZE changed iv lp");
            // requestLayout:
            fragFrame.requestLayout();
            shareButtonsFrame.requestLayout();
            mainIv.requestLayout();
        }

        mainIv.setImageBitmap(bmp);

        ImageView shareButton = findViewById(R.id.ds_share);

        needsApproval = ad != null && ad.needsApproval();
        if (needsApproval) {
            TextView approvalTab = findViewById(R.id.draft_approval_tab_btn);
            approvalTab.setVisibility(View.VISIBLE);
            shareButton.setImageResource(R.drawable.rectangle_send2approve);
//            if (isFcbk && fcbkNoTag){ // show the frag with "no added text" message
//                ConstraintLayout.LayoutParams tabLp = (ConstraintLayout.LayoutParams) approvalTab.getLayoutParams();
//                tabLp.rightToRight = ConstraintLayout.LayoutParams.UNSET;
//                tabLp.leftMargin = AyUtils.convertDpToPixel(15, this);
//                approvalTab.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
//            }
        } else {
            findViewById(R.id.draft_approval_tab_btn).setVisibility(View.INVISIBLE);
        }

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Fragment frag = new ShareSummaryFrag();
        ft.replace(R.id.ds_frag_frame, frag, TAG);
        ft.commit();

//        // measure the tab's text [not good for sp]
//        float density = getResources().getDisplayMetrics().density;
//        int pixelW = getResources().getDisplayMetrics().widthPixels;
//        int tabTextSize = 17;
//        Paint p = new Paint();
//        String tabsString = getResources().getString(R.string.summary_allcaps) +
//                getResources().getString(R.string.text_allcaps) +
//                getResources().getString(R.string.approval_process_allcaps);
//        p.setTextSize(density * tabTextSize);
//        float fText = p.measureText(tabsString);
//        while (fText + 18*density > pixelW){
//            tabTextSize --;
//            p.setTextSize(density * tabTextSize);
//            fText = p.measureText(tabsString);
//        }
//        Timber.i("tabTextSize: %s pixelW: %s density: %s fText: %s", tabTextSize, pixelW, density, fText);

        for (int tabId : tabIds) {
            findViewById(tabId).setOnClickListener(tabButtonClickListener);
        }

        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
                if (needsApproval && !APPROVAL_NOTICE_PREF.equals(AyUtils.getStringPref(APPROVAL_NOTICE_PREF))){
                    Bundle args = new Bundle();
                    int mesRes = isFcbk && isFirst ? R.string.approval_process_short_test_fcbk :
                            isFcbk ? R.string.approval_process_short_fcbk :
                                    isFirst ? R.string.approval_process_short_test :
                                            R.string.approval_process_short;
                    args.putInt(AyDialogFragment.ARG_MESSAGE_RES, mesRes);
                    args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
                    AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                    dialogFragment.setListener(AyDialogFragment.VALUE_LEFT, new AyDialogFragment.AyClickListener() {
                        @Override
                        public void onButtonClicked(View v) {
                            sendPost();
                        }
                    });
                    AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
                    AyUtils.saveStringPreference(APPROVAL_NOTICE_PREF, APPROVAL_NOTICE_PREF);
                    return;
                }

                sendPost();

            }
        });

        model.getPingServerResponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (isFcbkLink && !needsApproval && s != null && s.startsWith(SharePostViewModel.SHARE_OK)){
                    String serverReply = s.replace(SharePostViewModel.SHARE_OK, "");
                    String url = null;
                    String postId = null;
                    try {
                        JSONObject jsonObject = new JSONObject(serverReply);
                        url = jsonObject.optString("url");
                        postId = jsonObject.optString("postId");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
//                    Timber.d("fb_dbg about to share: %s", url);
                    if (ShareDialog.canShow(ShareLinkContent.class) && url != null && !url.isEmpty() && postId != null){
                        final String f_adinlay_postId = postId;
                        shareDialog.registerCallback(fcbkCallbackManager, new FacebookCallback<Sharer.Result>() {
                            @Override
                            public void onSuccess(Sharer.Result result) {
                                Timber.d("fb_dbg onSuccess: %s for adinlayPostId: %s", result.getPostId(), f_adinlay_postId);
                                model.reportFcbkPostResult(f_adinlay_postId, true);
                            }

                            @Override
                            public void onCancel() {
                                Timber.i("fb_dbg onCancel");
                                model.reportFcbkPostResult(f_adinlay_postId, false);
                            }

                            @Override
                            public void onError(FacebookException error) {
                                Timber.i(error, "fb_dbg onError"); // testing shows facebook display a toast "Something went wrong. Please try again."
                                model.reportFcbkPostResult(f_adinlay_postId, false);
                            }
                        });
                        String hashTag = AdinlayApp.getSelectedAd() == null ? "" : AdinlayApp.getSelectedAd().getInstagramTag();
                        ShareLinkContent slContent = new ShareLinkContent.Builder()
                                .setContentUrl(Uri.parse(url))
//                                .setQuote("quote") // Don't use! adding quote prevents backend from finding the post!!! (if empty - doesn't show)
                                .setShareHashtag(new ShareHashtag.Builder() // adding a hashTag seems OK. testing showed: # must be first character
                                        .setHashtag(hashTag) // (followed by at least one character), or dialog fails. Text cut at first space or #, if empty: won't show
                                        .build())
                                .build();
                        shareDialog.show(slContent, ShareDialog.Mode.FEED);
                        return;
                    } else {
                        s = getResources().getString(R.string.something_wrong_share_post);
                    }
                }

                boolean success = SharePostViewModel.SHARE_OK.equals(s) ||
                        (isFcbk && needsApproval && s != null && s.startsWith(SharePostViewModel.SHARE_OK));
                String message = s == null || s.isEmpty() ?
                        getResources().getString(R.string.something_wrong_share_post) :
                        success && needsApproval && isFirst && isFcbk ? getResources().getString(R.string.test_post_was_sent_to_approval_fcbk) :
                        success && needsApproval && isFirst && !isFcbk ? getResources().getString(R.string.test_post_was_sent_to_approval_ig) :
                        success && needsApproval && isFree ? getResources().getString(R.string.your_post_was_sent_to_approval_short) :
                        success && needsApproval && isFcbk ? getResources().getString(R.string.your_post_was_sent_to_approval_fcbk) :
                        success && needsApproval ? getResources().getString(R.string.your_post_was_sent_to_approval) :
                        success && isFirst ? getResources().getString(R.string.your_test_post_was_shared) :
                        success && isFree ? getResources().getString(R.string.your_post_was_shared_short) :
                        success ? getResources().getString(R.string.your_post_was_shared) : s;
                showResultDialog(success, message);
                if (SharePostViewModel.SHARE_OK.equals(s) && isFirst){
                    logFirebaseEvent();
                } else if (!success){
                    logFirebaseEvent("share_fail", null, null);
                }

            }
        });

        model.getSpinnerLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean show) {
                if (show == null || !show){
                    spinner.hide();
                    spinnerFrame.setVisibility(View.GONE);
                } else {
                    spinnerFrame.setVisibility(View.VISIBLE);
                    spinner.show();
                }
            }
        });

        model.getPingReportFBsuccess().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (SharePostViewModel.SHARE_OK.equals(s)){
                    String msg = isFirst ? getResources().getString(R.string.your_test_post_was_shared) :
                            isFree ? getString(R.string.your_post_was_shared_short) : getString(R.string.your_post_was_shared);
                    showResultDialog(true, msg);
                    if (isFirst){
                        logFirebaseEvent();
                    }
                } else if (s != null){
                    // show error dialog
                    String message = s.isEmpty() ? getString(R.string.something_wrong_share_post) : s;
                    showResultDialog(false, message);
                }
            }
        });

        spinnerFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do nothing - prevent further clicks
            }
        });

        findViewById(R.id.share_post_layout_root).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
            }
        });

    }

    private void sendPost(){
        DraftPost draft = AdinlayApp.getDraftPost();
        if (draft != null) {
            if (isFcbk && !isFcbkLink){ // a free fcbk campaign, we post as image:
                Bitmap bitmap = draft.getDraftPostBitmap() == null ? null :
                        draft.getDraftPostBitmap().copy(Bitmap.Config.ARGB_8888, true);
                if (bitmap != null && ShareDialog.canShow(SharePhotoContent.class)){
                    String hashTag = AdinlayApp.getSelectedAd() == null ? "" : AdinlayApp.getSelectedAd().getInstagramTag();
                    SharePhoto sharePhoto = new SharePhoto.Builder().setBitmap(bitmap).build();
                    SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(sharePhoto)
                            .setShareHashtag(new ShareHashtag.Builder()
                                    .setHashtag(hashTag)
                                    .build())
                            .build();
                    shareDialog.registerCallback(fcbkCallbackManager, new FacebookCallback<Sharer.Result>() {
                        @Override
                        public void onSuccess(Sharer.Result result) {
                            Timber.d("fb_Sticker_dbg onSuccess: %s", result.getPostId());
                            model.sendPostToServer(true, true);
                        }

                        @Override
                        public void onCancel() {
                            Timber.d("fb_Sticker_dbg onCancel");
                        }

                        @Override
                        public void onError(FacebookException error) {
                            Timber.d(error, "fb_Sticker_dbg onError"); // testing shows facebook display a toast "Something went wrong. Please try again."
                        }
                    });
                    shareDialog.show(content);
                } else {
                    showResultDialog(false, getResources().getString(R.string.something_wrong_share_post));
                }

                return;
            }
            String userText = userEt.getText().toString();
            draft.setUserText(userText);
            model.sendPostToServer(isFcbk, false);
        } else { // shouldn't happen, we check for state at onStart
            Bundle args = new Bundle();
            args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.error);
            args.putInt(AyDialogFragment.ARG_MESSAGE_RES, R.string.something_wrong_share_post_try_later);
            args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
            args.putInt(AyDialogFragment.ARG_CANCELABLE_BACK, AyDialogFragment.VALUE_FALSE);
            args.putInt(AyDialogFragment.ARG_CANCELABLE_OUTSIDE, AyDialogFragment.VALUE_FALSE);
            AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
            dialogFragment.setListener(AyDialogFragment.VALUE_LEFT, new AyDialogFragment.AyClickListener() {
                @Override
                public void onButtonClicked(View v) {
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            });
            AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fcbkCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private int[] tabIds = new int[]{R.id.draft_summary_tab_btn,
            R.id.draft_inlay_text_tab_btn, R.id.draft_approval_tab_btn};

    View.OnClickListener tabButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            FragmentManager fm = getSupportFragmentManager();

            Fragment currFrag = fm.findFragmentByTag(TAG);
            Fragment newFrag = null;

            switch (v.getId()){
                case R.id.draft_summary_tab_btn:
                    if (currFrag instanceof ShareSummaryFrag){
                        return;
                    }
                    newFrag = new ShareSummaryFrag();
                    break;
                case R.id.draft_inlay_text_tab_btn:
                    if (currFrag instanceof ShareInfoFrag){
                        return;
                    }
                    newFrag = new ShareInfoFrag();
                    break;
                case R.id.draft_approval_tab_btn:
                    if (currFrag instanceof ShareApprovalProcessFrag){
                        return;
                    }
                    newFrag = new ShareApprovalProcessFrag();
                    break;
            }
            if (newFrag == null){
                return;
            }
            if (currFrag instanceof ShareSummaryFrag){
                ((ShareSummaryFrag) currFrag).clearPicker();
            }
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.ds_frag_frame, newFrag, TAG);
            ft.commit();
            for (int tabId : tabIds) {
                TextView tv = findViewById(tabId);
                Typeface typeface = ResourcesCompat.getFont(SharePostActivity.this, R.font.assistant);
                if (tabId == v.getId()){
                    typeface = ResourcesCompat.getFont(SharePostActivity.this, R.font.assistant_bold);
                    tv.setTextColor(Color.BLACK);
                } else {
                    tv.setTextColor(getResources().getColor(R.color.color_tab_unselected));
                }
                tv.setTypeface(typeface);
            }
        }
    };

    private void showResultDialog(final boolean success, String message){
        Bundle args = new Bundle();
        args.putInt(AyDialogFragment.ARG_TITLE_RES, success? R.string.success : R.string.error);
        args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
        int iconVal = success ? AyDialogFragment.VALUE_TRUE : AyDialogFragment.VALUE_FALSE;
        args.putInt(AyDialogFragment.ARG_RESULT_ICON, iconVal);
        args.putInt(AyDialogFragment.ARG_CANCELABLE_BACK, AyDialogFragment.VALUE_FALSE);
        args.putInt(AyDialogFragment.ARG_CANCELABLE_OUTSIDE, AyDialogFragment.VALUE_FALSE);
        String bold = getString(R.string.post_sent_bold_text);
        if (message != null && message.contains(bold)){
            SpannableStringBuilder spBuilder = new SpannableStringBuilder(message);
            int start = message.indexOf(bold);
            int end = start + bold.length();
            spBuilder.setSpan(new StyleSpan(Typeface.BOLD), start, end, 0);
            args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, spBuilder);
        } else {
            args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, message);
        }
        AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
        dialogFragment.setListener(AyDialogFragment.VALUE_LEFT, new AyDialogFragment.AyClickListener() {
            @Override
            public void onButtonClicked(View v) {
                if (success) {
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_OK, returnIntent);
                    System.gc();
                    finish();
                }
            }
        });

        if (!isFinishing()) {
            AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
        }
    }

    private void logFirebaseEvent(){
        if (isFirst){
            logFirebaseEvent("share_test_post", "media", isFcbk? "facebook" : "instagram");
        }
    }

    private void logFirebaseEvent(@NonNull String event, String paramKey, String paramVal){
        Bundle params = null;
        if (paramKey != null && paramVal != null){
            params = new Bundle();
            params.putString(paramKey, paramVal);
        }
        FirebaseAnalytics.getInstance(this).logEvent(event, params);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (AdinlayApp.getUser() == null || AdinlayApp.getDraftPost() == null){
            // lost state - let's let mainActivity deal with it
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if (spinnerFrame.getVisibility() == View.VISIBLE){
            return;
        }

        // remove picker dialog, if relevant
        FragmentManager fm = getSupportFragmentManager();
        Fragment currFrag = fm.findFragmentByTag(TAG);
        if (currFrag instanceof ShareSummaryFrag && ((ShareSummaryFrag) currFrag).clearPicker()){
            return;
        }

        resetPostReleaseTime();
        super.onBackPressed();
    }

    private void resetPostReleaseTime(){
        DraftPost draftPost = AdinlayApp.getDraftPost();
        if (draftPost != null){
            Timber.i("ZDT_DBG will reset scheduled time from: %s", draftPost.getScheduledPostTimeMilli());
            draftPost.setScheduledPostTimes(null, DraftPost.POST_SCHEDULE_UNDEFINED);
        }
    }

    private void hideKeyboard(View v){
        if (v != null){
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE && isFcbk
                && !getResources().getBoolean(R.bool.isTablet)){
            Intent intent = new Intent(SharePostActivity.this, LandscapeImageActivity.class);
            startActivity(intent);
        }
    }
}
