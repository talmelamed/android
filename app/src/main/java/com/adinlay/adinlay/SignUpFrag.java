package com.adinlay.adinlay;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Noya on 6/14/2018.
 */

public class SignUpFrag extends LoginFrag {

    private static final String TAG = SignUpFrag.class.getSimpleName();

    private LoginViewModel viewModel;
    private EditText emailEt = null;
    private EditText passwordEt = null;
    private EditText passwordConfirmEt = null;
    private View passwordWarnTV = null;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity()).get(LoginViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_register1, container, false);

        viewModel.updateFlowData(R.drawable.group_5_signup, getResources().getString(R.string.sign_up_allcaps), true, true);

        emailEt = root.findViewById(R.id.register_email);
        passwordEt = root.findViewById(R.id.register_password);
        passwordConfirmEt = root.findViewById(R.id.register_password_confirm);
        passwordWarnTV = root.findViewById(R.id.register_password_warning);

        passwordEt.addTextChangedListener(passwordConfirmWatcher);
        passwordConfirmEt.addTextChangedListener(passwordConfirmWatcher);
        return root;
    }

    @Override
    public boolean onMainClicked() {
        String email = emailEt == null ? "" : emailEt.getText().toString();
        String password = passwordEt == null ? "" : passwordEt.getText().toString();
        String passwordConfirm = passwordConfirmEt == null ? "" : passwordConfirmEt.getText().toString();

        if (email.matches(AyUtils.email_REGEX) && password.length() >= 6 && password.equals(passwordConfirm)){
            // we can send signup to server!
            viewModel.sendSignUpRequest(email, password);
        } else if (getActivity() != null){
            String text = "";
            if (!email.matches(AyUtils.email_REGEX)){
                emailEt.setTextColor(getResources().getColor(R.color.red));
                addTextWatcherToEmailEt();
                text = getResources().getString(R.string.invalid_email);
            }
            if (password.length() < 6 || !password.equals(passwordConfirm)){
                if (text.length() > 0){
                    text += "\n";
                }
                if (password.length() < 6){
                    text += getResources().getString(R.string.toast_password_too_short);
                } else {
                    text += getResources().getString(R.string.password_is_not_confirmed);
                }
            }
            Toast.makeText(getActivity().getApplicationContext(), text, Toast.LENGTH_LONG).show();


        }

        return false;
    }

    @Override
    public boolean onBackClicked() {
        return false;
    }

    private void addTextWatcherToEmailEt(){
        emailEt.removeTextChangedListener(emailEtTextWatcher);
        emailEt.addTextChangedListener(emailEtTextWatcher);
    }

    TextWatcher emailEtTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (emailEt != null) {
                // return the text color to black
                emailEt.setTextColor(Color.BLACK);
                // remove the textWatcher
                emailEt.removeTextChangedListener(emailEtTextWatcher);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    TextWatcher passwordConfirmWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String password = passwordEt == null? "" : passwordEt.getText().toString();
            String passwordConf = passwordConfirmEt == null? "" : passwordConfirmEt.getText().toString();
            boolean match = passwordConf.length() == 0 || password.equals(passwordConf);
            int warnVisibility = match ? View.GONE : View.VISIBLE;
            if (passwordWarnTV != null){
                passwordWarnTV.setVisibility(warnVisibility);
            }
            if (passwordConfirmEt != null){
                if (match){
                    passwordConfirmEt.setTextColor(Color.BLACK);
                }else {
                    passwordConfirmEt.setTextColor(getResources().getColor(R.color.red));
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    protected String getFragStageName() {
        return "register";
    }
}
