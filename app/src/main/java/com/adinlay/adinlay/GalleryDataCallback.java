package com.adinlay.adinlay;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.provider.MediaStore;
import android.support.v7.util.AsyncListUtil;

import java.lang.ref.WeakReference;

/**
 * Created by Noya on 4/17/2018.
 */

public class GalleryDataCallback extends AsyncListUtil.DataCallback<GalleryImageObject> {

    private static final String TAG = GalleryDataCallback.class.getSimpleName();

    Cursor _cursor = null;
    WeakReference<Context> wr_context;

    public GalleryDataCallback(Context context) {
        wr_context = new WeakReference<Context>(context);
    }

    private Cursor getCursor(){
        Context context = wr_context.get();
        if ((_cursor == null || _cursor.isClosed()) && context != null){
            String[] projection  = new String[]{"_id", MediaStore.MediaColumns.DATA};

            MatrixCursor mc = new MatrixCursor(projection);
            mc.addRow(new Object[]{GalleryImageObject.CAM_ID, ""});

            Cursor queryCursor = context.getContentResolver().query(
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, "_id DESC");

            _cursor = new MergeCursor(new Cursor[]{mc, queryCursor});
//            _cursor.registerContentObserver(); // todo: add content observer!

        }
        return _cursor;
    }

    @Override
    public int refreshData() {
        return getCount();
    }

    private int getCount(){
        if (getCursor() != null){
            return _cursor.getCount();
        }
        return 0;
    }

    @Override
    public void fillData(GalleryImageObject[] data, int startPosition, int itemCount) {
        if (data != null){
            for (int i = 0; i < itemCount; i++){
                data[i] = getMediaItem(startPosition + i);
            }
        }
    }

    private GalleryImageObject getMediaItem(int position){
        GalleryImageObject galleryItem = null;
        if (getCursor() != null){
            _cursor.moveToPosition(position);
//            Timber.v("ALU_DBG getMediaItem at position %s cursor showing: %s url: %s",
//                    position, _cursor.getString(_cursor.getColumnIndex("_id")),
//                    _cursor.getString(_cursor.getColumnIndex(MediaStore.MediaColumns.DATA)));
            galleryItem = new GalleryImageObject(_cursor);
        }
        return galleryItem;
    }

    public void closeCursor(){
        if (_cursor != null && !_cursor.isClosed()){
            _cursor.close();
        }
    }


}
