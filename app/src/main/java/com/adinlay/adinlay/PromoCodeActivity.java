package com.adinlay.adinlay;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.adinlay.adinlay.objects.PromoItem;

import timber.log.Timber;

public class PromoCodeActivity extends BasicActivity implements ItemClick{

    private EditText promoEt;
//    private View submitButton;
    private RecyclerView recyclerView;
    private ContentLoadingProgressBar spinner = null;
    private View spinnerFrame = null;

    private ApprovedPostViewModel viewModel = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        promoEt = findViewById(R.id.promo_input_et);
        recyclerView = findViewById(R.id.promo_rv);
        spinner = findViewById(R.id.promo_spinner);
        spinnerFrame = findViewById(R.id.promo_spinner_frame);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        viewModel = ViewModelProviders.of(this).get(ApprovedPostViewModel.class);

        findViewById(R.id.promo_submit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String promo = promoEt.getText().toString();
                if (promo.isEmpty()){
                    Toast.makeText(PromoCodeActivity.this, R.string.please_provide_promo,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                hideKeyboard(v);
                viewModel.sendPromoToServer(promo, true);
            }
        });

        viewModel.getPingServerResponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {

                setAdapter();

                if (s != null && s.equals(ApprovedPostViewModel.SHARE_OK)){
                    Intent doneIntent = new Intent(PromoCodeActivity.this, PromoDoneActivity.class);
                    startActivity(doneIntent);
                    promoEt.setText("");
                    return;
                }

                if (s == null || s.isEmpty() || s.equals(ApprovedPostViewModel.DELETE_OK)){
                    return;
                }

                String message = s.replace(ApprovedPostViewModel.SHARE_FAIL, "");
                if (message.isEmpty()){
                    message = getString(R.string.something_wrong_send_promo);
                } else {
                    message = message.replace(ApprovedPostViewModel.DELETE_FAIL, "");
                    if (message.isEmpty()){
                        message = getString(R.string.something_wrong_delete_promo);
                    }
                }
                Bundle args = new Bundle();
                args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.error);
                args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, message);
                args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
                AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                if (!isFinishing()) {
                    AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
                }
            }
        });

        setAdapter();

        // spinner:
        viewModel.getSpinnerLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean show) {
                if (show == null || !show){
                    spinner.hide();
                    spinnerFrame.setVisibility(View.GONE);
                } else {
                    spinnerFrame.setVisibility(View.VISIBLE);
                    spinner.show();
                }
            }
        });

        spinnerFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do nothing - prevent further clicks
            }
        });

        // keyboard
        findViewById(R.id.promo_root).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
            }
        });

    }

    @Override
    int getLayoutResourceId() {
        return R.layout.activity_promo_codes;
    }

    private void setAdapter(){
        User user = AdinlayApp.getUser();
        if (user != null){
            Timber.i("PROMO_ number of items: %s", user.getPromotionCodes().size());
            PromoCodesRvAdapter adapter = new PromoCodesRvAdapter(user.getPromotionCodes(), this);
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void onItemClicked(Object object) {
        if (object instanceof PromoItem){
            final String promoCode = ((PromoItem) object).getCode();
            Bundle args = new Bundle();
            args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.clear_promo_code);
            args.putInt(AyDialogFragment.ARG_MESSAGE_RES, R.string.clear_promo_code_ask);
            args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.cancel);
            args.putInt(AyDialogFragment.ARG_BUTTON_R_RES, R.string.remove_button);
            AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
            dialogFragment.setListener(AyDialogFragment.VALUE_RIGHT, new AyDialogFragment.AyClickListener() {
                @Override
                public void onButtonClicked(View v) {
                    viewModel.sendPromoToServer(promoCode, false);
                }
            });
            AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
        }
    }
}
