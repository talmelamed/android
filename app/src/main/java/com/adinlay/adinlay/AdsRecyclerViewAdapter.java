package com.adinlay.adinlay;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.objects.Ad;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterInside;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by Noya on 4/12/2018.
 */

public class AdsRecyclerViewAdapter extends RecyclerView.Adapter<AdsRecyclerViewAdapter.AvailableAdViewHolder> {

    private final static String TAG = AdsRecyclerViewAdapter.class.getSimpleName();
    private ArrayList<Ad> adsList;
    private WeakReference<ItemClick> wr_itemClickCallback;
    private GradientDrawable gradientDrawable = null;
    private WeakReference<GlideRequests> wr_glideWith = new WeakReference<GlideRequests>(null);
    private WeakReference<GlideRequest<Drawable>> wr_requestBuilder = new WeakReference<GlideRequest<Drawable>>(null);

    private int cellW = 0;
    private boolean getFullSizeThumbnail = true;

    public AdsRecyclerViewAdapter(ArrayList<Ad> adsList, GlideRequests glideRequests, ItemClick itemClickCallback) {
        this.adsList = adsList;
        wr_glideWith = new WeakReference<GlideRequests>(glideRequests);
        if (glideRequests != null){
            wr_requestBuilder = new WeakReference<GlideRequest<Drawable>>(glideRequests.asDrawable().transforms(new CenterInside()));
        }
        wr_itemClickCallback = new WeakReference<ItemClick>(itemClickCallback);
    }

    @NonNull
    @Override
    public AvailableAdViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_available_ad, parent, false);

//        Timber.d("parent height: %s parent width: %s", parent.getMeasuredHeight(), parent.getMeasuredWidth());
        if (cellW <= 0){
            cellW = parent.getMeasuredWidth() / 3; // fixme: should get span count in constructor
        }
//        Timber.d("parent w in dp: %s", AyUtils.convertPixelToDp(parent.getMeasuredWidth()/3, parent.getContext()));

        return new AvailableAdViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull AvailableAdViewHolder holder, int position) {
        Ad ad = adsList.get(position);

        Object taggedObject = holder.itemView.getTag(R.id.object_tag);
        if (taggedObject instanceof Ad &&
                !ad.getId().isEmpty() &&
                ad.getId().equals(((Ad) taggedObject).getId())){
            // same object!
            return;
        }

        String targetMedia = ad.getTargetMedia();
        boolean isInsta = "instagram".equalsIgnoreCase(targetMedia);
        boolean isFcbk = "facebook".equalsIgnoreCase(targetMedia);
        boolean connectedToMedia = ad.isConnectedToMedia();
        boolean isNotRegularCampaign = !connectedToMedia || ad.isFreeCampaign() || ad.isTestingCampaign();

        if (!ad.getUrl(false).isEmpty()){
            GlideRequests gr = wr_glideWith.get();
            if (gr != null){
                gr.clear(holder.adIv);
            }
            if (wr_requestBuilder.get() == null && gr != null){
                wr_requestBuilder = new WeakReference<GlideRequest<Drawable>>(gr.asDrawable().transforms(new CenterInside()));
            }
            GlideRequest<Drawable> requestBuilder = wr_requestBuilder.get();
            if (requestBuilder != null){

                requestBuilder = requestBuilder.clone()
                        .load(ad.getUrl(true));
                if (cellW > 0){
                    requestBuilder = requestBuilder.override(cellW, cellW);
                }
                if (getFullSizeThumbnail){
                    requestBuilder = requestBuilder.thumbnail(requestBuilder.clone().load(ad.getUrl(true)));
                } else {
                    requestBuilder = requestBuilder.thumbnail(0.2f);
                }
                requestBuilder.into(holder.adIv);
            }
        }
        holder.adIv.setBackground(getGradientDrawable());

        if (isNotRegularCampaign){
            holder.minPriceTv.setText("");
        } else {
            String price = AyUtils.getStringAmountWithCurrencySymbol(ad.getAmountBigDec(Ad.PRICE_MIN_KEY),
                    AyUtils.DECIMAL_PLACES_DEFAULT, holder.minPriceTv.getResources());
            holder.minPriceTv.setText(price);
        }


        if (isNotRegularCampaign){
            if (!connectedToMedia) {
                holder.maxPriceTv.setText(R.string.not_connected);
            } else {
                holder.maxPriceTv.setText(ad.getTitle());
            }
            holder.maxPriceTv.setVisibility(View.VISIBLE);
        } else if (ad.campaignHasOffers()) {
            String upToPrice = AyUtils.getContext().getResources().getString(R.string.up_to);
            upToPrice += " " + AyUtils.getStringAmountWithCurrencySymbol(ad.getAmountBigDec(Ad.PRICE_MAX_KEY),
                    AyUtils.DECIMAL_PLACES_DEFAULT, holder.maxPriceTv.getResources());
            holder.maxPriceTv.setText(upToPrice);
            holder.maxPriceTv.setVisibility(View.VISIBLE);
        } else {
            holder.maxPriceTv.setVisibility(View.INVISIBLE);
        }


        if(!connectedToMedia || (ad.isFreeCampaign() && !ad.isTestingCampaign()) || !ad.needsApproval()){
            holder.approvalTimeTv.setVisibility(View.INVISIBLE); // don't use gone - so we have the same positioning for the platform icon
        } else {
            holder.approvalTimeTv.setVisibility(View.VISIBLE);
            String approvalTime = holder.approvalTimeTv.getResources().getString(
                    R.string.formatted_approval_allcaps,"" + ad.getApprovalTime());
            holder.approvalTimeTv.setText(approvalTime);
        }

        int res = isInsta ? R.drawable.inst_ad :
                isFcbk ? R.drawable.facebook_ad : 0;
        holder.platformIv.setImageResource(res);

        holder.itemView.setOnClickListener(itemClickListener);

        holder.itemView.setTag(R.id.object_tag, ad);
    }

    @Override
    public int getItemCount() {
        return adsList.size();
    }

    View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (wr_itemClickCallback.get() != null){
                wr_itemClickCallback.get().onItemClicked(v.getTag(R.id.object_tag));
            }
        }
    };

    public void setGetFullSizeThumbnail(boolean getFullSizeThumbnail) {
        this.getFullSizeThumbnail = getFullSizeThumbnail;
    }

    public static class AvailableAdViewHolder extends RecyclerView.ViewHolder {

        ImageView adIv;
        TextView minPriceTv;
        TextView maxPriceTv;
        TextView approvalTimeTv;
        ImageView platformIv;

        public AvailableAdViewHolder(View itemView) {
            super(itemView);

            adIv = itemView.findViewById(R.id.aa_iv);
            minPriceTv = itemView.findViewById(R.id.aa_min_price_tv);
            maxPriceTv = itemView.findViewById(R.id.aa_up_to_tv);
            approvalTimeTv = itemView.findViewById(R.id.aa_approval_time);
            platformIv = itemView.findViewById(R.id.aa_platform);
        }
    }

    private GradientDrawable getGradientDrawable(){
        if (gradientDrawable == null){
            gradientDrawable = new GradientDrawable(
                    GradientDrawable.Orientation.TOP_BOTTOM,
                    new int[]{0xfff9f8f4, 0xfffff9b1, 0xfffff797, 0xffffee14});
        }
        return gradientDrawable;
    }

}
