package com.adinlay.adinlay;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.adinlay.adinlay.objects.TabLauncher;

import java.util.ArrayList;

import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

    final private static String TAG = MainActivity.class.getSimpleName();

    TabStrip tabStrip = new TabStrip(); // TODO: 3/29/2018 should probably be in viewModel!

    MainViewModel model;

    Dialog dialog = null;
    TextView badgeTv = null;

    BroadcastReceiver fcmReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent == null ? null : intent.getExtras();
//            Timber.i("NFT_ update? %s", (extras != null && "update".equals(extras.getString("approved"))));
            if (extras != null && AyUtils.UPDATE_VAL.equals(extras.getString(AyUtils.APPROVED_KEY))){
                model.fetchApprovedPosts();
                if (AdinlayApp.getUser() != null && AdinlayApp.getUser().socialMediaTestIncomplete()){
                    ServerManager.getUpdatedUserInfo(null);
                }
            }
            if (extras != null && AyUtils.UPDATE_VAL.equals(extras.getString(AyUtils.WALLET_KEY))){
                if (tabStrip.getSelectedTab() != null && TabLauncher.WALLET_TAG.equals(tabStrip.getSelectedTab().getLauncherTag())) {
                    model.fetchWalletInfo();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        Toolbar tb = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(null);

//        android.support.v7.app.ActionBar actionBar =getSupportActionBar();
//        if (actionBar != null){
//            actionBar.setDisplayShowHomeEnabled(false);
//            actionBar.setDisplayHomeAsUpEnabled(false);
//            actionBar.setDisplayShowCustomEnabled(true);
//            actionBar.setDisplayShowTitleEnabled(false);
//            Timber.d("got support action bar");
//        }

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= 23){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
        }

        findViewById(R.id.toolbar_right_tv).setVisibility(View.GONE);
        findViewById(R.id.toolbar_left_tv).setVisibility(View.GONE);

        badgeTv = findViewById(R.id.counter_badge_approved);

        View homeTab = findViewById(R.id.home_container);
        View advertisersTab = findViewById(R.id.advertisers_container);
        View shareTab = findViewById(R.id.share_container);
        View walletTab = findViewById(R.id.wallet_container);
        View profileTab = findViewById(R.id.profile_container);

//        ImageView homeIv = findViewById(R.id.home_iv);
//        ImageView advertisersIv = findViewById(R.id.advertisers_iv);
//        ImageView shareIv = findViewById(R.id.share_iv);
//        ImageView walletIv = findViewById(R.id.wallet_iv);
//        ImageView profileIv = findViewById(R.id.profile_iv);


        homeTab.setOnClickListener(tabsBarListener);
        advertisersTab.setOnClickListener(tabsBarListener);
        shareTab.setOnClickListener(tabsBarListener);
        walletTab.setOnClickListener(tabsBarListener);
        profileTab.setOnClickListener(tabsBarListener);

        homeTab.setTag(R.id.tab_launcher_tag, tabStrip.getTabs().get(TabLauncher.HOME_TAG));
        advertisersTab.setTag(R.id.tab_launcher_tag, tabStrip.getTabs().get(TabLauncher.ADVERTISERS_TAG));
        shareTab.setTag(R.id.tab_launcher_tag, tabStrip.getTabs().get(TabLauncher.SHARE_TAG));
        walletTab.setTag(R.id.tab_launcher_tag, tabStrip.getTabs().get(TabLauncher.WALLET_TAG));
        profileTab.setTag(R.id.tab_launcher_tag, tabStrip.getTabs().get(TabLauncher.PROFILE_TAG));

        findViewById(R.id.tabstripContainer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // prevent clicks under tabstripContainer
            }
        });

        FrameLayout frame = findViewById(R.id.mainFrame);
        if (frame != null && frame.getChildCount() > 0){
            Timber.w("frag frame has children in onCreate! %s", frame.getChildCount());
            frame.removeAllViews();
        }

        model = ViewModelProviders.of(this).get(MainViewModel.class);

        if (savedInstanceState != null && savedInstanceState.getBoolean("had_user") &&
                AdinlayApp.getUser() == null){
            // oy, app lost its state! need to re-fetch the user
            Timber.w("FYI app lost state, going back to splash");
            Intent intent = new Intent(MainActivity.this, SplashActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        model.getPingApprovedPosts().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                updateApprovalBadge();
            }
        });

        selectTab(tabStrip.getTabs().get(TabLauncher.HOME_TAG), true);

        requestMissingPermissions();

        model.getBrands();
        model.fetchApprovedPosts();

        AyUtils.saveStringPreference(AyUtils.PREF_UPDATE_WALLET, null);

//        Bundle extras = getIntent() == null ? null : getIntent().getExtras();
//        if (extras != null && AyUtils.UPDATE_VAL.equals(extras.getString(AyUtils.APPROVED_KEY))){
//            Intent intent  = new Intent(MainActivity.this, PhotoHubActivity.class);
//            intent.putExtra(AyUtils.APPROVED_KEY, AyUtils.UPDATE_VAL);
//            startActivity(intent);
//            getIntent().removeExtra(AyUtils.APPROVED_KEY);
//        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (AdinlayApp.getUser() != null ){
            outState.putBoolean("had_user", true);
        }
        super.onSaveInstanceState(outState);
    }

    private void requestMissingPermissions(){
        final ArrayList<String> missingPermissions = new ArrayList<String>();
        ArrayList<String> explanationsList = new ArrayList<>();

        // read external storage:
        if (addPermissionToList(missingPermissions, Manifest.permission.READ_EXTERNAL_STORAGE)){
            explanationsList.add("Read external storage");
        }
        // write external storage:
        if (addPermissionToList(missingPermissions, Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            explanationsList.add("Write external storage");
        }

        if (missingPermissions.size() > 0){
            if (explanationsList.size() > 0){
                // we need to show rationale for the permissions:
                StringBuilder sb = new StringBuilder();
                sb.append(getString(R.string.permission_request_explain));

                for (int i = 0; i < explanationsList.size(); i++){
                    if (i > 0){
                        sb.append(", ");
                    }
                    sb.append(explanationsList.get(i));
                }

                showDialogOKorCancel(sb.toString(), new AyDialogFragment.AyClickListener() {
                    @Override
                    public void onButtonClicked(View v) {
                        ActivityCompat.requestPermissions(MainActivity.this,
                                missingPermissions.toArray(new String[0]),
                                AyUtils.get16BitId(R.id.permission_code_multiple));
                    }
                });
            } else {
                Timber.i("PERM_DBG asking for permissions");
                ActivityCompat.requestPermissions(MainActivity.this,
                        missingPermissions.toArray(new String[missingPermissions.size()]),
                        AyUtils.get16BitId(R.id.permission_code_multiple));
            }
        }


    }

    /**
     * if permission is missing, add it to the missingPermissions list, and check if
     * we have to show the rationale for the missing permission
     * @param missingPermissions if permission is missing, add it to this list
     * @param permission to check
     * @return true if permission is missing and showing its rationale is required
     */
    private boolean addPermissionToList(ArrayList<String> missingPermissions, String permission){
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED){
            Timber.i("PERM_DBG missing the permission: %s", permission);
            missingPermissions.add(permission);
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
                return true;
            }
        }
        return false;
    }

    private void showDialogOKorCancel(String message, AyDialogFragment.AyClickListener okClickListener){

        Bundle args = new Bundle();
        args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, message);
        args.putInt(AyDialogFragment.ARG_BUTTON_R_RES, R.string.ok_allcaps);
        args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.cancel);
        AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
        dialogFragment.setListener(AyDialogFragment.VALUE_RIGHT, okClickListener);
        if (!isFinishing()) {
            AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (permissions.length == 0){
            // as per the documentation - an empty array should be treated as a cancellation
            // FIXME: 4/23/2018 should we try again?
            Timber.i("PERM_DBG got empty list of missing permissions");
            return;
        }

        switch (requestCode){
            case R.id.permission_code_multiple & 0x0000FFFF:
                final ArrayList<String> missingPermissions = new ArrayList<>();
                ArrayList<String> doNotAskAgainPerms = new ArrayList<>();
                for (int i = 0; i < permissions.length; i++){
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED){
                        missingPermissions.add(permissions[i]);
                        if (!ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permissions[i])){
                            doNotAskAgainPerms.add(permissions[i]);
                        }
                    }
                }
                if (missingPermissions.size() > 0){
                    if (doNotAskAgainPerms.size() > 0){
                        // need to send the user to the device's settings to grant permissions
                        Toast.makeText(getApplicationContext(),
                                R.string.permission_request,
                                Toast.LENGTH_LONG).show();
                        // for opening the settings app
                        Intent i = new Intent();
                        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        i.addCategory(Intent.CATEGORY_DEFAULT);
                        i.setData(Uri.parse("package:" + getPackageName()));
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        startActivity(i);
                        finish();
                        return;
                    } else {
                        Toast.makeText(getApplicationContext(),
                                "Please grant the permissions required to run this app",
                                Toast.LENGTH_LONG).show();
                        // FIXME: 4/25/2018 needs testing, should this be a loop? should dialog have a dismiss option, also on back / click outside?
                        showDialogOKorCancel(getString(R.string.permission_request_2), new AyDialogFragment.AyClickListener() {
                            @Override
                            public void onButtonClicked(View v) {
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        missingPermissions.toArray(new String[0]),
                                        AyUtils.get16BitId(R.id.permission_code_multiple));
                            }
                        });
                    }
                }
                return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void selectTab(@NonNull TabLauncher newTab, boolean forceSelect){

        TabLauncher oldTab = tabStrip.getSelectedTab();
        if (newTab.equals(oldTab)&& !forceSelect){
            Timber.i("reselected current tab %s", tabStrip.getSelectedTab().getLauncherTag());
            return;
        }

        if (newTab.shouldReplaceFragOnClick()){

            int oldPos = oldTab.getPosition();
            tabStrip.setSelectedTab(newTab);

            // show which button is selected:
            if (!newTab.equals(oldTab)){
                changeTabAsset(oldTab, false);
            }
            changeTabAsset(newTab, true);

            //replace frag:
            Fragment frag = newTab.getNewFragment();
            android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            if (oldPos < newTab.getPosition() && !forceSelect) {
                ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            } else if (oldPos > newTab.getPosition() && !forceSelect) {
                ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
            ft.replace(R.id.mainFrame, frag, "current");
            ft.commit();
        } else {
            if (TabLauncher.SHARE_TAG.equals(newTab.getLauncherTag()) && (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)){
                String message = getString(R.string.permission_missing) + ".\n" + getString(R.string.permission_request_2) + ".";
                showDialogOKorCancel(message, new AyDialogFragment.AyClickListener() {
                    @Override
                    public void onButtonClicked(View v) {
                        Intent i = new Intent();
                        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        i.addCategory(Intent.CATEGORY_DEFAULT);
                        i.setData(Uri.parse("package:" + getPackageName()));
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                        startActivity(i);
                    }
                });
                return;
            }
            newTab.onTabClicked(this);
        }

    }

    private void changeTabAsset(TabLauncher tl, boolean isSelected){
        ImageView iv = findViewById(tl.getIvId());
        if (iv != null){
            int res = isSelected? tl.getSelectedSrcId() : tl.getSrcId();
            iv.setImageResource(res);
        }
        TextView tv = findViewById(tl.getTvId());
        if (tv != null){
            tv.setTextColor(isSelected ? Color.BLACK : getResources().getColor(R.color.color_tab_unselected));
        }
    }

    private void updateApprovalBadge(){
        int num = AdinlayApp.getNumberOfApprovedPosts();
        if (AdinlayApp.isOneApprovedPostRemoved()){
            AdinlayApp.setIsOneApprovedPostRemoved(false);
            model.fetchApprovedPosts();
            num = num == 0 ? 0 : --num;
            if (AdinlayApp.getUser() != null && AdinlayApp.getUser().socialMediaTestIncomplete()){
                ServerManager.getUpdatedUserInfo(null);
            }
        }
        if (num < 1){
            badgeTv.setVisibility(View.GONE);
        } else {
            badgeTv.setVisibility(View.VISIBLE);
            String numStr = num < 10 ? " " + num + " " : "" + num;
            badgeTv.setText(numStr);
        }
    }

    View.OnClickListener tabsBarListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view.getTag(R.id.tab_launcher_tag) instanceof TabLauncher){
                selectTab((TabLauncher) view.getTag(R.id.tab_launcher_tag), false);
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Timber.i("mainactivity_ onActivityResult");
        switch (requestCode){
            case R.id.intent_share_new_post & 0x0000FFFF:
                if (resultCode == Activity.RESULT_OK){
//                    Timber.i("share_dbg");
                    model.setRefreshPosts();
                }
                if (resultCode == AyUtils.MISSING_MEDIA_RESULT){
                    selectTab(tabStrip.getTabs().get(TabLauncher.PROFILE_TAG), true);
                    return;
                }
                selectTab(tabStrip.getTabs().get(TabLauncher.HOME_TAG), true);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateApprovalBadge(); // update badge may trigger fetch, so we first update the badge, only then check last time fetched
        long now = System.currentTimeMillis();
        boolean testPending = AdinlayApp.getUser() != null && AdinlayApp.getUser().socialMediaTestIsPending();
//        Timber.i("NFT_ onResume update? %s", (now - AyUtils.getLongPref(AyUtils.PREF_LAST_UPDATE_APPROVED_long, 0) > AyUtils.WAIT_INTERVAL_UPDATE_APPROVED));
        if (testPending || now - AyUtils.getLongPref(AyUtils.PREF_LAST_UPDATE_APPROVED_long, 0) >
                AyUtils.WAIT_INTERVAL_UPDATE_APPROVED){
            model.fetchApprovedPosts();
            if (AdinlayApp.getUser() != null && AdinlayApp.getUser().socialMediaTestIncomplete()){
                ServerManager.getUpdatedUserInfo(null);
            }
        }
        if (AyUtils.getStringPref(AyUtils.PREF_UPDATE_WALLET) != null){
            AyUtils.saveStringPreference(AyUtils.PREF_UPDATE_WALLET, null);
            if (tabStrip.getSelectedTab() != null && TabLauncher.WALLET_TAG.equals(tabStrip.getSelectedTab().getLauncherTag())) {
                model.fetchWalletInfo();
            }
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(fcmReceiver,
                new IntentFilter(getString(R.string.filter_approvals_action)));
        if (AyUtils.needsUpdate()){
            AyUtils.setNeedUpdate(false);
            model.getPosts(false);
            model.getBrands();
        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(fcmReceiver);
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (AdinlayApp.getUser() == null){
            // lost state - go back to splash
            Intent intent = new Intent(MainActivity.this, SplashActivity.class);
            startActivity(intent);
            finish();
            return;
        }
        if (AyUtils.getStringPref(AyUtils.PREF_UPDATE_FCM_REQUIRED) != null){
            model.reFetchFcmToken();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (dialog != null){
            dialog.dismiss();
            dialog = null;
        }
    }

    @Override
    public void onBackPressed() {
        TabLauncher currTab = tabStrip.getSelectedTab();
        TabLauncher homeTab = tabStrip.getTabs().get(TabLauncher.HOME_TAG);
        if (currTab != null && !currTab.equals(homeTab)){
            selectTab(tabStrip.getTabs().get(TabLauncher.HOME_TAG), false);
            return;
        } else if (currTab != null){
            android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
            Fragment fragment = fm.findFragmentByTag("current");
            if (fragment instanceof  HomeFrag && ((HomeFrag) fragment).switchToAllAdsIfNeeded()){
                return;
            }
        }
        super.onBackPressed();
    }
}
