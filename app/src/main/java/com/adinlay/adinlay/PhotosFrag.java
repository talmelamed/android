package com.adinlay.adinlay;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;

public class PhotosFrag extends Fragment {

    EditPostViewModel model = null;
    private WeakReference<ItemClick> wr_itemClick = new WeakReference<ItemClick>(null);

    protected RecyclerView recyclerView = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(EditPostViewModel.class);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_recyclerview, container, false);

        if (model == null && getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(EditPostViewModel.class);
        }

        recyclerView = root.findViewById(R.id.recycler);
        recyclerView.setPadding(0, 0,0,0);
        recyclerView.setHasFixedSize(true);

        root.findViewById(R.id.refresh_layout).setEnabled(false);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(inflater.getContext(), 3);
        recyclerView.setLayoutManager(layoutManager);

        GlideRequests gr = getActivity() == null ? GlideApp.with(this) : GlideApp.with(getActivity());
        RecyclerView.Adapter adapter = new GalleryRecyclerAdapter(model.getPhotosList(),
                3, gr, -1, wr_itemClick.get());
        recyclerView.setAdapter(adapter);


        return root;
    }

    public void setItemClickCallback(ItemClick callback){
        wr_itemClick = new WeakReference<ItemClick>(callback);
    }

}
