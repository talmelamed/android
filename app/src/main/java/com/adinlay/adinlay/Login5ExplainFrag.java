package com.adinlay.adinlay;


import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Noya on 6/5/2018.
 */

public class Login5ExplainFrag extends LoginFrag {

    private static final String TAG = Login5ExplainFrag.class.getSimpleName();
    LoginViewModel viewModel;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_login5_explanation, container, false);

        String heading = "";
        String explanation = getResources().getString(R.string.login5_explanation);
        if (SignInManager.EXPLAIN_YOU_STAGE.equals(AdinlayApp.getLoginStage())){
            String name = AdinlayApp.getUserNameUppercase();
            heading = name.isEmpty() ? "" : name + ",";
        } else if (SignInManager.EXPLAIN_INCOMPLETE_STAGE.equals(AdinlayApp.getLoginStage())){
            heading = getResources().getString(R.string.reg_incomplete);
            explanation = getResources().getString(R.string.incomplete_txt);
        } else if (SignInManager.EXPLAIN_FOLLOWERS_STAGE.equals(AdinlayApp.getLoginStage())){
            heading = getResources().getString(R.string.followers_heading);
            explanation = getResources().getString(R.string.followers_explanation);
        }
        TextView tv = root.findViewById(R.id.login5_name_tv);
        tv.setText(heading);
        TextView tvBody = root.findViewById(R.id.login5_expln_body);
        tvBody.setText(explanation);

        viewModel.updateFlowData(R.drawable.group_next_profile, getResources().getString(R.string.next_allcaps), false, true);
        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = ViewModelProviders.of(getActivity()).get(LoginViewModel.class);
    }

    @Override
    public boolean onMainClicked() {
        return true;
    }

    @Override
    public boolean onBackClicked() {
        return false;
    }

    @Override
    protected String getFragStageName() {
        return AdinlayApp.getLoginStage();
    }
}
