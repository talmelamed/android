package com.adinlay.adinlay;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Browser;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.objects.Offer;

import java.util.ArrayList;

/**
 * Created by Noya on 7/17/2018.
 */

public class OffersRvAdapter extends RecyclerView.Adapter<OffersRvAdapter.ViewHolder> {

    private static final String TAG = OffersRvAdapter.class.getSimpleName();

    private ArrayList<Offer> offersList;
    private GlideRequest<Drawable> requestBuilder;

    public OffersRvAdapter(@NonNull ArrayList<Offer> offersList, GlideRequests glideRequests) {
        this.offersList = offersList;
        requestBuilder = glideRequests.asDrawable().centerInside();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_offer, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Offer offer = offersList.get(position);

        holder.title.setText(offer.getTitle());
//        holder.desc.setText(offer.getDesc());
        if (!offer.getPhotoUrl().isEmpty()) {
//            Timber.i("offer getW: %s size: %s url: %s", holder.offerIv.getWidth(), AyUtils.convertDpToPixel(100, null), offer.getPhotoUrl());
            GlideRequest<Drawable> currRequest = requestBuilder.clone().load(offer.getPhotoUrl());
            int resize = holder.offerIv.getWidth() > 0 ? holder.offerIv.getWidth() : AyUtils.convertDpToPixel(100, null);
            if (resize > 0){
                currRequest = currRequest.override(resize, resize);
            }
            currRequest.into(holder.offerIv);
        }

        holder.offerButton.setTag(R.id.link_tag, offer.getOfferLink());
        holder.offerButton.setOnClickListener(offerClickListener);
    }

    @Override
    public int getItemCount() {
        return offersList.size();
    }

    private View.OnClickListener offerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (v.getTag(R.id.link_tag) instanceof String &&
                    !(((String) v.getTag(R.id.link_tag)).isEmpty())){
                String link = (String) v.getTag(R.id.link_tag);
                Uri intentUri = Uri.parse(link);
                Intent webIntent = new Intent(Intent.ACTION_VIEW, intentUri);

                String token = AyUtils.getDecryptedToken();
                if (token != null){
                    Bundle bundle = new Bundle();
                    bundle.putString(ServerManager.HEADRER, "JWT: " + token);
                    webIntent.putExtra(Browser.EXTRA_HEADERS, bundle);
                }

                v.getContext().startActivity(webIntent);
            }
        }
    };

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView title;
        TextView desc;
        ImageView offerIv;
        View offerButton;

        public ViewHolder(View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.offer_title);
            desc = itemView.findViewById(R.id.offer_desc);
            offerIv = itemView.findViewById(R.id.offer_iv);
            offerButton= itemView.findViewById(R.id.offer_button);
        }
    }
}
