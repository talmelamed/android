package com.adinlay.adinlay;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;

import timber.log.Timber;

/**
 * Created by Noya on 6/21/2018.
 */

public class VideoActivity extends AppCompatActivity {

    private static final String TAG = VideoActivity.class.getSimpleName();
    public static final String ORIGIN = "origin";

    VideoView videoView;
    boolean fromProfileFrag = false;
    boolean needToStartMain = true;

    private final String MOVIE_URL = "https://d13yhm7y6hvyqo.cloudfront.net/adinlay-movie.mp4";
    private final String SHARE_URL = "https://d13yhm7y6hvyqo.cloudfront.net/adinlay-share.mp4";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);

        videoView = findViewById(R.id.video_view);

        fromProfileFrag = getIntent() != null && "ProfileFrag".equals(getIntent().getStringExtra(ORIGIN));
        needToStartMain = !fromProfileFrag;

//        Timber.i("prep");
//        String path = "android.resource://" + getPackageName() + File.separator + R.raw.adinlay;
        String url = fromProfileFrag? MOVIE_URL : SHARE_URL;
        Uri video = Uri.parse(url);
        videoView.setVideoURI(video);
        videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Timber.w("onError what: %s extra: %s", what, extra);
                if (fromProfileFrag){
                    Toast.makeText(VideoActivity.this, "Sorry, video is not available",
                            Toast.LENGTH_LONG).show();
                }
                // FIXME: 6/21/2018 need videos in format that is good for all devices, probably resolution of 480x360
                // see bvd answer at https://stackoverflow.com/questions/12998820/videoview-plays-a-video-in-a-device-but-not-in-another
                goToMainActivity();
                return true;
            }
        });

        SkipMediaController mediaController = new SkipMediaController(this);
        mediaController.setSkipListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Timber.i("skip");
                View spinner = findViewById(R.id.video_spinner);
                if (spinner != null){
                    spinner.setVisibility(View.VISIBLE);
                }
                if (videoView != null && videoView.isPlaying()){
                    videoView.stopPlayback();
                }
                goToMainActivity();
            }
        });
        mediaController.setAnchorView(videoView);
        videoView.setMediaController(mediaController);
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                goToMainActivity();
            }
        });

        videoView.start();
//        Timber.i("started");

    }

    private void goToMainActivity(){
        if (needToStartMain) {
            needToStartMain = false;
            Intent intent = new Intent(VideoActivity.this, MainActivity.class);
            startActivity(intent);
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        goToMainActivity();
    }
}
