package com.adinlay.adinlay;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.adinlay.adinlay.objects.SettingsRvItem;

import java.util.ArrayList;

/**
 * Created by Noya on 4/12/2018.
 */

public class ProfileSettingsFrag extends Fragment implements ItemClick {

    private static final String TAG = ProfileSettingsFrag.class.getSimpleName();

    RecyclerView rv = null;
//    private MainViewModel model = null;

//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (getActivity() != null){
//            model = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
//        }
//    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_profile_settings, container, false);

//        if (model == null && getActivity() != null){
//            model = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
//        }

        rv = root.findViewById(R.id.profile_settings_rv);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(layoutManager);
        rv.setHasFixedSize(true);
        RecyclerView.Adapter adapter = new SettingsRvAdapter(getSettingsList(), this);
        rv.setAdapter(adapter);

        return root;
    }

    private ArrayList<SettingsRvItem> getSettingsList(){
        ArrayList<SettingsRvItem> items = new ArrayList<>();
        items.add(new SettingsRvItem(R.string.promo_title, R.drawable.button_add));
        items.add(new SettingsRvItem(R.string.contact_support_camel, R.drawable.button_email));
        items.add(new SettingsRvItem(R.string.share_with_friends, R.drawable.button_share));
        items.add(new SettingsRvItem(R.string.terms_and_conditions_camel, R.drawable.button_view));
        items.add(new SettingsRvItem(R.string.privacy_policy_camel, R.drawable.button_view));
//        if (AdinlayApp.getUser() != null && AdinlayApp.getUser().isDeveloper() && !AdinlayApp.getUserId().isEmpty()){
//            int stringResId = AyUtils.getBooleanPref(AyUtils.PREF_IG_LOGIN_IN_CLIENT + AdinlayApp.getUserId()) ?
//                    R.string.dev_is_on : R.string.dev_is_off;
//            items.add(new SettingsRvItem(stringResId, R.drawable.button_edit));
//        }
        return items;
    }

    @Override
    public void onItemClicked(Object object) {
        if (object instanceof Integer){
            int objectId = ((Integer)object);
            switch (objectId) {
//                case R.string.account:
//                    AdinlayApp.setLoginStage(SignInManager.UPDATE_CONFIRM_STAGE);
//                    Intent intent = new Intent(getActivity(), Login2FlowActivity.class);
//                    intent.putExtra("isSingleUpdate", "isSingleUpdate");
//                    startActivity(intent);
//                    break;
                case R.string.terms_and_conditions_camel:
                case R.string.privacy_policy_camel:
                    Intent privacyIntent = new Intent(getActivity(), TermsActivity.class);
                    if (objectId == R.string.privacy_policy_camel) {
                        privacyIntent.putExtra("privacy", "privacy");
                    }
                    startActivity(privacyIntent);
                    break;
                case R.string.promo_title:
                    Intent promoIntent = new Intent(getActivity(), PromoCodeActivity.class);
                    startActivity(promoIntent);
                    break;
                case R.string.contact_support_camel:
                    Intent sendEmailIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sendEmailIntent.setType("plain/text");
                    sendEmailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,new String[] {getResources().getString(R.string.support_enmail) });
//                    sendEmailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
//                    sendEmailIntent.putExtra(android.content.Intent.EXTRA_TEXT,"");
                    startActivity(Intent.createChooser(sendEmailIntent, "Send email..."));
                    break;
                case R.string.share_with_friends:
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.share_app_title));
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getResources().getString(R.string.share_app_text));
                    startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.share_with_friends)));
                    break;
//                case  R.string.dev_is_on:
//                case  R.string.dev_is_off:
//                    if (AdinlayApp.getUser() != null && AdinlayApp.getUser().isDeveloper() && !AdinlayApp.getUserId().isEmpty()){
//                        boolean changeTo = objectId == R.string.dev_is_off; // if Dev-Mode-Off, we want to change to true (Dev-Mode-On)
//                        AyUtils.setBooleanPref(AyUtils.PREF_IG_LOGIN_IN_CLIENT + AdinlayApp.getUserId(), changeTo);
//                    }
//                    RecyclerView.Adapter adapter = new SettingsRvAdapter(getSettingsList(), this);
//                    rv.setAdapter(adapter);
//                    break;
            }
        }
    }
}
