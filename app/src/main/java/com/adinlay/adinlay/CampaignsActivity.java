package com.adinlay.adinlay;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.adinlay.adinlay.objects.Ad;
import com.adinlay.adinlay.objects.DraftPost;

import timber.log.Timber;

public class CampaignsActivity extends BasicActivity implements ItemClick {

    SelectedAdViewModel viewModel;
    private TextView approvedButtonTv = null;
    private TextView approvedBadge = null;
    private ImageView approvedIcon = null;
    private View approvedButton = null;

    BroadcastReceiver fcmReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent == null ? null : intent.getExtras();
            if (extras != null && AyUtils.UPDATE_VAL.equals(extras.getString(AyUtils.APPROVED_KEY))){
                viewModel.fetchApprovedPosts();
                if (AdinlayApp.getUser() != null && AdinlayApp.getUser().socialMediaTestIncomplete()){
                    viewModel.getAvailableAds(); // this also updates the socialMediaStatus
                }
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = ViewModelProviders.of(this).get(SelectedAdViewModel.class);

        FrameLayout tabsFrame = findViewById(R.id.tb_top_bar);
        if (tabsFrame.getChildCount() > 0){
            tabsFrame.removeAllViews();
        }
        tabsFrame.addView(getLayoutInflater().inflate(R.layout.bar_campaigns, tabsFrame, false));
        approvedButtonTv = findViewById(R.id.approved_tv);
        approvedBadge = findViewById(R.id.approved_counter_tv);
        approvedIcon = findViewById(R.id.approved_icon_iv);
        approvedButton = findViewById(R.id.approved_btn);

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        CampaignsRvFrag frag = new CampaignsRvFrag();
        frag.setItemClik(this);
        ft.add(R.id.tb_mainFrame, frag);
        ft.commit();

//        viewModel.fetchFirstImage(this); // let's do thin in onResume

        viewModel.getPingApprovedPosts().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if (SelectedAdViewModel.FETCH_APPROVED_OK.equals(s)){
                    updateApprovedButton();
                }
            }
        });

        if (AyUtils.getMinRecyclerHeightDp() < 0) {
            approvedIcon.post(new Runnable() {
                @Override
                public void run() {
                    WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
                    if (wm == null){
                        return;
                    }
                    Display display = wm.getDefaultDisplay();
                    DisplayMetrics metrics = new DisplayMetrics();
                    display.getMetrics(metrics);
                    int screenHeight = metrics.heightPixels;
                    int screenW = metrics.widthPixels;

                    int[] loc = new int[2];
                    approvedIcon.getLocationOnScreen(loc);
                    int h = loc[1] + screenW;
                    int delta = screenHeight - h - getResources().getDimensionPixelSize(R.dimen.draft_post_strip_h);
                    int dpDelta = AyUtils.convertPixelToDp(delta, getApplicationContext());
                    int desiredDeltaDp = AyUtils.convertPixelToDp(screenW/3, CampaignsActivity.this) + 8;
                    if (dpDelta < desiredDeltaDp){
                        AyUtils.setMinRecyclerHeightDp(desiredDeltaDp, screenW, screenHeight);
                    } else {
                        AyUtils.setMinRecyclerHeightDp(0, screenW, screenHeight);
                    }
//                    Timber.d("MAIN_RESIZE screen-height: %s h: %s delta: %s in dp %s desired dpDelta: %s", screenHeight, h, delta, dpDelta, desiredDeltaDp);
                }
            });
        }

    }

    @Override
    int getLayoutResourceId() {
        return R.layout.two_bar_layout;
    }


    @Override
    public void onItemClicked(Object object) {
        if (object instanceof Ad){
            Ad ad = (Ad) object;
            User user = AdinlayApp.getUser();
            boolean needToRunTest = user != null && user.needsToTestSocialMedia() && !ad.isTestingCampaign();
            boolean testPostIsPending = user != null && user.socialMediaTestIsPending();

            if (!ad.isConnectedToMedia() || needToRunTest || testPostIsPending){

                Bundle args = new Bundle();
                AyDialogFragment.AyClickListener rightListener = null;
                if (!ad.isConnectedToMedia() && !testPostIsPending) {
                    args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.connect_2_soc_med_allCaps);
                    String message = getString(R.string.ad_need2connect_2_soc_med, ad.getTargetMedia(), ad.getTargetMedia());
                    args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, message);
                    args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.later_allcaps);
                    args.putInt(AyDialogFragment.ARG_BUTTON_R_RES, R.string.log_in);
                    rightListener = new AyDialogFragment.AyClickListener() {
                        @Override
                        public void onButtonClicked(View v) {
                            Intent returnIntent = new Intent();
                            setResult(AyUtils.MISSING_MEDIA_RESULT, returnIntent);
                            finish();
                        }
                    };
                } else if (!testPostIsPending) {
                    args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.test_required);
                    args.putInt(AyDialogFragment.ARG_MESSAGE_RES, R.string.choose_test_campaign);
                    args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
                } else {
                    int messageRes = AdinlayApp.getNumberOfApprovedPosts() <=0 ? R.string.wait_for_test_approval : R.string.need_to_publish_test_approval;
                    args.putInt(AyDialogFragment.ARG_MESSAGE_RES, messageRes);
                    args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
                }
                AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
                if (rightListener != null){
                    dialogFragment.setListener(AyDialogFragment.VALUE_RIGHT, rightListener);
                }
                AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
                return;
            }

            String imagePath = viewModel.getFileUrl();
            AdinlayApp.setDraftPost(new DraftPost(imagePath));
            AdinlayApp.setSelectedAd(ad);
            Timber.i("selected ad: %s", object);
            Intent intent = new Intent(CampaignsActivity.this, PrepDraftActivity.class);
            if (imagePath != null) {
                intent.putExtra(AyUtils.SELECTED_IMAGE_PATH, imagePath);
            }
            startActivityForResult(intent, AyUtils.get16BitId(R.id.intent_share_new_post));
        }
    }

    private void updateApprovedButton(){
        int waitingPosts = AdinlayApp.getNumberOfApprovedPosts();
        approvedIcon.setImageResource(R.drawable.ic_a_gray_small);
        Typeface typeface = ResourcesCompat.getFont(this, R.font.assistant);
        approvedButtonTv.setTypeface(typeface);
        if (waitingPosts < 1){
            approvedButtonTv.setTextColor(getResources().getColor(R.color.color_tab_disabled));
            approvedButton.setOnClickListener(null);
            approvedBadge.setVisibility(View.INVISIBLE);
            approvedIcon.setAlpha(0.4f);
        } else {
            approvedButtonTv.setTextColor(getResources().getColor(R.color.color_tab_unselected));
            approvedButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(CampaignsActivity.this, ApprovedPostActivity.class);
                    startActivityForResult(intent, AyUtils.get16BitId(R.id.intent_share_new_post));
                }
            });
            approvedBadge.setVisibility(View.VISIBLE);
            String numStr = waitingPosts < 10 ? " " + waitingPosts + " " : "" + waitingPosts;
            approvedBadge.setText(numStr);
            approvedIcon.setAlpha(1f);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        long now = System.currentTimeMillis();
        boolean testPending = AdinlayApp.getUser() != null && AdinlayApp.getUser().socialMediaTestIsPending(); // ads are also refreshed, but at onResume of frag
        if (testPending || now - AyUtils.getLongPref(AyUtils.PREF_LAST_UPDATE_APPROVED_long, 0) >
                AyUtils.WAIT_INTERVAL_UPDATE_APPROVED){
            viewModel.fetchApprovedPosts();
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(fcmReceiver,
                new IntentFilter(getString(R.string.filter_approvals_action)));
        updateApprovedButton();
        viewModel.fetchFirstImage(this);
    }

    @Override
    protected void onPause() {
        // stop listening to the receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(fcmReceiver);
        super.onPause();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (AdinlayApp.getUser() == null){
            // lost state - let's let mainActivity deal with it
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case R.id.intent_share_new_post & 0x0000FFFF:
                if (resultCode == RESULT_OK) {
                    Intent returnIntent = new Intent();
                    setResult(resultCode, returnIntent);
                    finish();
                }
                break;
        }
    }

    @Override
    public void finish() {
        super.finish();
        AdinlayApp.setSelectedAd(null); // todo: clear whole draft?!
    }
}
