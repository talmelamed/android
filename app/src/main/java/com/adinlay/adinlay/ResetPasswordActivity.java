package com.adinlay.adinlay;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.SignInMethodQueryResult;

public class ResetPasswordActivity extends AppCompatActivity {

    private EditText emailEt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_reset_password);

        findViewById(R.id.toolbar_right_tv).setVisibility(View.GONE);
        TextView btn1 = findViewById(R.id.toolbar_left_tv);
        btn1.setText(getResources().getString(R.string.arrow_and_back));
        btn1.setTextColor(Color.BLACK);
        findViewById(R.id.toolbar_left_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
                finish();
            }
        });

        emailEt = findViewById(R.id.reset_et);

        ImageView stripIv = findViewById(R.id.reset_iv);
        GlideApp.with(this).load("https://d13yhm7y6hvyqo.cloudfront.net/reset-password.jpg")
                .into(stripIv);

        findViewById(R.id.reset_send_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = emailEt.getText().toString();

                if (email.isEmpty() || !email.matches(AyUtils.email_REGEX)){
                    showErrorMessage(getString(R.string.invalid_email));
                    return;
                }

                SignInManager.getmAuth().fetchSignInMethodsForEmail(email).addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
                    @Override
                    public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
                        if (task.isSuccessful() && task.getResult() != null && task.getResult().getSignInMethods() != null &&
                                task.getResult().getSignInMethods().size() > 0 && task.getResult().getSignInMethods().get(0) != null){
                            String method = task.getResult().getSignInMethods().get(0);
                            if ("password".equals(method)){
                                SignInManager.getmAuth().sendPasswordResetEmail(email)
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()){
                                                    finish();
                                                } else {
                                                    showErrorMessage(getString(R.string.reset_password_fail_send));
                                                }
                                            }
                                        });
                                return;
                            }
                            // for any other method: show error message
                            showErrorMessage(getString(R.string.reset_password_fail_method, method));
                        } else {
                            showErrorMessage(getString(R.string.reset_password_fail_no_account));
                        }
                    }
                });
            }
        });

        findViewById(R.id.reset_con_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
            }
        });

    }

    private void showErrorMessage(@NonNull String message){

        Bundle args = new Bundle();
        args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, message);
        args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
        AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
        if (!isFinishing()) {
            AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
        }
    }

    private void hideKeyboard(View v){
        if (v != null){
            v.post(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null){
                        imm.hideSoftInputFromWindow(emailEt.getWindowToken(), 0);
                    }
                }
            });
        }
    }


}
