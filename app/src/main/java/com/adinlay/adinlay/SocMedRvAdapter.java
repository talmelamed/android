package com.adinlay.adinlay;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adinlay.adinlay.objects.SocialMediaRvItem;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import timber.log.Timber;

public class SocMedRvAdapter extends RecyclerView.Adapter<SocMedRvAdapter.SocMedItemViewHolder> {

    ArrayList<SocialMediaRvItem> items;
    private WeakReference<ItemClick> wr_itemClickCallback;
    private boolean isLogin;

    public SocMedRvAdapter(ArrayList<SocialMediaRvItem> items, ItemClick itemClick) {
        this(items, itemClick, true);
    }

    public SocMedRvAdapter(ArrayList<SocialMediaRvItem> items, ItemClick itemClick, boolean isLogin) {
        this.items = items;
        wr_itemClickCallback = new WeakReference<>(itemClick);
        this.isLogin = isLogin;
    }

    @NonNull
    @Override
    public SocMedItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_social_media, parent, false);
        return new SocMedItemViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull SocMedItemViewHolder holder, int position) {
        SocialMediaRvItem item = items.get(position);
//        Timber.i("IGFB_ item %s, for %s, connected? %s", position, item.getName(), item.isConnected());
        holder.icon.setImageResource(item.getIconOnResId());
        holder.icon.setAlpha(item.isConnected()? 1.0f : 0.3f);

        if (isLogin) {
            holder.name.setText(item.getName());
            holder.followers.setText("");
        } else {
//            Timber.i("IGFB_ adding name %s, and followers %s", item.getDisplayName(), item.getFollowersNum());
            holder.name.setText(item.getDisplayName());
            String num = !item.isConnected() ? "" : String.valueOf(item.getFollowersNum());
            String followers = num.isEmpty() ? "" : item.getMedia() == SocialMediaRvItem.SOCIAL_MEDIA_IG ?
                    holder.followers.getResources().getString(R.string.number_of_followers, num) :
                    holder.followers.getResources().getString(R.string.number_of_friends, num);
            holder.followers.setText(followers);
        }
        holder.switchView.setChecked(item.isConnected());
        holder.switchView.setEnabled(false);

        if (isLogin || !item.isConnected()){
            holder.prefsTitle.setVisibility(View.GONE);
            holder.prefsButton.setVisibility(View.GONE);
        } else {
            holder.prefsTitle.setVisibility(View.VISIBLE);
            holder.prefsButton.setVisibility(View.VISIBLE);
            holder.prefsButton.setContentDescription(item.getMedia() + " " + holder.itemView.getResources().getString(R.string.preferences_allcaps));
            holder.prefsButton.setTag(R.id.object_tag, -1*item.getMedia());
            holder.prefsButton.setOnClickListener(itemListener);
        }

        holder.mainClickArea.setContentDescription(item.getName());
        holder.mainClickArea.setTag(R.id.object_tag, item.getMedia());
        holder.mainClickArea.setOnClickListener(itemListener);

        AyUtils.setOddEvenBackgroundColor(position, holder.itemView);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private View.OnClickListener itemListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (wr_itemClickCallback.get() != null){
                wr_itemClickCallback.get().onItemClicked(v.getTag(R.id.object_tag));
            }
        }
    };


    public static class SocMedItemViewHolder extends RecyclerView.ViewHolder{

        ImageView icon;
        TextView name;
        TextView followers;
        SwitchCompat switchView;
        View mainClickArea;
        View prefsTitle;
        View prefsButton;

        public SocMedItemViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.smi_title);
            icon = itemView.findViewById(R.id.smi_iv);
            switchView = itemView.findViewById(R.id.smi_switcher);
            mainClickArea = itemView.findViewById(R.id.smi_main_click_area);
            followers = itemView.findViewById(R.id.smi_followers);
            prefsTitle = itemView.findViewById(R.id.smi_prefs_tv);
            prefsButton = itemView.findViewById(R.id.smi_prefs_edit);
        }
    }
}
