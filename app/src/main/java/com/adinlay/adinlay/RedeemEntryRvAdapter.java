package com.adinlay.adinlay;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adinlay.adinlay.objects.RedeemEntry;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by Noya on 10/11/2018.
 */

public class RedeemEntryRvAdapter extends RecyclerView.Adapter<RedeemEntryRvAdapter.ViewHolder> {

    private ArrayList<RedeemEntry> entriesList;
    private WeakReference<ItemClick> wr_callback;

    public RedeemEntryRvAdapter(@NonNull ArrayList<RedeemEntry> entriesList, ItemClick callback) {
        this.entriesList = entriesList;
        wr_callback = new WeakReference<ItemClick>(callback);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wallet_history_entry, parent, false);

        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        RedeemEntry entry = entriesList.get(position);

        String amount = AyUtils.getStringAmountWithCurrencySymbol(entry.getAmount(),
                AyUtils.DECIMAL_PLACES_DEFAULT, holder.amountTv.getResources());
        holder.amountTv.setText(amount);
        holder.dateTv.setText(AyUtils.parseUtcStringToDdMmYyyy(entry.getDateString()));

        holder.goButton.setTag(R.id.object_tag, entry);
        holder.goButton.setOnClickListener(itemClickListener);

        AyUtils.setOddEvenBackgroundColor(position, holder.itemView);

    }

    @Override
    public int getItemCount() {
        return entriesList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView dateTv;
        TextView amountTv;
        View goButton;

        public ViewHolder(View itemView) {
            super(itemView);

            dateTv = itemView.findViewById(R.id.whe_date);
            amountTv = itemView.findViewById(R.id.whe_amount_tv);
            goButton = itemView.findViewById(R.id.whe_go_button);
        }
    }

    View.OnClickListener itemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (wr_callback.get() != null){
                wr_callback.get().onItemClicked(v.getTag(R.id.object_tag));
            }
        }
    };

}
