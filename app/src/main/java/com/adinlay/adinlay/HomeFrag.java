package com.adinlay.adinlay;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adinlay.adinlay.objects.Post;

import timber.log.Timber;

/**
 * Created by Noya on 4/8/2018.
 */

public class HomeFrag extends android.support.v4.app.Fragment implements PostsRecyclerViewAdapter.ItemClick {

    private static final String TAG = HomeFrag.class.getSimpleName();

    public static final int ALL_ADS_MOSAIC = 0;
    public static final int ALL_ADS = 1;
    public static final int MY_ADS = 2;

    private TextView myAdsTv = null;
    private TextView allAdsTv = null;

    MainViewModel model = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.i("onCreate");
        if (getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_home, container, false);

        if (model == null && getActivity() != null){
            model = ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        }

        myAdsTv = root.findViewById(R.id.my_ads_btn_tv);
        allAdsTv = root.findViewById(R.id.all_ads_btn_tv);

        PostsFrag frag = new PostsFrag();
        frag.setDisplayMode(ALL_ADS_MOSAIC);
        frag.setItemClickCallback(this);
        android.support.v4.app.FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.home_frag_mainFrame, frag, TAG);
        ft.commit();
        updateInMyAdsColor(ALL_ADS_MOSAIC);

        myAdsTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceChildFrag(MY_ADS);
            }
        });

        allAdsTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceChildFrag(ALL_ADS_MOSAIC);
            }
        });

        root.findViewById(R.id.home_tabs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // do nothing, prevent clicks
            }
        });

        return root;
    }

    private void replaceChildFrag(int mode){
        if (!isAdded()){
            return;
        }
        android.support.v4.app.FragmentManager fm = getChildFragmentManager();
        Fragment fragment = fm.findFragmentByTag(TAG);
        if (fragment instanceof PostsFrag){
            int fragMode = ((PostsFrag) fragment).getDisplayMode();
            if (mode == fragMode){
                // for now - just return. perhaps refresh?
                return;
            }

            PostsFrag postsFrag = new PostsFrag();
            postsFrag.setDisplayMode(mode);
            // if changing from allList to mosaic - update mosaic pos (because onPause saved another one
            postsFrag.setItemClickCallback(this);
            FragmentTransaction ft = fm.beginTransaction();
            if (mode == ALL_ADS_MOSAIC){
                ft.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            } else {
                ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            }
            ft.replace(R.id.home_frag_mainFrame, postsFrag, TAG);
            ft.commit();
            updateInMyAdsColor(mode);
        }


    }

    public void updateInMyAdsColor(int mode){
        Context context = getContext() != null ? getContext() : myAdsTv.getContext();
        Typeface typefaceReg = ResourcesCompat.getFont(context, R.font.assistant);
        Typeface typefaceBold = ResourcesCompat.getFont(context, R.font.assistant_bold);
        if (mode == MY_ADS){
            myAdsTv.setTextColor(Color.BLACK);
            myAdsTv.setTypeface(typefaceBold);
            allAdsTv.setTextColor(getResources().getColor(R.color.color_tab_unselected));
            allAdsTv.setTypeface(typefaceReg);
        } else {
            allAdsTv.setTextColor(Color.BLACK);
            allAdsTv.setTypeface(typefaceBold);
            myAdsTv.setTextColor(getResources().getColor(R.color.color_tab_unselected));
            myAdsTv.setTypeface(typefaceReg);
        }
    }


    @Override
    public void onMosaicItemClicked(Post post) {
        if (getActivity() != null && post != null){
            Intent intent = new Intent(getActivity(), SinglePostActivity.class);
            intent.putExtra(SinglePostActivity.POST_EXTRA_KEY, post.getJsonString());
            getActivity().startActivity(intent);
        }
    }

    /**
     * @return true if we replaced frag
     */
    public boolean switchToAllAdsIfNeeded(){
        android.support.v4.app.FragmentManager fm = getChildFragmentManager();
        Fragment fragment = fm.findFragmentByTag(TAG);
        if (fragment instanceof PostsFrag && ((PostsFrag) fragment).getDisplayMode() != ALL_ADS_MOSAIC){
            replaceChildFrag(ALL_ADS_MOSAIC);
            return true;
        }
        return false;
    }

}
