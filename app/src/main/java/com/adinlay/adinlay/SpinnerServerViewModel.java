package com.adinlay.adinlay;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;


public class SpinnerServerViewModel extends ViewModel {

    private MutableLiveData<String> pingServerResponse;
    private MutableLiveData<Boolean> spinnerLiveData;

    public MutableLiveData<String> getPingServerResponse(){
        if (pingServerResponse == null){
            pingServerResponse = new MutableLiveData<>();
        }
        return pingServerResponse;
    }

    public MutableLiveData<Boolean> getSpinnerLiveData() {
        if (spinnerLiveData == null){
            spinnerLiveData = new MutableLiveData<>();
        }
        return spinnerLiveData;
    }


    private void onServerResponse(String pingMessage){
        getSpinnerLiveData().postValue(false);
        getPingServerResponse().postValue(pingMessage);
    }


}
