package com.adinlay.adinlay;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.Loader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 4/21/2018.
 * an asyncTask loader, to fetch gallery images from the device
 */

public class GalleryDataLoader extends android.support.v4.content.AsyncTaskLoader<List<GalleryImageObject>> {

    private static final String[] PROJECTION = new String[] {MediaStore.Images.ImageColumns._ID, MediaStore.MediaColumns.DATA};

    private List<GalleryImageObject> cached;
    private boolean observerRegistered = false;
    private final Loader.ForceLoadContentObserver forceLoadContentObserver = new Loader.ForceLoadContentObserver();


    public GalleryDataLoader(Context context) {
        super(context);
    }

    @Override
    public void deliverResult(@Nullable List<GalleryImageObject> data) {
        if (!isReset() && isStarted()) {
            super.deliverResult(data);
        }
    }

    @Override
    protected void onStartLoading() {
        if (cached != null) {
            deliverResult(cached);
        }
        if (takeContentChanged() || cached == null) {
            forceLoad();
        }
        registerContentObserver();
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

    @Override
    protected void onReset() {
        super.onReset();

        onStopLoading();
        cached = null;
        unregisterContentObserver();
    }

    @Override
    protected void onAbandon() {
        super.onAbandon();
        unregisterContentObserver();
    }

    @Override
    public List<GalleryImageObject> loadInBackground() {

        List<GalleryImageObject> data = new ArrayList<>();
        Cursor cursor = getContext().getContentResolver()
                .query(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        PROJECTION, null, null, "_id DESC");

        if (cursor == null){
            return data;
        }

        while (cursor.moveToNext()){
            data.add(new GalleryImageObject(cursor));
        }

        cursor.close();

        return data;
    }

    private void registerContentObserver() {
        if (!observerRegistered) {
            ContentResolver cr = getContext().getContentResolver();
            cr.registerContentObserver(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, false,
                    forceLoadContentObserver);
            cr.registerContentObserver(MediaStore.Video.Media.EXTERNAL_CONTENT_URI, false,
                    forceLoadContentObserver);

            observerRegistered = true;
        }
    }

    private void unregisterContentObserver() {
        if (observerRegistered) {
            observerRegistered = false;

            getContext().getContentResolver().unregisterContentObserver(forceLoadContentObserver);
        }
    }


}
