package com.adinlay.adinlay;

import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.adinlay.adinlay.objects.RedeemEntry;

import java.math.BigDecimal;

/**
 * Created by Noya on 7/23/2018.
 */

public class RedeemActivity extends AppCompatActivity {

    private final static String TAG = RedeemActivity.class.getSimpleName();

    public final static String BALANCE_STRING_KEY = "balanceString";
    public final static String BALANCE_FLOAT_KEY = "balanceFloat";

    SharePostViewModel model;

    private TextView msgTv;
    private TextView balanceAmountTv;
    private TextView redeemedEntryAmountTv;
    private View contactSupportTv;
    private View supportEmailTv;
    private View goButton;
    private View redeemButton;
    private View entryFrame;
    private EditText amountInputET;
    private EditText idInputET;
    private float fBalance; // in server currency
    private boolean needsId = false;

    private ContentLoadingProgressBar spinner = null;
    private View spinnerFrame = null;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.redeem);

        // light background to status bar:
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= 23){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
        }

        model = ViewModelProviders.of(this).get(SharePostViewModel.class);

        Intent intent = getIntent();
        String balanceString = intent.getStringExtra(BALANCE_STRING_KEY);
        fBalance = intent.getFloatExtra(BALANCE_FLOAT_KEY, 0);
        if (balanceString == null || fBalance == 0){
            finish();
            return;
        }

        findViewById(R.id.toolbar_right_tv).setVisibility(View.GONE);
        TextView btn1 = findViewById(R.id.toolbar_left_tv);
        btn1.setText(getResources().getString(R.string.arrow_and_back));
        btn1.setTextColor(Color.BLACK);
        findViewById(R.id.toolbar_left_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        msgTv = findViewById(R.id.redeem_msg1);
        contactSupportTv = findViewById(R.id.redeem_msg2);
        supportEmailTv = findViewById(R.id.redeem_msg2_support_email);
        balanceAmountTv = findViewById(R.id.redeem_balance_tv);
        entryFrame = findViewById(R.id.redeem_whe_frame);
        redeemedEntryAmountTv = findViewById(R.id.whe_amount_tv);
        goButton = findViewById(R.id.whe_go_button);
        redeemButton = findViewById(R.id.redeem_button_iv);
        amountInputET = findViewById(R.id.redeem_amount_et);
        idInputET = findViewById(R.id.redeem_id_et);
        spinnerFrame = findViewById(R.id.redeem_spinner_frame);
        spinner = findViewById(R.id.redeem_spinner);

        balanceAmountTv.setText(balanceString);

        String text = getResources().getString(R.string.redeem_confirm_email, AdinlayApp.getUserEmail());
        msgTv.setText(AyUtils.getSpannable(text, AdinlayApp.getUserEmail(), "#B3000000"), TextView.BufferType.SPANNABLE);

//        contactSupportTv.setText(AyUtils.getSpannable(getResources().getString(R.string.redeem_contact_support),
//                "support@adinlay.com", "#0000EE"), TextView.BufferType.SPANNABLE);

        supportEmailTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendEmailIntent = new Intent(android.content.Intent.ACTION_SEND);
                sendEmailIntent.setType("plain/text");
                sendEmailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,new String[] {getResources().getString(R.string.support_enmail) });
//                sendEmailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
//                sendEmailIntent.putExtra(android.content.Intent.EXTRA_TEXT,"");
                startActivity(Intent.createChooser(sendEmailIntent, "Send email..."));
            }
        });

        entryFrame.setVisibility(View.GONE);


        User user = AdinlayApp.getUser();
        if (user == null){
            finish();
            return;
        }
        needsId = user.isCurrencyIls();

        if (!needsId){
            idInputET.setVisibility(View.GONE);
        } else {
            idInputET.setVisibility(View.VISIBLE);
            amountInputET.setHint(R.string.hint_amount_ils);
            findViewById(R.id.redeem_currency_xcng_explain).setVisibility(View.VISIBLE);
        }

        model.getSpinnerLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean show) {
                if (show == null || !show){
                    spinner.hide();
                    spinnerFrame.setVisibility(View.GONE);
                } else {
                    spinnerFrame.setVisibility(View.VISIBLE);
                    spinner.show();
                }
            }
        });

        spinnerFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do nothing - prevent further clicks
            }
        });

        model.getPingServerResponse().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                boolean success = SharePostViewModel.SHARE_OK.equals(s);
                if (success) {
                    RedeemEntry entry = model.getRedeemEntry();
                    if (entry != null){
                        // change layout:
                        redeemButton.setOnClickListener(null);
                        redeemButton.setVisibility(View.GONE);
                        balanceAmountTv.setTextColor(getResources().getColor(R.color.disabled_black));
                        ((TextView) findViewById(R.id.redeem_title_balance)).
                                setTextColor(getResources().getColor(R.color.disabled_black));
                        msgTv.setTextColor(Color.BLACK);
                        msgTv.setText(getResources().getString(R.string.redeem_explain_go));
                        contactSupportTv.setVisibility(View.GONE);
                        supportEmailTv.setVisibility(View.GONE);
                        amountInputET.setTextColor(getResources().getColor(R.color.disabled_black));
                        amountInputET.setEnabled(false);
                        idInputET.setTextColor(getResources().getColor(R.color.disabled_black));
                        idInputET.setEnabled(false);

                        // add entry info:
                        entryFrame.setVisibility(View.VISIBLE);
                        String formattedAmount = AyUtils.getStringAmountWithCurrencySymbol(entry.getAmount(),
                                AyUtils.DECIMAL_PLACES_DEFAULT, redeemedEntryAmountTv.getResources());
                        redeemedEntryAmountTv.setText(formattedAmount);

                        TextView date = findViewById(R.id.whe_date);
                        date.setText(AyUtils.parseUtcStringToDdMmYyyy(entry.getDateString()));

                        goButton.setTag(R.id.object_tag, entry.getUri());
                        goButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (v.getTag(R.id.object_tag) instanceof String){
                                    String uriPath = (String) v.getTag(R.id.object_tag);
                                    Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(uriPath));
                                    startActivity(webIntent);
                                }
                            }
                        });
                        Intent returnIntent = new Intent();
                        setResult(Activity.RESULT_OK, returnIntent);
                        return;
                    }
                }
                // if we got here - something went wrong. show dialog:
                s = s== null || s.isEmpty() || success ? "something went wrong" : s;
                showErrorDialog(s);
            }
        });

        redeemButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                hideKeyboard(v);

                String amountStr = amountInputET.getText().toString();
                if (amountStr.isEmpty()){
                    Toast.makeText(getApplicationContext(),
                            "Please provide an amount", Toast.LENGTH_SHORT).show();
                    return;
                }
                float userRequestAmount = -1f;
                try {
                    BigDecimal bigDecimal = new BigDecimal(amountStr);
                    User user = AdinlayApp.getUser();
                    if (user != null && user.isCurrencyIls() && user.getUserExchangeRate().compareTo(BigDecimal.ZERO) != 0) {
                        int scale = bigDecimal.scale() > AyUtils.DECIMAL_PLACES_DEFAULT ? bigDecimal.scale() : AyUtils.DECIMAL_PLACES_CURRENCY;
                        bigDecimal = bigDecimal.divide(user.getUserExchangeRate(), scale, BigDecimal.ROUND_DOWN);
//                        Timber.i("REDEEM_ we had %s after exchange rate: %s", amountStr, bigDecimal);
                    }
                    userRequestAmount = bigDecimal.floatValue();
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Please provide a number amount", Toast.LENGTH_SHORT).show();
                    return;
                }

                String message = null;
                if (userRequestAmount <= 0){
                    message = "Please provide an amount to redeem";
                } else if (userRequestAmount > fBalance){
                    message = "Amount cannot exceed your balance";
                }
                if (message != null){
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                    return;
                }

                String id = null;
                String location = null;
                if (needsId){
                    location = User.ISRAEL_COUNTRY_CODE;
                    id = idInputET.getText().toString();
                    if (id.length() != 9){
                        Toast.makeText(getApplicationContext(),
                                "Please provide a 9 digit ID number", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                model.sendRedeemRequestToServer(userRequestAmount, location, id);

            }
        });

        findViewById(R.id.redeem_layout_root).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
            }
        });

    }


    private void showErrorDialog(String message){

        Bundle args = new Bundle();
        args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.error);
        args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, message);
        args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
        AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
        if (!isFinishing()) {
            AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (AdinlayApp.getUser() == null){
            // lost state - let's let previous activity deal with it
            finish();
            return;
        }
    }

    private void hideKeyboard(View v){

        if (v != null){
            v.post(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null){
                        imm.hideSoftInputFromWindow(balanceAmountTv.getWindowToken(), 0);
                    }
                }
            });
        }
    }

}
