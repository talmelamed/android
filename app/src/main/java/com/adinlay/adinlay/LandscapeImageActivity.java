package com.adinlay.adinlay;

import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by Noya on 10/22/2018.
 */

public class LandscapeImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_just_iv);

        setSupportActionBar(null);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= 23){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else if (Build.VERSION.SDK_INT >= 21){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
        }

        ImageView mainIv = findViewById(R.id.item_iv);

        Bitmap bmp = AdinlayApp.getDraftPost() == null ? null :
                AdinlayApp.getDraftPost().getDraftPostBitmap().copy(Bitmap.Config.ARGB_8888, true);
        if (bmp == null || AdinlayApp.getSelectedAd() == null){
            finish();
            return;
        }

        mainIv.setImageBitmap(bmp);
        showExitMessage();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            finish();
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        showExitMessage();
    }

    private void showExitMessage(){
        Toast.makeText(getApplicationContext(),
                getResources().getString(R.string.flip_to_exit), Toast.LENGTH_SHORT).show();
    }
}
