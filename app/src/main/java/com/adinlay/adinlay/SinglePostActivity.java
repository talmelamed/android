package com.adinlay.adinlay;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.ablanco.zoomy.Zoomy;
import com.adinlay.adinlay.objects.Brand;
import com.adinlay.adinlay.objects.Offer;
import com.adinlay.adinlay.objects.Post;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import timber.log.Timber;

public class SinglePostActivity extends AppCompatActivity {

    final private static String TAG = SinglePostActivity.class.getSimpleName();
    final public static String POST_EXTRA_KEY = "post_extra";

    ImageView postIv = null;
//    View adinLikeFrame = null;
    Post post = null;

    private RecyclerView recyclerView = null;
    private RecyclerView.Adapter adapter = null;
    private LinearLayoutManager layoutManager = null;

//    SimpleTarget<Bitmap> glideTarget = new SimpleTarget<Bitmap>() {
//        @Override
//        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
//            postIv.setImageBitmap(resource);
////            adinLikeFrame.setVisibility(View.VISIBLE);
//        }
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_post);

        setSupportActionBar(null);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= 23){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
        }

        postIv = findViewById(R.id.spa_ad_iv);
//        adinLikeFrame = findViewById(R.id.sp_adinlike_frame);
        recyclerView = findViewById(R.id.offers_recycler);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, layoutManager.getOrientation());
//        recyclerView.addItemDecoration(dividerItemDecoration); // todo: add if we have more than one item

        // get the post:
        Intent intent = getIntent();
        JSONObject jPost = null;
        if (intent.hasExtra(POST_EXTRA_KEY)){
            String postString = intent.getStringExtra(POST_EXTRA_KEY);
            if (postString != null && !postString.isEmpty()){
                try {
                    jPost = new JSONObject(postString);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        if (jPost == null){
            finish();
            return;
        }
        post = new Post(jPost);
        boolean isFcbk = post.checkPlatform(Post.PLATFORM_FACEBOOK);
        boolean isFcbkLinkDimen = post.isFacebookLinkSize();

        // let's load the thumbnail first:
        String thumbPath = post.getThmbnailUrl();
        if (!thumbPath.isEmpty()){
            GlideApp.with(this)
                    .load(thumbPath)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .centerCrop()
                    .into(postIv);
        }

        if (isFcbkLinkDimen){
            ConstraintLayout.LayoutParams lpIv  = (ConstraintLayout.LayoutParams) postIv.getLayoutParams();
            lpIv.dimensionRatio = "1200:630";
            loadIvTarget();
            constrainRecyclerUnderIv();
        } else {
            // if bottom part is less than 150dp, give it at least 0.25 of screen:
            postIv.post(new Runnable() {
                @Override
                public void run() {

                    WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
                    if (wm == null){
                        loadIvTarget();
                        return;
                    }
                    Display display = wm.getDefaultDisplay();
                    DisplayMetrics metrics = new DisplayMetrics();
                    display.getMetrics(metrics);
                    int screenHeight = metrics.heightPixels;

                    int[] loc = new int[2];
                    postIv.getLocationOnScreen(loc);
                    int h = loc[1] +postIv.getHeight();
                    int delta = screenHeight - h;
                    int dpDelta = AyUtils.convertPixelToDp(delta, getApplicationContext());
                    Timber.d("OFFER_RESIZE screen-height: %s iv.h: %s delta: %s  in dp: %s", screenHeight, h, delta, dpDelta);
                    if (dpDelta < 150){
                        ConstraintLayout.LayoutParams lpIv = (ConstraintLayout.LayoutParams) postIv.getLayoutParams();
                        lpIv.dimensionRatio = "w,1:1";
                        lpIv.bottomToTop = R.id.single_post_guideline;
                        Timber.d( "OFFER_RESIZE changed iv lp");
                        postIv.requestLayout();
                    } else {
                        constrainRecyclerUnderIv();
                    }
                    loadIvTarget();
                }
            });
        }

        findViewById(R.id.toolbar_right_tv).setVisibility(View.GONE);
        TextView btn1 = findViewById(R.id.toolbar_left_tv);
        btn1.setText(getResources().getString(R.string.arrow_and_back));
        btn1.setTextColor(Color.BLACK);
        findViewById(R.id.toolbar_left_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        int mediaRes = post.checkPlatform(Post.PLATFORM_INSTAGRAM) ? R.drawable.page_1_c :
                isFcbk ? R.drawable.facebook_c : 0;
        ImageView mediaIv = findViewById(R.id.social_indicators);
        mediaIv.setImageResource(mediaRes);

        ArrayList<Offer> offers = getPostOffers();
        if (offers.size() > 0){
            adapter = new OffersRvAdapter(offers, GlideApp.with(this));
            recyclerView.setAdapter(adapter);
        } else {
            findViewById(R.id.sp_no_offers_message).setVisibility(View.VISIBLE);
        }

        Zoomy.Builder zoomyBuilder = new Zoomy.Builder(this).
                target(postIv);
//                .zoomListener(new ZoomListener() {
//            @Override
//            public void onViewStartedZooming(View view) {
//                // hide the like strip
//                adinLikeFrame.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onViewEndedZooming(View view) {
//                // show the like strip
//                adinLikeFrame.setVisibility(View.VISIBLE);
//            }
//        });
        zoomyBuilder.register();

        // todo: need publisher avatar
        TextView publisherName = findViewById(R.id.publisher_name);
        publisherName.setText(post.getDisplayName());
        GlideApp.with(this).load(R.drawable.avtar_icon).transform(new CircleCrop()).into((ImageView) findViewById(R.id.publisher_photo));

        TextView brandName = findViewById(R.id.sp_brand_name);
        brandName.setText(AyUtils.getBrandName(post.getBrandId()));

    }

    private void loadIvTarget(){
        // display the post:
        if (!post.getPhotoUrl().isEmpty() && !isFinishing()) {
            String thumbUrl = post.getThmbnailUrl().isEmpty() ? post.getPhotoUrl() : post.getThmbnailUrl();
            GlideRequest<Drawable> request = GlideApp.with(this).
                    load(post.getPhotoUrl()).
                    transform(new CenterCrop()).
                    thumbnail(GlideApp.with(this)
                        .load(thumbUrl)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                            .override(300)
                        .centerInside()).
                    diskCacheStrategy(DiskCacheStrategy.NONE).
                    skipMemoryCache(true);
            if (postIv.getWidth() > 0 && postIv.getHeight() > 0){
//                Timber.i("TEMP__ got w: %s", postIv.getWidth());
                request = request.override(postIv.getWidth(), postIv.getHeight());
            }
//            else {
//                Timber.w("TEMP__ got w: %s", postIv.getWidth());
//            }
            request.into(postIv);
        }
    }

    private void constrainRecyclerUnderIv(){
        ConstraintLayout.LayoutParams recyclerLP = (ConstraintLayout.LayoutParams) recyclerView.getLayoutParams();
        recyclerLP.topToBottom = R.id.spa_ad_iv;
        recyclerView.requestLayout();
    }


    private ArrayList<Offer> getPostOffers(){
        ArrayList<Offer> offers = new ArrayList<>();

        String link = post == null ? "" : post.getOfferLink();
        if (link.isEmpty()){
            return offers;
        }

        for (int i = 0; i < 1; i++) { // for now just one
            try {
                JSONObject jOffer = new JSONObject();
                Brand brand = AyUtils.getBrand(post.getBrandId());
                if (brand != null){
                    jOffer.put(Offer.TITLE_KEY, brand.getName());
                    String url = brand.getSmallestLogoUrl();
                    if (!url.isEmpty()) {
                        jOffer.put(Offer.PHOTO_KEY, url);
                    }
                    jOffer.put(Post.OFFER_URL_KEY, link);
                }

                offers.add(new Offer(jOffer));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return offers;
    }


}
