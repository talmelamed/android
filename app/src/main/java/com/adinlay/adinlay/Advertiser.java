package com.adinlay.adinlay;

/**
 * Created by Noya on 4/8/2018.
 */

public class Advertiser {
    private int tempRes;

    public Advertiser(int tempRes) {
        this.tempRes = tempRes;
    }

    public int getTempRes() {
        return tempRes;
    }

    public void setTempRes(int tempRes) {
        this.tempRes = tempRes;
    }

}
