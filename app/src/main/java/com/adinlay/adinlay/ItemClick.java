package com.adinlay.adinlay;

/**
 * Created by Noya on 7/24/2018.
 */

public interface ItemClick {
    void onItemClicked(Object object);
}
