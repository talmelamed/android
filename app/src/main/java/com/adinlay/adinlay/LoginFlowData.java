package com.adinlay.adinlay;

/**
 * Created by Noya on 5/26/2018.
 */

public class LoginFlowData {

    private static final String TAG = LoginFlowData.class.getSimpleName();

    private String mainButtonText;
    private int mainButtonRes;
    private boolean showBack;
    private boolean showLogo;
    private boolean showTermsLink;


    public LoginFlowData(int mainButtonRes, String mainButtonText, boolean showBack, boolean showLogo, boolean showTermsLink){
        this.mainButtonRes = mainButtonRes;
        this.mainButtonText = mainButtonText;
        this.showLogo = showLogo;
        this.showBack = showBack;
        this.showTermsLink = showTermsLink;
    }


    public String getMainButtonText() {
        return mainButtonText;
    }

    public int getMainButtonRes() {
        return mainButtonRes;
    }

    public void setMainButtonText(String mainButtonText) {
        this.mainButtonText = mainButtonText;
    }

    public boolean showLogo() {
        return showLogo;
    }

    public void setShowLogo(boolean showLogo) {
        this.showLogo = showLogo;
    }

    public boolean showBackButton() {
        return showBack;
    }

    public void setShowBack(boolean showBack) {
        this.showBack = showBack;
    }

    public boolean isShowTermsLink() {
        return showTermsLink;
    }
}
