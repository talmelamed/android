package com.adinlay.adinlay;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

public class PromoDoneActivity extends BasicActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        findViewById(R.id.toolbar_right_tv).setVisibility(View.GONE);
        View leftTv = findViewById(R.id.toolbar_left_tv);
        leftTv.setVisibility(View.GONE);


        leftTv.postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 1500);

    }

    @Override
    int getLayoutResourceId() {
        return R.layout.activity_promo_done;
    }

    @Override
    public boolean shouldSetToolbarWithBack() {
        return false;
    }
}
