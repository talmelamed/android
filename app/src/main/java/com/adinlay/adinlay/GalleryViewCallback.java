package com.adinlay.adinlay;

import android.support.v7.util.AsyncListUtil;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.lang.ref.WeakReference;

/**
 * Created by Noya on 4/17/2018.
 */

public class GalleryViewCallback extends AsyncListUtil.ViewCallback {

    WeakReference<RecyclerView> wr_recyclerView;

    public GalleryViewCallback(RecyclerView recyclerView) {
        wr_recyclerView = new WeakReference<RecyclerView>(recyclerView);
    }

    @Override
    public void getItemRangeInto(int[] outRange) {
        if (outRange == null){
            return;
        }
        RecyclerView rv = wr_recyclerView.get();
        if (rv != null){
            LinearLayoutManager lm = (LinearLayoutManager) rv.getLayoutManager();
            outRange[0] = lm.findFirstVisibleItemPosition();
            outRange[1] = lm.findLastVisibleItemPosition();
        }

        if (outRange[0] == -1 && outRange[1] == -1) {
            outRange[0] = 0;
            outRange[1] = 0;
        }
    }

    @Override
    public void onDataRefresh() {
        RecyclerView rv = wr_recyclerView.get();
        if (rv != null && rv.getAdapter() != null){
            rv.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void onItemLoaded(int position) {
        RecyclerView rv = wr_recyclerView.get();
        if (rv != null && rv.getAdapter() != null){
            rv.getAdapter().notifyItemChanged(position);
        }
    }
}
