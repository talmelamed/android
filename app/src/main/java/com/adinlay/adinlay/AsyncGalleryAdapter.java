package com.adinlay.adinlay;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Looper;
import android.support.v7.util.AsyncListUtil;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;

import timber.log.Timber;

/**
 * Created by Noya on 4/17/2018.
 */

public class AsyncGalleryAdapter extends RecyclerView.Adapter<AsyncGalleryAdapter.ViewHolder> {

    private static final String TAG = AsyncGalleryAdapter.class.getSimpleName();

    private GalleryDataCallback dataCallback;
    private AsyncListUtil<GalleryImageObject> asyncListUtil;
    private RecyclerView.OnScrollListener scrollListener;

    private int spanCount;
    private int size = 0;


    public AsyncGalleryAdapter(Context context, RecyclerView recyclerView, int spanCount) {
        this.spanCount = spanCount;
        dataCallback = new GalleryDataCallback(context);
        asyncListUtil = new AsyncListUtil<>(GalleryImageObject.class,
                100,
                dataCallback,
                new GalleryViewCallback(recyclerView));
        scrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                asyncListUtil.onRangeChanged();
            }
        };
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_advertiser, parent, false);

        GridLayoutManager.LayoutParams lp = (GridLayoutManager.LayoutParams) root.getLayoutParams();
        lp.height = parent.getMeasuredWidth() / spanCount;
        size = lp.height;

        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GalleryImageObject galleryImage = asyncListUtil.getItem(position);
        boolean isCam = galleryImage != null && galleryImage.getId() == GalleryImageObject.CAM_ID;

        holder.iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
        int id = galleryImage == null ? GalleryImageObject.NO_ID : galleryImage.getId();
        holder.iv.setTag(R.id.image_id_tag, id);

        if (galleryImage == null || galleryImage.getFileUrl().isEmpty() && !isCam){
            holder.iv.setImageResource(android.R.drawable.ic_menu_gallery);
            Timber.d("ALU_DBG got null item!!!!");
            return;
        }
        if (isCam){
            holder.iv.setImageResource(android.R.drawable.ic_menu_camera);
            Timber.d("ALU_DBG setting image for CAM");
        } else {
//            String path = galleryImage.getFileUrl().startsWith("file") ?
//                    galleryImage.getFileUrl() : "file://" + galleryImage.getFileUrl(); // loading url to IV requires the "file://" prefix
            Timber.d("ALU_DBG path for item is: %s", galleryImage.getFileUrl());
            decodeAndLoadImage(galleryImage, size, holder.iv);
        }
    }

    @Override
    public int getItemCount() {
        return asyncListUtil.getItemCount();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView iv;

        public ViewHolder(View itemView) {
            super(itemView);
            this.iv = itemView.findViewById(R.id.advertiser_iv);
        }
    }

    public void onStart(RecyclerView recyclerView){
        dataCallback.refreshData();
        recyclerView.addOnScrollListener(scrollListener);
    }

    public void onStop(RecyclerView recyclerView){
        recyclerView.removeOnScrollListener(scrollListener);
        dataCallback.closeCursor();
    }

    private void decodeAndLoadImage(final GalleryImageObject gio, final int scaleSize, ImageView imageView){
        final WeakReference<ImageView> wrImageView = new WeakReference<ImageView>(imageView);

        new Thread(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap = getSizedBitmap(gio.getFileUrl(), scaleSize);

                if (bitmap != null){
                    android.os.Handler mainHandler = new android.os.Handler(Looper.getMainLooper());
                    final Bitmap finalBitmap = bitmap;
                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            ImageView iv = wrImageView.get();
                            if (iv != null) {
                                Integer ivTag = (Integer) iv.getTag(R.id.image_id_tag);
                                if (ivTag != null && ivTag == gio.getId()) {
                                    iv.setImageBitmap(finalBitmap);
                                } else {
                                    Timber.d("ALU_DBG not loading pic, got different gio id vs iv tag: %s --- %s", gio.getId(), ivTag);
                                }
                            }
                        }
                    });
                }
            }
        }).start();

    }

    private Bitmap getSizedBitmap(String path, int scaleSize){
        Bitmap bitmap = null;
        File imgFile = new File(path);
        if (imgFile.exists()){
            InputStream is = null;
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                is = new FileInputStream(imgFile);
                BitmapFactory.decodeStream(is, null, options);

                int scale = 1;
                while (options.outWidth / scale / 2 >= scaleSize &&
                        options.outHeight / scale / 2 >= scaleSize) {
                    scale *= 2;
                }

                BitmapFactory.Options options2 = new BitmapFactory.Options();
                options2.inSampleSize = scale;

                is = new FileInputStream(imgFile);

                bitmap = BitmapFactory.decodeStream(is,null, options2);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (is != null) {
                        is.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bitmap;
    }
}
