package com.adinlay.adinlay;


public class ProfileEditViewModel extends SpinnerServerViewModel {


    public void deleteFcmToken(){
        getSpinnerLiveData().postValue(true);
        ServerManager.deleteFcmTokenFromServer(new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                getPingServerResponse().postValue(MainViewModel.DELETE_FCM_OK);
                getSpinnerLiveData().postValue(false);
            }

            @Override
            public void onError(String message) {
                getPingServerResponse().postValue(message);
                getSpinnerLiveData().postValue(false);
            }
        });
    }

}
