package com.adinlay.adinlay;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.adinlay.adinlay.gui.AyDialogFragment;
import com.adinlay.adinlay.gui.SignInDialogFragment;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.util.Arrays;

import timber.log.Timber;

public class Login1Activity extends AppCompatActivity {

    private final static String TAG = Login1Activity.class.getSimpleName();

    CallbackManager fcbkCallbackManager;
    GoogleSignInClient googleSignInClient;

    private LoginOptionsViewModel model;

    private ContentLoadingProgressBar spinner = null;
    private View spinnerFrame = null;
    View layoutRoot = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login1);

        spinner = findViewById(R.id.l1_spinner);
        spinnerFrame = findViewById(R.id.l1_spinner_frame);

        model = ViewModelProviders.of(this).get(LoginOptionsViewModel.class);
        model.getServerRequestLiveData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                if ("ready".equals(s) && AdinlayApp.getUser() != null){
                    ServerManager.checkSocMedConnections(null);
                    if ("completed".equals(AdinlayApp.getUser().getRegistrationStatus())) {
                        // go to MainActivity
                        AdinlayApp.setLoginStage(SignInManager.COMPLETE_STAGE);
                        Intent intent = new Intent(Login1Activity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                        AyUtils.saveStringPreference(AyUtils.PREF_DEEP_LINK, null); // clear any DL we may have
                    } else {
                        // anything other than "completed" => we assume it is "started"
                        Timber.i("got a PARTIAL user");
                        if (!"started".equals(AdinlayApp.getUser().getRegistrationStatus())){
                            Timber.i("got a user with registrationStatus: %s", AdinlayApp.getUser().getRegistrationStatus());
                        }
                        // go to the login flow:
                        AdinlayApp.setLoginStage(SignInManager.EXPLAIN_INCOMPLETE_STAGE);
                        Intent intent = new Intent(Login1Activity.this, Login2FlowActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    // handle exception messages:
                    String message = s != null && !s.isEmpty() ? s :
                            getResources().getString(R.string.something_wrong_adinlay_login);
                    if (message.startsWith(ServerManager.USER_INFO_TRY_FROM_SCRATCH) ||
                            message.startsWith(ServerManager.POOR_CONNECTIVITY)){
                        AyUtils.logoutUser();
                        if (message.startsWith(ServerManager.USER_INFO_TRY_FROM_SCRATCH)){
                            message = message.replace(ServerManager.USER_INFO_TRY_FROM_SCRATCH, "");
                            if (message.isEmpty()) {
                                message = getResources().getString(R.string.something_wrong_adinlay_login);
                            }
                        } else if (message.startsWith(ServerManager.POOR_CONNECTIVITY)){
                            message = getResources().getString(R.string.something_wrong_connectivity);
                        }
                    }
                    showErrorDialog(null, message);
                }
            }
        });

        model.getSpinnerLiveData().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean show) {
                if (show == null || !show){
                    spinnerFrame.setVisibility(View.GONE);
                    spinner.setVisibility(View.GONE);
                } else {
                    spinnerFrame.setVisibility(View.VISIBLE);
                    spinner.show();
                }
            }
        });


        // sign-up:
        findViewById(R.id.sign_up_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AdinlayApp.setLoginStage(SignInManager.SIGN_UP_STAGE);
                Intent intent = new Intent(Login1Activity.this, Login2FlowActivity.class);
                startActivity(intent);
                finish();
            }
        });

        // e-mail&passwordEt login
        findViewById(R.id.signin_email).setOnClickListener(openSignInDialogListener);
        findViewById(R.id.signin_password).setOnClickListener(openSignInDialogListener);
        findViewById(R.id.sign_in_btn).setOnClickListener(openSignInDialogListener);

        // facebook login
        fcbkCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(fcbkCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Timber.i("FacebookCallback onSuccess %s", loginResult.getAccessToken());
                model.signInWithFacebookToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                model.getSpinnerLiveData().setValue(false);
                Timber.v("FacebookCallback onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Timber.w(error, "FacebookCallback onError");
                model.getSpinnerLiveData().setValue(false);

                // show error dialog:
                String message = getResources().getString(R.string.something_wrong_facebook_login);
                showErrorDialog(error, message);
            }
        });
        findViewById(R.id.fb_login_frame).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if ("dev".equals(BuildConfig.FLAVOR)){ // code to disable facebook login
//                    Toast t = Toast.makeText(getApplicationContext(),
//                            "Unfortunately, signing in with Facebook is temporarily disabled.\nPlease use a different method", Toast.LENGTH_LONG);
//                    t.setGravity(Gravity.CENTER, 0, 0);
//                    t.show();
//                    return;
//                }

                model.getSpinnerLiveData().setValue(true);
                // send call to facebook.loginManager to sign-in:
                LoginManager.getInstance().logInWithReadPermissions(Login1Activity.this, Arrays.asList("public_profile", "email", "user_friends", "user_posts"));
            }
        });

        // google+ login
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);
        findViewById(R.id.gp_login_frame).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                model.getSpinnerLiveData().setValue(true);
                Intent signInIntent = googleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, AyUtils.get16BitId(R.id.intent_google_plus_signin));
            }
        });

        layoutRoot = findViewById(R.id.login1_con_layout);
//        layoutRoot.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                Rect rect = new Rect();
//                layoutRoot.getWindowVisibleDisplayFrame(rect);
//
//                int heightDiff = layoutRoot.getRootView().getHeight() - (rect.bottom - rect.top);
////                Timber.i("KBRD_DBG %s - %s = %s", (root.getRootView().getHeight()), (rect.bottom - rect.top), heightDiff);
////                Timber.i("KBRD_DBG %s", AyUtils.convertDpToPixel(100, root.getContext()));
//                int visibility = View.VISIBLE;
//                if (heightDiff > AyUtils.convertDpToPixel(100, layoutRoot.getContext())){
//                    Timber.d(TAG, "KBRD_DBG open %s", heightDiff);
//                    visibility = View.GONE;
//                }
////                else {
////                    Timber.d("KBRD_DBG closed %s", heightDiff);
////                }
//
//                toggleViewVisibility(layoutRoot.findViewById(R.id.fb_login_frame), visibility);
//                toggleViewVisibility(layoutRoot.findViewById(R.id.gp_login_frame), visibility);
//                toggleViewVisibility(layoutRoot.findViewById(R.id.chain_spacer2), visibility);
//                toggleViewVisibility(layoutRoot.findViewById(R.id.or_tv), visibility);
//                toggleViewVisibility(layoutRoot.findViewById(R.id.is_it_tv), visibility);
//                toggleViewVisibility(layoutRoot.findViewById(R.id.sign_up_btn), visibility);
//                toggleViewVisibility(layoutRoot.findViewById(R.id.chain_spacer8), visibility);
//
//
//            }
//        });
        layoutRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
            }
        });

        spinnerFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // do nothing - prevent further clicks
            }
        });

        findViewById(R.id.signin_forgot_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login1Activity.this, ResetPasswordActivity.class);
                startActivity(intent);
            }
        });

    }

//    private void toggleViewVisibility(View v, int visibility){
//        if (v != null){
//            v.setVisibility(visibility);
//        }
//    }

    private void hideKeyboard(View v){

        if (v != null){
            v.post(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null){
                        imm.hideSoftInputFromWindow(layoutRoot.getWindowToken(), 0);
                    }
                }
            });
        }
    }

    View.OnClickListener openSignInDialogListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            SignInDialogFragment signInDialogFrag = SignInDialogFragment.newInstance(new Bundle());
            signInDialogFrag.setListener(new SignInDialogFragment.SignInDialogListener() {
                @Override
                public void signInClicked(View v, String email, String password) {
                    model.getSpinnerLiveData().setValue(true);
                    model.signInUser(email, password);
                }
            });
            AyUtils.showDialogFragment(signInDialogFrag, getSupportFragmentManager());
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        fcbkCallbackManager.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case R.id.intent_google_plus_signin & 0x0000FFFF:
                // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...)
                // The Task returned from this call is always completed, no need to attach
                // a listener
                try {
                    Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                    GoogleSignInAccount account = task.getResult(ApiException.class);
                    model.signInWithGoogleAccount(account);
                } catch (ApiException e) {
                    // Google Sign In failed
                    e.printStackTrace();
                    Timber.w(e, "Google sign in failed");
                    model.getSpinnerLiveData().setValue(false);
                    showErrorDialog(e, getResources().getString(R.string.something_wrong_google_login));
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showErrorDialog(Exception e, String defaultMessage){
        String message = defaultMessage;
        if (e != null && e.getMessage() != null && !e.getMessage().isEmpty()){
            if (e instanceof ApiException){
                message = "Google sign in error: " + e.getMessage();
            } else {
                message = e.getMessage();
            }
        }
        if (message == null || message.isEmpty()){
            return;
        }
        Bundle args = new Bundle();
        args.putInt(AyDialogFragment.ARG_TITLE_RES, R.string.error);
        args.putCharSequence(AyDialogFragment.ARG_MESSAGE_CHAR_SEQ, message);
        args.putInt(AyDialogFragment.ARG_BUTTON_L_RES, R.string.ok_allcaps);
        args.putInt(AyDialogFragment.ARG_RESULT_ICON, AyDialogFragment.VALUE_FALSE);
        AyDialogFragment dialogFragment = AyDialogFragment.newInstance(args);
        if (!isFinishing()) {
            AyUtils.showDialogFragment(dialogFragment, getSupportFragmentManager());
        }
    }

}
