package com.adinlay.adinlay;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adinlay.adinlay.objects.WalletEntry;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Noya on 8/23/2018.
 */

public class WalletEntryRvAdapter extends RecyclerView.Adapter<WalletEntryRvAdapter.ViewHolder> {

    private ArrayList<WalletEntry> entriesList;

    public WalletEntryRvAdapter(@NonNull ArrayList<WalletEntry> entriesList) {
        this.entriesList = entriesList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wallet_entry, parent, false);

        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        WalletEntry entry = entriesList.get(position);
        Timber.v("position: %s", position);

        if (WalletEntry.TITLE_ENTRY_ID.equals(entry.getEntryId())) {
            holder.brandTv.setText(R.string.brand_underlined);
            holder.dateTv.setText(R.string.date_underlined);
            holder.statusTv.setText(R.string.status_underlined);
            holder.statusTv.setTextSize(16);
            holder.statusTv.setGravity(Gravity.LEFT);
        } else {
            holder.brandTv.setText(entry.getBrandName());
            holder.dateTv.setText(AyUtils.parseUtcStringToDdMmYyyy(entry.getDateString()));
            String amount = AyUtils.getStringAmountWithCurrencySymbol(entry.getAmountBigDec(),
                    AyUtils.DECIMAL_PLACES_DEFAULT, holder.statusTv.getResources());
            holder.statusTv.setText(amount);
            holder.statusTv.setTextSize(14);
            holder.statusTv.setGravity(Gravity.CENTER);
        }

        if (entry.isBalanceAvailable()){
            holder.statusTv.setBackgroundResource(R.drawable.btn_bg_green_round);
        } else if (entry.isPending()){
            holder.statusTv.setBackgroundResource(R.drawable.btn_bg_orange_round);
        } else {
            holder.statusTv.setBackgroundResource(0);
        }

        if (position != 0) { // position 0 is title!!!!
            AyUtils.setOddEvenBackgroundColor(position + 1, holder.itemView);
        }

    }

    @Override
    public int getItemCount() {
        return entriesList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView brandTv;
        TextView dateTv;
        TextView statusTv;


        public ViewHolder(View itemView) {
            super(itemView);

            brandTv = itemView.findViewById(R.id.we_brand_name);
            dateTv = itemView.findViewById(R.id.we_date);
            statusTv = itemView.findViewById(R.id.we_status);
        }
    }

}
