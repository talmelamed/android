package com.adinlay.adinlay;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.CenterInside;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by Noya on 8/26/2018.
 */

public class InstructionsAdapter extends PagerAdapter {

    private ArrayList<String> urls;
    private GlideRequest<Drawable> requestBuilder;
    WeakReference<ItemClick> wr_listener;

    public InstructionsAdapter(@NonNull ArrayList<String> urls, GlideRequests glideRequests, ItemClick itemClick) {
        this.urls = urls;
        requestBuilder = glideRequests.asDrawable().transforms(new CenterInside());
        wr_listener = new WeakReference<>(itemClick);
    }

    @Override
    public int getCount() {
        return urls.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View root = LayoutInflater.from(container.getContext()).inflate(R.layout.item_just_iv, container, false);

        ImageView iv = root.findViewById(R.id.item_iv);
        iv.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        iv.setContentDescription(iv.getResources().getString(R.string.instructions_slide));

        if(!urls.get(position).isEmpty()){
            GlideRequest<Drawable> requestBuilderIv = requestBuilder.clone();
            if (position == 0){
//                final String url = urls.get(position);
                requestBuilderIv = requestBuilderIv
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                ItemClick listener = wr_listener.get();
                                if (listener != null){
//                                Timber.i("got error fetching instructions, will notify frag about %s", url);
                                    listener.onItemClicked(false);
                                }
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                ItemClick listener = wr_listener.get();
                                if (listener != null){
                                    listener.onItemClicked(true);
                                }
                                return false;
                            }
                        });
            }
            requestBuilderIv
//                    .thumbnail(0.2f)
                    .load(urls.get(position)).into(iv);
        }

        container.addView(root);

        return root;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
//        super.destroyItem(container, position, object);
        if (object instanceof View){
            container.removeView((View) object);
        }
    }
}
