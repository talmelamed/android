package com.adinlay.adinlay;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adinlay.adinlay.objects.CheckableObject;
import com.adinlay.adinlay.objects.ProfileTopic;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;

import org.json.JSONArray;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by Noya on 9/9/2018.
 */

public class ProfileTopicFrag extends LoginFrag implements ItemClick {

    private static final String TAG = ProfileTopicFrag.class.getSimpleName();

    LoginViewModel viewModel;
    private ProfileTopic topic;
    private boolean isAboutUser = true;
    private boolean isUpdate = true;
    private String socialMediaId = null;
    private TextView oneLinerTv = null;
    String path = ""; // temp - should consider holding the selected items

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.frag_profile_topic, container, false);

        if (viewModel == null && getActivity() != null){
            viewModel = ViewModelProviders.of(getActivity()).get(LoginViewModel.class);
        }

        viewModel.updateFlowData(R.drawable.group_next_profile, getResources().getString(R.string.next_allcaps), false, true);

        topic = isAboutUser? AdinlayApp.getCurrUserTopic() : AdinlayApp.getCurrFollowersTopic();

        Timber.i("topic_dbg topic: %s", topic);
        if (topic != null){
            // the image:
            ImageView avatarIv = root.findViewById(R.id.picat_avatar_iv);
            ImageView stripIv = root.findViewById(R.id.picat_iv);
            if (topic.getImageUrl().isEmpty()){
                avatarIv.setVisibility(View.VISIBLE);
                if (AdinlayApp.getUser() != null && !AdinlayApp.getUser().getAvatarUrl().isEmpty()){
                    RequestOptions options = new RequestOptions().error(R.drawable.avtar_icon)
                            .placeholder(R.drawable.avtar_icon)
                            .transform(new CenterCrop());
//                    .transforms(new CenterCrop(), new RoundedCorners(radius));
                    GlideApp.with(this).load(AdinlayApp.getUser().getAvatarUrl())
                            .apply(options).into(avatarIv);
                }
                root.findViewById(R.id.picat_change_pic_tv).setVisibility(View.VISIBLE);
                stripIv.setVisibility(View.INVISIBLE);
            } else {
                GlideApp.with(this).load(topic.getImageUrl())
                        .into(stripIv);
            }

            TextView headerTv = root.findViewById(R.id.picat_title);
            headerTv.setText(topic.getTitle());
            oneLinerTv = root.findViewById(R.id.picat_one_liner);
            oneLinerTv.setText(topic.getDescription());

            ProfileTopicItemsFrag itemFrag;
            if (topic.isTypeInput()){
                itemFrag = new ProfileTopicInputFrag();
                ((ProfileTopicInputFrag) itemFrag).setTopicInfo(topic.getTypeOfInputType(),
                        getUserInputForTopic(), topic.getTitle());
            } else {
                ProfileTopicRvFrag rvFrag = new ProfileTopicRvFrag();
                ArrayList<CheckableObject> topicItems = new ArrayList<CheckableObject>(topic.getItems(true)); // the list is new, but the items are shared
                if (!topic.isHierarchic()){
                    initSelectionFromUser(topicItems);
                }
                rvFrag.setItems(topicItems, topic.isSingleChoice(), topic.mustMakeChoice(),
                        topic.isHierarchic() ? this : null, topic.isHierarchic());
                itemFrag = rvFrag;
            }
            android.support.v4.app.FragmentManager fm = getChildFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.picat_items_frame, itemFrag, TAG);
            ft.addToBackStack(TAG);
            ft.commit();

        }

        int currPos = isAboutUser ? AdinlayApp.getUserTopicPos() :
                AdinlayApp.getUserTopicPos() + 1+ AdinlayApp.getFollowersTopicPos();

        RecyclerView counterRv = root.findViewById(R.id.picat_counter_rv);
        if (AdinlayApp.getLoginStage().equals(SignInManager.UPDATE_YOU_STAGE) ||
                AdinlayApp.getLoginStage().equals(SignInManager.UPDATE_FOLLOWERS_STAGE)){
            counterRv.setVisibility(View.GONE);
        } else {
            initProgressRecycler(counterRv, SignInManager.getTotalStages(), currPos);
        }

        return root;
    }

    public void setIsAboutUser(boolean isAboutUser, boolean isUpdate, String socialMediaId){
        this.isAboutUser = isAboutUser;
        this.isUpdate = isUpdate;
        this.socialMediaId = socialMediaId != null && socialMediaId.isEmpty() ? null : socialMediaId;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getActivity() != null) {
            viewModel = ViewModelProviders.of(getActivity()).get(LoginViewModel.class);
        }
    }

    @Override
    public boolean onMainClicked() {

        User user = AdinlayApp.getUser();
        android.support.v4.app.FragmentManager fm = getChildFragmentManager();
        Fragment currFrag = fm.findFragmentByTag(TAG);
        if (user != null && currFrag instanceof ProfileTopicItemsFrag){
            ArrayList<String> userNewSelection = ((ProfileTopicItemsFrag) currFrag).getSelection();
            if (topic.mustMakeChoice() && userNewSelection.isEmpty()){
                if (getActivity() != null) {
                    String message = topic.isTypeInput() ? getResources().getString(R.string.must_provide_info) :
                            getResources().getString(R.string.must_choose);
                    Toast.makeText(getActivity().getApplicationContext(), message, Toast.LENGTH_LONG).show();
                }
                return false;
            }
            if (socialMediaId == null || socialMediaId.isEmpty()) {
                user.changePendingJsonProfileField(topic.getId(), userNewSelection, isAboutUser);
            } else {
                user.changePendingSocMedProfileField(topic.getId(), userNewSelection, socialMediaId);
            }
        }

        return true;
    }

    @Override
    public boolean onBackClicked() {
        android.support.v4.app.FragmentManager fm = getChildFragmentManager();
        if (topic.isHierarchic()){
            int backStackSize = fm.getBackStackEntryCount();
            if (backStackSize > 1){
                fm.popBackStackImmediate();
                viewModel.updateFlowData(R.drawable.group_next_profile, getResources().getString(R.string.next_allcaps), backStackSize -1 > 1, true);
                int index = path.lastIndexOf("\\");
                if (index >= 0){
                    path = path.substring(0, index);
                } else {
                    path = "";
                    oneLinerTv.setGravity(Gravity.LEFT);
                }
                String text = path.isEmpty() ? topic.getDescription() : path;
                oneLinerTv.setText(text);
                return true;
            }
        }
        Fragment currFrag = fm.findFragmentByTag(TAG);
        if (currFrag instanceof ProfileTopicItemsFrag && ((ProfileTopicItemsFrag) currFrag).consumeBackClick()){
            return true;
        }

        return false;
    }

    @Override
    public void onItemClicked(Object object) {
        if (topic.isHierarchic() && object instanceof CheckableObject &&
                ((CheckableObject) object).hasSubItems()){
            ProfileTopicRvFrag rvFrag = new ProfileTopicRvFrag();
            rvFrag.setItems(((CheckableObject) object).getSubItems(), topic.isSingleChoice(), topic.mustMakeChoice(), this, true);
            android.support.v4.app.FragmentManager fm = getChildFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.picat_items_frame, rvFrag, TAG);
            ft.addToBackStack(TAG);
            ft.commit();
            viewModel.updateFlowData(R.drawable.group_next_profile, getResources().getString(R.string.next_allcaps), true, true);
            path = ((CheckableObject) object).getFullTitle();
            oneLinerTv.setText(path);
            oneLinerTv.setGravity(Gravity.CENTER);
        }
    }

    private void initSelectionFromUser(ArrayList<CheckableObject> items){
        User user = AdinlayApp.getUser();
        if (user == null || items == null || items.isEmpty()){
            return;
        }
        JSONArray selectedArray = user.getProfileTopicSelection(topic.getId(), socialMediaId, isAboutUser);
        boolean allSelected = true;
        if (topic.isMultipleSelection() && topic.isDefaultAll() && isUpdate &&
                selectedArray.length() >= items.size()){

            // check if indeed all items are selected
            for (CheckableObject object : items){
                boolean isSelected = false;
                object.setChecked(false);
                for (int i = 0; i < selectedArray.length(); i++){
                    String selectedId = selectedArray.optString(i);
                    if (!selectedId.isEmpty() && selectedId.startsWith(object.getId())){
                        isSelected = true;
                    }
                }
                allSelected = allSelected && isSelected;
            }

            if (allSelected){
                items.add(0, new CheckableObject("Default - All", true));
                return;
            }
        } else if (topic.isMultipleSelection() && topic.isDefaultAll() &&
                selectedArray.length() == 0){
            items.add(0, new CheckableObject("Default - All", true));
            return;
        }

        for (CheckableObject object : items){
            if (!isUpdate && topic.isMultipleSelection() && topic.isDefaultAll()){
                object.setChecked(false);
                continue;
            }
            object.setChecked(false);
            for (int i = 0; i < selectedArray.length(); i++){
                String selectedId = selectedArray.optString(i);
                if (!selectedId.isEmpty() && selectedId.startsWith(object.getId())){
                    object.setChecked(true);
                }
            }
//            Timber.i("topic_dbg checking for %s selected? %s and allSelected? %s", object.getTitle(), object.isChecked(), allSelected);
            allSelected = allSelected && object.isChecked();
        }

        if (topic.isMultipleSelection() && topic.isDefaultAll()){
            items.add(0, new CheckableObject("Default - All", allSelected));
        }

    }

    private String getUserInputForTopic(){
        User user = AdinlayApp.getUser();
        if (user == null){
            return null;
        }
        JSONArray selectedArray = user.getProfileTopicSelection(topic.getId(), isAboutUser);
        return selectedArray.length() == 0 ? null : selectedArray.optString(0, null);
    }

    @Override
    protected String getFragStageName() {
        String prefix = isUpdate ? "update_" : "";
        prefix += isAboutUser ? "user_" : "followers_";
        return prefix + topic.getId();
    }

    @Override
    protected boolean shouldChangeScreenName() {
        return !isUpdate; // not changing firebase-screenName for updates
    }
}
