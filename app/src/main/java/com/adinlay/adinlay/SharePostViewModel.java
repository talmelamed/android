package com.adinlay.adinlay;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import com.adinlay.adinlay.objects.RedeemEntry;

import org.json.JSONException;
import org.json.JSONObject;

import timber.log.Timber;

/**
 * Created by Noya on 8/15/2018.
 */

public class SharePostViewModel extends ViewModel {

    private static String TAG = SharePostViewModel.class.getSimpleName();
    public static String SHARE_OK = "shareOk";


    private MutableLiveData<String> pingServerResponse;
    private MutableLiveData<String> pingReportFBsuccess;
    private MutableLiveData<Boolean> spinnerLiveData;

    private RedeemEntry redeemEntry = null;

    public MutableLiveData<String> getPingServerResponse(){
        if (pingServerResponse == null){
            pingServerResponse = new MutableLiveData<>();
        }
        return pingServerResponse;
    }

    public MutableLiveData<String> getPingReportFBsuccess(){
        if (pingReportFBsuccess == null){
            pingReportFBsuccess = new MutableLiveData<>();
        }
        return pingReportFBsuccess;
    }

    public MutableLiveData<Boolean> getSpinnerLiveData() {
        if (spinnerLiveData == null){
            spinnerLiveData = new MutableLiveData<>();
        }
        return spinnerLiveData;
    }

    public RedeemEntry getRedeemEntry() {
        return redeemEntry;
    }

    public void sendPostToServer(final boolean isFcbk, final boolean isFcbkFree){
        getSpinnerLiveData().setValue(true);
        ServerManager.uploadNewPost(isFcbk, isFcbkFree, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                Timber.i("onResponseOk: %s", responseBody);
                User user = AdinlayApp.getUser();
                if (user != null && user.socialMediaTestIncomplete()){
                    onFirstPostSuccess();
                }
                String message = isFcbk && !isFcbkFree ? SHARE_OK + responseBody : SHARE_OK;
                onServerResponse(message);
            }

            @Override
            public void onError(String message) {
                Timber.i("onError: %s", message);
                onServerResponse(message);
            }
        });
    }

    public void reportFcbkPostResult(String postId, boolean isSuccess){
        ServerManager.Callback successCallback = new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                User user = AdinlayApp.getUser();
                if (user != null && user.socialMediaTestIncomplete()){
                    onFirstPostSuccess();
                }
                getSpinnerLiveData().postValue(false);
                getPingReportFBsuccess().postValue(SHARE_OK);
            }

            @Override
            public void onError(String message) {
                getSpinnerLiveData().postValue(false);
                // todo fb tbd: do we show regular error dialog?
                getPingReportFBsuccess().postValue(message);
            }
        };
        getSpinnerLiveData().postValue(isSuccess);
        ServerManager.reportFacebookPostResult(postId, isSuccess, isSuccess? successCallback : null);

    }

    private void onFirstPostSuccess(){
        ServerManager.getUpdatedUserInfo(null);
    }

    private void onServerResponse(String pingMessage){
        getSpinnerLiveData().postValue(false);
        getPingServerResponse().postValue(pingMessage);
    }

    public void sendRedeemRequestToServer(float amount, String location, String personalId){
        getSpinnerLiveData().setValue(true);
        ServerManager.sendRedeemRequest(amount, location, personalId, new ServerManager.Callback() {
            @Override
            public void onResponseOk(String responseBody) {
                Timber.i( "onResponseOk: %s", responseBody);
                if (responseBody != null) {
                    try {
                        JSONObject redeemJson = new JSONObject(responseBody);
                        if (redeemJson.has(RedeemEntry.ID_KEY) && redeemJson.has(RedeemEntry.CREATED_DATE_KEY)){
                            redeemEntry = new RedeemEntry(redeemJson);
                            onServerResponse(SHARE_OK);
                            return;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                onServerResponse("Something went wrong, please try again later");
            }

            @Override
            public void onError(String message) {
                Timber.i("onError: %s", message);
                onServerResponse(message);
            }
        });
    }


}
