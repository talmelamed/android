package com.adinlay.adinlay;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

public abstract class BasicActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResourceId());

        if (shouldSetLightActionBar()){
            setLightActionBar();
        }

        if (shouldSetToolbarWithBack()){
            setToolbarWithBack();
        }
    }

    protected void setLightActionBar(){
        setSupportActionBar(null);

        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT >= 23){
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            }
        }
    }

    protected void setToolbarWithBack(){
        // toolbar:
        View rightTv = findViewById(R.id.toolbar_right_tv);
        if (rightTv != null){
            rightTv.setVisibility(View.GONE);
        }
        View leftView = findViewById(R.id.toolbar_left_tv);
        if (leftView instanceof TextView){
            TextView leftTv = (TextView) leftView;
            leftTv.setText(getResources().getString(R.string.arrow_and_back));
            leftTv.setTextColor(Color.BLACK);
        }
        View leftButton = findViewById(R.id.toolbar_left_btn);
        if (leftButton != null) {
            leftButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }

    abstract int getLayoutResourceId();

    public boolean shouldSetLightActionBar(){
        return true;
    }

    public boolean shouldSetToolbarWithBack(){
        return true;
    }

    protected void hideKeyboard(View v){

        if (v != null){
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        }
    }

}
